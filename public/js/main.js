
$(document).ready(function() {

	var winHeight = $(window).height();
	var textOutter = $('.textOutter').height()/2;

	var headerHeight = $("header").height();

	$(".textOutter").css({
		"min-height" : winHeight-60,
		"margin-top": -50
	});

	$('.main').css({
		"min-height" : winHeight
	});

	$('.leftNavigation').css({
		"min-height" : winHeight - headerHeight-30,
		"overflow": "auto"
	});

	$("#rangeBtn").on('click', function() {
		$(".doubleBox").toggle();
        $(".signleBox").toggle();
	});

	$("#eyeIcon").on('click', function() {
		$(this).toggleClass("show");
	});

	//Window Mobile View
	if($(window).width() < 768){
		$('.leftNavigation').css({
			"min-height": "auto"
		});

		var leftNavWidth = $(".leftNavigation").innerWidth();
		var headerHeight = $("header").innerHeight();

		$(".leftNavigation").css({
			"margin-top": headerHeight,
			"left": -leftNavWidth -10,
			"max-height": winHeight - headerHeight,
			"overflow": "auto"
		})

		$(".contentWrapper").css({
			"min-height" : winHeight - headerHeight,
		}, 10000);


		//Toggle Menu ICon
		$("#toggleMenu").on('click', function() {
			$("#toggleMenu").toggleClass("active");
			$(".leftNavigation").toggleClass("show");
			$(".contentWrapper").toggleClass("padLeft");
		});

	}

	$(".dataTables_filter .form-control").attr("placeholder", "Enter Keywords");
	

	
});	






// function centerBlck(){
// 	var winDHeight = $(window).innerHeight()/2;
// 	var Centerheight = -($(".centerBlock").innerHeight())/2;

// 	$(".centerBlock").css({
// 		"margin-top": Centerheight,
// 		"padding-top": winDHeight
// 	});

// }


 

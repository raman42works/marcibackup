-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 14, 2018 at 05:15 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 5.6.32-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kros`
--

-- --------------------------------------------------------

--
-- Table structure for table `Activity`
--

CREATE TABLE `Activity` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `metadata` longtext COLLATE utf8_unicode_ci NOT NULL,
  `meta_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Activity`
--

INSERT INTO `Activity` (`id`, `user_id`, `type`, `metadata`, `meta_id`, `created_at`) VALUES
(1, 5, 'Post', 'You created a post "Hello my f..."', 1, '2017-12-04 17:01:12'),
(2, 5, 'Post', 'You created a post "Text post..."', 2, '2017-12-04 17:06:26'),
(3, 5, 'Event', 'You created an event "Birthday e..."', 1, '2017-12-04 17:08:14'),
(4, 5, 'Post', 'You commented on a post "Hello first comment..."', 2, '2017-12-04 17:09:04'),
(5, 5, 'Post', 'You commented on a post "Okok..."', 2, '2017-12-04 17:11:39'),
(6, 5, 'Post', 'You commented on a post "hellooo..."', 2, '2017-12-04 17:13:18'),
(7, 5, 'Post', 'You commented on a post "hellooo..."', 2, '2017-12-04 17:13:29'),
(8, 5, 'Post', 'You commented on a post "Great !..."', 2, '2017-12-04 17:13:32'),
(9, 5, 'Event', 'You commented on an event "Hello first event comment..."', 1, '2017-12-04 17:18:54'),
(10, 5, 'Post', 'You commented on a post "Great !..."', 2, '2017-12-04 17:22:00'),
(11, 5, 'Post', 'You commented on a post "Okok..."', 2, '2017-12-04 17:23:12'),
(12, 5, 'Event', 'You commented on an event "Another comment..."', 1, '2017-12-04 17:23:29'),
(13, 5, 'Event', 'You created an event "Another ev..."', 2, '2017-12-04 17:30:55'),
(14, 5, 'Post', 'You created a post "Hello anot..."', 3, '2017-12-05 10:53:08'),
(15, 6, 'Post', 'You created a post "Test..."', 4, '2017-12-05 10:54:20'),
(16, 6, 'Post', 'You commented on a post "Ankit kk ..."', 4, '2017-12-05 10:56:24'),
(17, 6, 'Post', 'You commented on a post "@..."', 4, '2017-12-05 10:56:31'),
(18, 6, 'Post', 'You commented on a post "    ..."', 4, '2017-12-05 10:56:35'),
(19, 6, 'Post', 'You commented on a post " \n\n\n\n\n\n\n\n\n   \n\n\n..."', 4, '2017-12-05 10:56:47'),
(20, 6, 'Post', 'You commented on a post "Test..."', 4, '2017-12-05 10:56:57'),
(21, 6, 'Post', 'You commented on a post "@..."', 2, '2017-12-05 10:57:55'),
(22, 6, 'Post', 'You commented on a post "@nishant\n..."', 2, '2017-12-05 10:58:22'),
(23, 5, 'Post', 'You created a post "Text postt..."', 5, '2017-12-05 11:01:27'),
(24, 6, 'Event', 'You created an event "Title..."', 3, '2017-12-05 11:01:53'),
(25, 5, 'Post', 'You created a post "www.fb.com..."', 6, '2017-12-05 11:04:32'),
(26, 5, 'Post', 'You created a post "Hello..."', 7, '2017-12-05 11:05:05'),
(27, 5, 'Post', 'You created a post "Album..."', 8, '2017-12-05 11:07:34'),
(28, 5, 'Post', 'You created a post "Another al..."', 9, '2017-12-05 11:07:52'),
(29, 5, 'Post', 'You created a post "Post..."', 10, '2017-12-05 12:08:30'),
(30, 5, 'Post', 'You created a post "Albumm..."', 11, '2017-12-05 12:08:46'),
(31, 5, 'Post', 'You created a post "Img..."', 12, '2017-12-05 12:11:11'),
(32, 5, 'Post', 'You created a post "Img..."', 13, '2017-12-05 12:57:05'),
(33, 6, 'Post', 'You created a post "Test..."', 14, '2017-12-05 13:13:44'),
(34, 6, 'Post', 'You commented on a post "@..."', 14, '2017-12-05 13:15:10'),
(35, 5, 'Post', 'You commented on a post "Hello..."', 4, '2017-12-05 13:16:03'),
(36, 5, 'Post', 'You commented on a post "Okok..."', 4, '2017-12-05 13:16:31'),
(37, 5, 'Event', 'You commented on an event "Good..."', 3, '2017-12-05 13:17:04'),
(38, 6, 'Post', 'You commented on a post "    \n\n\n\n..."', 14, '2017-12-05 13:17:05'),
(39, 6, 'Event', 'You created an event "Create eve..."', 4, '2017-12-05 13:27:59'),
(40, 6, 'Event', 'You commented on an event "Test..."', 4, '2017-12-05 13:29:42'),
(41, 7, 'Post', 'You commented on a post "Okk..."', 14, '2017-12-05 13:39:01'),
(42, 7, 'Post', 'You commented on a post "Hhhh..."', 14, '2017-12-05 13:39:11'),
(43, 7, 'Post', 'You commented on a post "hello..."', 14, '2017-12-05 14:44:51'),
(44, 7, 'Post', 'You commented on a post "hello..."', 14, '2017-12-05 14:47:03'),
(45, 7, 'Post', 'You commented on a post "Good..."', 2, '2017-12-05 16:23:53'),
(46, 7, 'Post', 'You commented on a post "Good..."', 2, '2017-12-05 16:26:54'),
(47, 7, 'Post', 'You commented on a post "Hello test comment..."', 14, '2017-12-05 16:29:16'),
(48, 7, 'Event', 'You commented on an event "Event first comment..."', 4, '2017-12-05 16:31:57'),
(49, 8, 'Post', 'You created a post "Hi..."', 16, '2017-12-29 10:51:01'),
(50, 12, 'Post', 'You created a post "Hi..."', 17, '2018-01-04 11:53:07'),
(51, 12, 'Post', 'You created a post "Hi..."', 18, '2018-01-04 11:53:37'),
(52, 12, 'Post', 'You created a post "hi..."', 19, '2018-01-04 11:54:12'),
(53, 12, 'Post', 'You created a post "Hello..."', 20, '2018-01-04 11:59:49'),
(54, 12, 'Post', 'You created a post "Hey..."', 21, '2018-01-04 12:00:24'),
(55, 12, 'Post', 'You created a post "Hiiiiiii..."', 22, '2018-01-04 12:00:49'),
(56, 12, 'Post', 'You created a post "Hi..."', 23, '2018-01-04 12:04:20'),
(57, 8, 'Post', 'You created a post "..."', 25, '2018-01-05 16:49:03'),
(58, 12, 'Post', 'You created a post "cu..."', 26, '2018-01-12 10:26:54'),
(59, 12, 'Post', 'You created a post "Test post..."', 27, '2018-01-15 13:02:25'),
(60, 12, 'Post', 'You created a post "Another te..."', 28, '2018-01-15 13:03:32'),
(61, 12, 'Post', 'You created a post "Hi..."', 29, '2018-01-15 13:18:55'),
(62, 87, 'Post', 'You created a post "Hshhsjsjs ..."', 30, '2018-01-30 12:55:04'),
(63, 87, 'Post', 'You commented on a post "Hi Ankit kk..."', 30, '2018-01-30 12:56:06'),
(64, 87, 'Post', 'You commented on a post "Hello..."', 30, '2018-01-30 12:58:12'),
(65, 87, 'Post', 'You commented on a post "Nishant..."', 29, '2018-01-30 13:01:01'),
(66, 87, 'Event', 'You created an event "Cricket Ma..."', 5, '2018-01-30 13:10:20'),
(67, 87, 'Event', 'You commented on an event "Hi ..."', 5, '2018-01-30 13:14:23'),
(68, 12, 'Post', 'You commented on a post "Testing..."', 30, '2018-01-30 14:54:34'),
(69, 12, 'Post', 'You commented on a post "Simran..."', 30, '2018-01-30 15:15:06'),
(70, 12, 'Post', 'You commented on a post "Simran..."', 30, '2018-01-30 15:15:28'),
(71, 87, 'Post', 'You commented on a post "Hi..."', 30, '2018-01-30 15:15:42'),
(72, 12, 'Post', 'You commented on a post "Hello comment..."', 30, '2018-01-30 15:25:12'),
(73, 12, 'Event', 'You commented on an event "Hello..."', 5, '2018-01-30 15:26:17'),
(74, 12, 'Event', 'You commented on an event "Hello..."', 5, '2018-01-30 15:26:22'),
(75, 12, 'Event', 'You commented on an event "Hello..."', 5, '2018-01-30 15:27:23'),
(76, 12, 'Post', 'You commented on a post "Ok..."', 30, '2018-01-30 15:27:33'),
(77, 12, 'Post', 'You commented on a post "hello okok..."', 30, '2018-01-30 15:32:37'),
(78, 8, 'Post', 'You created a post "Hello..."', 31, '2018-01-30 16:01:31'),
(79, 12, 'Post', 'You commented on a post "test..."', 30, '2018-01-30 16:07:53'),
(80, 12, 'Post', 'You commented on a post "test..."', 30, '2018-01-30 16:30:46'),
(81, 12, 'Post', 'You commented on a post "hello okok..."', 30, '2018-01-30 16:31:21'),
(82, 12, 'Post', 'You commented on a post "hello okok..."', 30, '2018-01-30 16:31:26'),
(83, 12, 'Post', 'You commented on a post "test..."', 30, '2018-01-30 16:36:46'),
(84, 12, 'Post', 'You commented on a post "test..."', 30, '2018-01-30 16:40:05'),
(85, 12, 'Post', 'You commented on a post "test..."', 30, '2018-01-30 16:40:11'),
(86, 92, 'Post', 'You created a post "Testing of..."', 32, '2018-02-01 11:07:12'),
(87, 92, 'Post', 'You commented on a post "Nishant test..."', 32, '2018-02-01 11:11:40'),
(88, 92, 'Post', 'You commented on a post "Test..."', 32, '2018-02-01 11:11:48'),
(89, 92, 'Post', 'You commented on a post "#..."', 32, '2018-02-01 11:11:57'),
(90, 92, 'Post', 'You commented on a post "@..."', 32, '2018-02-01 11:12:01'),
(91, 92, 'Post', 'You commented on a post "@dfgv..."', 32, '2018-02-01 11:12:08'),
(92, 92, 'Post', 'You commented on a post "   \n\n\n\n\n\n..."', 32, '2018-02-01 11:13:10'),
(93, 92, 'Post', 'You commented on a post "Ankit kk ..."', 32, '2018-02-01 11:13:24'),
(94, 92, 'Post', 'You created a post "Imagepost..."', 33, '2018-02-01 11:16:50'),
(95, 92, 'Post', 'You commented on a post "Nishant An......"', 33, '2018-02-01 11:19:05'),
(96, 92, 'Post', 'You commented on a post "Nishant , Ankit kk ..."', 33, '2018-02-01 11:19:47'),
(97, 92, 'Post', 'You commented on a post "Tetstsgsbb......"', 33, '2018-02-01 11:20:04'),
(98, 92, 'Event', 'You created an event "Event for ..."', 6, '2018-02-01 11:37:16'),
(99, 92, 'Event', 'You commented on an event "@ Tango charlie ..."', 6, '2018-02-01 11:40:03'),
(102, 92, 'Event', 'You created an event "Title..."', 9, '2018-02-01 11:43:05'),
(103, 87, 'Post', 'You commented on a post "Hi..."', 33, '2018-02-01 11:58:35'),
(104, 87, 'Post', 'You commented on a post "Nishant ..."', 33, '2018-02-01 11:58:48'),
(107, 95, 'Post', 'You commented on a post "Tango charlie ..."', 33, '2018-02-01 12:29:17'),
(109, 92, 'Post', 'You created a post "Test..."', 35, '2018-02-01 12:41:18'),
(116, 91, 'Event', 'You created an event "Party even..."', 14, '2018-02-02 11:13:35'),
(117, 8, 'Post', 'You commented on a post "Tango charlie..."', 35, '2018-02-02 11:31:25'),
(118, 8, 'Event', 'You created an event "Adsfsd..."', 15, '2018-02-02 11:41:35'),
(119, 8, 'Event', 'You created an event "Sdfasd..."', 16, '2018-02-02 11:42:34'),
(120, 8, 'Event', 'You created an event "Sdfsd..."', 17, '2018-02-02 11:55:25'),
(121, 92, 'Post', 'You commented on a post "Add a comm......"', 28, '2018-02-02 13:08:52'),
(122, 92, 'Post', 'You commented on a post "Add a comm......"', 28, '2018-02-02 13:09:50'),
(123, 92, 'Post', 'You commented on a post "Add a comm......"', 28, '2018-02-02 13:10:08'),
(124, 92, 'Post', 'You commented on a post "@Nishant..."', 28, '2018-02-02 13:11:42'),
(125, 92, 'Post', 'You commented on a post "Tango charlie..."', 35, '2018-02-02 13:21:56'),
(126, 92, 'Post', 'You commented on a post "@Nishant..."', 35, '2018-02-02 13:23:36'),
(127, 104, 'Post', 'You created a post "Teat..."', 39, '2018-02-02 13:26:26'),
(128, 92, 'Post', 'You created a post "Test..."', 40, '2018-02-02 13:29:02'),
(129, 92, 'Post', 'You created a post "..."', 41, '2018-02-02 13:31:24'),
(130, 92, 'Post', 'You commented on a post "Test..."', 41, '2018-02-02 13:38:17'),
(131, 92, 'Post', 'You commented on a post "Testinf..."', 41, '2018-02-02 13:40:47'),
(132, 104, 'Post', 'You commented on a post "Hello..."', 41, '2018-02-02 13:41:03'),
(133, 91, 'Post', 'You commented on a post "Gudd..."', 41, '2018-02-02 14:35:01'),
(134, 91, 'Post', 'You commented on a post "Okok..."', 41, '2018-02-02 14:40:01'),
(135, 92, 'Event', 'You created an event "Test Event..."', 18, '2018-02-02 14:46:22'),
(136, 92, 'Event', 'You created an event "Test Event..."', 19, '2018-02-02 14:47:02'),
(137, 92, 'Event', 'You commented on an event "Test..."', 18, '2018-02-02 14:52:52'),
(138, 92, 'Event', 'You created an event "Event..."', 20, '2018-02-02 14:53:54'),
(139, 91, 'Post', 'You created a post "Img post..."', 42, '2018-02-02 15:03:11'),
(140, 90, 'Post', 'You commented on a post "Hi nice post......."', 42, '2018-02-02 15:04:04'),
(141, 90, 'Post', 'You commented on a post "Hello..."', 42, '2018-02-02 15:04:31'),
(142, 92, 'Post', 'You commented on a post "Great post..."', 42, '2018-02-02 15:07:07'),
(143, 90, 'Event', 'You created an event "Birthday e..."', 21, '2018-02-02 15:10:45'),
(146, 90, 'Post', 'You commented on a post "Nice post....."', 42, '2018-02-02 15:13:30'),
(149, 91, 'Post', 'You commented on a post "Hmmmm..."', 42, '2018-02-02 15:14:50'),
(150, 90, 'Post', 'You commented on a post "Gudd..."', 42, '2018-02-02 15:17:14'),
(151, 91, 'Event', 'You created an event "sdfasdf..."', 22, '2018-02-02 15:56:28'),
(187, 91, 'Post', 'You commented on a post "@..."', 42, '2018-02-02 17:10:59'),
(188, 91, 'Post', 'You commented on a post "Simran Jit..."', 42, '2018-02-02 17:11:07'),
(189, 91, 'Post', 'You commented on a post "sdfasdfasdfads..."', 42, '2018-02-02 17:11:18'),
(190, 91, 'Event', 'You commented on an event "Am..."', 21, '2018-02-02 17:26:51'),
(191, 91, 'Event', 'You commented on an event "hi..."', 21, '2018-02-02 17:28:43'),
(192, 91, 'Event', 'You commented on an event "test..."', 21, '2018-02-02 17:32:23'),
(193, 91, 'Event', 'You commented on an event "Ss..."', 21, '2018-02-02 17:32:29'),
(194, 91, 'Event', 'You commented on an event "Add a comm......"', 21, '2018-02-02 17:32:40'),
(195, 91, 'Event', 'You commented on an event "hi..."', 21, '2018-02-02 17:36:03'),
(196, 91, 'Post', 'You commented on a post "Rdd..."', 42, '2018-02-02 17:37:03'),
(197, 91, 'Post', 'You commented on a post "sdfasdf..."', 42, '2018-02-02 17:42:02'),
(198, 91, 'Post', 'You commented on a post "hi..."', 41, '2018-02-02 17:49:48'),
(199, 91, 'Post', 'You commented on a post "Hi..."', 41, '2018-02-02 17:53:17'),
(200, 91, 'Post', 'You created a post "Yahoo.com ..."', 44, '2018-02-03 15:44:49'),
(201, 91, 'Post', 'You created a post "Yahoo.com ..."', 45, '2018-02-03 15:48:37'),
(202, 91, 'Event', 'You created an event "sdfasdf..."', 23, '2018-02-03 16:12:35'),
(203, 91, 'Event', 'You created an event "Sdfasdf..."', 24, '2018-02-03 16:13:40'),
(204, 91, 'Post', 'You created a post "Sadfasdfas..."', 46, '2018-02-03 16:24:22'),
(205, 92, 'Event', 'You created an event "Title..."', 25, '2018-02-03 17:34:53'),
(206, 92, 'Post', 'You commented on a post "@..."', 41, '2018-02-03 17:48:30'),
(207, 92, 'Post', 'You commented on a post "-..."', 41, '2018-02-03 17:48:35'),
(208, 92, 'Post', 'You commented on a post "Rahul..."', 41, '2018-02-03 17:48:46'),
(209, 92, 'Post', 'You commented on a post "RamanUser..."', 41, '2018-02-03 17:48:54'),
(210, 87, 'Post', 'You commented on a post "Hi..."', 44, '2018-02-06 12:28:58'),
(211, 87, 'Post', 'You commented on a post "Ankit kk ..."', 44, '2018-02-06 12:50:11'),
(212, 87, 'Post', 'You created a post "Hehjdhdhdj..."', 47, '2018-02-06 12:58:30'),
(214, 87, 'Post', 'You created a post "Ghhshshsbs..."', 49, '2018-02-06 15:33:20'),
(215, 87, 'Post', 'You created a post "Ghhiih\nHji..."', 50, '2018-02-06 15:35:41'),
(216, 87, 'Post', 'You created a post "Vhjjnb\nHhj..."', 51, '2018-02-06 15:42:03'),
(217, 87, 'Post', 'You created a post "Hhiuu\nHiih..."', 52, '2018-02-06 15:45:33');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `comment` longtext COLLATE utf8_unicode_ci NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `tagged_users` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `user_id`, `post_id`, `comment`, `is_deleted`, `tagged_users`, `created_at`, `updated_at`) VALUES
(1, 5, 2, 'Hello first comment', 0, 'a:0:{}', '2017-12-04 17:09:04', '2017-12-04 17:09:04'),
(2, 5, 2, 'Okok', 0, 'a:0:{}', '2017-12-04 17:11:39', '2017-12-04 17:11:39'),
(3, 5, 2, 'hellooo', 0, 'a:0:{}', '2017-12-04 17:13:18', '2017-12-04 17:13:18'),
(4, 5, 2, 'hellooo', 0, 'a:0:{}', '2017-12-04 17:13:29', '2017-12-04 17:13:29'),
(5, 5, 2, 'Great !', 0, 'a:0:{}', '2017-12-04 17:13:32', '2017-12-04 17:13:32'),
(6, 5, 2, 'Great !', 0, 'a:0:{}', '2017-12-04 17:22:00', '2017-12-04 17:22:00'),
(7, 5, 2, 'Okok', 0, 'a:0:{}', '2017-12-04 17:23:12', '2017-12-04 17:23:12'),
(8, 6, 4, 'Ankit kk ', 0, 'a:1:{i:0;s:27:"{"id":5,"name":"Ankit kk "}";}', '2017-12-05 10:56:24', '2017-12-05 10:56:24'),
(9, 6, 4, '@', 0, 'a:1:{i:0;s:27:"{"id":5,"name":"Ankit kk "}";}', '2017-12-05 10:56:31', '2017-12-05 10:56:31'),
(10, 6, 4, '    ', 0, 'a:1:{i:0;s:27:"{"id":5,"name":"Ankit kk "}";}', '2017-12-05 10:56:35', '2017-12-05 10:56:35'),
(11, 6, 4, ' \n\n\n\n\n\n\n\n\n   \n\n\n', 0, 'a:1:{i:0;s:27:"{"id":5,"name":"Ankit kk "}";}', '2017-12-05 10:56:47', '2017-12-05 10:56:47'),
(12, 6, 4, 'Test', 0, 'a:1:{i:0;s:27:"{"id":5,"name":"Ankit kk "}";}', '2017-12-05 10:56:57', '2017-12-05 10:56:57'),
(13, 6, 2, '@', 0, 'a:0:{}', '2017-12-05 10:57:55', '2017-12-05 10:57:55'),
(14, 6, 2, '@nishant\n', 0, 'a:0:{}', '2017-12-05 10:58:22', '2017-12-05 10:58:22'),
(15, 6, 14, '@', 0, 'a:0:{}', '2017-12-05 13:15:10', '2017-12-05 13:15:10'),
(16, 5, 4, 'Hello', 0, 'a:0:{}', '2017-12-05 13:16:03', '2017-12-05 13:16:03'),
(17, 5, 4, 'Okok', 0, 'a:0:{}', '2017-12-05 13:16:31', '2017-12-05 13:16:31'),
(18, 6, 14, '    \n\n\n\n', 0, 'a:0:{}', '2017-12-05 13:17:05', '2017-12-05 13:17:05'),
(19, 7, 14, 'Okk', 0, 'a:0:{}', '2017-12-05 13:39:01', '2017-12-05 13:39:01'),
(20, 7, 14, 'Hhhh', 0, 'a:0:{}', '2017-12-05 13:39:11', '2017-12-05 13:39:11'),
(21, 7, 14, 'hello', 0, 'a:0:{}', '2017-12-05 14:44:51', '2017-12-05 14:44:51'),
(22, 7, 14, 'hello', 0, 'a:0:{}', '2017-12-05 14:47:03', '2017-12-05 14:47:03'),
(23, 7, 2, 'Good', 0, 'a:0:{}', '2017-12-05 16:23:53', '2017-12-05 16:23:53'),
(24, 7, 2, 'Good', 0, 'a:0:{}', '2017-12-05 16:26:54', '2017-12-05 16:26:54'),
(25, 7, 14, 'Hello test comment', 0, 'a:0:{}', '2017-12-05 16:29:16', '2017-12-05 16:29:16'),
(26, 87, 30, 'Hi Ankit kk', 0, 'a:1:{i:0;s:27:"{"id":5,"name":"Ankit kk "}";}', '2018-01-30 12:56:06', '2018-01-30 12:56:06'),
(27, 87, 30, 'Hello', 0, 'a:0:{}', '2018-01-30 12:58:12', '2018-01-30 12:58:12'),
(28, 87, 29, 'Nishant', 0, 'a:1:{i:0;s:26:"{"id":6,"name":"Nishant "}";}', '2018-01-30 13:01:01', '2018-01-30 13:01:01'),
(29, 12, 30, 'Testing', 0, 'a:0:{}', '2018-01-30 14:54:34', '2018-01-30 14:54:34'),
(30, 12, 30, 'Simran', 0, 'a:0:{}', '2018-01-30 15:15:06', '2018-01-30 15:15:06'),
(31, 12, 30, 'Simran', 0, 'a:0:{}', '2018-01-30 15:15:28', '2018-01-30 15:15:28'),
(32, 87, 30, 'Hi', 0, 'a:0:{}', '2018-01-30 15:15:42', '2018-01-30 15:15:42'),
(33, 12, 30, 'Hello comment', 0, 'a:0:{}', '2018-01-30 15:25:12', '2018-01-30 15:25:12'),
(34, 12, 30, 'Ok', 0, 'a:0:{}', '2018-01-30 15:27:33', '2018-01-30 15:27:33'),
(35, 12, 30, 'hello okok', 0, 'a:0:{}', '2018-01-30 15:32:37', '2018-01-30 15:32:37'),
(36, 12, 30, 'test', 0, 'a:0:{}', '2018-01-30 16:07:53', '2018-01-30 16:07:53'),
(37, 12, 30, 'test', 0, 'a:0:{}', '2018-01-30 16:30:46', '2018-01-30 16:30:46'),
(38, 12, 30, 'hello okok', 0, 'a:0:{}', '2018-01-30 16:31:21', '2018-01-30 16:31:21'),
(39, 12, 30, 'hello okok', 0, 'a:0:{}', '2018-01-30 16:31:26', '2018-01-30 16:31:26'),
(40, 12, 30, 'test', 0, 'a:0:{}', '2018-01-30 16:36:46', '2018-01-30 16:36:46'),
(41, 12, 30, 'test', 0, 'a:0:{}', '2018-01-30 16:40:05', '2018-01-30 16:40:05'),
(42, 12, 30, 'test', 0, 'a:0:{}', '2018-01-30 16:40:11', '2018-01-30 16:40:11'),
(43, 92, 32, 'Nishant test', 0, 'a:1:{i:0;s:26:"{"id":6,"name":"Nishant "}";}', '2018-02-01 11:11:40', '2018-02-01 11:11:40'),
(44, 92, 32, 'Test', 0, 'a:1:{i:0;s:26:"{"id":6,"name":"Nishant "}";}', '2018-02-01 11:11:48', '2018-02-01 11:11:48'),
(45, 92, 32, '#', 0, 'a:1:{i:0;s:26:"{"id":6,"name":"Nishant "}";}', '2018-02-01 11:11:57', '2018-02-01 11:11:57'),
(46, 92, 32, '@', 0, 'a:1:{i:0;s:26:"{"id":6,"name":"Nishant "}";}', '2018-02-01 11:12:01', '2018-02-01 11:12:01'),
(47, 92, 32, '@dfgv', 0, 'a:1:{i:0;s:26:"{"id":6,"name":"Nishant "}";}', '2018-02-01 11:12:08', '2018-02-01 11:12:08'),
(49, 92, 32, 'Ankit kk @', 0, 'a:1:{i:0;s:27:"{"id":5,"name":"Ankit kk "}";}', '2018-02-01 11:13:24', '2018-02-01 11:13:24'),
(50, 92, 33, 'Nishant Ankit kk Simran Jit simran ', 0, 'a:4:{i:0;s:26:"{"id":6,"name":"Nishant "}";i:1;s:27:"{"id":5,"name":"Ankit kk "}";i:2;s:29:"{"id":8,"name":"Simran Jit "}";i:3;s:26:"{"id":12,"name":"simran "}";}', '2018-02-01 11:19:05', '2018-02-01 11:19:05'),
(51, 92, 33, 'Nishant , Ankit kk ', 0, 'a:6:{i:0;s:26:"{"id":6,"name":"Nishant "}";i:1;s:27:"{"id":5,"name":"Ankit kk "}";i:2;s:29:"{"id":8,"name":"Simran Jit "}";i:3;s:26:"{"id":12,"name":"simran "}";i:4;s:26:"{"id":6,"name":"Nishant "}";i:5;s:27:"{"id":5,"name":"Ankit kk "}";}', '2018-02-01 11:19:47', '2018-02-01 11:19:47'),
(52, 92, 33, 'Tetstsgsbbsbshdbmsjsbbdhndbshsmsnsnsjhzbdnmdjzbbzmdmsbbznzksnzbzjkzndbd. Znzjjsbdjs', 0, 'a:6:{i:0;s:26:"{"id":6,"name":"Nishant "}";i:1;s:27:"{"id":5,"name":"Ankit kk "}";i:2;s:29:"{"id":8,"name":"Simran Jit "}";i:3;s:26:"{"id":12,"name":"simran "}";i:4;s:26:"{"id":6,"name":"Nishant "}";i:5;s:27:"{"id":5,"name":"Ankit kk "}";}', '2018-02-01 11:20:04', '2018-02-01 11:20:04'),
(53, 87, 33, 'Hi', 0, 'a:0:{}', '2018-02-01 11:58:35', '2018-02-01 11:58:35'),
(54, 87, 33, 'Nishant ', 0, 'a:1:{i:0;s:26:"{"id":6,"name":"Nishant "}";}', '2018-02-01 11:58:48', '2018-02-01 11:58:48'),
(55, 95, 33, 'Tango charlie ', 0, 'a:1:{i:0;s:33:"{"id":92,"name":"Tango charlie "}";}', '2018-02-01 12:29:17', '2018-02-01 12:29:17'),
(56, 98, 38, 'Hello commwnt', 0, 'a:0:{}', '2018-02-01 13:46:27', '2018-02-01 13:46:27'),
(57, 8, 35, 'Tango charlie', 0, 'a:1:{i:0;s:33:"{"id":92,"name":"Tango charlie "}";}', '2018-02-02 11:31:25', '2018-02-02 11:31:25'),
(58, 92, 28, 'Add a comment or tag a user using @', 0, 'a:0:{}', '2018-02-02 13:08:52', '2018-02-02 13:08:52'),
(59, 92, 28, 'Add a comment or tag a user using @', 0, 'a:0:{}', '2018-02-02 13:09:50', '2018-02-02 13:09:50'),
(60, 92, 28, 'Add a comment or tag a user using @', 0, 'a:0:{}', '2018-02-02 13:10:08', '2018-02-02 13:10:08'),
(61, 92, 28, '@Nishant', 0, 'a:0:{}', '2018-02-02 13:11:42', '2018-02-02 13:11:42'),
(62, 92, 35, 'Tango charlie', 0, 'a:1:{i:0;s:33:"{"id":92,"name":"Tango charlie "}";}', '2018-02-02 13:21:56', '2018-02-02 13:21:56'),
(63, 92, 35, '@Nishant', 0, 'a:0:{}', '2018-02-02 13:23:36', '2018-02-02 13:23:36'),
(64, 92, 41, 'Test', 0, 'a:0:{}', '2018-02-02 13:38:17', '2018-02-02 13:38:17'),
(65, 92, 41, 'Testinf', 0, 'a:0:{}', '2018-02-02 13:40:47', '2018-02-02 13:40:47'),
(66, 104, 41, 'Hello', 0, 'a:0:{}', '2018-02-02 13:41:03', '2018-02-02 13:41:03'),
(67, 91, 41, 'Gudd', 0, 'a:0:{}', '2018-02-02 14:35:01', '2018-02-02 14:35:01'),
(68, 91, 41, 'Okok', 0, 'a:0:{}', '2018-02-02 14:40:01', '2018-02-02 14:40:01'),
(69, 90, 42, 'Hi nice post....', 0, 'a:0:{}', '2018-02-02 15:04:04', '2018-02-02 15:04:04'),
(70, 90, 42, 'Hello', 0, 'a:0:{}', '2018-02-02 15:04:31', '2018-02-02 15:04:31'),
(71, 92, 42, 'Great post', 0, 'a:0:{}', '2018-02-02 15:07:07', '2018-02-02 15:07:07'),
(72, 91, 43, 'Comment', 0, 'a:0:{}', '2018-02-02 15:12:41', '2018-02-02 15:12:41'),
(73, 90, 42, 'Nice post..', 0, 'a:0:{}', '2018-02-02 15:13:30', '2018-02-02 15:13:30'),
(74, 91, 43, 'Yoo', 0, 'a:0:{}', '2018-02-02 15:14:20', '2018-02-02 15:14:20'),
(75, 91, 43, 'Okok', 0, 'a:0:{}', '2018-02-02 15:14:32', '2018-02-02 15:14:32'),
(76, 91, 42, 'Hmmmm', 0, 'a:0:{}', '2018-02-02 15:14:50', '2018-02-02 15:14:50'),
(77, 90, 42, 'Gudd', 0, 'a:0:{}', '2018-02-02 15:17:14', '2018-02-02 15:17:14'),
(78, 91, 43, 'Add a comment or tag a user using @', 0, 'a:0:{}', '2018-02-02 16:13:18', '2018-02-02 16:13:18'),
(79, 91, 43, 'Add a comment or tag a user using @', 0, 'a:0:{}', '2018-02-02 16:14:08', '2018-02-02 16:14:08'),
(80, 91, 43, 'Add a comment or tag a user using @', 0, 'a:0:{}', '2018-02-02 16:14:41', '2018-02-02 16:14:41'),
(81, 91, 43, 'Add a comment or tag a user using @', 0, 'a:0:{}', '2018-02-02 16:15:39', '2018-02-02 16:15:39'),
(82, 91, 43, '@', 0, 'a:0:{}', '2018-02-02 16:18:40', '2018-02-02 16:18:40'),
(83, 91, 43, 'Rahul', 0, 'a:1:{i:0;s:25:"{"id":91,"name":"Rahul "}";}', '2018-02-02 16:18:49', '2018-02-02 16:18:49'),
(84, 91, 43, 'sfdsfsdfds', 0, 'a:0:{}', '2018-02-02 16:22:27', '2018-02-02 16:22:27'),
(85, 91, 43, 'Simran Jit', 0, 'a:1:{i:0;s:29:"{"id":8,"name":"Simran Jit "}";}', '2018-02-02 16:22:32', '2018-02-02 16:22:32'),
(86, 91, 43, 'Tango charlie', 0, 'a:1:{i:0;s:33:"{"id":92,"name":"Tango charlie "}";}', '2018-02-02 16:22:39', '2018-02-02 16:22:39'),
(87, 91, 43, 'Simran Jit', 0, 'a:1:{i:0;s:29:"{"id":8,"name":"Simran Jit "}";}', '2018-02-02 16:23:44', '2018-02-02 16:23:44'),
(88, 91, 43, 'Tango charlie', 0, 'a:1:{i:0;s:33:"{"id":92,"name":"Tango charlie "}";}', '2018-02-02 16:23:56', '2018-02-02 16:23:56'),
(89, 91, 43, 'Add a comment or tag a user using @', 0, 'a:0:{}', '2018-02-02 16:24:22', '2018-02-02 16:24:22'),
(90, 91, 43, 'dasdfsdfdas', 0, 'a:0:{}', '2018-02-02 16:24:48', '2018-02-02 16:24:48'),
(91, 91, 43, 'dsfasdfasd Tango charlie', 0, 'a:1:{i:0;s:33:"{"id":92,"name":"Tango charlie "}";}', '2018-02-02 16:24:57', '2018-02-02 16:24:57'),
(92, 91, 43, 'Add a comment or tag a user using @', 0, 'a:0:{}', '2018-02-02 16:26:33', '2018-02-02 16:26:33'),
(93, 91, 43, 'dfgsdfgdfsg', 0, 'a:0:{}', '2018-02-02 16:27:20', '2018-02-02 16:27:20'),
(94, 91, 43, 'Tango charlie', 0, 'a:1:{i:0;s:33:"{"id":92,"name":"Tango charlie "}";}', '2018-02-02 16:27:25', '2018-02-02 16:27:25'),
(95, 90, 43, 'Hello Simran Jit ', 0, 'a:1:{i:0;s:29:"{"id":8,"name":"Simran Jit "}";}', '2018-02-02 16:32:37', '2018-02-02 16:32:37'),
(96, 90, 43, 'Simran Jit ', 0, 'a:2:{i:0;s:29:"{"id":8,"name":"Simran Jit "}";i:1;s:29:"{"id":8,"name":"Simran Jit "}";}', '2018-02-02 16:33:03', '2018-02-02 16:33:03'),
(97, 91, 43, 'Simran Jit', 0, 'a:1:{i:0;s:29:"{"id":8,"name":"Simran Jit "}";}', '2018-02-02 16:34:04', '2018-02-02 16:34:04'),
(98, 91, 43, 'Ggg', 0, 'a:0:{}', '2018-02-02 16:36:53', '2018-02-02 16:36:53'),
(99, 91, 43, 'Tango charlie Ankit kk ', 0, 'a:2:{i:0;s:33:"{"id":92,"name":"Tango charlie "}";i:1;s:27:"{"id":5,"name":"Ankit kk "}";}', '2018-02-02 16:39:26', '2018-02-02 16:39:26'),
(100, 91, 43, 'Tango charlie', 0, 'a:1:{i:0;s:33:"{"id":92,"name":"Tango charlie "}";}', '2018-02-02 16:44:32', '2018-02-02 16:44:32'),
(101, 91, 43, 'Tango charlie', 0, 'a:1:{i:0;s:33:"{"id":92,"name":"Tango charlie "}";}', '2018-02-02 16:45:10', '2018-02-02 16:45:10'),
(102, 91, 43, 'sdfsd', 0, 'a:0:{}', '2018-02-02 16:47:12', '2018-02-02 16:47:12'),
(103, 90, 43, 'Hello I am interested in this opportunity and I hope', 0, 'a:0:{}', '2018-02-02 16:48:27', '2018-02-02 16:48:27'),
(104, 90, 43, 'Ok I just wanted', 0, 'a:0:{}', '2018-02-02 16:49:46', '2018-02-02 16:49:46'),
(105, 90, 43, 'We start with that and it', 0, 'a:0:{}', '2018-02-02 16:54:28', '2018-02-02 16:54:28'),
(106, 90, 43, 'I have been in touch to be', 0, 'a:0:{}', '2018-02-02 16:57:35', '2018-02-02 16:57:35'),
(107, 91, 43, 'xcvxcv', 0, 'a:0:{}', '2018-02-02 16:59:40', '2018-02-02 16:59:40'),
(108, 90, 43, 'Guddd', 0, 'a:0:{}', '2018-02-02 16:59:46', '2018-02-02 16:59:46'),
(109, 91, 43, 'Simran Jit', 0, 'a:1:{i:0;s:29:"{"id":8,"name":"Simran Jit "}";}', '2018-02-02 16:59:49', '2018-02-02 16:59:49'),
(110, 90, 43, 'We start with', 0, 'a:0:{}', '2018-02-02 17:00:02', '2018-02-02 17:00:02'),
(111, 90, 43, 'Ok I just wanted', 0, 'a:0:{}', '2018-02-02 17:00:21', '2018-02-02 17:00:21'),
(112, 91, 43, 'Ggg', 0, 'a:0:{}', '2018-02-02 17:09:58', '2018-02-02 17:09:58'),
(113, 91, 42, '@', 0, 'a:0:{}', '2018-02-02 17:10:59', '2018-02-02 17:10:59'),
(114, 91, 42, 'Simran Jit', 0, 'a:1:{i:0;s:29:"{"id":8,"name":"Simran Jit "}";}', '2018-02-02 17:11:07', '2018-02-02 17:11:07'),
(115, 91, 42, 'sdfasdfasdfads', 0, 'a:0:{}', '2018-02-02 17:11:18', '2018-02-02 17:11:18'),
(116, 91, 42, 'Rdd', 0, 'a:0:{}', '2018-02-02 17:37:03', '2018-02-02 17:37:03'),
(117, 91, 42, 'sdfasdf', 0, 'a:0:{}', '2018-02-02 17:42:02', '2018-02-02 17:42:02'),
(118, 91, 41, 'hi', 0, 'a:0:{}', '2018-02-02 17:49:48', '2018-02-02 17:49:48'),
(119, 91, 41, 'Hi', 0, 'a:0:{}', '2018-02-02 17:53:17', '2018-02-02 17:53:17'),
(120, 92, 41, '@', 0, 'a:0:{}', '2018-02-03 17:48:30', '2018-02-03 17:48:30'),
(121, 92, 41, '-', 0, 'a:0:{}', '2018-02-03 17:48:35', '2018-02-03 17:48:35'),
(122, 92, 41, 'Rahul', 0, 'a:1:{i:0;s:25:"{"id":91,"name":"Rahul "}";}', '2018-02-03 17:48:46', '2018-02-03 17:48:46'),
(123, 92, 41, 'RamanUser', 0, 'a:1:{i:0;s:30:"{"id":118,"name":"RamanUser "}";}', '2018-02-03 17:48:54', '2018-02-03 17:48:54'),
(124, 87, 44, 'Hi', 0, 'a:0:{}', '2018-02-06 12:28:58', '2018-02-06 12:28:58'),
(125, 87, 44, 'Ankit kk ', 0, 'a:1:{i:0;s:27:"{"id":5,"name":"Ankit kk "}";}', '2018-02-06 12:50:11', '2018-02-06 12:50:11'),
(126, 87, 53, 'Hi', 0, 'a:0:{}', '2018-02-12 12:42:52', '2018-02-12 12:42:52'),
(127, 87, 53, 'Simran ', 0, 'a:1:{i:0;s:26:"{"id":10,"name":"Simran "}";}', '2018-02-12 12:43:12', '2018-02-12 12:43:12');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `venue` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `is_deleted` tinyint(1) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `start_date` datetime NOT NULL,
  `start_time` time NOT NULL,
  `ending_date` datetime NOT NULL,
  `ending_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `user_id`, `media_id`, `latitude`, `longitude`, `location`, `venue`, `name`, `description`, `is_deleted`, `is_published`, `start_date`, `start_time`, `ending_date`, `ending_time`, `created_at`, `updated_at`) VALUES
(1, 5, 4, '13.0827', '80.2707', 'Chennai', 'Nyc', 'Birthday event', 'Friend birthday', 0, 1, '2017-12-05 00:00:00', '04:00:00', '2017-12-18 00:00:00', '04:35:00', '2017-12-04 17:08:14', '2017-12-04 17:08:14'),
(2, 5, NULL, '13.0827', '80.2707', 'Chennai', 'Nyc dc', 'Another event', 'Farewell event', 0, 1, '2017-12-21 00:00:00', '00:00:00', '2017-12-28 00:00:00', '06:00:00', '2017-12-04 17:30:55', '2017-12-04 17:30:55'),
(3, 6, 7, '', '', 'Mumbai', 'Tet', 'Title', 'Test', 0, 1, '2017-12-05 00:00:00', '18:00:00', '2017-12-05 00:00:00', '04:53:00', '2017-12-05 11:01:53', '2017-12-05 11:01:53'),
(4, 6, 31, '17.3700', '78.4800', 'Hyderabad', 'Test', 'Create event', 'Test', 0, 1, '2017-12-05 00:00:00', '21:29:00', '2017-12-21 00:00:00', '14:50:00', '2017-12-05 13:27:59', '2017-12-05 13:27:59'),
(5, 87, NULL, '30.75', '76.78', 'Chandigarh', 'St.kabir ', 'Cricket Match ', 'Description', 0, 1, '2018-02-02 00:00:00', '10:09:00', '2018-02-04 00:00:00', '10:15:00', '2018-01-30 13:10:20', '2018-01-30 13:10:20'),
(6, 92, 97, '-54.801912099999996', '-68.3029511', 'Ushuaia', 'Test', 'Event for test', 'Description', 0, 1, '2018-02-01 00:00:00', '00:00:00', '2018-02-01 00:00:00', '01:05:00', '2018-02-01 11:37:16', '2018-02-01 11:37:16'),
(7, 91, 98, '12.9667', '77.5667', 'Bangalore', 'Phae 7', 'Test event', 'Birthday event', 1, 1, '2018-02-01 00:00:00', '13:00:00', '2018-02-09 00:00:00', '13:20:00', '2018-02-01 11:40:58', '2018-02-01 11:40:58'),
(8, 91, 99, '13.0827', '80.2707', 'Chennai', 'Mohali', 'Another event', 'Test', 1, 1, '2018-02-01 00:00:00', '00:00:00', '2018-02-23 00:00:00', '05:00:00', '2018-02-01 11:43:02', '2018-02-01 11:43:02'),
(9, 92, NULL, '18.9750', '72.8258', 'Mumbai', 'Title', 'Title', 'Description', 0, 1, '2018-02-28 00:00:00', '09:00:00', '2018-02-28 00:00:00', '09:05:00', '2018-02-01 11:43:05', '2018-02-01 11:43:05'),
(10, 91, 101, '17.3700', '78.4800', 'Hyderabad', 'Mohali', 'Anothrr evnt', 'Test', 1, 1, '2018-02-14 00:00:00', '00:00:00', '2018-02-23 00:00:00', '03:00:00', '2018-02-01 12:08:43', '2018-02-01 12:08:43'),
(11, 91, 102, '12.9667', '77.5667', 'Bangalore', 'Mohali', 'Race event', 'Phae 7', 1, 1, '2018-02-01 00:00:00', '00:00:00', '2018-02-23 00:00:00', '00:00:00', '2018-02-01 12:10:20', '2018-02-01 12:10:20'),
(12, 91, 105, '17.3700', '78.4800', 'Hyderabad', 'Phase7', 'Test event', 'Test event', 1, 1, '2018-02-01 00:00:00', '04:00:00', '2018-02-23 00:00:00', '04:20:00', '2018-02-01 13:20:29', '2018-02-01 13:20:29'),
(13, 91, 106, '13.0827', '80.2707', 'Chennai', 'Pjase 7', 'Test event', 'Test', 1, 1, '2018-02-01 00:00:00', '00:00:00', '2018-02-01 00:00:00', '05:00:00', '2018-02-01 13:27:42', '2018-02-01 13:27:42'),
(14, 91, NULL, '12.9667', '77.5667', 'Bangalore', 'Phase7', 'Party event', 'Party', 0, 1, '2018-02-02 00:00:00', '00:00:00', '2018-02-13 00:00:00', '00:00:00', '2018-02-02 11:13:35', '2018-02-02 11:13:35'),
(15, 8, 107, '13.0827', '80.2707', 'Chennai', 'Sdfasdf', 'Adsfsd', 'Sdfasdf', 0, 1, '2018-02-02 00:00:00', '11:41:00', '2018-02-02 00:00:00', '11:46:00', '2018-02-02 11:41:35', '2018-02-02 11:41:35'),
(16, 8, NULL, '30.75', '76.78', 'Chandigarh', 'Sdfasdf', 'Sdfasd', 'Sdfasdf', 0, 1, '2018-02-02 00:00:00', '11:42:00', '2018-02-02 00:00:00', '11:47:00', '2018-02-02 11:42:34', '2018-02-02 11:42:34'),
(17, 8, NULL, '22.5667', '88.3667', 'Kolkata', 'Dsfasd', 'Sdfsd', 'Sdfsdfasf', 0, 0, '2018-02-02 00:00:00', '11:55:00', '2018-02-02 00:00:00', '12:00:00', '2018-02-02 11:55:25', '2018-02-02 11:55:25'),
(18, 92, 114, '13.0827', '80.2707', 'Chennai', 'Venue', 'Test Event', 'Description ', 0, 1, '2018-02-02 00:00:00', '15:55:00', '2037-03-02 00:00:00', '14:50:00', '2018-02-02 14:46:22', '2018-02-02 14:46:22'),
(19, 92, NULL, '18.975', '72.8258', 'Mumbai', 'Test', 'Test Event 2', 'Test', 0, 1, '2018-03-02 00:00:00', '14:46:00', '2018-04-02 00:00:00', '15:51:00', '2018-02-02 14:47:02', '2018-02-02 14:47:02'),
(20, 92, NULL, '0.0', '0.0', 'Cañada de Gómez, Santa Fe Province, Argentina', 'Test', 'Event', 'Test', 0, 1, '2018-02-02 00:00:00', '15:55:00', '2032-02-02 00:00:00', '09:58:00', '2018-02-02 14:53:54', '2018-02-02 14:53:54'),
(21, 90, 116, '12.9667', '77.5667', 'Bangalore', 'Phase8', 'Birthday event', 'Partyy', 0, 1, '2018-02-02 00:00:00', '04:00:00', '2018-02-16 00:00:00', '04:00:00', '2018-02-02 15:10:45', '2018-02-02 15:10:45'),
(22, 91, NULL, '0.0', '0.0', 'Ahmedabad, Uttar Pradesh, India', 'Sad', 'sdfasdf', 'sdfasdf', 0, 0, '2018-02-02 00:00:00', '15:56:00', '2018-02-02 00:00:00', '16:01:00', '2018-02-02 15:56:28', '2018-02-02 15:56:28'),
(23, 91, NULL, '13.0827', '80.2707', 'Chennai', 'Sdfasdf', 'sdfasdf', 'Sdfasdfasd', 0, 0, '2018-02-03 00:00:00', '16:12:00', '2018-02-03 00:00:00', '16:17:00', '2018-02-03 16:12:35', '2018-02-03 16:12:35'),
(24, 91, NULL, '22.5667', '88.3667', 'Kolkata', 'Sdfasdfas', 'Change13', 'Sdfasdf', 0, 1, '2018-02-03 00:00:00', '16:23:00', '2018-02-03 00:00:00', '16:28:00', '2018-02-03 16:13:40', '2018-02-03 16:13:40'),
(25, 92, 122, '18.975', '72.8258', 'Mumbai', 'Venue', 'Title', 'Description ', 0, 1, '2018-02-03 00:00:00', '17:34:00', '2018-02-15 00:00:00', '17:39:00', '2018-02-03 17:34:53', '2018-02-03 17:34:53');

-- --------------------------------------------------------

--
-- Table structure for table `EventComment`
--

CREATE TABLE `EventComment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `comment` longtext COLLATE utf8_unicode_ci NOT NULL,
  `isdeleted` tinyint(1) NOT NULL,
  `taggedUsers` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `EventComment`
--

INSERT INTO `EventComment` (`id`, `user_id`, `event_id`, `comment`, `isdeleted`, `taggedUsers`, `created_at`, `updated_at`) VALUES
(1, 5, 1, 'Hello first event comment', 0, 'a:0:{}', '2017-12-04 17:18:54', '2017-12-04 17:18:54'),
(2, 5, 1, 'Another comment', 0, 'a:0:{}', '2017-12-04 17:23:29', '2017-12-04 17:23:29'),
(3, 5, 3, 'Good', 0, 'a:0:{}', '2017-12-05 13:17:04', '2017-12-05 13:17:04'),
(4, 6, 4, 'Test', 0, 'a:0:{}', '2017-12-05 13:29:42', '2017-12-05 13:29:42'),
(5, 7, 4, 'Event first comment', 0, 'a:0:{}', '2017-12-05 16:31:57', '2017-12-05 16:31:57'),
(6, 87, 5, 'Hi ', 0, 'a:0:{}', '2018-01-30 13:14:23', '2018-01-30 13:14:23'),
(7, 12, 5, 'Hello', 0, 'a:0:{}', '2018-01-30 15:26:17', '2018-01-30 15:26:17'),
(8, 12, 5, 'Hello', 0, 'a:0:{}', '2018-01-30 15:26:22', '2018-01-30 15:26:22'),
(9, 12, 5, 'Hello', 0, 'a:0:{}', '2018-01-30 15:27:23', '2018-01-30 15:27:23'),
(10, 92, 6, '@ Tango charlie ', 0, 'a:1:{i:0;s:33:"{"id":92,"name":"Tango charlie "}";}', '2018-02-01 11:40:03', '2018-02-01 11:40:03'),
(11, 92, 18, 'Test', 0, 'a:0:{}', '2018-02-02 14:52:52', '2018-02-02 14:52:52'),
(12, 91, 21, 'Am', 0, 'a:0:{}', '2018-02-02 17:26:51', '2018-02-02 17:26:51'),
(13, 91, 21, 'hi', 0, 'a:0:{}', '2018-02-02 17:28:43', '2018-02-02 17:28:43'),
(14, 91, 21, 'test', 0, 'a:0:{}', '2018-02-02 17:32:23', '2018-02-02 17:32:23'),
(15, 91, 21, 'Ss', 0, 'a:0:{}', '2018-02-02 17:32:29', '2018-02-02 17:32:29'),
(16, 91, 21, 'Add a comment or tag a user using @', 0, 'a:0:{}', '2018-02-02 17:32:40', '2018-02-02 17:32:40'),
(17, 91, 21, 'hi', 0, 'a:0:{}', '2018-02-02 17:36:03', '2018-02-02 17:36:03');

-- --------------------------------------------------------

--
-- Table structure for table `EventLikes`
--

CREATE TABLE `EventLikes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `EventLikes`
--

INSERT INTO `EventLikes` (`id`, `user_id`, `event_id`, `status`, `created_at`) VALUES
(1, 6, 4, 1, '2017-12-05 13:29:38'),
(2, 87, 5, 1, '2018-01-30 13:14:18'),
(3, 92, 6, 1, '2018-02-01 11:38:43'),
(4, 92, 9, 1, '2018-02-01 11:49:07'),
(5, 92, 18, 1, '2018-02-02 14:52:46'),
(6, 92, 20, 1, '2018-02-02 14:54:31'),
(7, 91, 21, 1, '2018-02-02 17:35:38');

-- --------------------------------------------------------

--
-- Table structure for table `event_invited_users`
--

CREATE TABLE `event_invited_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `event_invited_users`
--

INSERT INTO `event_invited_users` (`id`, `user_id`, `event_id`, `status`) VALUES
(1, 5, 1, 3),
(2, 6, 4, 1),
(3, 92, 7, 1),
(4, 92, 9, 1),
(5, 92, 18, 1),
(6, 92, 25, 2);

-- --------------------------------------------------------

--
-- Table structure for table `event_user`
--

CREATE TABLE `event_user` (
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `event_user`
--

INSERT INTO `event_user` (`event_id`, `user_id`) VALUES
(9, 5),
(9, 8),
(9, 12),
(9, 91),
(9, 96),
(18, 5),
(18, 8),
(18, 12),
(18, 91),
(18, 96),
(20, 5),
(20, 96),
(20, 117);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `user_id`, `email`, `title`, `description`, `type`, `created_at`, `status`) VALUES
(1, 6, 'nishant@42works.net', NULL, 'Test', 'Contact', '2017-12-05 13:12:44', 0),
(2, 92, 'tango42charlie@gmail.com', NULL, 'Testing purpose', 'Contact', '2018-02-01 11:33:09', 0),
(3, 92, 'tango42charlie@gmail.com', NULL, 'Testing purpose', 'Contact', '2018-02-01 11:33:18', 0),
(4, 91, 'ankit123@yopmail.com', NULL, 'Good', 'Contact', '2018-02-01 11:39:45', 0),
(5, 91, 'testankit@yopmail.com', 'Hero', 'asdsdfsdf', 'sdfdsg', '2018-02-02 11:50:44', 0),
(6, 91, 'testankit@yopmail.com', 'Hero', 'asdsdfsdf', 'sdfdsg', '2018-02-02 12:01:34', 0),
(7, 104, 'test42@yopmail.com', 'Other', 'Test comment', 'feedback', '2018-02-02 13:03:44', 0),
(8, 91, 'ankit123@yopmail.com', 'General Enquiry', 'sdfsadfasdf', 'feedback', '2018-02-02 17:57:56', 0),
(9, 91, 'ankit123@yopmail.com', 'General Enquiry', 'sdfasdfasdf', 'feedback', '2018-02-02 17:59:08', 0),
(10, 92, 'tango42charlie@gmail.com', 'Other', 'Description ', 'feedback', '2018-02-03 17:33:11', 0),
(11, 91, 'ankit123@yopmail.com', 'General Enquiry', 'Hello', 'feedback', '2018-02-03 22:55:36', 0),
(12, 91, 'ankit123@yopmail.com', 'General Enquiry', 'Dad', 'feedback', '2018-02-03 22:56:03', 0),
(13, 91, 'ankit123@yopmail.com', 'General Enquiry', 'Sofas', 'feedback', '2018-02-03 22:59:39', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `media_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_privacy` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `regno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_verified` int(11) NOT NULL,
  `fb_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fb_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedin_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedinToken` text COLLATE utf8_unicode_ci,
  `android_device_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ios_device_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `batch_start_year` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `batch` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_alternate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_alternate_privacy` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marital_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `children_studying` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `universityCS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `industry` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isApproved` int(11) NOT NULL,
  `search_count` int(11) NOT NULL,
  `isCompletedProfile` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `curriculum` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fos_user`
--

INSERT INTO `fos_user` (`id`, `media_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `first_name`, `last_name`, `phone_number`, `phone_privacy`, `latitude`, `longitude`, `location`, `regno`, `email_verified`, `fb_id`, `fb_token`, `linkedin_id`, `linkedinToken`, `android_device_token`, `ios_device_token`, `access_token`, `birthday`, `gender`, `course`, `batch_start_year`, `batch`, `company`, `current_status`, `phone_alternate`, `phone_alternate_privacy`, `marital_status`, `children_studying`, `universityCS`, `designation`, `city`, `country`, `industry`, `isApproved`, `search_count`, `isCompletedProfile`, `created_at`, `curriculum`) VALUES
(1, 34, 'admin', 'admin', 'admin', 'admin', 1, 'rz4cls24trkoswcokg8k88w8sk8gg0c', '$2y$13$rz4cls24trkoswcokg8k8u8v69nTfDGcDA2UGA7VhDYb6pUfdoMqC', '2018-03-14 10:36:22', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', 0, NULL, 'KROS Alumni', NULL, NULL, 'Public', '12.12', '34.34', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '6598ca31ef2bd30f60e46a6b5a309f06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2017-12-04', NULL),
(2, NULL, 'rahul@42works.net', 'rahul@42works.net', 'rahul@42works.net', 'rahul@42works.net', 1, 'khadptnbtioook048oog40s04osgcwo', '$2y$13$khadptnbtioook048oog4urd2hGKNeGk.vOKhtxWHI0uDjzpd0pI6', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Rahul', NULL, '2347329847', 'Public', '20.593', '78.9629', 'Mohali', '232323', 1, NULL, NULL, NULL, NULL, '121212', NULL, 'a6a69032a15a6b77cd5ef524084ddc1c', '2017-12-04', NULL, 'MCA', NULL, '2000', NULL, NULL, NULL, NULL, NULL, NULL, 'PTU', 'Soft', NULL, NULL, 'IT', 0, 0, 0, '2017-12-04', ''),
(3, NULL, 'kamboj.ankit1988@gmail.com', 'kamboj.ankit1988@gmail.com', 'kamboj.ankit1988@gmail.com', 'kamboj.ankit1988@gmail.com', 1, 'rq5d34b5k5cwgsw84goocg4kgc4csg4', '$2y$13$rq5d34b5k5cwgsw84goocelWCy5iv/l5zLMUySZS7YRJ7w5EPHn7.', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '12345', NULL, '44664433', 'Public', '13.0827', '80.2707', 'Cañada de Gómez', 'A001', 0, NULL, NULL, NULL, NULL, NULL, NULL, '9285dc225fdfbeee4aa6699d79bff44d', '2017-12-04', NULL, '', NULL, '2014', NULL, NULL, NULL, NULL, NULL, NULL, 'Ptu', 'Student', NULL, NULL, 'Animation', 0, 0, 0, '2017-12-04', ''),
(4, NULL, 'rahulsingh@yopmail.com', 'rahulsingh@yopmail.com', 'rahulsingh@yopmail.com', 'rahulsingh@yopmail.com', 1, 'p0mrczkhzi80k8sgwcos08kgkgww40k', '$2y$13$p0mrczkhzi80k8sgwcos0uBXYS0PI1usrkbPtPAbZeubDcxOHT7Bu', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '12345', NULL, '46466111', 'Public', '18.9750', '72.8258', 'Mumbai', 'A002', 0, NULL, NULL, NULL, NULL, NULL, NULL, '7410e8d9df73f2016287a7ca8f3ad135', '2017-12-04', NULL, '', NULL, '2015', NULL, NULL, NULL, NULL, NULL, NULL, 'Pu', 'Student', NULL, NULL, 'Alternative Medicine', 0, 0, 0, '2017-12-04', ''),
(5, 25, 'ankit121@yopmail.com', 'ankit121@yopmail.com', 'ankit121@yopmail.com', 'ankit121@yopmail.com', 1, '4nk4dxgo2xic4o0ck004wcskwkwossw', '$2y$13$4nk4dxgo2xic4o0ck004wOdWgPVX09OLZv0TU9GSNJ4ZT0rUUQCxq', '2017-12-05 10:38:30', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Ankit kk', NULL, '11334496', 'Private', '0.0', '0.0', 'Hyderabad', 'A003', 1, NULL, NULL, NULL, NULL, NULL, NULL, '66475c20221590ea90acf2dac332c990', '2017-04-12', NULL, '', NULL, '2015', 'Google', NULL, NULL, NULL, NULL, NULL, 'Pu', 'Student', NULL, NULL, 'Architecture & Planning', 1, 12, 1, '2017-12-04', ''),
(6, 30, 'nishant@42works.net', 'nishant@42works.net', 'nishant@42works.net', 'nishant@42works.net', 0, '162q16lfww3k8gowo8o48sss8wkgsc0', '$2y$13$162q16lfww3k8gowo8o48ew0Bvy3/cSbR8vfnJgopC1UgZXsQsp.a', '2017-12-05 10:33:35', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Nishant', NULL, '789645213', 'Public', '0.0', '0.0', 'Sahibzada Ajit Singh Nagar', 'QA Tester', 1, NULL, NULL, NULL, NULL, NULL, NULL, '6468fa5696fabdb0e0e1ed0c1040e2f5', '2017-05-12', NULL, '', NULL, '2010', 'Test\'s', NULL, NULL, NULL, NULL, NULL, 'Test\'s, Test\'s, Test\'s, Test\'s, Test\'s', 'Student', NULL, NULL, 'International Trade and Development', 1, 7, 1, '2017-12-05', ''),
(7, NULL, 'ankit122@yopmail.com', 'ankit122@yopmail.com', 'ankit122@yopmail.com', 'ankit122@yopmail.com', 1, '1edqjpko2tus80c44k0go4kkwogsws', '$2y$13$1edqjpko2tus80c44k0gours/CUULF6p7f6C0atzYca5MMkz6v2Y2', '2017-12-05 13:26:28', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Ankit', NULL, '66443311', 'Public', '12.9667', '77.5667', 'Bangalore', 'A002', 1, NULL, NULL, NULL, NULL, 'dCholc-aPVQ:APA91bG_trqVdfL-qycJ0jqUK8zpuFExdXNe-mRJMA8_uaA0heOarKg8-iuyT4BfcjAN8vu8dsdwZZLFCDQChz13n6P-tqSjEaSmreX-TTgNYpgtaXOqC1uPMzENxgqsYrcF6EmiYyQh', NULL, 'e399c08be2b02948d6e7085a8344af36', '2017-12-05', NULL, '', NULL, '2014', NULL, NULL, NULL, NULL, NULL, NULL, 'Ptu', 'Student', NULL, NULL, 'Apparel & Fashion', 1, 6, 0, '2017-12-05', ''),
(8, 35, 'simran271289@gmail.com', 'simran271289@gmail.com', 'simran271289@gmail.com', 'simran271289@gmail.com', 1, '920wztma5jc4o8oggw40wgk44kswkgc', '$2y$13$920wztma5jc4o8oggw40wek883naT036t20.3WLBufEwVKdJP3nOi', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Simran Jit', NULL, '', 'Public', '20.593', '78.9629', NULL, '', 0, '1723067951096740', '', '', '', NULL, '', '56819e019d28a680e67021ee69eed129', '2017-12-22', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', 1, 0, 1, '2017-12-22', ''),
(10, NULL, 'simran@gmail.com', 'simran@gmail.com', 'simran@gmail.com', 'simran@gmail.com', 1, '29gs2ryc73y84g4k048cwooo8css40s', '$2y$13$29gs2ryc73y84g4k048cweGHRJrmpxKeuJgw3txIiwqMmh84EHA9S', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Simran', NULL, '123454648', 'Public', '17.37', '78.48', 'Hyderabad', 'ahaha', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'aa259deb76851ace709150e8eafd067e', '2018-01-02', NULL, '', NULL, '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'sggsgs', 'KROS', NULL, NULL, 'Alternative Dispute Resolution', 1, 4, 0, '2018-01-02', ''),
(11, NULL, 'simran1@gmail.com', 'simran1@gmail.com', 'simran1@gmail.com', 'simran1@gmail.com', 1, '6j7dfthyidsssgkk80040s8o0os0ocw', '$2y$13$6j7dfthyidsssgkk80040evev0321iIEH9Orx5YK.puFMSsnTYm1W', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'simran', NULL, '8052525252', 'Public', '12.9667', '77.5667', 'Bangalore', 'cavsv', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'fb466667178936ff3bf20930d2006750', '2018-01-04', NULL, '', NULL, '2012', NULL, NULL, NULL, NULL, NULL, NULL, 'afvavs', 'KROS', NULL, NULL, 'Architecture & Planning', 1, 4, 0, '2018-01-04', ''),
(12, NULL, 'harsimranjit@42works.net', 'harsimranjit@42works.net', 'harsimranjit@42works.net', 'harsimranjit@42works.net', 1, 'uzxlpjuz8kgg8wg8sksgcc8kksw4cg', '$2y$13$uzxlpjuz8kgg8wg8sksgcOYyzhvQwLo/srqjVolXlYW8mBc5sxsKW', '2018-01-30 15:24:41', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'simran', NULL, '8054545454', 'Public', '0', '0', 'Chennai', 'r1', 1, NULL, NULL, NULL, NULL, NULL, NULL, '558170387effb9a00dc31ba4c7bccce9', '2018-01-04', NULL, '', NULL, '2012', 'dd', NULL, NULL, NULL, NULL, NULL, 'abcd', 'KROS', NULL, NULL, 'Arts and Crafts', 1, 11, 1, '2018-01-04', ''),
(13, NULL, 'imprabasdflt@gmail.com', 'imprabasdflt@gmail.com', 'imprabasdflt@gmail.com', 'imprabasdflt@gmail.com', 1, 'dmivykkuga8sw804s4g8kkgcwg80gwo', '$2y$13$dmivykkuga8sw804s4g8keAu6oJxd3TVssAs7HCbfLaJ3/NScSsxK', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '', NULL, '', 'Public', '20.593', '78.9629', NULL, '', 0, NULL, NULL, NULL, NULL, NULL, NULL, '26a7b9d06422bf8e789a6a3d4df3eb01', '2018-01-04', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', 0, 0, 0, '2018-01-04', ''),
(14, NULL, 'imprabaltha@gmail.com', 'imprabaltha@gmail.com', 'imprabaltha@gmail.com', 'imprabaltha@gmail.com', 1, '7g9e4ju6z1wco80cko48kkw0o8sggcg', '$2y$13$7g9e4ju6z1wco80cko48ke16q2xjQ5psZCbcXtbzf0svZgfW/Rlg6', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '', NULL, '', 'Public', '20.593', '78.9629', NULL, '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'a3e45dd690b607aa6af96d6cc4e7c8d6', '2018-01-05', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', 0, 0, 0, '2018-01-05', ''),
(15, NULL, 'asd@gmail.com', 'asd@gmail.com', 'asd@gmail.com', 'asd@gmail.com', 1, 'q7cekjtl0msckok04gs4owso8sscgo4', '$2y$13$q7cekjtl0msckok04gs4ouh36jd2rRPOfUcFtdJ9TDkMpYysM/Euy', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '', NULL, '', 'Public', '20.593', '78.9629', NULL, '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'b13d89dc1a67d7743ea5f2007fa75785', '2018-01-05', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', 0, 0, 0, '2018-01-05', ''),
(16, NULL, 'sdfsdf@gmail.com', 'sdfsdf@gmail.com', 'sdfsdf@gmail.com', 'sdfsdf@gmail.com', 1, 'ggs3yirbtjwwssw4kk04sg880okg8oo', '$2y$13$ggs3yirbtjwwssw4kk04sew0hcL5rxHDBZe/9vL.bTGbGWVmyf8vK', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '', NULL, '', 'Public', '20.593', '78.9629', NULL, '', 0, NULL, NULL, NULL, NULL, NULL, NULL, '507faf60cc13c4eb9ca088814293315e', '2018-01-05', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', 0, 0, 0, '2018-01-05', ''),
(17, NULL, 'sdfsdf1@gmail.com', 'sdfsdf1@gmail.com', 'sdfsdf1@gmail.com', 'sdfsdf1@gmail.com', 1, 'e4qdqv7xwjs4k4cswok0sg4ssk84wwg', '$2y$13$e4qdqv7xwjs4k4cswok0sehWlg/O74DjxMI1YKEyCjz5D2o/jDzIO', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '', NULL, '', 'Public', '20.593', '78.9629', NULL, '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'c08718e887fe871aba084f871650d880', '2018-01-05', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', 0, 0, 0, '2018-01-05', ''),
(63, 74, 'Test45@gmail.com', 'test45@gmail.com', 'Test45@gmail.com', 'test45@gmail.com', 1, 'rtky7wdkjb4wck000w8s8g00ow0g4gk', '$2y$13$rtky7wdkjb4wck000w8s8eMJha6jvzWuDzIjEzNGEf3zHC6q777EK', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '', NULL, '', 'Public', '20.593', '78.9629', NULL, '', 0, NULL, NULL, NULL, NULL, NULL, NULL, '09cee60751d8a98c4c7c1994f3bc3e06', '2018-01-10', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', 0, 0, 0, '2018-01-10', ''),
(64, 75, 'Test46@gmail.com', 'test46@gmail.com', 'Test46@gmail.com', 'test46@gmail.com', 1, 'b5kg5ouxsvks0sokgg0kcoc0c4o88cc', '$2y$13$b5kg5ouxsvks0sokgg0kcegxGm5UX641pPRLR1ixMAcSDJsNFTHJC', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '', NULL, '', 'Public', '20.593', '78.9629', NULL, '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'c22576d077e927a591e23b2f89030246', '2018-01-10', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', 0, 0, 0, '2018-01-10', ''),
(65, 76, 'hello@gmail.com', 'hello@gmail.com', 'hello@gmail.com', 'hello@gmail.com', 1, 'epa5j48k1uokw8ks0o8c80oo4g0owsc', '$2y$13$epa5j48k1uokw8ks0o8c8un8eA99lCDh1.yagcmhkQl7GXrSglNfG', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'test user', NULL, '4664644', 'Public', '12.9667', '77.5667', 'Bangalore', 'a001', 0, NULL, NULL, NULL, NULL, NULL, NULL, '85d586629c443219e0ef5984f94bb487', '2018-01-10', NULL, '', NULL, '2011', NULL, NULL, NULL, NULL, NULL, NULL, 'pu', 'Student', NULL, NULL, 'Automotive', 1, 2, 0, '2018-01-10', ''),
(66, 78, 'sjdfsd@.sdfasd..com', 'sjdfsd@.sdfasd..com', 'sjdfsd@.sdfasd..com', 'sjdfsd@.sdfasd..com', 1, 'mz1gr59e8nkcsosk4kkc0sk0g40gkc4', '$2y$13$mz1gr59e8nkcsosk4kkc0eDA/UUio0p1ONsJSQ7OJdH/5i5ymrh/W', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Simran', NULL, '343423', 'Public', '13.0827', '80.2707', 'Chennai', 'sdfadsf', 0, NULL, NULL, NULL, NULL, NULL, NULL, '0d56467adb0f2824c9e390e0dcf2b53f', '2018-01-12', NULL, '', NULL, '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'sdfasdf', 'KROS', NULL, NULL, 'Hospital & Health Care', 0, 0, 0, '2018-01-12', ''),
(67, NULL, 'abcde@gmail.com', 'abcde@gmail.com', 'abcde@gmail.com', 'abcde@gmail.com', 1, 'dui5tqbqsvwckcg0ksokkg0osk0ssg0', '$2y$13$dui5tqbqsvwckcg0ksokkebGAb5yH2Iw2syWk514byhnquOxOjevy', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Simran', NULL, '23423423', 'Public', '22.5667', '88.3667', 'Kolkata', 'sdfasdf', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'f5be242c4e4dfb8363998c3554583f63', '2018-01-12', NULL, '', NULL, '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'sdfasdfds', 'KROS', NULL, NULL, 'Hospital & Health Care', 0, 0, 0, '2018-01-12', ''),
(68, 79, 'sdfasdf@digs.dfgsdf', 'sdfasdf@digs.dfgsdf', 'sdfasdf@digs.dfgsdf', 'sdfasdf@digs.dfgsdf', 1, 'dmnu8t4t0v4g4sck0g8csgcwkwkg04w', '$2y$13$dmnu8t4t0v4g4sck0g8csevgvmBEhkiVT056HwehRFwwQ2laQ81Bi', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Dfgsfdg', NULL, 'dfgdfgsdf', 'Public', '22.5667', '88.3667', 'Kolkata', 'dfgdfsg', 0, NULL, NULL, NULL, NULL, NULL, NULL, '80f8d287c8a943b203bda5f989f62859', '2018-01-12', NULL, '', NULL, '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'Gaddafi', 'KROS', NULL, NULL, 'Hospital & Health Care', 0, 0, 0, '2018-01-12', ''),
(69, 80, 'dds@fdg.fghdfg', 'dds@fdg.fghdfg', 'dds@fdg.fghdfg', 'dds@fdg.fghdfg', 1, 'o6icyoe6on4wkkk0k8oskkc04g8ggsg', '$2y$13$o6icyoe6on4wkkk0k8oskexC4MM3IPvhMLEoO9pKJ2Uh5muftxeQK', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Sdfasd', NULL, '34423', 'Public', '17.37', '78.48', 'Hyderabad', 'sdfasdf a', 0, NULL, NULL, NULL, NULL, NULL, NULL, '90ef2a12ccbb869020e75105985f5986', '2018-01-12', NULL, '', NULL, '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'sdfasdf', 'KROS', NULL, NULL, 'Hospital & Health Care', 0, 0, 0, '2018-01-12', ''),
(70, 84, 'abcd@gmail.com', 'abcd@gmail.com', 'abcd@gmail.com', 'abcd@gmail.com', 1, 'f3ckpd4839ws088gcswcwc0sok8ocws', '$2y$13$f3ckpd4839ws088gcswcwOzyei5p3s.QKTUr00Oq20zRL4hft4Swi', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Hdjdj', NULL, '8764643464', 'Public', '17.37', '78.48', 'Hyderabad', 'Gajalakshmi', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'c95d8edc443cd646c8b421feaa4208cd', '2018-01-15', NULL, '', NULL, '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'gahsjajaj', 'KROS', NULL, NULL, 'Hospital & Health Care', 1, 0, 0, '2018-01-15', ''),
(71, 85, 'abcd@gmail.bjn', 'abcd@gmail.bjn', 'abcd@gmail.bjn', 'abcd@gmail.bjn', 1, 'jtgk2w62c00w88kks4swwo00k0g0scg', '$2y$13$jtgk2w62c00w88kks4swweI33PsHAhq0UeJwwYDyBuSLfY5Y4.Th2', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Fghjj', NULL, '585835335', 'Public', '22.5667', '88.3667', 'Kolkata', 'check', 0, NULL, NULL, NULL, NULL, NULL, NULL, '53db395f7a97c1334869310a9d42b578', '2018-01-15', NULL, '', NULL, '2018', NULL, NULL, NULL, NULL, NULL, NULL, 'fj in', 'KROS', NULL, NULL, 'Hospital & Health Care', 0, 0, 0, '2018-01-15', ''),
(74, NULL, 'ankit129@yopmail.com', 'ankit129@yopmail.com', 'ankit129@yopmail.com', 'ankit129@yopmail.com', 1, '2bzva7zlb15w888w804kgo4gowsw08w', '$2y$13$2bzva7zlb15w888w804kgeTMSwIptwJK0yg8Qjqiulhe2dvtVxvpW', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Ankit', NULL, '3366999', 'Public', '12.9667', '77.5667', 'Bangalore', 'A001', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'bf3a072e421daf4d055c7c51421baf1a', '2018-01-29', NULL, '', NULL, '2012', NULL, NULL, NULL, NULL, NULL, NULL, 'Pu', 'KROS', NULL, NULL, 'Arts and Crafts', 1, 2, 0, '2018-01-29', ''),
(75, NULL, 'ankit130@yopmail.com', 'ankit130@yopmail.com', 'ankit130@yopmail.com', 'ankit130@yopmail.com', 1, 'f8nlh4pn7pwsc4ksgo048cwcocoo4sw', '$2y$13$f8nlh4pn7pwsc4ksgo048OkiYJLA19TzO6rUFf9BnO0J54dKhdjBy', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Ankit', NULL, '64646494', 'Public', '17.3700', '78.4800', 'Hyderabad', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'a2df468810e80b801928a1f02ae68826', '2018-01-29', NULL, '', NULL, '2013', NULL, NULL, NULL, NULL, NULL, NULL, 'Pu', 'KROS', NULL, NULL, 'Arts and Crafts', 1, 0, 0, '2018-01-29', ''),
(76, NULL, 'testkabir@yopmail.com', 'testkabir@yopmail.com', 'testkabir@yopmail.com', 'testkabir@yopmail.com', 1, 'lznfidx2ig0kgk4gcoswko0wsk0c84w', '$2y$13$lznfidx2ig0kgk4gcoswkeDoVzNKu5YF9b0CFU9U/DvsQJcbIOnuK', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'test', NULL, '12121212', 'Public', '12', '12', '12', '1212', 0, NULL, NULL, NULL, NULL, '12', '12', '6531993dae51be2f7bd80d8d5fc7c4e5', '1990-02-03', NULL, '1212', NULL, '123', NULL, NULL, NULL, NULL, NULL, NULL, '12', '121', NULL, NULL, '12', 0, 0, 0, '2018-01-29', ''),
(77, NULL, 'testkabir1@yopmail.com', 'testkabir1@yopmail.com', 'testkabir1@yopmail.com', 'testkabir1@yopmail.com', 1, 'hwlazcbact4c8k000w84ww4c004o4w0', '$2y$13$hwlazcbact4c8k000w84wubOMTbZmOMk9ce3p9Sco3E54zAMuHF7a', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'test', NULL, '12121212', 'Public', '12', '12', '12', '1212', 0, NULL, NULL, NULL, NULL, '12', '12', '91ea275ffbf5f0fe7d5ebd263b378da6', '1990-02-03', NULL, '1212', NULL, '123', NULL, NULL, NULL, NULL, NULL, NULL, '12', '121', NULL, NULL, '12', 0, 0, 0, '2018-01-29', ''),
(78, NULL, 'testkabir11@yopmail.com', 'testkabir11@yopmail.com', 'testkabir11@yopmail.com', 'testkabir11@yopmail.com', 1, '8i3sbxdcapcsgs0wo08soo4kksc4cww', '$2y$13$8i3sbxdcapcsgs0wo08soeX4cD1VL5BDV.TWNWe5Bi9U3GrpmZxeW', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'test', NULL, '12121212', 'Public', '12', '12', '12', '1212', 0, NULL, NULL, NULL, NULL, '12', '12', '3a83d856c5ef798810c92c4529e06b9b', '1990-02-03', NULL, '1212', NULL, '123', NULL, NULL, NULL, NULL, NULL, NULL, '12', '121', NULL, NULL, '12', 0, 0, 0, '2018-01-29', ''),
(79, NULL, 'testkabir13@yopmail.com', 'testkabir13@yopmail.com', 'testkabir13@yopmail.com', 'testkabir13@yopmail.com', 1, 'lu6r1h028y8c40o4gk4gwcsogwccgsk', '$2y$13$lu6r1h028y8c40o4gk4gwOBTDYXwau05BmxNX431RvvPTDpFMzQ0m', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'test', NULL, '12121212', 'Public', '12', '12', '12', '1212', 1, NULL, NULL, NULL, NULL, '12', '12', 'e6f58bf9c547069c8c854b2b47882073', '1990-02-03', NULL, '1212', NULL, '123', NULL, NULL, NULL, NULL, NULL, NULL, '12', '121', NULL, NULL, '12', 0, 0, 0, '2018-01-29', ''),
(80, NULL, 'ankit131@yopmail.com', 'ankit131@yopmail.com', 'ankit131@yopmail.com', 'ankit131@yopmail.com', 1, 'rltlmzg2spw404cg80s8k0o8sgsogc8', '$2y$13$rltlmzg2spw404cg80s8kuQHJ/hqx2YSzLatkyNsdXFosCiHi6Ycm', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Ankit', NULL, '46649494', 'Public', '13.0827', '80.2707', 'Chennai', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, '4de459289b9f7e37276f6a019a82b483', '2018-01-29', NULL, '', NULL, '2017', NULL, NULL, NULL, NULL, NULL, NULL, 'Pu', 'KROS', NULL, NULL, 'Automotive', 1, 2, 0, '2018-01-29', ''),
(81, NULL, 'testkabir15@yopmail.com', 'testkabir15@yopmail.com', 'testkabir15@yopmail.com', 'testkabir15@yopmail.com', 1, '5yvwc8c2zy0wkk4co4s8o8o804wc8ks', '$2y$13$5yvwc8c2zy0wkk4co4s8ougZTqLdzMZ9AiNVpkebyT3n2gIv1Jsz6', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'test', NULL, '12121212', 'Public', '12', '12', '12', '1212', 0, NULL, NULL, NULL, NULL, '12', '12', '74704a6a1b4b8be4fbfb6cbb0c19f3eb', '1990-02-03', NULL, '1212', NULL, '123', NULL, NULL, NULL, NULL, NULL, NULL, '12', '121', NULL, NULL, '12', 0, 0, 0, '2018-01-29', ''),
(82, NULL, 'testkabir16@yopmail.com', 'testkabir16@yopmail.com', 'testkabir16@yopmail.com', 'testkabir16@yopmail.com', 1, 'bznbdbv0ensws4gcswsc4so8g44gswg', '$2y$13$bznbdbv0ensws4gcswsc4epDhN/La1quS6DrbXuZ2LlQlNDmuoPgq', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'test', NULL, '12121212', 'Public', '12', '12', '12', '1212', 0, NULL, NULL, NULL, NULL, '12', '12', '108ba62fadba8facf6a548a6ca7da9c9', '1990-02-03', NULL, '1212', NULL, '123', NULL, NULL, NULL, NULL, NULL, NULL, '12', '121', NULL, NULL, '12', 0, 0, 0, '2018-01-29', ''),
(83, NULL, 'testkabir17@yopmail.com', 'testkabir17@yopmail.com', 'testkabir17@yopmail.com', 'testkabir17@yopmail.com', 1, 'm4nriu3310gkg4ksgcwk0kowkksw88o', '$2y$13$m4nriu3310gkg4ksgcwk0eHA20BT14nEr37Qx0Sgv84/8r4rUip2G', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'test', NULL, '12121212', 'Public', '12', '12', '12', '1212', 0, NULL, NULL, NULL, NULL, '12', '12', '9ee273bd2a73bc4ac585d10a2abcda43', '1990-02-03', NULL, '1212', NULL, '123', NULL, NULL, NULL, NULL, NULL, NULL, '12', '121', NULL, NULL, '12', 0, 0, 0, '2018-01-29', ''),
(84, NULL, 'karanhooda15@gmail.com', 'karanhooda15@gmail.com', 'karanhooda15@gmail.com', 'karanhooda15@gmail.com', 1, 'l3lhpeert9sso8gogswwcg0ocwc088c', '$2y$13$l3lhpeert9sso8gogswwce1Pw/RR1MWXf6OqlNlDsykX73M1x/SW.', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Karan Hooda', NULL, '9915024395', 'Public', '30.75', '76.78', 'Chandigarh', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, '7f16d6fc1c2af73c48ddf4e4bd0e3d07', '2018-01-30', NULL, '', NULL, '2007', NULL, NULL, NULL, NULL, NULL, NULL, 'chitkara ', 'KROS', NULL, NULL, 'Education Management', 1, 1, 0, '2018-01-30', ''),
(86, NULL, 'ankit138@yopmail.com', 'ankit138@yopmail.com', 'ankit138@yopmail.com', 'ankit138@yopmail.com', 1, '4gr7soicjlwkcc8wsosog0scco88c8c', '$2y$13$4gr7soicjlwkcc8wsosoguljRolMePTz28TS9fJwl7KWzx58MOwWK', '2018-01-30 12:41:39', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Ankit', NULL, '4664644', 'Public', '13.0827', '80.2707', 'Chennai', '', 1, NULL, NULL, NULL, NULL, 'c1yBWw-zmMs:APA91bGCVW1qBAQppKa-UdtL6aZBe_50Enwn1Fz8TTFN1OS931Sn9TLvc8ag9dB0brF0TZzVcAaOfsYw5xwthzQ898wLQbaFA1kDLbIKzukfMhNKDP6ZUDIXIm1uTAk8vebbKoHlf0Rp', NULL, 'da4e06be871969bd45fd35879ff703a5', '2018-01-30', NULL, '', NULL, '2011', NULL, NULL, NULL, NULL, NULL, NULL, 'Pu', 'KROS', NULL, NULL, 'Automotive', 1, 2, 0, '2018-01-30', ''),
(87, NULL, 'karan.hooda@futured.in', 'karan.hooda@futured.in', 'karan.hooda@futured.in', 'karan.hooda@futured.in', 1, '7acyu3t5o680ocwws4ggw0wgow4s440', '$2y$13$7acyu3t5o680ocwws4ggwuxBs43ULTALVSS7c1/QmqI13FiuMr0Vy', '2018-03-07 14:29:14', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Karan Hooda', NULL, '9915024395', 'Public', '30.75', '76.78', 'Chandigarh', '', 1, NULL, NULL, NULL, NULL, 'fGekJqibjOA:APA91bHb3oPjEh3ZdSjQWJNXZayHtFPjkvV8roxTMIGH_QEDw4HdAl7jlbHWy36_TeWfp_l_V5WBrOwUXzi_8eG6qXcsKngbAiEpdjKlU6itl7ctwMfG_RG_sxriRwBdbiisjJu7fxGq', NULL, '5a43119fed0ae163733d7717e10fc587', '2018-01-30', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'chitkara ', 'Teacher', NULL, NULL, 'Education Management', 1, 0, 0, '2018-01-30', ''),
(88, NULL, 'ankit148@yopmail.com', 'ankit148@yopmail.com', 'ankit148@yopmail.com', 'ankit148@yopmail.com', 1, 'gc0xbcgj5m0o4s84sow0wssoks8sgck', '$2y$13$gc0xbcgj5m0o4s84sow0wePq1lmJgnbW4Tu7ry0hD2qHiTFmtlSj6', '2018-01-30 15:39:00', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Ankit', NULL, '79949464', 'Public', '17.3700', '78.4800', 'Hyderabad', '', 1, NULL, NULL, NULL, NULL, 'e85-USE-y8w:APA91bGiGBJfwOwIdnxmbJP3MiwFkncIT6b0oH5x6ntahVK6MuGAl6NV71Xtl-D_hJpmfi5S1yTgzIrBkwMCEgOOUTJJf0EtMPtmVhpMhjCuAX0P7fn7SIwWx_S8u_qe0aftGu0WPFS4', NULL, 'd8b3dc63e197273034391e33dc14577c', '2018-01-30', NULL, '', NULL, '2011', NULL, NULL, NULL, NULL, NULL, NULL, 'Pu', 'KROS', NULL, NULL, 'Automotive', 1, 0, 0, '2018-01-30', ''),
(90, NULL, 'ankit141@yopmail.com', 'ankit141@yopmail.com', 'ankit141@yopmail.com', 'ankit141@yopmail.com', 1, '5p9mlajld0cg0w48c4occk4gos084ww', '$2y$13$5p9mlajld0cg0w48c4occeCUQKnHMJa61r1HLGZhS9k/UtgHoUhb6', '2018-02-02 15:23:39', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Ankit', NULL, '97676', 'Public', '12.9715987', '77.5945627', 'abcdef', '', 1, NULL, NULL, NULL, NULL, 'ctM3IhR6RsI:APA91bE0Tnkfiwdz53SuHq0DJI4FBZLE47MVbNFQ2f9C6mUFAyLhWoARdTW_JZUcqf8Urh-USl19BYJbQLhu9DBCHsdrXz93rcrKQiNcZ6ODgf5BmczfOtvjNuSRMAUz5aGvod7t4Ir1', NULL, '4741193554bbe055eac651c8ce0d1fff', '2018-01-31', NULL, '', '1974', '2011', NULL, 'Select', NULL, 'Private', 'Select', 'Select', 'Pu', 'KROS', NULL, NULL, 'Automotive', 1, 0, 0, '2018-01-31', ''),
(91, 118, 'ankit123@yopmail.com', 'ankit123@yopmail.com', 'ankit123@yopmail.com', 'ankit123@yopmail.com', 1, '2smbqc88gyec0ogw804g4wks8wkcwsg', '$2y$13$2smbqc88gyec0ogw804g4uKduggFQhTxLjRjyUcBrHBuxVUZSXo4a', '2018-02-06 15:27:19', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Rahul', NULL, '89684236', 'Private', NULL, NULL, 'Mumbai', '12fgh', 1, NULL, NULL, NULL, NULL, NULL, NULL, '571d5ca06c8762a13c1271e129ab282b', '2018-02-05', NULL, '12', '1974', '2019', '42works', 'Studying', '333464646', 'Private', 'Single', 'Yes', 'Pu', 'KROS', NULL, NULL, 'Architecture & Planning', 1, 3, 1, '2018-02-01', ''),
(92, 96, 'tango42charlie@gmail.com', 'tango42charlie@gmail.com', 'tango42charlie@gmail.com', 'tango42charlie@gmail.com', 1, 'facvkpcmdpcggkko8ow8s8okgg0co8o', '$2y$13$facvkpcmdpcggkko8ow8suwdVhC1suzkv.rF6JjfEB1j6mImq3iTi', '2018-02-03 17:01:14', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Tango charlie', NULL, '98156948536', 'Public', '0.0', '0.0', 'raman', '987', 1, NULL, NULL, NULL, NULL, NULL, 'a8c978437f50d6fc7d103770a5cabc5e2b9e6d5a87f4b5f1eed035b9003bcc7b', 'ac2dd0b305ae4bb49ddeb993720cfe54', '2018-02-01', NULL, '', '0', '0', 'NA', 'Working', '', '', 'Single', 'No', 'PU,    CTU,    Hpu, Univ1, Univ2, Univ3', 'Teacher', NULL, NULL, 'Investment Management', 1, 10, 1, '2018-02-01', ''),
(94, NULL, 'test@yopmail.com', 'test@yopmail.com', 'test@yopmail.com', 'test@yopmail.com', 1, '2qzxyhac178kowscc048os0okks8004', '$2y$13$2qzxyhac178kowscc048oeCEi6f2ksSU3uzhJ0cSrUm3Qherei69u', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Nishant', NULL, '948784884', 'Public', '13.0826802', '80.2707184', 'Chennai', '986', 0, NULL, NULL, NULL, NULL, NULL, NULL, '88977d41316fed35a53635c1a4011d42', '2018-02-01', NULL, '', NULL, '2014', NULL, NULL, NULL, NULL, NULL, NULL, 'Pu', 'Teacher', NULL, NULL, 'Automotive', 1, 2, 0, '2018-02-01', ''),
(95, NULL, 'test21@yopmail.com', 'test21@yopmail.com', 'test21@yopmail.com', 'test21@yopmail.com', 1, '6111tae3p4sg84sgk8c0w0wkwwcssos', '$2y$13$6111tae3p4sg84sgk8c0wuOfHffwWrvoT6g4ONoYUWgkige/JwJQG', '2018-02-01 12:21:57', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Test', NULL, '985461328', 'Public', '12.9667', '77.5667', 'Bangalore', '986', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'f2e44b5d6e321f2ac90329b91466f8a6', '2018-02-01', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'PU', 'Teacher', NULL, NULL, 'Architecture & Planning', 1, 4, 0, '2018-02-01', ''),
(96, NULL, 'hello123@yopmail.com', 'hello123@yopmail.com', 'hello123@yopmail.com', 'hello123@yopmail.com', 1, 'px70uduayqokw00wsw4040wkwkwg8wc', '$2y$13$px70uduayqokw00wsw404utOAh62XwFKqboE7JqR.g7hUJhtYAc0a', '2018-02-01 13:04:32', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Hello', NULL, '46644994', 'Public', '0.0', '0.0', 'Bangalore', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'fc74a706a6706bf4a8779d7d69b07318', '2018-02-01', NULL, '', NULL, '2012', 'Google', NULL, NULL, NULL, NULL, NULL, 'Pu', 'KROS', NULL, NULL, 'Architecture & Planning', 1, 6, 1, '2018-02-01', ''),
(97, NULL, 'hello122@yopmail.com', 'hello122@yopmail.com', 'hello122@yopmail.com', 'hello122@yopmail.com', 1, '6c8daeew2kw8w040s4gwsgwsoc8woss', '$2y$13$6c8daeew2kw8w040s4gwsew6rHpGy1Lr3fRVLMekEeztsVgTMPRT2', '2018-02-01 13:12:09', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Helloo', NULL, '13464949', 'Public', '17.3700', '78.4800', 'Hyderabad', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'cd7256ddd854ae0cc15ffedcc8cbc81a', '2018-02-01', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'Pu', 'Teacher', NULL, NULL, 'Arts and Crafts', 1, 0, 0, '2018-02-01', ''),
(98, NULL, 'rahul123@yopmail.com', 'rahul123@yopmail.com', 'rahul123@yopmail.com', 'rahul123@yopmail.com', 1, '7u6n6b3uqn0gk8cwkg0048ok00osss4', '$2y$13$7u6n6b3uqn0gk8cwkg004uYYmfNO6XOg.OCOHCQz8gUJq2kwMC4j2', '2018-02-03 11:37:26', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Rahul', NULL, '9894944', 'Public', '30.900965', '75.8572758', 'ludhiana', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, '4b7a72bcaef9687508bcadc197fed55f', '2018-02-01', NULL, '', NULL, '2014', NULL, NULL, NULL, NULL, NULL, NULL, 'Pu', 'KROS', NULL, NULL, 'Apparel & Fashion', 1, 4, 0, '2018-02-01', ''),
(99, NULL, 'rattanbir@gmail.com', 'rattanbir@gmail.com', 'rattanbir@gmail.com', 'rattanbir@gmail.com', 1, 'd5tx2am2d8g0wco48gc88k04csk8wck', '$2y$13$d5tx2am2d8g0wco48gc88eTPBSl.aORKxVq6gZhngnFPx20DjcLDS', '2018-02-01 16:26:45', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Lt Col Rattanbir Singh', NULL, '9873355715', 'Public', '30.7281466', '76.8129403', 'Chandigarh', '', 1, NULL, NULL, NULL, NULL, 'du9711se5Y0:APA91bGpcQ9zw0Sbei0vIbGkLSpnuKPYAk65rc75opTGDphzUAACHWN296tBWzEGX0q1hvWJRBuEjJYiXbzvo8m9Rwxuw8Wmhv-SuUI17N2-xcZZtP1VtXRiwC8zaoP8TXuOWudoR7k6', NULL, '9c4b4789dd2d5e9c0a81a5990406fdbe', '2018-02-01', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'GND University Amritsar ', 'Teacher', NULL, NULL, 'Education Management', 1, 0, 0, '2018-02-01', ''),
(100, NULL, 'test12ankit@yopmail.com', 'test12ankit@yopmail.com', 'test12ankit@yopmail.com', 'test12ankit@yopmail.com', 1, 'co2vyk7vcq8sowwss0kgww8s40gko4w', '$2y$13$co2vyk7vcq8sowwss0kgwu0gwtRpWBLUCFV0gQnr/DM/2uNIRR9be', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'test', NULL, '8968428607', 'Public', '43.9695148', '-99.9018131', 'sd', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'efd64f44d342908ef49cb4b033a873a9', '1987-03-02', NULL, '12', NULL, '2014', '42works', NULL, NULL, NULL, NULL, NULL, 'sd', 'KROS', NULL, NULL, 'sd', 1, 0, 0, '2018-02-02', ''),
(103, 108, 'thesimplehorizon@gmail.com', 'thesimplehorizon@gmail.com', 'thesimplehorizon@gmail.com', 'thesimplehorizon@gmail.com', 1, '4iq2x8ce210k8o8wkkg0sw0kcwgckkg', '$2y$13$4iq2x8ce210k8o8wkkg0suLMF5XIwnJ059GFdQkJVox65czhJchhu', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Ankitt Kamboj', NULL, '', 'Public', '20.593', '78.9629', NULL, '', 0, '1744560858917123', 'EAABxZBxMfy3UBAGpPQdqwt7k0TteHpGDIfkT8YZBti49yZBpG0sZBchvS7yubfxcsbMYobcYb3yYKx7Idfqm4sjZBk9pBOvO5helnp2dpZA0JxWZCQxWKfAvzZApQgZAXqI6WOZAXuG6ZBm30vrhuZC3DjlnBxXF1Y5DYGHunnuh4FnCAbH8peXg66CYLBDYpUwyAl8ZD', '', '', NULL, NULL, 'aa3ab5baee57baaed7bf7073560e5472', '2018-02-02', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', 1, 0, 0, '2018-02-02', ''),
(104, 109, 'test42@yopmail.com', 'test42@yopmail.com', 'test42@yopmail.com', 'test42@yopmail.com', 1, 's82t5zpfqv44844ow8k4koowc8wc8ss', '$2y$13$s82t5zpfqv44844ow8k4ker8NUinR1YxPhAuWhU9luERyVSYaCsY2', '2018-02-02 13:06:49', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Nishant', NULL, '875421369', 'Public', '-32.8211646', '-61.3953734', 'Cañada de Gómez, Santa Fe Province, Argentina', '987', 1, NULL, NULL, NULL, NULL, 'ewTskhpayIY:APA91bHNw2_2opjltU3mnBQ_UnLAC_rYZ5diJL1EJyEK8V0Acn9WHscCwmbpJgjPgTu2iuI8Mu1psXALlm7ab2V-Hhjuo7YT9cHzZttZXMoiY4TC2IDCvWE4vYcilPrILKyVougcDq8u', NULL, 'fe763e9d2f0d413bc7f5ae37d970d216', '2018-02-02', NULL, '', '1974', '2018', 'NA', 'Working', NULL, 'Private', 'Single', 'Yes', 'PU', 'KROS', NULL, NULL, 'Hospital & Health Care', 1, 0, 0, '2018-02-02', ''),
(105, 110, '42workstab@gmail.com', '42workstab@gmail.com', '42workstab@gmail.com', '42workstab@gmail.com', 1, '2j8n0qm8lz8k8ggk04cw0sc0o08oogs', '$2y$13$2j8n0qm8lz8k8ggk04cw0edHESnDFFuxOQw6TcEgVPuH5yS2PVDfK', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Mamta K', NULL, '', 'Public', '20.593', '78.9629', NULL, '', 0, '460016554400982', 'EAABxZBxMfy3UBAHZB5FvwXm0ZBZAmNwUjGFYqPnzrEu8uGgamtjr9KjZALO4b8pEvv24o53KSkidySN9ZCZB6vn2Kp5j0HwRsWOJBkLopn19ZCKK6nBompqH6Gy64wkvHRZBCrwI1j0BnRS35JC8T6yZAuZBNlwNvNgFMeO9oylKA2djUzE5quSJhF6KtQjxr9BzZCRvOGijHLM2WrZAUt8KcE3dJ7jSlnCKk5GthZABBZA2PdkVAZDZD', '', '', NULL, NULL, 'b9d1f2411603e9ac228b710d0ffa4831', '2018-02-02', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', 1, 0, 0, '2018-02-02', ''),
(106, NULL, 'raman@42works.net', 'raman@42works.net', 'raman@42works.net', 'raman@42works.net', 1, 'm3xnnn81ey8scsc0k8404gssc4w840w', '$2y$13$m3xnnn81ey8scsc0k8404eIhXstRA2900Upy9SGbsHhQGfbMMbbGy', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'test', NULL, '', 'Public', '20.593', '78.9629', NULL, '', 0, '', '', 'Gt2n76Rth1', 'AQVVtCx4NG7HSSJ34f7qcRGBSvYmlI0BKFVvjB2W3Wm_tVyscUfrRDhYXC_jK4uKpha9PsOiZ9NFJi9Z7HkRwxYKPBHWrTxrKvuXFF-eKufu9ix5w31Nq_jdhf-63cAsnZ8NdnjZHHWYOisFBZuJ64occGLMg_79jDeo_dYaQMs184syaDvM-evZFA_t5yq8ypi2UQ2bFS9SapJUp_p7QwtmjLasZSLfKlvZEtye_jI7alFXbozosS2XzTbxUxHpe1Kdvoz3TSOACriAB8WRHK08_oBbubZqkQal_Kr8Cn3sIGV5LJRp8aBJOaQ9icmhosVfUWv94QnVF0SIygioc3XS8tGz5w', NULL, NULL, '7e7b5b63c67cb6e7f1dd122ea2c129ad', '2018-02-02', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', 1, 0, 0, '2018-02-02', ''),
(107, NULL, 'test12raman@yopmail.com', 'test12raman@yopmail.com', 'test12raman@yopmail.com', 'test12raman@yopmail.com', 1, 'qbwkwwhe16o0owwcw848wg4wwoww08k', '$2y$13$qbwkwwhe16o0owwcw848wevaq7SvMMUeOfUdjDXC5Z0Kwc/3R2NAS', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'test', NULL, '12121212', 'Private', '20.593', '78.9629', NULL, '', 0, NULL, NULL, NULL, NULL, '12', '12', '89fb701b9d9c397b92a07e03d65bdcd5', '1987-02-02', NULL, '12', '2008', '2004', NULL, 'Working', '12121212', 'Public', 'Married', NULL, '', 'KROS', NULL, NULL, '', 0, 0, 0, '2018-02-02', ''),
(108, NULL, 'test12raman12@yopmail.com', 'test12raman12@yopmail.com', 'test12raman12@yopmail.com', 'test12raman12@yopmail.com', 1, '3094d82yjpkwwwg8ks4o0sss4gw0kck', '$2y$13$3094d82yjpkwwwg8ks4o0eF3Vyu3xZffL.Soz.Fne3DYWcQlgGXKC', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'test', NULL, '12121212', 'Private', '20.593', '78.9629', NULL, '', 0, NULL, NULL, NULL, NULL, '12', '12', '620a6e02b2475c03f57dd375010f9d8c', '1987-02-02', NULL, '12', '2008', '2004', NULL, 'Working', '12121212', 'Public', 'Married', NULL, '', 'KROS', NULL, NULL, '', 0, 0, 0, '2018-02-02', ''),
(109, NULL, 'ankit151@yopmail.com', 'ankit151@yopmail.com', 'ankit151@yopmail.com', 'ankit151@yopmail.com', 1, 'd9wknzj97p4w00cg0c88ks8sc4k0gwk', '$2y$13$d9wknzj97p4w00cg0c88kef77JbPEJJE6WxqnI44SacPMF/q.wW7.', '2018-02-02 17:30:16', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Rahull', NULL, '79979797', 'Public', '28.6139', '77.2090', 'New Delhi', 'A001', 1, NULL, NULL, NULL, NULL, NULL, NULL, '9f707d4fd51c74026410c1312b1c59dd', '2018-02-02', NULL, '', '', '2017', 'Google', NULL, NULL, NULL, NULL, NULL, 'Pu', 'KROS', NULL, NULL, 'Architecture & Planning', 1, 2, 0, '2018-02-02', ''),
(110, NULL, 'mohit161@yopmail.com', 'mohit161@yopmail.com', 'mohit161@yopmail.com', 'mohit161@yopmail.com', 1, '91pbkjw4ytc0coc888o8ockkgg480c0', '$2y$13$91pbkjw4ytc0coc888o8oOo8sXqdsPuQRxOOiTx/SC0UuJ2xGCiN2', '2018-02-02 17:40:26', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'MohitUser', NULL, '49979794', 'Public', '13.0827', '80.2707', 'Chennai', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, '780393ff9dbaa2faccf058ad8fb21b9e', '2018-02-02', NULL, '', '', '2016', 'Google', NULL, NULL, NULL, NULL, NULL, 'Pu', 'Teacher', NULL, NULL, 'Alternative Medicine', 1, 4, 0, '2018-02-02', ''),
(111, NULL, 'test12raman123@yopmail.com', 'test12raman123@yopmail.com', 'test12raman123@yopmail.com', 'test12raman123@yopmail.com', 1, '8tplq3gpbcgscokk0o84o0ooccgoc4w', '$2y$13$8tplq3gpbcgscokk0o84ouWN4hqii.GeHSLRe2bSHtqbuUv0ZY/Lu', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'test', NULL, '12121212', 'Private', '20.593', '78.9629', NULL, '', 0, NULL, NULL, NULL, NULL, '12', '12', 'dcda0f5b8cd55cbf47cea50c95985fb4', '1987-02-02', NULL, '12', '2008', '2004', NULL, 'Working', '12121212', 'Public', 'Married', 'studying', '', 'KROS', NULL, NULL, '', 0, 0, 0, '2018-02-02', ''),
(112, NULL, 'mohit167@yopmail.com', 'mohit167@yopmail.com', 'mohit167@yopmail.com', 'mohit167@yopmail.com', 1, 'sr6r3d1hakgwoskwc8o408sgk040880', '$2y$13$sr6r3d1hakgwoskwc8o40uy2/dH1tRBoqHlpOSNB/VylgYiLxqrmK', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Mohit kumar', NULL, '333333', 'Public', '12.9667', '77.5667', 'Bangalore', 'A001', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'def36255b8930fa3b949ada19c9c5778', '2018-02-02', NULL, '', '2016', '2016', 'Google', 'Working', '111111', 'Private', 'Married', 'Yes', 'Pu', 'Teacher', NULL, NULL, 'Alternative Medicine', 0, 0, 0, '2018-02-02', ''),
(113, NULL, 'mohit168@yopmail.com', 'mohit168@yopmail.com', 'mohit168@yopmail.com', 'mohit168@yopmail.com', 1, 'odo8qcnzqo04swgg0og0wkogk0wcgg0', '$2y$13$odo8qcnzqo04swgg0og0weia4Xd2UbTJhdXnCYf957NBW7ABTAs1y', '2018-02-02 18:25:27', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Mohit Sharma', NULL, '111111', 'Public', '19.0759837', '72.8776559', 'Mumbai', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, '8e3bdc1be2887057d4c70b96c7cc88f1', '2018-02-02', NULL, '', '2004', '2008', 'Gmail inc.', 'Working', '222222', 'Private', 'Married', 'No', 'Pu', 'Teacher', NULL, NULL, 'Animation', 1, 3, 1, '2018-02-02', ''),
(114, NULL, 'ssdf@dfgfd.fghdf', 'ssdf@dfgfd.fghdf', 'ssdf@dfgfd.fghdf', 'ssdf@dfgfd.fghdf', 1, '8zgd81k8v484gso0gc4k0wogw8sc84w', '$2y$13$8zgd81k8v484gso0gc4k0uSa6/ffr.KMvwtbnIcDBSIG5RAPC5Ya2', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'gdfgsdfg', NULL, '23423324', 'Public', '13.0827', '80.2707', 'Chennai', '3243243', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'b83f3f6b4873e1a6c18f4512c9c85e02', '2018-02-02', NULL, '', 'Year of Joining: 2025', 'Year of Leaving: 2025', '324234324', 'Working', '32432432432', 'Private', 'Single', 'Children studying in St.Kabir: Yes', 'B.B.', 'Teacher', NULL, NULL, 'Hospital & Health Care', 0, 0, 0, '2018-02-02', ''),
(115, NULL, 'mohit170@yopmail.com', 'mohit170@yopmail.com', 'mohit170@yopmail.com', 'mohit170@yopmail.com', 1, '4n6v8b2e7j8kg4g4wcg44s48wc4gwkg', '$2y$13$4n6v8b2e7j8kg4g4wcg44emalu6qOwXWv9o83kPLCifBFfdmQA/wq', '2018-02-02 18:42:42', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'MohitSaldi', NULL, '111111', 'Public', '19.0759837', '72.8776559', 'Mumbai', 'B001', 1, NULL, NULL, NULL, NULL, NULL, NULL, '394c5470c2363ba5c0f7bd6caff1c025', '2018-02-02', NULL, '', '2000', '2019', 'Google inc.', 'Select', '333333', 'Private', 'Single', 'No', 'Pu', 'Teacher', NULL, NULL, 'Apparel & Fashion', 1, 4, 0, '2018-02-02', ''),
(116, NULL, 'mohit171@yopmail.com', 'mohit171@yopmail.com', 'mohit171@yopmail.com', 'mohit171@yopmail.com', 1, '46gg4ii3c4sgw0o880wscwcckcw8ogg', '$2y$13$46gg4ii3c4sgw0o880wscuL1mtKXwYuWcevGCRVeGNem4.MxPKpZi', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'mohitKumar', NULL, '23223', 'Private', '20.593', '78.9629', NULL, '', 0, NULL, NULL, NULL, NULL, NULL, NULL, '4ae501fd1598155d384233f553b966f5', '2018-02-02', NULL, '', '2014', '2019', NULL, 'Working', '333333', 'Public', 'Single', 'Yes', '', '', NULL, NULL, '', 0, 0, 0, '2018-02-02', ''),
(117, NULL, 'mohit180@yopmail.com', 'mohit180@yopmail.com', 'mohit180@yopmail.com', 'mohit180@yopmail.com', 1, '2f39s1wuqjk080s8wo8ck4oog8o0gkk', '$2y$13$2f39s1wuqjk080s8wo8ckuZgH7PE3NWkuXQrVECeJoafLttfXUJFm', '2018-02-02 19:05:23', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Mohit', NULL, '33333', 'Public', '0.0', '0.0', 'Hyderabad', 'A001', 1, NULL, NULL, NULL, NULL, NULL, NULL, '1079551e9cc38f29da80dd85ab95e642', '2018-02-02', NULL, '', '2015', '2020', 'Gmail', 'Working', '111111', 'Private', 'Married', 'No', 'Pu', 'Teacher', NULL, NULL, 'Accounting', 1, 2, 1, '2018-02-02', ''),
(118, NULL, 'raman123@yopmail.com', 'raman123@yopmail.com', 'raman123@yopmail.com', 'raman123@yopmail.com', 1, 'n2o1y660nbk84ww4ss4gg0o40oswo4w', '$2y$13$n2o1y660nbk84ww4ss4gguFqo0kMO.JJ2WzKmA6TuVbEduo1KWHYm', '2018-02-03 13:13:27', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'RamanUser', NULL, '25252525', 'Public', '0.0', '0.0', 'Bangalore', 'A001', 1, NULL, NULL, NULL, NULL, NULL, NULL, '08d6335230c2943bb836be515f00ea3c', '2018-02-03', NULL, '', '2012', '2019', 'Google inc.', 'Studying', '', '', 'Married', 'Yes', 'Pu', 'Teacher', NULL, NULL, 'Architecture & Planning', 1, 4, 1, '2018-02-03', ''),
(119, NULL, 'abcd@gmail.com1', 'abcd@gmail.com1', 'abcd@gmail.com1', 'abcd@gmail.com1', 1, 'dcoxpjfhca0ook4cgsocokosg8gk0ok', '$2y$13$dcoxpjfhca0ook4cgsocoe.teU/b5QoDHxLInEPGHprcJ8T./.ceC', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Simran', NULL, 'w4234234', 'Private', '13.0826802', '80.2707184', 'Chennai', '32423423', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'a103dfb30336a697ba4a494fa107b57f', '2018-02-03', NULL, '', '1974', '1976', '34234324', 'Select', '324234234', 'Private', 'Select', 'Select', 'sfddsfasdf, Sdfasdfasdf, Sdfasdf', 'Teacher', NULL, NULL, 'Hospital & Health Care', 0, 0, 0, '2018-02-03', ''),
(120, NULL, 'test@test.com', 'test@test.com', 'test@test.com', 'test@test.com', 1, 'rtogqta45n480800so8oo4ookw04kkk', '$2y$13$rtogqta45n480800so8oouMwuMJMyKb5C4HvoxzQCr4RXxcwUNr9a', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Test', NULL, '254845', 'Public', '-32.8211646', '-61.3953734', 'Cañada de Gómez, Santa Fe Province, Argentina', '987', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'f96e86821de8cdf344471dfafb97ced6', '2018-02-03', NULL, '', '2025', '1974', 'NA', 'Studying', '8484545485', 'Public', 'Married', 'Select', 'pu', 'KROS', NULL, NULL, 'Hospital & Health Care', 0, 0, 0, '2018-02-03', ''),
(121, NULL, 'harsimranjit@yopmail.com', 'harsimranjit@yopmail.com', 'harsimranjit@yopmail.com', 'harsimranjit@yopmail.com', 1, 'd0oy1vg7ajso4ossogkosckgo408400', '$2y$13$d0oy1vg7ajso4ossogkosO17EkCC/eElwTuljn7BfBgwzGzH0EvxG', '2018-02-05 12:27:45', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Sim ran', NULL, '1234567890', 'Private', '18.975', '72.8258', 'Mumbai', 'wee', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'e03461c172410fe51e63c3355117a3ea', '2018-02-05', NULL, '', '2022', '', 'QWERTY', 'Working', '1245808', 'Public', 'Married', 'No', 'Seth', 'KROS', NULL, NULL, 'Accounting', 1, 0, 0, '2018-02-05', ''),
(122, NULL, 'vishal_kros@yahoo.com', 'vishal_kros@yahoo.com', 'vishal_kros@yahoo.com', 'vishal_kros@yahoo.com', 1, 'osm2ddscfnk448g48c0gow0wkoggk8c', '$2y$13$osm2ddscfnk448g48c0gouAHowgANDX/bulht22/dHzyFTjlCE8R2', '2018-02-05 17:19:55', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Vishal aggarwal', NULL, '9814690605', 'Private', '30.75', '76.78', 'Chandigarh', 'NA', 1, NULL, NULL, NULL, NULL, NULL, 'df35ca889cef5ea30e7f54d453ea3945470802f41b61e2fc50467da156b3c79e', 'b66d98b43a3a9b07bc83710dcd6ce1ef', '2018-02-05', NULL, '', '1983', '', 'NA', 'Working', '01722549605', 'Private', 'Married', 'Yes', 'NA', 'KROS', NULL, NULL, 'Law Practice', 1, 0, 0, '2018-02-05', ''),
(123, NULL, 'navdeep_kalsi@yahoo.con', 'navdeep_kalsi@yahoo.con', 'navdeep_kalsi@yahoo.con', 'navdeep_kalsi@yahoo.con', 1, 'hopk7owhgg00ccko488000kk4o0oow8', '$2y$13$hopk7owhgg00ccko48800uhh6sQ6t.1bPQ7PmxKCoIlvH3LCX7JNe', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Navdeep Singh', NULL, '9815000132', 'Public', '30.75', '76.78', 'Chandigarh', 'KA0132', 0, NULL, NULL, NULL, NULL, NULL, NULL, '494ea46cff316943c8b575a58de15a6e', '2018-03-05', NULL, '', '1988', '', 'an!mate studioz ', 'Working', '', '', 'Married', 'Yes', 'University of Technology Sydney', 'KROS', NULL, NULL, 'Information Technology and Services', 1, 0, 0, '2018-03-05', ''),
(124, 137, 'wadherareet@gmail.com', 'wadherareet@gmail.com', 'wadherareet@gmail.com', 'wadherareet@gmail.com', 1, 'syg9k4xlrbk8sgoo4ok4sgo80g0ko00', '$2y$13$syg9k4xlrbk8sgoo4ok4seqwbR6eMSzwIXuN.dX6xIGaAf3TNnKAa', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Reet', NULL, '', NULL, '20.593', '78.9629', NULL, '', 0, '548573222195312', '', NULL, NULL, NULL, NULL, '3ff85703885af3fa4156eb40f70e5912', '2018-03-13', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '', 1, 0, 0, '2018-03-13', '');

-- --------------------------------------------------------

--
-- Table structure for table `Fundraiser`
--

CREATE TABLE `Fundraiser` (
  `id` int(11) NOT NULL,
  `media_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `goal` bigint(20) NOT NULL,
  `edit_achieved_dummy` bigint(20) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `FundraiserComment`
--

CREATE TABLE `FundraiserComment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `fundraiser_id` int(11) DEFAULT NULL,
  `comment` longtext COLLATE utf8_unicode_ci NOT NULL,
  `taggedUsers` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `isDeleted` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `FundraiserLike`
--

CREATE TABLE `FundraiserLike` (
  `id` int(11) NOT NULL,
  `fundraiser_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `FundraiserPayment`
--

CREATE TABLE `FundraiserPayment` (
  `id` int(11) NOT NULL,
  `donar_id` int(11) DEFAULT NULL,
  `fundraiser_id` int(11) DEFAULT NULL,
  `amount` bigint(20) NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_metadata` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hash_tag`
--

CREATE TABLE `hash_tag` (
  `id` int(11) NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hash_tag`
--

INSERT INTO `hash_tag` (`id`, `tag`) VALUES
(10, '134'),
(15, 'A'),
(6, 'Ask'),
(16, 'Celebrate'),
(18, 'Celebration'),
(9, 'Cvhhhbbj'),
(3, 'Fundraising'),
(8, 'Gettogether'),
(7, 'Jobs'),
(19, 'KROS'),
(17, 'Meet'),
(5, 'Mentorship'),
(4, 'News'),
(1, 'Offer'),
(2, 'Startup');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `post_id`, `status`, `created_at`) VALUES
(1, 5, 2, 1, '2017-12-04 17:24:03'),
(4, 6, 4, 1, '2017-12-05 10:56:15'),
(6, 6, 2, 1, '2017-12-05 10:58:13'),
(7, 5, 4, 1, '2017-12-05 13:05:38'),
(8, 6, 14, 1, '2017-12-05 13:15:01'),
(9, 87, 30, 1, '2018-01-30 12:55:53'),
(10, 87, 29, 1, '2018-01-30 13:00:48'),
(11, 92, 28, 1, '2018-02-01 11:03:34'),
(12, 92, 30, 1, '2018-02-01 11:03:37'),
(14, 92, 32, 1, '2018-02-01 11:12:24'),
(15, 92, 33, 1, '2018-02-01 11:18:40'),
(16, 92, 23, 1, '2018-02-01 11:48:25'),
(17, 92, 25, 1, '2018-02-01 11:48:27'),
(18, 87, 33, 1, '2018-02-01 11:58:27'),
(19, 95, 33, 1, '2018-02-01 12:29:07'),
(20, 98, 38, 1, '2018-02-01 13:46:20'),
(21, 8, 35, 1, '2018-02-02 11:31:16'),
(22, 92, 35, 1, '2018-02-02 13:21:45'),
(23, 92, 41, 1, '2018-02-02 13:38:07'),
(24, 90, 42, 1, '2018-02-02 15:04:45'),
(25, 91, 43, 1, '2018-02-02 15:12:32'),
(27, 91, 42, 1, '2018-02-03 13:21:30'),
(28, 87, 44, 1, '2018-02-06 12:28:50'),
(29, 87, 53, 1, '2018-02-12 12:42:36');

-- --------------------------------------------------------

--
-- Table structure for table `MasterUsers`
--

CREATE TABLE `MasterUsers` (
  `first_name` varchar(60) DEFAULT NULL,
  `last_name` varchar(60) DEFAULT NULL,
  `father_name` varchar(60) DEFAULT NULL,
  `mother_name` varchar(60) DEFAULT NULL,
  `dob` varchar(30) DEFAULT NULL,
  `Year_of_joining` varchar(30) DEFAULT NULL,
  `Year_of_leaving` varchar(30) DEFAULT NULL,
  `class_of_leaving` varchar(30) DEFAULT NULL,
  `batch` varchar(50) DEFAULT NULL,
  `house` varchar(50) DEFAULT NULL,
  `marital_status` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `contact_number` varchar(50) DEFAULT NULL,
  `permanent_address` varchar(60) DEFAULT NULL,
  `current_location` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `university` varchar(50) DEFAULT NULL,
  `profession` varchar(50) DEFAULT NULL,
  `organization` varchar(50) DEFAULT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `engagement_avenues` varchar(50) DEFAULT NULL,
  `rating` varchar(50) DEFAULT NULL,
  `applied_for_membership_1yer` varchar(50) DEFAULT NULL,
  `special_achievments_in_school` varchar(50) DEFAULT NULL,
  `suggestions` varchar(50) DEFAULT NULL,
  `main_roles` varchar(50) DEFAULT NULL,
  `main_phone_number` varchar(50) DEFAULT NULL,
  `main_location` varchar(50) DEFAULT NULL,
  `main_fb_id` varchar(50) DEFAULT NULL,
  `main_birthday` varchar(50) DEFAULT NULL,
  `main_gender` varchar(50) DEFAULT NULL,
  `main_course` varchar(50) DEFAULT NULL,
  `main_batch` varchar(50) DEFAULT NULL,
  `main_company` varchar(50) DEFAULT NULL,
  `main_university` varchar(50) DEFAULT NULL,
  `main_designation` varchar(50) DEFAULT NULL,
  `main_city` varchar(50) DEFAULT NULL,
  `main_country` varchar(50) DEFAULT NULL,
  `main_industry` varchar(50) DEFAULT NULL,
  `main_curriculum` varchar(50) DEFAULT NULL,
  `Is_app_user` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MasterUsers`
--

INSERT INTO `MasterUsers` (`first_name`, `last_name`, `father_name`, `mother_name`, `dob`, `Year_of_joining`, `Year_of_leaving`, `class_of_leaving`, `batch`, `house`, `marital_status`, `email`, `contact_number`, `permanent_address`, `current_location`, `country`, `university`, `profession`, `organization`, `designation`, `engagement_avenues`, `rating`, `applied_for_membership_1yer`, `special_achievments_in_school`, `suggestions`, `main_roles`, `main_phone_number`, `main_location`, `main_fb_id`, `main_birthday`, `main_gender`, `main_course`, `main_batch`, `main_company`, `main_university`, `main_designation`, `main_city`, `main_country`, `main_industry`, `main_curriculum`, `Is_app_user`) VALUES
('Siddharth Mitra', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Virat Mohan', '', '', '', '', '2003', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ankit Duggal', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhay Chattopadhyay', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sonali  Saraogi', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanisha Chawla', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Neha Sharma', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditi Chadha', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saudamini Mattu', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siya Bhalla', '', '', '', '', '1990', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjun Laroia', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rishabh Kale', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sonam Singh', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Samar Kachwaha', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arpita Sinha', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mitali  wadhwa ', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kartik Mathur', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shrey Gupta ', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ujjwal Dhawan', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Harshit Sharma', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nupur Bhargava', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sumat Mittal', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Hiteshwar Kochhar', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ananya Dhall', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kartik Kalaan', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pallavi Pokhriyal ', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mayank Malhotra', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Geetika Sanon', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Keshav Anand', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Unmay Shroff', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pooja Bajaj', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sakshi Anand', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akanksha Kapoor', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shailika Garg', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikkita Anil Kumar', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhinav Kumar', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aaina Dutt', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sidharth  Savant', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rupankit Saha', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Adhirath  Bhatotia', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivam Khanna', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mehak Sehgal', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anavi Kajla', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sukhdeep  Saini', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Srayana Sanyal', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tushar Kapoor', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ruchika Sarma', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Meher Deva', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanvir Malik', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sahana Kakkar', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Paranjoy Roy Chaudhury', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjun Gaur', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Unnattee  Eusebius', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Agni Bose', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Avantika (Jhingon) Malhotra', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Isha Agarwal', '', '', '', '', '1995', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikhil  Ratti Kapoor ', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Raghav Paul', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Raghav khurana', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arushi Garg', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dhruv  Sureka ', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akash Mittal', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Raghav Agarwal', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Astaaq  Ahmed', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('dishaa siingh', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nayanika Sharma', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Deeksha  Bhatnagar ', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanvi  Ajmani', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vedansh Kumar', '', '', '', '', '1998', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jai Agarwal', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rohan Kumar', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikita Seth', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pragya Bhargava', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aakaar gandhi', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhi Chauhan', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jyotika Jain', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivalik  Chandan ', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Surina Kakkar ', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ananya Chandra', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Janhavi Hiranandani', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhimanyu Bhardwaj', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nidhi Yadav', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikhil  Chowdhry', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shriya Kunatharaju', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aakash Bajaj', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Samya Singh', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rhea  Kumar', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('shranya  gambhir ', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aman Venkateswaran', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pallavi Pathania', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikita Gour', '', '', '', '', '1999', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Utkarsh Pandey', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivangi Gadhok', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanmaya Jain', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Praharsh Johorey', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vivan Kumar', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bhavya Iyer', '', '', '', '', '1998', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Divij Sahijwani', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Priyanjali Mitra', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhilasha Sinha', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Samarthya  bhargava', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Diptangshu Bhattacharyya', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ayushi Kulsreshtha', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ayesha Arora', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Udit Bery', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kokila Mohini Beri', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ravi Kulsreshtha', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sai Kiranmayi Turlapati', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mrityunjay Singh Jamwal', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dhruv Kalaan', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddhant  Rai', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sushrut Prabhakar', '', '', '', '', '1996', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Savneet Kambo', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Adhish Chhabra', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kriti Bagga', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhinav Handa', '', '', '', '', '1999', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dhruv Bhatnagar', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Namit Joshi', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mathew Vadakkel', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vyushita Sahay', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Lauren Robinson', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Devang  Seth', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vedika  Haralalka ', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Audrika Rakshit', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prabhleen Kaur', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanvi Gupta', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishita Matharu', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Angad Warrik', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bhumika Chhabra', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ujan Dutts', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ipshita Bose', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanishtha Vaid', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vasundhara Sharma', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shrisha S Bhat', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishita Tewari ', '', '', '', '', '1999', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kshitij Gangwal', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kaartikeya  Pandey', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Natasha Singh ', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('sarthak mishra', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abheek Malhotra', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishwinder Bhatia', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tejal Shroff', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Swikriti Vaid', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Himanshu  Khanna', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Krittika Paul', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prerna Kapahi', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Niyatee Sharma', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dhruv  Gopinath ', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikhil Shah', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Samarth Hazari', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Varun  Singh Hooda', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pallavi Chadha', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sanjana  Raman ', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nishtha Singh', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('khushboo kapoor', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivam Bakshi', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sukirat Bakshi', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Amrita  Nanda', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Viren Bajaj', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanya Chatrath', '', '', '', '', '2003', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikita Robinson', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Disha Kapoor', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Navya Joshi', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shaurya  Chauhan', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tushar Tikoo', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Adira Andlay', '', '', '', '', '1999', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aadya Bhatia', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Karan Puri', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Navya Bhatnagar ', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jivjeet Singh', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Karishma  Batra', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikita  Manocha ', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Yash Deshpande ', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tarini Sundar', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mridumena  Kohli', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pallavi Doshi', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Gaurab Bose', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Namman  Chaudhry ', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shibani Dayal', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Niharika Bhatia', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pankhuri Jain', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Misha Rawal', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shreeya Sachdeva ', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhishek Chatrath', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Megha Devraj', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Samyak Jain', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shubham Bansal', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saachi  Verma ', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Michelle Alam Shah', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Parul  Sharma ', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nirati Nayak', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Natasha Ratti Kapoor', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Trisha  Krishnan', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishan Krishan', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Baani Anand', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shraddha Kaur', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Yashvi Dhal', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Athreya Varadarajan', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prannoy Ovid  Reuben ', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Harit Bajaj', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditi Aggarwal', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aradhna Mangla', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shelly Saksena', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Gauriesh Bindra', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aakshi Dutt', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Eishita  vasdev ', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vidisha Mehta', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ranojoy Basu', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sanaa Sharma', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kaveri Pathela', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Fiza Chopra', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Samarth  Tewari', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Isheeta Khanna', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kshitiz Rao', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanvi  Puri', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ipsita Mallick', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditya Rastogi ', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ankit Sawhney', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akshat Pradhan', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shahnaaz  Khan', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sharmi Palit', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rohan Anand', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Harshit Chadha', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Stuti  Kakar ', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Imrat  Singh', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Zahaan Qureshi', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prithvi Mahabaleshwara', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ramneek Suri', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Archit Agarwal', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kairavi Chahal', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Devika Chandra', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Suchita Malhotra', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vedica Bhasin', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aakriti Arora', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tuhin  Kalia ', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Harjit Nalwa', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nehal  Garg', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anuraag Majumdar', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Neel Adhiraj Kumar', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vikram Iyer', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nalin  Johry ', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjit  Shankar ', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shefali Kaul', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tithi  Sanyal', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kanishk Chaturvedi', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prateek Goel', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditya Malhotra', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditi Rajgarhia', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikita Kochhar', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sadhvi Vaish', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kabir Bhargava', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditi Sarao', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shreshtha Raizada', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arushi  Kanwar', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Amisha Agarwala', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shashwath MM', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Madhav Maharaj', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kritika Pant', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shreya Sikka ', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjun Nath', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `MasterUsers` (`first_name`, `last_name`, `father_name`, `mother_name`, `dob`, `Year_of_joining`, `Year_of_leaving`, `class_of_leaving`, `batch`, `house`, `marital_status`, `email`, `contact_number`, `permanent_address`, `current_location`, `country`, `university`, `profession`, `organization`, `designation`, `engagement_avenues`, `rating`, `applied_for_membership_1yer`, `special_achievments_in_school`, `suggestions`, `main_roles`, `main_phone_number`, `main_location`, `main_fb_id`, `main_birthday`, `main_gender`, `main_course`, `main_batch`, `main_company`, `main_university`, `main_designation`, `main_city`, `main_country`, `main_industry`, `main_curriculum`, `Is_app_user`) VALUES
('Sumer Gill', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Serena Alam Shah', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sumedha  Awasthy ', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pretika  Khanna', '', '', '', '', '2004', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vidit Jain', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Chiara Saldanha', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Devika Mallik', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shefali Sharma', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sukanya  Dutta Gupta', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kritika Khanijo', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jawahar Khetan', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tushar Krishnan', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sarojini Sapru', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Garv Bhatia', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Samaksh Gupta', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kanika Pathania', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Gauri Sahni', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Reva Kansal', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Eshan Jain', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kinkini Bhaduri', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mayank Chawla', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manjari Sahay', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Palak Katyal', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Neha  Taneja', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vasudha  Bassi', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sumant Srivastava', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Alshay Yadav ', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditi Iyer', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddharth Todi', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shreya Das Gupta ', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Chandni Arora', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Meghaa  Nayyar', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kanupriya Rungta', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('saara thakur', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Supriya Paul ', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Daksh Sethi', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saurabhi Phanasgaonkar', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Issh Kochhar', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vrinda Mathur', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('waivasvata manu', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Srishti Bhargava', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('PerinAnn Katrak', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tushya Iyer', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anshul Gupta', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Udaikaran  Thakur', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nihaar Kuthiala', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sarthak Tayla', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anushka Baruah', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Deepthi  Sai ', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Trishila Malhotra', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Devanshi Sachdeva', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanvi Kapur', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mahir Dittia', '', '', '', '', '1999', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivin  Khanna ', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Apooruv  Jhamb ', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('sudip guha', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rhea Maheshwari', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Raghav  Sehgal', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhishek Majumdar', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shradha Malik', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Alysha Banerji', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sunanda Sehgal', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Riddhima  Bhatnagar ', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anirudh Gupta', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('TCA Sharad Raghavan', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddhant Bery', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vir Singh Anand', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Naina  Narain', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vaibhav Goel', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sparsh Gupta', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishita Mathur', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Natasha Dhawan', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Noora Manchanda', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nitasha Kapahi', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anuj Sakhuja', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tara Sondhi', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shreya Nair', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Laksh Pal Singh', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Yusman Manchanda', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aastha Bhardwaj', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Radhika Chakraborty', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rohan  Bahri', '', '', '', '', '2005', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Triveni Wahi', '', '', '', '', '1999', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pallavi Dixit', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Amar Pandey', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Charu Sukheja', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nandita Singh', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vardaan Agarwal', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abheer  Bipin ', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ambika Singh ', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sonakshi Gandhi', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mayank Rungta', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanvi Midha', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Esha Seth', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sahil Mehta', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rahul jain', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saachi Kapoor', '', '', '', '', '2003', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Angad Malli', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Simrun Mehta', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mukul Kashyap', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arnav Adhikari', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ambika Malhotra', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nishchay Bahl', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shweta Chopra', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pooja Premkumar', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aayushman Sahuwala Goyal', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sahil Nair', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Deeptanshu Kapur', '', '', '', '', '2005', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('harkaran dhingra', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivali Malhotra', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Simran Nanda', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sambhabi Dutta', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Hiresh Choudhary', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Trishla  Chadha ', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vatsal Aggarwal', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prarthna  Clare', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tushar Dadhwal', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Zubair  Chopra', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abiali Shaikh', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rohan Sood', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Roshni Sindhwani', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Maahin Beri', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Deepankar Sharma', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arushi Sachdeva', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vikrant kumar', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishan Taneja ', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhimanyu Dubey', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Lavanya  Bhatnagar ', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('PAAVANI SINGH', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ashwati Balraj', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhinav Dawar', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vasudha Mittal', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Swarnabh Ghosh', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Swati Kalra', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Harman Singh', '', '', '', '', '1997', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishrat Kaur', '', '', '', '', '2004', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anushka Patara', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vaishnavi Rao', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sidhant Shori', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhishek  Marla ', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kushank Gulati', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('abhishek sharma ', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nehmat Kaur', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vignesh Gurumoorthy', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dhruv Arora', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shashank Arora', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Asit Dhingra', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Karan sharma', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shambhavi Thakur', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Harsh Yadav', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Adya  Rajkotia Luthra', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rhea Malvai', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pallavi Bhalla', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditya Mallik', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishita Sachdeva', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tulika Gupta', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ekta Arora', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nivedita Chanana', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Samiksha Dhawan', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ramnik Singh', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ankita Mukherji', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('ashish bhatnagar', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikhil Nanda', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ritika Singla', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vaani Chopra', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sana Talwar', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ritwik Sarkar', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Samira Bose ', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddharth  Grover', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhishek Yadav', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Asmira  Saxena ', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Augustus Varun Mithal', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Neha  Sharma ', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vipul  Gauba', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('chandini ahuja', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Faiza Mookerjee', '', '', '', '', '2005', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kartik Lal', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Archit Chenoy', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Apoorv Agarwal', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sriram Jaikumar', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akshat Trivedi', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prateek Sayal', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tahir Anand ', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saranbir  Singh', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Udaytaj Singh', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanvi Kankan', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rachit Gupta', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Lara Sinha', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anant  Mital', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tigmika Srivastava', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prakhar  Raja', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jaiveer dugal', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishan Kaicker', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Angad Rangar', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sarasija Subramanian', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Harnain  Sethi ', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kanchan  Puri', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Amy Singh', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Misha Singh', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tani Malhotra', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Priyanka  Atal', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjun Khurana', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Neha Jain', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ruchika  Singh', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Punya Luthra', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anindita Adhikari', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jai Mittal', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Srimoyee  Biswas', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rahul Gupta', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Avalok Langer', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arushi sur', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kritika  Oberoi', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sukriti Sharma', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('biplav agarwal', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Gayatri Sahgal', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Neetika Agarwal', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Carol  Singh', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bharat Roy ', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Amrita  Ghulati', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('jaideep chadha', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Yuv Bharatram', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishan Iyer', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('raghav mendiratta', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shafali Kashyap', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shambhavi Sawhney ', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mansi Jain ', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vasundhara  Singh', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sakshi Uberoi', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivraj Anand', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Karan Bhargava', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ankit Gujral', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Medhavi Parmar', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Amrita Khosla', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nitya Budhraja', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Charita Vig', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rohan Sehgal', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Isha Mital', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sunaina Sahni', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Stuti Gujral', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manvi Chopra', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sanjit Sarkar', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanya Singh', '', '', '', '', '2006', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tarini Unnikrishnan', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mahima Kumar Mahajan', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishaan Day', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Yuvraj Kumar Mahajan', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Swati Kankan', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sharika Parmar', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sameer Gujral', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Myra Khanna', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikhil Tandon', '', '', '', '', '2006', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manas Punhani', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saniya yadav ', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mukul Pasricha', '', '', '', '', '2005', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nidhi Kulkarni', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vasundhra  Jain', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nihar Kantipudi', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abeer Mithal', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Avirat Agarwal', '', '', '', '', '2003', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Chirag Ahluwalia', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `MasterUsers` (`first_name`, `last_name`, `father_name`, `mother_name`, `dob`, `Year_of_joining`, `Year_of_leaving`, `class_of_leaving`, `batch`, `house`, `marital_status`, `email`, `contact_number`, `permanent_address`, `current_location`, `country`, `university`, `profession`, `organization`, `designation`, `engagement_avenues`, `rating`, `applied_for_membership_1yer`, `special_achievments_in_school`, `suggestions`, `main_roles`, `main_phone_number`, `main_location`, `main_fb_id`, `main_birthday`, `main_gender`, `main_course`, `main_batch`, `main_company`, `main_university`, `main_designation`, `main_city`, `main_country`, `main_industry`, `main_curriculum`, `Is_app_user`) VALUES
('Saniya Agarwal', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aaditya Bugga', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sarthak  Malhotra', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mridul Chandgothia', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Riya  Chadha ', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Karun Singh', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ritvij Basant', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sumedha Vashistha', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Madhubanti  Bhattacharyya ', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Deeya Bajaj', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rathin Bector', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sehar Ansari', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shweta Poddar', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishan Abraham', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sohrab Bhujwala', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ojasvita Sawhney', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vrinda  Pareek ', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Karanvir Dayal', '', '', '', '', '2006', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saba Kapoor', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tavisha Parmar', '', '', '', '', '1997', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shashvat Somany', '', '', '', '', '2003', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dhruv Khurana', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sankalp Gupta', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Amiya Chopra', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kartavya Jain', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Chiring Phunchok', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mallika Manchanda', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhay Sen', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bhavna Sethi', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dona Bajaj', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('parull khanna', '', '', '', '', '2003', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anjanaa Sudhakar', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dona  Khurana', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kabir Bose', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanvi  Metre ', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Yasmin Hussain', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tej Brar', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bani Brar', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ahana bahl', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Meher  Mehta', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sukrit Dogra', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Azusa Banerjee', '', '', '', '', '2005', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kabir rikhy', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akshat Dwivedi', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anurati Tandon', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Raghu Sagar', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Karan Sunda', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Madhav Gujral', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mangala Dar', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ananyaa Mital', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sidhant Kalra', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivang Tayal', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kashneet Kaur', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Megha Mukherji ', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Radhika Venkataraman', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pratyush Pranav', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Naasha mehta', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Harveen  Rekhi ', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bani Nanda', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manyata Malhotra', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sibani Malhotra', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('radhika kapoor', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kartika Chaudhary', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Deeksha Kochhar', '', '', '', '', '2006', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Udayvir Rana', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dilawar Singh Gill', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dishant Narang', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tejas Rajkotia Luthra', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Chandni Aggarwal', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aparna Kapoor', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sahib singh  Kochhar ', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Agrim Singhal', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rohini Atal', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Netra Sundaresan', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bhavya Bishnoi', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aaryaman Shukla', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('nikita noronha', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prerna Mathur', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Surbhi Kapoor', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanya Popli', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aashim Usgonkar', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Soumya Puri Sudhir ', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vandana Mirchandani', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjun Sakhuja', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rohan Thadani', '', '', '', '', '2003', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mehak Sagar', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sahil Maheshwari', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Simran  Kochhar ', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('karan sood', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akshay Jugran', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Paritosh Gupta', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shikhar Dogra', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shavir Banaji', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditya Gurwara', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Veer Angad Berry', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Divya Kohli', '', '', '', '', '1988', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Divij Kapur', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manik Jhingon ', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pallavi Malviya Gupta', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjun Singh', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Varun Dua', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('hitesh bhatia', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ambika  Balraj', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kratu Beri', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjun Nair', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vedika Sharma', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sachit Galhotra', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tushar Prakash', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vishnu Chopra', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sunila Bhargava', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sugandha  Dhawan', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('vibhor mathur', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Reva  Dutta', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Maneesh Pamnani', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akhil  Chadha', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shreya Kapoor', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Apoorvi  Mittal', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Firdaus Kishwar', '', '', '', '', '2005', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikhil Sardana', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arvind  Gopal', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aalika Kohli', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Malaika Dhar-James', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivani Kachwaha', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Drishti Mahajan', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Paavani Khanna', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saniya  Srivastava', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('parul jain', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('DIYA PUDOTA', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Harjeev Singh Chawla', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manika Bhargava', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mohit Goel', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rajanya Bhandari', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Seher  Arora', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vaibhav Garg', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Harsh Malhotra', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vaneet Kaur Chhabra ', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Chaitanya  Gupta', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Natalie  Grover', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ira  Singh', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditi Prasad', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nicholas Law', '', '', '', '', '2006', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Priyanka Galav', '', '', '', '', '2004', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kamakshi  Bawa', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Simran Issar', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shevantika Nanda', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Madhulika  Chebrol ', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Devikaa Nanda', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aman Banerji', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kartik Kanaujia', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Juhi  Nanda', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Naved  Sarna', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikita Kapoor', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('SHREY KUMAR', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mitali Gupta ', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Guneesha Sethi', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Srishti Gyrwara', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddharth Vohra', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pallavi  Surana', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sanjana Kashyap', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jayesha Koushik', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('sehr chopra', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sameera  Sandhu', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ayesha Nageshwaran', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shruti Khurana (Kapoor)', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Neha Shanker Bahadur', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shantanu Roy-Chaudhury', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishita Bahadur', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sanat Sinha', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Yatin Nanda', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditya Jain', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mrinal Kanwar', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shantala  Palat', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Purti Wakankar', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manasi Adya', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Avanti Narayanan', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjun Lahiri', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Neha Priyadarshini', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anadya Gujral', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Amrit Chopra', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saniya Makker', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Apratim Kumar', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Devika  Pandey', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Puneet Sharma', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishaan Bhattacharya', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mayank Bhramar', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Karandeep Chadha', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manish Goyal', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shirish Baskaran', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tarini Nayak', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Priya mirchandani', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddharth  Nair ', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditya  Mehta', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditya Pandeu', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Savil Srivastava', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Varun Tandon', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aarti Thakur', '', '', '', '', '2005', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sakshi Asudani Tuli', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vikrant  Sanon ', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Raghuvir Khare', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shashvat Pandit', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Namrata Kumar', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Avika Sood', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ankush Gujral', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Divya Bhalla', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jai Dayal', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('vir vohra', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishaan Singh Bedi', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rudrajit Sabhaney', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ridhi Kale', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nitish Verma', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tarini Nirula', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aman Mitra', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sonali Thapar', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Karan Kohli', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dhruv Laroia', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sejal Goyal', '', '', '', '', '2004', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kunal Raj Bahadur', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akshita Kolluru', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Divya Iyer', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjun Rajgopal', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kavya Trehan', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vrinda  Kanvinde ', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditi sharma', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mohit Bhalla', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ira Jhangiani', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Raghav bhalla', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Esha Malhotra', '', '', '', '', '2005', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Malikah Mehra', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Riya  Sarin', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditya  Sarin', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sunaina Kathpalia', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tania Kohli', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shaurya Dimri', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Deepshekhar Gupta', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhishek Gupta', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sidharth Gajapati Raju', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jatin  Sindhu', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Angad  Arora ', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jayati sehgal', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Janhavi Navlakha ', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Lakshya Thukral', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manali Jain', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pooja Roy', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Amrita Siyan', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tarun sharma', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Unmisha Singh', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sahil Vasudeva', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arpan Arora', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Seher  Gopal', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Gavin Kochar', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('AMOGH BATRA', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mahima Varma', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rishika Duda', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shubhra Kumar', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivani Nirula', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sunandini Seth', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Paraj Saxena', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aman Roy', '', '', '', '', '2003', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prihana Bhasin', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `MasterUsers` (`first_name`, `last_name`, `father_name`, `mother_name`, `dob`, `Year_of_joining`, `Year_of_leaving`, `class_of_leaving`, `batch`, `house`, `marital_status`, `email`, `contact_number`, `permanent_address`, `current_location`, `country`, `university`, `profession`, `organization`, `designation`, `engagement_avenues`, `rating`, `applied_for_membership_1yer`, `special_achievments_in_school`, `suggestions`, `main_roles`, `main_phone_number`, `main_location`, `main_fb_id`, `main_birthday`, `main_gender`, `main_course`, `main_batch`, `main_company`, `main_university`, `main_designation`, `main_city`, `main_country`, `main_industry`, `main_curriculum`, `Is_app_user`) VALUES
('Aantika Tandon', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('bhavna garg', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aashica khanna', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Urvashi Bhatia', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishaan Rastogi', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mansi Sharma', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arushi Anand', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sonali Bhasin', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mihir Samir Verma', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Avani Venkateswaran', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shreya Guha Roy', '', '', '', '', '2003', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tarini Khurana', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sanjana  Rajagopal', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('sanjana batra', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhinav Dawar', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Devika Pathak', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Angad Singh', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Trisha Chattopadhyay', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jyotika Bindra', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sneha Cyriac', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shayoni Nair', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mansi Midha', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditya Varma', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Raahat Vikram Singh ', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Roshni Jain', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rohan Kapur', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vivek Raman', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('viveka singh', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Geetanjali Mukherjee ', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vinayak Kumar', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikhil  Chawla', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Madhuri Chowdhury', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('sudhanshu agarwal', '', '', '', '', '2004', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Samyek Jain', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anaqat  Kamboj ', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Hansa Sahu', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akshay Yadav', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('yamini mandava', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vibhooshitha Raghuraman', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Harsh Bhargava', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Divya Sihag', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Naghm Ghei', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anmol  Ahlawat', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ambika Sharma', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arunika Ganguly', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shreya Sharma', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ann Sandra George', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sharan Gill', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sahaj Chawla', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akshay Santhanam', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abeer Saha', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pranav Gour', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivangi Jain', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Yashasvi Vedula', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arushi Pandey', '', '', '', '', '1999', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sankarshan Talluri', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Satyam Bharadwaj', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Karan Arora', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sukrit Chhabra', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prannay Kohli', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vrinda Sharma', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Annanya Dwivedi', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rohan Dhariwal', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Geetika Pandya', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manvi Chaudhary', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sharang sasan ', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sukhmani  Bakshi ', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('SUKANYA SUNDER', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aashvi Bana', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shardul  Sasan', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhinav Prakash', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vani Kapoor', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Samyukta Santhakumar', '', '', '', '', '1999', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dhruv Hingorani ', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shirshendu Banerjee', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nausheen Khan', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishita Chaudhry', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kanan Kapoor', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Venayak Saran  Gupta', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('sanya mahajan', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sushmita  Arora', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aamena Ahmad', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Geetika Tandon', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjun Chopra', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nitish Puri', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddharth Sinha', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Naintara Singh', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shashvat Jaiswal', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Varun Agarwal', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mohnish  Bhatia', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Priyanka Paul', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Atishay Jand', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nachiket Bhatia', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akshit Kapoor', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Lubhaan kumar', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditya Rai', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aman Saxena', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dhruv Vatsa', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Divyam  Sood', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sanat  Bathla', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Armaan Singh', '', '', '', '', '1995', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prathum Saraf', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arnav Sur', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Reuben Sahae ', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shreya Ramachandran', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Cosmena  Mahapatra', '', '', '', '', '1988', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Addy Singh Arora', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vidur Hans', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Auroni Mookerjee', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manini Chadha', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjun yadav', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Roneeta Datta', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dhruv Chugh', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jayant Gaurav Chaturvedi', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aanavi Dewan', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aniesha Sangal', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Palak Sehgal', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('sanjana choudhari', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sonali Batra', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Radhika Singh', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Raghav Sood', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Noor Shergill', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vasu  Sharma', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kabeer Khurana', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Asees Chadha', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Lipakshi Kakar', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sejal Choudhari', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rashmi Kulkarni', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Namrata Mehta', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Hunar Khanna', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sahiba Rangar', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kopal Seth', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('RADHIKA KATHPALIA', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manasi  Kohli ', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Malika Singh', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sakshi Agarwal', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pranay Manchanda', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Reshma Seth', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Devika Thapar', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mallika Chandna', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhinav Pasricha', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Chandrika Chahar', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aanand Vardhan', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Yashveer Chaudhry', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ambika Verma', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ansh  Seth', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anavi Chaturvedi', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Hideo Daikoku', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akanksha Marwaha', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shruti - ', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjun Khanna', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Devki Pande', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kritank Chowdhry', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anirudh  Chakradhar', '', '', '', '', '2004', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nidhi Thapar', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shriya  Bishnoi ', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aayushi Srivastava', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ravish Rawal', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ananda Dhar-James', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Amiya Dev', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Devansh Goel', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aarzoo Bedi', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Udai Singh', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rohan Barua', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kushagra  Srivastava', '', '', '', '', '2004', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anushka  Arora', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abha Kukreja', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Priya  Sitaraman ', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditi Chandna', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mohit Karwasra', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shinara  Sunderlal ', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddharth Joshi ', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kunal Barua', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shlagha Sharma', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kanishk Thareja', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sulochan sareen', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vedaant Nag', '', '', '', '', '2003', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arvind Jamkhedkar', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Phalguni  Jain', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shreya  Sahay', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Neerav Shankar', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ankush Ohri', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vallika Verma', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bhavya Kulshreshtha', '', '', '', '', '1999', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('mitiksha  shahani ', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sayantan Banerjee', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nitya Bhutani', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Salil Parekh', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akanksha  Kumar', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kethaki Nair', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ravjeet singh', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Satyarth Grover', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jaskiran Warrik', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aaryaman Shukla', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Adhiti Gupta', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Avyakta Kapur', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Meetali Kutty', '', '', '', '', '2003', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Avantika Kolluru', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sampriti Dwivedy', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivang  Mehta', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Antaraa Vasudev', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sohraab Bawa', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Suma Balaram', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sahana  Narasimhan', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Eshan Rastogi', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ilena Bose', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nitin  Lal ', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shreya Kaicker', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Megha Bisaria', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Lavanya Arora', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikita Butalia', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Disha Sukhija', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Apoorva Shankar', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivangini Peshoria', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pragun Jindal ', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bharat Ahuja', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vidur Gupta', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Varun Anand', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Yasmeen  Busrai ', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kunal Sanwalka', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Disha Budhraja', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ameya  Somany', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sahana Kaul', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanya Jain', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Devaki Handa', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Suvid  Ajmera', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anuj Prakash', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vidush  Dwivedi', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rajika Seth', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sahir Kochhar', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pranav Data', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prithvi Bajaj', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vanshika  Choudhary', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Riya Sawhney', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddhartha Mukherjee', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishaan Joshi', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prerna Kapoor', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aashrey Kapoor', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aradhita  Gupta ', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sanskriti Swami', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prashat Passi', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Medha Kapur', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Umang Bansal', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saubhagya Raizada', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shreshta Venkataramanan', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Harish Sai', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vibhuti Gour', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prateek Sayal', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rohit bhasin', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ehsaas Mehta', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shuchi Prasad', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rahul Wahi', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pranay Nanda', '', '', '', '', '1991', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Joyita Ghose', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sahil Narain', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjun Abrol', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('AKANKSHA  MENON', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vedant Batra', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aparajita Kapoor', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manak  Singh ', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `MasterUsers` (`first_name`, `last_name`, `father_name`, `mother_name`, `dob`, `Year_of_joining`, `Year_of_leaving`, `class_of_leaving`, `batch`, `house`, `marital_status`, `email`, `contact_number`, `permanent_address`, `current_location`, `country`, `university`, `profession`, `organization`, `designation`, `engagement_avenues`, `rating`, `applied_for_membership_1yer`, `special_achievments_in_school`, `suggestions`, `main_roles`, `main_phone_number`, `main_location`, `main_fb_id`, `main_birthday`, `main_gender`, `main_course`, `main_batch`, `main_company`, `main_university`, `main_designation`, `main_city`, `main_country`, `main_industry`, `main_curriculum`, `Is_app_user`) VALUES
('Ria Bhargava', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shanin Koura', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('daven kanti Mohan', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sushant Chib', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Adit Sachdeva', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prabhav  Jain', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('sparsh  seth', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mohul Kumar', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shyam Srinivasan', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('shylla sawhney', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('aansh lohia', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shreya Gurwara', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prakriti Saranga Khandekar ', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vasant Chandra', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aashna Agarwal', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikhil Johry', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikhil Maheshwari', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mahim Bhagat', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ayush Khanna', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pratik  Bhagat ', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Atoshi Das', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikhil Ahuja', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prakriti Singh', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Khyati Saraf', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sumana Nukala', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Harmeet  bedi', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanvi Kapoor', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mallika Das', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Varun Vig', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddhartha Prasad', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rohan Dang', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanya Anand', '', '', '', '', '1989', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Satmeet  Bedi', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pratyush Pandey', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aniruddha Mukerji', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Damini Passi', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anusha Ghosh', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Fahd Haroon', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kartik Kumar', '', '', '', '', '2003', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishita Sehgal', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tushar Chetal', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddhant  Puri', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vidyun Sahni', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('sidharth  sharma', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Khushi Srivastava', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Zorawer  Singh', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivika  Sharman', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('vinod subramanian', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Archit Verma', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Misha Kohli', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kriti Chowdhary', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shubhi Kapil', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anirudh Balakrishnan', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Viraj Yadav', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prannoy Ovid  Reuben ', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Adityan Kayalakal', '', '', '', '', '2003', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('JASPRIYA BHASIN', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anurag Dayal', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sukhman  Dhillon', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bhavika Kochar', '', '', '', '', '2006', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saksham Bajaj', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditi Jha', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddhant Tandon', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikhil Sahu', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kirith  Dsouza ', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pallavi Pasricha', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhishek Ganguly', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akash Kumar', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditi Sabbarwal', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abdul Aziz Radhi', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anoushka  Kumar', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Adhiraj Sirohi', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mohit Rao', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Yashaswini Mittal', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saema Husain', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sushaen Vasisht', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Madhulika  Mohan', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Paras Chopra', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vinayak Dimri', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanya Saxena', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Amrita sarna', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishaan Khandpur', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jaspriya Rekhi', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Neha Gupta', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Priyanka Singh', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Adil Saxena', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Chetna Goyal', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sakshi Jawarani', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bhumika Gupta', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Harshit Arya', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Priyanka Batra', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanvir  Singh', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sabeera Chandok', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddharth Kaundinya', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Urvashi  Nandrajog ', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vansa Bali', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shaurya Verma', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivam Agarwal', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Astha Madan', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Gauri Sindhu', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Reuben Jacob', '', '', '', '', '1999', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Zubin Baisiwala', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vedanti Sikka', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Soumya Madan', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Udairaj Arora', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Raghav  Nanda', '', '', '', '', '2003', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kedar Satyanand', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pragya Gopinath', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ujjwal Sharma', '', '', '', '', '2013', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tarini  Gupta ', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shrivats Agarwal', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kanishk Vashisht', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anushka Saxena', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Divij Singh', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Savini Mehta', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rahul Dmello', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jaisal  Azad', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Alysha Berry', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jaikaran Singh', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishrat Arora', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dhruv Baijal', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Simran Bhatia', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akul Malhotra', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mehr Malhotra', '', '', '', '', '2003', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Gaurav Prakash', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Amar  Bhasin', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vihaan Moorthy', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanvi Chopra', '', '', '', '', '2014', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kanika Jain', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sarthak Gupta', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Muskaan Nagi', '', '', '', '', '2014', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('RAHUL D\'MELLO', '', '', '', '', '2014', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arush Awasthy', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Varuni Khanna', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ria  Gulati', '', '', '', '', '2014', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Meet Singh', '', '', '', '', '2014', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prithvir  Bhalla', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Adam Tobit', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manavi Dixit', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nishtha Shanti ', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anoushka Gupta', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Yash Budhraja', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Zubin Duggal', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhishek Roy', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Daksh Singh', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Urvi Dhar', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vritti  Sahdev', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vimanyu Awal', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Riya Sharma', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Gunbir Singh  Sekhon', '', '', '', '', '2010', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vidur Mithal', '', '', '', '', '2011', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vrishali Sinha', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ashima Seth', '', '', '', '', '2012', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sohail Sahib Monga', '', '', '', '', '2014', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ojaswani  Aneja', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhinav Aggarwal ', '', '', '', '', '2014', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aryaan Ishan', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ambika Rana', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Karunya Banerjee', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kanishk Vashisht', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rukmini Roy', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vaishnavi Pareek', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shaan Batra', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Srishtti Talwar', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sumair Bawa', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shauryya Pratap Mishra', '', '', '', '', '2011', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sumer Drall', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Jyotica Singh', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pranya  Uberoi', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Himanshi Bajaj', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akshitaa Tandon', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vinnie Nanda', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Amanat Mehta', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kairavi  Bhargav', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Iman Sengupta', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Medhjaa  Jamwal', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nainika Chadha', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nishtha Madan', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sathya Sahay', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sahisth Chhabra', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vandana Vijay', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Yachna Khanna', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Naghm  Ghei', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Gauri Jhangiani', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Gauri Sharma', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mehul Thukral', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Suma Bhat', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pooja Gujral', '', '', '', '', '2004', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arsh Mehta ', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanmay  Shanker', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditya Maru', '', '', '', '', '2005', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mansi Kankan', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ananya  Mittal', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Meenakshi  Nair', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anoushka Virk', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mahrukh  Chaudhry ', '', '', '', '', '2011', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Umer Gupta', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Maanya Dhar', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shiv Dhawan', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddharth  Ganguly', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anant Sidh', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Suryash  Chakravarty ', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanya Katyal', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rohin Singh', '', '', '', '', '2012', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sehr Chopra', '', '', '', '', '1999', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Devaki Sharma', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Maanvi Chowdhary', '', '', '', '', '2014', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('DHRUV Narayan', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditya Ranjan', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Laxmikant (Lk) Sharda', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aman Jain', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Srishti Bhatnagar', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prakrit  Gupta', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anisha Mukherji', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akshay  Bhatia', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manya  Sawhney', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Viveka Singh', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arushi Gupta ', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sanya Kapoor ', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aman Kapur', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Divyani Kohlu', '', '', '', '', '1996', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bandeep Singh', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vidushi  Gupta', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishan Singh', '', '', '', '', '2005', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shrey Rajgarhia', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Hardik  Puri ', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ameeq Singh', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mohnish Varma', '', '', '', '', '1993', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Srivats Ramaswamy', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vaibhav Nanda', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Palak  Bhasin', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anisha Singh', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Satyaki Yadav', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anirudh Vijay', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Devang  Seth', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nakul  Ranjan', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(' Nishant Dhanendra', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Neharika Chandra', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanay Sharma', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tushar Kalyan', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Himanshi Tripathi', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Suktika Banerjee', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Desna Sharma', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prithvi Mahabaleshwara', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vishrut Chatrath', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pranav Bhalla', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saagar Gadhok', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shatakshi  Asthana', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivin  Khanna ', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prabhat Godse', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditya Mittal', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saahas Jain', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('shranya gambhir', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('jayant sengupta', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ankita  Gupta', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mallika Anand', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Amra Khullar', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishan Mudgal', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aakriti Kapoor', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Raakhi Sahijpal', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `MasterUsers` (`first_name`, `last_name`, `father_name`, `mother_name`, `dob`, `Year_of_joining`, `Year_of_leaving`, `class_of_leaving`, `batch`, `house`, `marital_status`, `email`, `contact_number`, `permanent_address`, `current_location`, `country`, `university`, `profession`, `organization`, `designation`, `engagement_avenues`, `rating`, `applied_for_membership_1yer`, `special_achievments_in_school`, `suggestions`, `main_roles`, `main_phone_number`, `main_location`, `main_fb_id`, `main_birthday`, `main_gender`, `main_course`, `main_batch`, `main_company`, `main_university`, `main_designation`, `main_city`, `main_country`, `main_industry`, `main_curriculum`, `Is_app_user`) VALUES
('Shubha Prasad', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Urgyen Joshi', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rahul Thairani', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Raghav  Prasad ', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Naiya  Mehra', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Medha Aurora', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nayan Srivastava', '', '', '', '', '2006', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sanjit Varma Gottumukkala ', '', '', '', '', '2012', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Meghna Prakash', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ritvik Handa', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sachin Sastri', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Romik Bose Mitra', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anirudh  Varma', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Suryakaran  Thakur', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikunj Gupta', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Gauri  Dimri ', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Raghav Kapila', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manan Arora', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Eeshan Chatterjee', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('ashriya  malik', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sidharth Savant', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Rohit Upadrashta', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pranav Vikram Shriram', '', '', '', '', '1998', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prabhav Chawla', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishan Agarwal', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nityesh Bohidar', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Laavanya Sundeep', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Angad Srivastava', '', '', '', '', '2006', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhijoy Saha', '', '', '', '', '2006', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Isha Agarwal', '', '', '', '', '1995', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishan Dikshit', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abhishek Aggarwal', '', '', '', '', '2013', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Raghav Singh', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sejal Widge', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kartikeya Khanna', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shivam Ahuja', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manjari Misra', '', '', '', '', '1999', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sehar Anand', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aayush Celly', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sonakshi Chaudhry', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dhruv Kharbanda', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ravin Bindra', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prakarsh Pandey', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nischay Mehta', '', '', '', '', '2012', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shreya V Basu', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dhruv Mishra', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saanya  Malhotra ', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Varuni  Khanna', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ashtha Madathil Mooliyl', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kabir Nath', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saarthak  Aurora', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mihika Johorey', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Smruti Agarwal', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vartika (Seth) Kapur', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('KSHITIJ KUMAR', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Adhiraj Rajpal', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Taarini Marwaha', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Kabir Khanna', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddhant Nag', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shantanu Navlakha', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Devanshu Sood', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanvi Sood', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bhavna Mathur', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Varunica  Agrawal', '', '', '', '', '2006', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saloni Dhir', '', '', '', '', '2001', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nikhil Tandon', '', '', '', '', '2006', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Natasha Dhawan', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arush Ujawal', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sushrut Prabhakar', '', '', '', '', '1996', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arunima   Mukherjee ', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Richa Sharma', '', '', '', '', '1990', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Seema Bharwani', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Malay Memani', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shantanu Kishwar', '', '', '', '', '2006', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Angad  Singh', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tishya Kakar', '', '', '', '', '2011', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vishnu Jain', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Akshat Chadha', '', '', '', '', '2002', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vinay Eapen', '', '', '', '', '2000', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Simran Bhalla', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Vir Chopra', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Siddharth Bahl', '', '', '', '', '2006', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anjini Chandra', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aronjoy Das', '', '', '', '', '2008', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bhavika Chugh', '', '', '', '', '2003', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Gayatri  Bajpai', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Yatharth Gulati', '', '', '', '', '2011', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prapti Khanna', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjun Nath', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bhumika Dullu', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Saniya Keswani', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishana Srivastava-Khan', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Pratap  Atwal', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sajal  Gupta', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Derik Bhardwaj', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Sidharth Mittal', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Faiz Khan', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Girika Bhalla', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Raghav Mathur', '', '', '', '', '2004', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditi Naval', '', '', '', '', '2005', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manisha Kumaran', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Dhruv Arora', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ranajoy Das', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tejas Dhingra', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('rukmini kapoor', '', '', '', '', '2007', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Nitin  Lal ', '', '', '', '', '1994', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ishuka Chibb Sharma', '', '', '', '', '2001', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Diksha Kaul', '', '', '', '', '2007', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Anika Guha', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditi Sabharwal', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('ambar madan', '', '', '', '', '1992', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mahin Rai', '', '', '', '', '2009', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Alisha Uppal', '', '', '', '', '1999', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shubhi Arya', '', '', '', '', '2002', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Tanuj Jain', '', '', '', '', '2000', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Mihir Jain', '', '', '', '', '2005', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Ashlyn Sehgal', '', '', '', '', '2015', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Arjun Madan', '', '', '', '', '2012', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditi Dasgupta', '', '', '', '', '2006', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Carishma Roy', '', '', '', '', '2004', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Manan Khurana', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Niladri Mitra', '', '', '', '', '2010', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Karthik Nagarajan', '', '', '', '', '1995', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Aditya Narayan', '', '', '', '', '1996', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shloka Ramachandran', '', '', '', '', '2008', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Prateek Bakshi', '', '', '', '', '1991', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shriya  Girotra ', '', '', '', '', '2004', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Antonio Ar\'Arazi Nadi', '', '', '', '', '1988', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Bhavya Bishnoi', '', '', '', '', '1997', '', '', '', 'Moulsari', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Shreya Chugh', '', '', '', '', '2009', '', '', '', 'Aravali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('Abcdd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'abcd@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '8764643464', 'Hyderabad', '', '', NULL, '', '2018', NULL, NULL, 'Alumni', NULL, NULL, 'Hospital & Health Care', '', '1'),
('Vaishali Sharma', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'vaishali.sharma@tsrs.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '9871266997', 'Gurugram, Haryana, India', '', '', NULL, '', '', NULL, NULL, 'Teacher', NULL, NULL, '', 'Aravali', '1'),
('Simran Nanda Singh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'simran.nanda@bain.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919818811132', 'New Delhi', '', '', NULL, '', '2004', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('SETHI VAIBHAV ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'vaibhavsethi88@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '8800116637', 'Gurugram', '', '', NULL, '', '2005', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Shrey Gupta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'shrey.g.gupta@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919811400936', 'Gurugram', '', '', NULL, '', '2008', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Ujjwal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ujjwal.dhawan@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10154894489777360', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Hiteshwar Kochhar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hiteshwar.kochhar@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '9717131444', 'New Delhi', '', '', NULL, '', '2008', NULL, NULL, 'Student', NULL, NULL, '', 'Aravali', '1'),
('Keshav Anand', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'keshavanand151@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9810097256', 'New Delhi', '', '', NULL, '', '2011', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Aman Bhatia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'amanbhatia_82@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10154588776494457', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Sachin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sachin@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '3232222', 'Kolkata', '', '', NULL, '', '2014', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Jaspreet Singh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jaspreet@42works.net', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '9814768811', 'Chandigarh', '', '', NULL, '', '2012', NULL, NULL, 'Student', NULL, NULL, '', 'Aravali', '1'),
('Abhi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'abhichauhan14@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10157947184350162', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Supriya Paul', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'supriya@joshtalks.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9818213867', 'Gurgaon, Haryana, India', '', '', NULL, '', '2011', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Ankit Kamboj', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ankit@42works.net', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Harman Singh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'harmansingh_93@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10154614269546253', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Harman Singh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'harmansingh.1993@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+918769420228', 'Gurugram', '', '', NULL, '', '2011', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Divyani kohli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dpivypanpi@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9958403836', 'Gurugram', '', '', NULL, '', '2010', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Nikkita Anil Kumar ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nikkita.anilkumar@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919940245650', 'Chennai', '', '', NULL, '', '2011', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Raghav', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rpires261@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10154391347949790', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Raghav', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'raghav.khurana@bath.edu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, 'Consumer Goods', '', '1'),
('Shirshendu Banerjee', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'banerjeeshirshendu@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '+919823994624', 'Pune, Maharashtra, India', '', '', NULL, '', '2012', NULL, NULL, 'Student', NULL, NULL, '', 'Aravali', '1'),
('Kshitij Gangwal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kshitij_g@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919810010667', 'New Delhi', '', '', NULL, '', '2001', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Sapna Dimri', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sapna.dimri@tsrs.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '9212117000', 'New Delhi', '', '', NULL, '', '', NULL, NULL, 'Teacher', NULL, NULL, '', 'Aravali', '1'),
('Shekhar Hans', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'shekharhans32@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '1239494752770584', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Tanmaya Jain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tanmayajain@infeedo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10158012221365244', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Shafali Kashyap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'shafali.kashyap@bbc.co.uk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919811650963', 'Delhi, India', '', '', NULL, '', '2012', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Sanya Sabharwal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bhavana.sabarwal09@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '9810157979', '', '', '', NULL, '', '2017', NULL, NULL, 'Student', NULL, NULL, '', 'Moulsari', '1'),
('Varun Tandon ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'varontandon@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919999990989', 'New Delhi', '', '', NULL, '', '2000', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Shivani Katyal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'shivanikat@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '9811053756', 'Gurugram, Haryana, India', '', '', NULL, '', '', NULL, NULL, 'Teacher', NULL, NULL, '', 'Aravali', '1'),
('Aryan handa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'vivikahanda01@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '9811098768', 'Delhi, India', '', '', NULL, '', '2017', NULL, NULL, 'Student', NULL, NULL, '', 'Moulsari', '1'),
('Kartikeya Khanna', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kartikeyakhanna@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10212215135238679', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Kartikeya Khanna', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kartikeyakhann@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9975692825', 'New Delhi', '', '', NULL, '', '2015', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Kartikeya Khanna', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kartikeya_khanna@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9650662727', 'New Delhi', '', '', NULL, '', '2015', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Sushma Yadav', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'drsushmayadav03@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '847717862037786', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Gabriel Mitra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gabrielmitra11@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '1209459289089527', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Raj S Raj', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'shauryaaraj@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '1411731325554092', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Shauryaa Raj', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'artest.shauryaa@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '7838544409', 'New Delhi', '', '', NULL, '', '2016', NULL, NULL, 'Student', NULL, NULL, '', 'Moulsari', '1'),
('Vincent Sindhwani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sindhwani.vincent@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '9310074264', 'New Delhi', '', '', NULL, '', '2016', NULL, NULL, 'Student', NULL, NULL, '', 'Moulsari', '1'),
('Fortytwo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'isha@42works.net', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '179567999151055', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Vedaant Nag', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'vedaant.united@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+918527201623', 'New Delhi', '', '', NULL, '', '2015', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Siddhant Nag', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'siddhant.nag93@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919958269148', 'New Delhi', '', '', NULL, '', '2011', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Shavir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'shavir@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919867598420', 'New Delhi', '', '', NULL, '', '2000', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Megha Bhatia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'megha1686@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9899714833', 'Gurugram, Haryana, India', '', '', NULL, '', '2004', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Rahul Wahi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rahul.wahi1987@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9818431114', 'New Delhi', '', '', NULL, '', '2005', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Arjun Nath', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'arjun.n.nath@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919560003481', 'Gurgaon', '', '', NULL, '', '2002', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Parichay Agarwal ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agrparichay@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9818425657', 'Gurugram, Haryana, India', '', '', NULL, '', '2017', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Arjun', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'arjun.nath@haygroup.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Dhruv Yadav', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dhruv1103@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '', '', '', '', NULL, '', '2017', NULL, NULL, 'Student', NULL, NULL, '', 'Moulsari', '1'),
('Shrey', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'shrey.gupta90@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10154315058642393', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Pritika khurana', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'anuk2539@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '9871436988', 'New Delhi', '', '', NULL, '', '2016', NULL, NULL, 'Student', NULL, NULL, '', 'Aravali', '1'),
('Pragya Bhargava', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pragya0512@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919810761693', '', '', '', NULL, '', '2011', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Saara Thakur ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Saarathakur10@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919810908133', 'Delhi, India', '', '', NULL, '', '2013', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Siddhant Tandon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tandon_siddhant@yahoo.co.in', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919971436000', 'Gurgaon, Haryana, India', '', '', NULL, '', '2013', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Sohraab Bawa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sohraabbawa@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9167589358', 'Mumbai', '', '', NULL, '', '2011', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Vrinda Sharma ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'vrindasharma26@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+16463067548', 'New York, NY, United States', '', '', NULL, '', '2011', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Ranajoy Das', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rdasofficialmusic@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919811203910', 'Gurugram, Haryana, India', '', '', NULL, '', '2011', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Akshita Kolluru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'akshita.kolluru93@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+6584072158', 'null', '', '', NULL, '', '2011', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Srishti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'srishtibhargava@outlook.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, 'Research', '', '1'),
('Mad Man', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'adamndoctor@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+48509717631', 'New Delhi', '', '', NULL, '', '2002', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Diptangshu Bhattacharyya', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dbhattacharyya.72@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9654069173', 'New Delhi', '', '', NULL, '', '2012', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Adit Sachdeva', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'aditsachdeva2005@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919818742255', 'New Delhi', '', '', NULL, '', '2015', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Shaiilesh Jadhav', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'adv.spjadhav@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '1652213014794989', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Sai Kiran', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'porifera_kiran@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10210014043921685', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Saikiranmayi Turlapati', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kiranturlapati92@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9953559194', 'Hyderabad', '', '', NULL, '', '2010', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Vardaan Agarwal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'vardaanagarwal@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919899492422', 'Gurugram, Haryana, India', '', '', NULL, '', '2006', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Priyanka', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'priya_preety_82@yahoo.co.in', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '1660948463921910', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Divij Mahajan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'divijdivijdivij@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9811204604', 'New Delhi', '', '', NULL, '', '2007', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Rahul', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rahullnvo@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9566549154', 'Chennai', '', '', NULL, '', '2016', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Aabha Kapuria ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'aabha_kapuria@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '+919810275544', 'New Delhi', '', '', NULL, '', '2017', NULL, NULL, 'Student', NULL, NULL, '', 'Aravali', '1'),
('Isha Mital', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ishamital@outlook.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919910471880', 'New Delhi', '', '', NULL, '', '2007', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Avirat Agarwal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avirat.agarwal@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '1783862791627301', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Siddharth Mitra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'siddharth.mitra12@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919871134970', 'Chennai', '', '', NULL, '', '2014', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Asit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asitdhingra@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10155253654703832', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Pratyush Pranav', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pranav.pratyush83@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+447766620893', 'Cambridge, United Kingdom', '', '', NULL, '', '2013', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Charita', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'charita.vig@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10154640874648295', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Aditya Narayan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'adityan.28@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10211248924892027', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Armaan Singh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'armaan1991@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+917702099985', 'Gurugram, Haryana, India', '', '', NULL, '', '2009', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Pallavi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pallavi_dixit_93@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10158653790150285', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Shreya Gurwara', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'shreya.gurwara@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9810057164', 'Delhi, India', '', '', NULL, '', '2006', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Ojasvita Sawhney ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ojasvita@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '15196975363', 'Toronto, ON, Canada', '', '', NULL, '', '2005', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Arhan Bagati', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'arhanbagati15@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919910399999', 'New Delhi, Delhi, India', '', '', NULL, '', '2017', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Ravi Kulsreshtha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ravi.ksh@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10155450330279866', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Pooja Sajeev', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sajeevpooja@gnail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '8527815966', 'Gurgaon, Haryana, India', '', '', NULL, '', '2017', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Pooja Sajeev', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sajeevpooja@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '8527815966', 'Gurgaon, Haryana, India', '', '', NULL, '', '2017', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1');
INSERT INTO `MasterUsers` (`first_name`, `last_name`, `father_name`, `mother_name`, `dob`, `Year_of_joining`, `Year_of_leaving`, `class_of_leaving`, `batch`, `house`, `marital_status`, `email`, `contact_number`, `permanent_address`, `current_location`, `country`, `university`, `profession`, `organization`, `designation`, `engagement_avenues`, `rating`, `applied_for_membership_1yer`, `special_achievments_in_school`, `suggestions`, `main_roles`, `main_phone_number`, `main_location`, `main_fb_id`, `main_birthday`, `main_gender`, `main_course`, `main_batch`, `main_company`, `main_university`, `main_designation`, `main_city`, `main_country`, `main_industry`, `main_curriculum`, `Is_app_user`) VALUES
('Gitansh Soni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gitanshsoni@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '1309264055789684', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Anirudh Gupta ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'anirudh101@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9811632194', 'New Delhi', '', '', NULL, '', '2002', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Rashi Sarraf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rashisarraf@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919810523023', 'New Delhi', '', '', NULL, '', '2017', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Suryavir Singh Bal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'suryavir.bal@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9654296629', 'Gurugram, Haryana, India', '', '', NULL, '', '2017', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Suryavir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'arsnal1234@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10203284981000978', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Sahaj Porwal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sahaj.porwal@outlook.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919899706754', 'Gurgaon, Haryana, India', '', '', NULL, '', '2017', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Samyak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'samyakpost@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10211639420614867', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Prithvir Bhalla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'prithvir89@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919999973751', 'Gurugram, Haryana, India', '', '', NULL, '', '2016', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Sankarshan talluri', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Sankarshan.talluri@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9899590684', 'Bangalore', '', '', NULL, '', '2012', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Maadhav Maheshwari ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'madhav98@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9899886122', 'Gurugram, Haryana, India', '', '', NULL, '', '2017', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Tuhin Kalia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tuhinkalia@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '+919769910666', 'Mumbai', '', '', NULL, '', '2009', NULL, NULL, 'Student', NULL, NULL, '', 'Aravali', '1'),
('Sana Talwar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sana.talwar84@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9810077115', 'New Delhi', '', '', NULL, '', '2002', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Vibhor mathur ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'vibhormathur83@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919818340101', 'New Delhi', '', '', NULL, '', '2001', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Vir Singh Brar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'virsbrar@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10156566672474768', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Abdul Aziz Radhi ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'abdul.radhi@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+971529915151', 'Dubai - United Arab Emirates', '', '', NULL, '', '2009', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Sahil Marwah ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'marwahsahil17@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '+919599955916', 'New Delhi', '', '', NULL, '', '2016', NULL, NULL, 'Student', NULL, NULL, '', 'Moulsari', '1'),
('Saurabhya Torres Sanket', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'saurabhya.sanket@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10214137557101823', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Arushi Sachdeva', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'arushisachdeva31@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919871510141', 'Boca Raton, FL, United States', '', '', NULL, '', '2010', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Rittika Ghosh ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'RittikaGhosh3004@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9910097524', 'Gurugram, Haryana, India', '', '', NULL, '', '2017', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Mohnish ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mohnish100@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+918383039903', 'New Delhi', '', '', NULL, '', '2009', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Neetu sharma ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'neetueducationchild@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '9891318445', 'New Delhi', '', '', NULL, '', '2016', NULL, NULL, 'Student', NULL, NULL, '', 'Aravali', '1'),
('Radhika Bhalla ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'radhikab@quadrangleconsulting.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '9810053494', 'Gurgaon, Haryana, India', '', '', NULL, '', '', NULL, NULL, 'Teacher', NULL, NULL, '', 'Aravali', '1'),
('Sidhant Malik ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'maliksidhant14@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '8383054637', 'Gurugram, Haryana, India', '', '', NULL, '', '2017', NULL, NULL, 'Student', NULL, NULL, '', 'Aravali', '1'),
('Anisha Singh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'anisha0singh@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9810233492', 'Bangalore', '', '', NULL, '', '2010', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Shubham Bansal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'shubhambansal@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9911168679', 'Mumbai', '', '', NULL, '', '2012', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Agni Kumar Bose', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agnibose1909@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919987112190', 'Gurugram, Haryana, India', '', '', NULL, '', '2012', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Sahil Singh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sahilsingh.indore@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '1428946947201963', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Prabhav Jain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'prabhav.pj@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919876199020', 'Ludhiana', '', '', NULL, '', '2015', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Sakshi Asudani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sasudani6@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9811272777', 'New Delhi', '', '', NULL, '', '2006', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Sahil Marwah ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sahil_marwah@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9810143564', 'New Delhi', '', '', NULL, '', '2002', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Harjeev Singh Chawla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'harjeevchawla@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919811066629', 'New Delhi, Delhi, India', '', '', NULL, '', '2003', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Dhananjay Vijay Kumar ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'djwork.03@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919873935339', 'New Delhi', '', '', NULL, '', '2003', NULL, NULL, 'Alumni', NULL, NULL, '', 'Moulsari', '1'),
('Sahil Narain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sahil.narain@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+919899097933', 'New Delhi', '', '', NULL, '', '2008', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Janhavi Hiranandani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'janhavi.hira@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '8800557196', 'Mumbai', '', '', NULL, '', '2015', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Nikita Seth', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nikitaseth.93@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9818779200', 'Gurgaon, Haryana, India', '', '', NULL, '', '2012', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Anirudh Balakrishnan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'balaonemail@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9717411377', 'New Delhi', '', '', NULL, '', '2005', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Gagan Preet Singh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 's.gagan.preet@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '+12069282237', 'Redmond, WA, United States', '', '', NULL, '', '2008', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Mamta Kaundal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mamta@42works.net', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '7837781523', 'Mohali', '', '', NULL, '', '2011', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Shashank', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'shashank409@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Mamta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mamta1@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '123456486', 'Mumbai', '', '', NULL, '', '2017', NULL, NULL, 'Alumni', NULL, NULL, '', 'Aravali', '1'),
('Amrit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'amrit_chopra@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '10155930469482430', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Rahul', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rahul@42works.net', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Soft', '2347329847', 'Mohali', '', '', NULL, 'MCA', '2000', NULL, NULL, 'Soft', NULL, NULL, 'IT', '', '1'),
('12345', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kamboj.ankit1988@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '44664433', 'Chennai', '', '', NULL, '', '2014', NULL, NULL, 'Student', NULL, NULL, 'Animation', '', '1'),
('12345', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rahulsingh@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '46466111', 'Mumbai', '', '', NULL, '', '2015', NULL, NULL, 'Student', NULL, NULL, 'Alternative Medicine', '', '1'),
('Ankit kk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ankit121@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '11334496', 'Hyderabad', '', '', NULL, '', '2015', 'Google', 'Pu', 'Student', NULL, NULL, 'Architecture & Planning', '', '1'),
('Nishant', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nishant@42works.net', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '789645213', 'Sahibzada Ajit Singh Nagar', '', '', NULL, '', '2010', 'Tests', 'Tests, Tests, Tests, Tests, Tests', 'Student', NULL, NULL, 'International Trade and Development', '', '1'),
('Ankit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ankit122@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '66443311', 'Bangalore', '', '', NULL, '', '2013', NULL, NULL, 'Student', NULL, NULL, 'Apparel & Fashion', '', '1'),
('Simran Jit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'simran271289@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '1723067951096740', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Simran', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'simran@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '123454648', 'Hyderabad', '', '', NULL, '', '2018', NULL, NULL, 'Alumni', NULL, NULL, 'Alternative Dispute Resolution', '', '1'),
('simran', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'simran1@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '8052525252', 'Bangalore', '', '', NULL, '', '2012', NULL, NULL, 'Alumni', NULL, NULL, 'Architecture & Planning', '', '1'),
('simran', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'harsimranjit@42works.net', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '8054545454', 'Chennai', '', '', NULL, '', '2012', 'dd', 'abcd', 'Alumni', NULL, NULL, 'Arts and Crafts', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'imprabasdflt@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'imprabasdflt123@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'imprabaltha@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asd@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sdfsdf@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sdfsdf1@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sdfsdf12@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sdfsdf123@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sdfsdf21@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '948784884', 'Chennai', '', '', NULL, '', '', NULL, NULL, 'Teacher', NULL, NULL, 'Automotive', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Test@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test1@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Test1@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Test44@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Test45@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Test46@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('test user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hello@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '4664644', 'Bangalore', '', '', NULL, '', '2011', NULL, NULL, 'Student', NULL, NULL, 'Automotive', '', '1'),
('Simran', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sjdfsd@.sdfasd..com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '343423', 'Chennai', '', '', NULL, '', '2018', NULL, NULL, 'Alumni', NULL, NULL, 'Hospital & Health Care', '', '1'),
('Simran', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'abcde@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '23423423', 'Kolkata', '', '', NULL, '', '2018', NULL, NULL, 'Alumni', NULL, NULL, 'Hospital & Health Care', '', '1'),
('Dfgsfdg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sdfasdf@digs.dfgsdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', 'dfgdfgsdf', 'Kolkata', '', '', NULL, '', '2018', NULL, NULL, 'Alumni', NULL, NULL, 'Hospital & Health Care', '', '1'),
('Sdfasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dds@fdg.fghdfg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '34423', 'Hyderabad', '', '', NULL, '', '2018', NULL, NULL, 'Alumni', NULL, NULL, 'Hospital & Health Care', '', '1'),
('Fghjj', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'abcd@gmail.bjn', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '585835335', 'Kolkata', '', '', NULL, '', '2018', NULL, NULL, 'Alumni', NULL, NULL, 'Hospital & Health Care', '', '1'),
('Karan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'karan_2837@yahoo.co.in', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '1963135377035329', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Karan Hooda', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'karanhooda15@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9915024395', 'Chandigarh', '', '', NULL, '', '2007', NULL, NULL, 'Alumni', NULL, NULL, 'Education Management', '', '1'),
('Ankit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ankit129@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '3366999', 'Bangalore', '', '', NULL, '', '2012', NULL, NULL, 'Alumni', NULL, NULL, 'Arts and Crafts', '', '1'),
('Ankit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ankit130@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '64646494', 'Hyderabad', '', '', NULL, '', '2013', NULL, NULL, 'Alumni', NULL, NULL, 'Arts and Crafts', '', '1'),
('test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testkabir@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '121', '12121212', '12', '', '2/3/1990', NULL, '1212', '123', NULL, NULL, '121', NULL, NULL, '12', '', '1'),
('test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testkabir1@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '121', '12121212', '12', '', '2/3/1990', NULL, '1212', '123', NULL, NULL, '121', NULL, NULL, '12', '', '1'),
('test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testkabir11@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '121', '12121212', '12', '', '2/3/1990', NULL, '1212', '123', NULL, NULL, '121', NULL, NULL, '12', '', '1'),
('test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testkabir13@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '121', '12121212', '12', '', '2/3/1990', NULL, '1212', '123', NULL, NULL, '121', NULL, NULL, '12', '', '1'),
('Ankit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ankit131@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '46649494', 'Chennai', '', '', NULL, '', '2017', NULL, NULL, 'Alumni', NULL, NULL, 'Automotive', '', '1'),
('test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testkabir15@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '121', '12121212', '12', '', '2/3/1990', NULL, '1212', '123', NULL, NULL, '121', NULL, NULL, '12', '', '1'),
('test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testkabir16@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '121', '12121212', '12', '', '2/3/1990', NULL, '1212', '123', NULL, NULL, '121', NULL, NULL, '12', '', '1'),
('test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testkabir17@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '121', '12121212', '12', '', '2/3/1990', NULL, '1212', '123', NULL, NULL, '121', NULL, NULL, '12', '', '1'),
('Ankit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ankit138@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '4664644', 'Chennai', '', '', NULL, '', '2011', NULL, NULL, 'Alumni', NULL, NULL, 'Automotive', '', '1'),
('Karan Hooda ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'karan.hooda@futured.in', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '9915024395', 'Chandigarh', '', '', NULL, '', '', NULL, NULL, 'Teacher', NULL, NULL, 'Education Management', '', '1'),
('Ankit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ankit148@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '79949464', 'Hyderabad', '', '', NULL, '', '2011', NULL, NULL, 'Alumni', NULL, NULL, 'Automotive', '', '1'),
('Ankitt Kamboj', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'thesimplehorizon@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '1744560858917123', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Ankit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ankit141@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '97676', 'Bangalore', '', '', NULL, '', '2011', NULL, NULL, 'Alumni', NULL, NULL, 'Automotive', '', '1'),
('Ankit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ankit123@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '89684236', 'Mumbai', '', '', NULL, '', '2014', '42works', 'Pu', 'KROS', NULL, NULL, 'Architecture & Planning', '', '1'),
('Tango charlie', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tango42charlie@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '98156948536', 'raman', '', '', NULL, '', '1974', 'NA', 'PU,    CTU,    Hpu, Univ1, Univ2, Univ3', 'Teacher', NULL, NULL, 'Investment Management', '', '1'),
('Nishant', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tnishant0891@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test21@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '985461328', 'Bangalore', '', '', NULL, '', '', NULL, NULL, 'Teacher', NULL, NULL, 'Architecture & Planning', '', '1'),
('Hello', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hello123@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '46644994', 'Bangalore', '', '', NULL, '', '2012', 'Google', 'Pu', 'Alumni', NULL, NULL, 'Architecture & Planning', '', '1'),
('Helloo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hello122@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '13464949', 'Hyderabad', '', '', NULL, '', '', NULL, NULL, 'Teacher', NULL, NULL, 'Arts and Crafts', '', '1'),
('Rahul', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rahul123@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alumni', '9894944', 'Chennai', '', '', NULL, '', '2013', NULL, NULL, 'Alumni', NULL, NULL, 'Apparel & Fashion', '', '1'),
('Lt Col Rattanbir Singh ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rattanbir@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '9873355715', 'Chandigarh', '', '', NULL, '', '', NULL, NULL, 'Teacher', NULL, NULL, 'Education Management', '', '1'),
('test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test12ankit@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Faculty', '8968428607', 'sd', '', '3/2/1987', NULL, '12', '12', NULL, NULL, 'Faculty', NULL, NULL, 'sd', '', '1'),
('Nishant', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test42@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Student', '875421369', 'Cañada de Gómez, Santa Fe Province, Argentina', '', '', NULL, '', '2018', NULL, NULL, 'Student', NULL, NULL, 'Hospital & Health Care', '', '1'),
('Mamta K', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '42workstab@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '460016554400982', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'raman@42works.net', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1'),
('test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test12raman@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KROS', '12121212', '', '', '02/02/1987', NULL, '12', '2004', NULL, NULL, 'KROS', NULL, NULL, '', '', '1'),
('test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test12raman12@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KROS', '12121212', '', '', '02/02/1987', NULL, '12', '2004', NULL, NULL, 'KROS', NULL, NULL, '', '', '1'),
('Rahull', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ankit151@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KROS', '79979797', 'New Delhi', '', '', NULL, '', '2017', NULL, NULL, 'KROS', NULL, NULL, 'Architecture & Planning', '', '1'),
('MohitUser', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mohit161@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '49979794', 'Chennai', '', '', NULL, '', '2016', NULL, NULL, 'Teacher', NULL, NULL, 'Alternative Medicine', '', '1'),
('test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test12raman123@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KROS', '12121212', '', '', '02/02/1987', NULL, '12', '2004', NULL, NULL, 'KROS', NULL, NULL, '', '', '1'),
('Mohit kumar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mohit167@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '333333', 'Bangalore', '', '', NULL, '', '2016', NULL, NULL, 'Teacher', NULL, NULL, 'Alternative Medicine', '', '1'),
('Mohit Sharma', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mohit168@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '111111', 'Mumbai', '', '', NULL, '', '2016', NULL, NULL, 'Teacher', NULL, NULL, 'Animation', '', '1'),
('gdfgsdfg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ssdf@dfgfd.fghdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '23423324', 'Chennai', '', '', NULL, '', 'Year of Leaving: 2025', NULL, NULL, 'Teacher', NULL, NULL, 'Hospital & Health Care', '', '1'),
('MohitSaldi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mohit170@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '111111', 'Mumbai', '', '', NULL, '', '2015', NULL, NULL, 'Teacher', NULL, NULL, 'Apparel & Fashion', '', '1'),
('mohitKumar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mohit171@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '23223', '', '', '', NULL, '', '2019', NULL, NULL, '', NULL, NULL, '', '', '1'),
('Mohit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mohit180@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '33333', 'Hyderabad', '', '', NULL, '', '2018', 'Gmail', 'Pu', 'Teacher', NULL, NULL, 'Accounting', '', '1'),
('RamanUser', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'raman123@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', '25252525', 'Bangalore', '', '', NULL, '', '2019', 'Google inc.', 'Pu', 'Teacher', NULL, NULL, 'Architecture & Planning', '', '1'),
('Simran', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'abcd@gmail.com1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Teacher', 'w4234234', 'Chennai', '', '', NULL, '', '2024', NULL, NULL, 'Teacher', NULL, NULL, 'Hospital & Health Care', '', '1'),
('Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test@test.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KROS', '254845', 'Cañada de Gómez, Santa Fe Province, Argentina', '', '', NULL, '', '2025', NULL, NULL, 'KROS', NULL, NULL, 'Hospital & Health Care', '', '1'),
('Sim ran', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'harsimranjit@yopmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KROS', '1234567890', 'Mumbai', '', '', NULL, '', '', NULL, NULL, 'KROS', NULL, NULL, 'Accounting', '', '1'),
('Vishal aggarwal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'vishal_kros@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KROS', '9814690605', 'Chandigarh', '', '', NULL, '', '', NULL, NULL, 'KROS', NULL, NULL, 'Law Practice', '', '1'),
('Navdeep Singh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'navdeep_kalsi@yahoo.con', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KROS', '9815000132', 'Chandigarh', '', '', NULL, '', '', NULL, NULL, 'KROS', NULL, NULL, 'Information Technology and Services', '', '1'),
('Reet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'wadherareet@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '548573222195312', '', NULL, '', '', NULL, NULL, '', NULL, NULL, '', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `media__gallery`
--

CREATE TABLE `media__gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `default_format` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media__gallery`
--

INSERT INTO `media__gallery` (`id`, `name`, `context`, `default_format`, `enabled`, `updated_at`, `created_at`) VALUES
(1, 'Post Gallery', 'default', 'default_small', 1, '2017-12-04 17:01:12', '2017-12-04 17:01:12'),
(2, 'Post Gallery', 'default', 'default_small', 1, '2017-12-04 17:06:26', '2017-12-04 17:06:26'),
(3, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 10:53:08', '2017-12-05 10:53:08'),
(4, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 10:54:19', '2017-12-05 10:54:19'),
(5, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 11:00:10', '2017-12-05 11:00:10'),
(6, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 11:01:01', '2017-12-05 11:01:01'),
(7, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 11:01:27', '2017-12-05 11:01:27'),
(8, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 11:04:32', '2017-12-05 11:04:32'),
(9, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 11:05:05', '2017-12-05 11:05:05'),
(10, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 11:05:24', '2017-12-05 11:05:24'),
(11, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 11:07:34', '2017-12-05 11:07:34'),
(12, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 11:07:52', '2017-12-05 11:07:52'),
(13, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 11:08:14', '2017-12-05 11:08:14'),
(14, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 12:08:03', '2017-12-05 12:08:03'),
(15, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 12:08:30', '2017-12-05 12:08:30'),
(16, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 12:08:46', '2017-12-05 12:08:46'),
(17, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 12:09:15', '2017-12-05 12:09:15'),
(18, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 12:11:08', '2017-12-05 12:11:08'),
(19, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 12:11:35', '2017-12-05 12:11:35'),
(20, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 12:11:49', '2017-12-05 12:11:49'),
(21, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 12:15:57', '2017-12-05 12:15:57'),
(22, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 12:57:05', '2017-12-05 12:57:05'),
(23, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 12:58:04', '2017-12-05 12:58:04'),
(24, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 13:13:43', '2017-12-05 13:13:43'),
(25, 'Post Gallery', 'default', 'default_small', 1, '2017-12-05 17:50:48', '2017-12-05 17:50:48'),
(26, 'Post Gallery', 'default', 'default_small', 1, '2017-12-29 10:51:01', '2017-12-29 10:51:01'),
(27, 'Post Gallery', 'default', 'default_small', 1, '2018-01-04 11:53:07', '2018-01-04 11:53:07'),
(28, 'Post Gallery', 'default', 'default_small', 1, '2018-01-04 11:53:37', '2018-01-04 11:53:37'),
(29, 'Post Gallery', 'default', 'default_small', 1, '2018-01-04 11:54:12', '2018-01-04 11:54:12'),
(30, 'Post Gallery', 'default', 'default_small', 1, '2018-01-04 11:59:49', '2018-01-04 11:59:49'),
(31, 'Post Gallery', 'default', 'default_small', 1, '2018-01-04 12:00:24', '2018-01-04 12:00:24'),
(32, 'Post Gallery', 'default', 'default_small', 1, '2018-01-04 12:00:49', '2018-01-04 12:00:49'),
(33, 'Post Gallery', 'default', 'default_small', 1, '2018-01-04 12:04:20', '2018-01-04 12:04:20'),
(34, 'Post Gallery', 'default', 'default_small', 1, '2018-01-04 12:07:10', '2018-01-04 12:07:10'),
(35, 'Post Gallery', 'default', 'default_small', 1, '2018-01-04 18:19:20', '2018-01-04 18:19:20'),
(36, 'Post Gallery', 'default', 'default_small', 1, '2018-01-05 16:49:03', '2018-01-05 16:49:03'),
(37, 'Post Gallery', 'default', 'default_small', 1, '2018-01-12 10:26:54', '2018-01-12 10:26:54'),
(38, 'Post Gallery', 'default', 'default_small', 1, '2018-01-15 13:02:23', '2018-01-15 13:02:23'),
(39, 'Post Gallery', 'default', 'default_small', 1, '2018-01-15 13:03:30', '2018-01-15 13:03:30'),
(40, 'Post Gallery', 'default', 'default_small', 1, '2018-01-15 13:16:50', '2018-01-15 13:16:50'),
(41, 'Post Gallery', 'default', 'default_small', 1, '2018-01-15 13:18:54', '2018-01-15 13:18:54'),
(42, 'Post Gallery', 'default', 'default_small', 1, '2018-01-25 15:07:47', '2018-01-25 15:07:47'),
(43, 'Post Gallery', 'default', 'default_small', 1, '2018-01-30 12:55:04', '2018-01-30 12:55:04'),
(44, 'Post Gallery', 'default', 'default_small', 1, '2018-01-30 16:01:28', '2018-01-30 16:01:28'),
(45, 'Post Gallery', 'default', 'default_small', 1, '2018-02-01 11:07:12', '2018-02-01 11:07:12'),
(46, 'Post Gallery', 'default', 'default_small', 1, '2018-02-01 11:16:50', '2018-02-01 11:16:50'),
(47, 'Post Gallery', 'default', 'default_small', 1, '2018-02-01 12:36:42', '2018-02-01 12:36:42'),
(48, 'Post Gallery', 'default', 'default_small', 1, '2018-02-01 12:41:18', '2018-02-01 12:41:18'),
(49, 'Post Gallery', 'default', 'default_small', 1, '2018-02-01 13:13:49', '2018-02-01 13:13:49'),
(50, 'Post Gallery', 'default', 'default_small', 1, '2018-02-01 13:18:48', '2018-02-01 13:18:48'),
(51, 'Post Gallery', 'default', 'default_small', 1, '2018-02-01 13:46:00', '2018-02-01 13:46:00'),
(52, 'Post Gallery', 'default', 'default_small', 1, '2018-02-02 13:26:26', '2018-02-02 13:26:26'),
(53, 'Post Gallery', 'default', 'default_small', 1, '2018-02-02 13:29:02', '2018-02-02 13:29:02'),
(54, 'Post Gallery', 'default', 'default_small', 1, '2018-02-02 13:31:21', '2018-02-02 13:31:21'),
(55, 'Post Gallery', 'default', 'default_small', 1, '2018-02-02 15:03:10', '2018-02-02 15:03:10'),
(56, 'Post Gallery', 'default', 'default_small', 1, '2018-02-02 15:11:36', '2018-02-02 15:11:36'),
(57, 'Post Gallery', 'default', 'default_small', 1, '2018-02-03 15:44:48', '2018-02-03 15:44:48'),
(58, 'Post Gallery', 'default', 'default_small', 1, '2018-02-03 15:48:37', '2018-02-03 15:48:37'),
(59, 'Post Gallery', 'default', 'default_small', 1, '2018-02-03 16:24:15', '2018-02-03 16:24:15'),
(60, 'Post Gallery', 'default', 'default_small', 1, '2018-02-06 12:51:36', '2018-02-06 12:51:36'),
(61, 'Post Gallery', 'default', 'default_small', 1, '2018-02-06 12:52:08', '2018-02-06 12:52:08'),
(62, 'Post Gallery', 'default', 'default_small', 1, '2018-02-06 12:53:35', '2018-02-06 12:53:35'),
(63, 'Post Gallery', 'default', 'default_small', 1, '2018-02-06 12:56:09', '2018-02-06 12:56:09'),
(64, 'Post Gallery', 'default', 'default_small', 1, '2018-02-06 12:56:34', '2018-02-06 12:56:34'),
(65, 'Post Gallery', 'default', 'default_small', 1, '2018-02-06 12:58:27', '2018-02-06 12:58:27'),
(66, 'Post Gallery', 'default', 'default_small', 1, '2018-02-06 13:00:27', '2018-02-06 13:00:27'),
(67, 'Post Gallery', 'default', 'default_small', 1, '2018-02-06 13:01:05', '2018-02-06 13:01:05'),
(68, 'Post Gallery', 'default', 'default_small', 1, '2018-02-06 13:03:08', '2018-02-06 13:03:08'),
(69, 'Post Gallery', 'default', 'default_small', 1, '2018-02-06 15:28:11', '2018-02-06 15:28:11'),
(70, 'Post Gallery', 'default', 'default_small', 1, '2018-02-06 15:30:42', '2018-02-06 15:30:42'),
(71, 'Post Gallery', 'default', 'default_small', 1, '2018-02-06 15:33:20', '2018-02-06 15:33:20'),
(72, 'Post Gallery', 'default', 'default_small', 1, '2018-02-06 15:35:41', '2018-02-06 15:35:41'),
(73, 'Post Gallery', 'default', 'default_small', 1, '2018-02-06 15:42:02', '2018-02-06 15:42:02'),
(74, 'Post Gallery', 'default', 'default_small', 1, '2018-02-06 15:45:29', '2018-02-06 15:45:29'),
(75, 'Post Gallery', 'default', 'default_small', 1, '2018-02-12 12:39:27', '2018-02-12 12:39:27'),
(76, 'Post Gallery', 'default', 'default_small', 1, '2018-03-13 11:30:05', '2018-03-13 11:30:05');

-- --------------------------------------------------------

--
-- Table structure for table `media__gallery_media`
--

CREATE TABLE `media__gallery_media` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media__gallery_media`
--

INSERT INTO `media__gallery_media` (`id`, `gallery_id`, `media_id`, `position`, `enabled`, `updated_at`, `created_at`) VALUES
(1, 1, 3, 1, 1, '2017-12-04 17:01:12', '2017-12-04 17:01:12'),
(2, 9, 8, 1, 1, '2017-12-05 11:05:05', '2017-12-05 11:05:05'),
(3, 11, 9, 1, 1, '2017-12-05 11:07:34', '2017-12-05 11:07:34'),
(4, 11, 10, 2, 1, '2017-12-05 11:07:34', '2017-12-05 11:07:34'),
(5, 12, 11, 1, 1, '2017-12-05 11:07:52', '2017-12-05 11:07:52'),
(6, 12, 12, 2, 1, '2017-12-05 11:07:52', '2017-12-05 11:07:52'),
(7, 12, 13, 3, 1, '2017-12-05 11:07:52', '2017-12-05 11:07:52'),
(8, 15, 15, 1, 1, '2017-12-05 12:08:30', '2017-12-05 12:08:30'),
(9, 15, 16, 2, 1, '2017-12-05 12:08:30', '2017-12-05 12:08:30'),
(10, 16, 17, 1, 1, '2017-12-05 12:08:46', '2017-12-05 12:08:46'),
(11, 16, 18, 2, 1, '2017-12-05 12:08:46', '2017-12-05 12:08:46'),
(12, 16, 19, 3, 1, '2017-12-05 12:08:46', '2017-12-05 12:08:46'),
(13, 18, 21, 1, 1, '2017-12-05 12:11:11', '2017-12-05 12:11:11'),
(14, 22, 26, 1, 1, '2017-12-05 12:57:05', '2017-12-05 12:57:05'),
(15, 24, 27, 1, 1, '2017-12-05 13:13:43', '2017-12-05 13:13:43'),
(16, 24, 28, 2, 1, '2017-12-05 13:13:44', '2017-12-05 13:13:44'),
(17, 24, 29, 3, 1, '2017-12-05 13:13:44', '2017-12-05 13:13:44'),
(18, 37, 77, 1, 1, '2018-01-12 10:26:54', '2018-01-12 10:26:54'),
(19, 38, 81, 1, 1, '2018-01-15 13:02:25', '2018-01-15 13:02:25'),
(20, 39, 82, 1, 1, '2018-01-15 13:03:32', '2018-01-15 13:03:32'),
(21, 39, 83, 2, 1, '2018-01-15 13:03:32', '2018-01-15 13:03:32'),
(22, 40, 86, 1, 1, '2018-01-15 13:16:50', '2018-01-15 13:16:50'),
(23, 41, 87, 1, 1, '2018-01-15 13:18:55', '2018-01-15 13:18:55'),
(24, 44, 89, 1, 1, '2018-01-30 16:01:30', '2018-01-30 16:01:30'),
(25, 44, 90, 2, 1, '2018-01-30 16:01:31', '2018-01-30 16:01:31'),
(26, 44, 91, 3, 1, '2018-01-30 16:01:31', '2018-01-30 16:01:31'),
(27, 46, 93, 1, 1, '2018-02-01 11:16:50', '2018-02-01 11:16:50'),
(28, 46, 94, 2, 1, '2018-02-01 11:16:50', '2018-02-01 11:16:50'),
(29, 46, 95, 3, 1, '2018-02-01 11:16:50', '2018-02-01 11:16:50'),
(30, 49, 103, 1, 1, '2018-02-01 13:13:49', '2018-02-01 13:13:49'),
(31, 50, 104, 1, 1, '2018-02-01 13:18:48', '2018-02-01 13:18:48'),
(32, 54, 111, 1, 1, '2018-02-02 13:31:22', '2018-02-02 13:31:22'),
(33, 54, 112, 2, 1, '2018-02-02 13:31:23', '2018-02-02 13:31:23'),
(34, 54, 113, 3, 1, '2018-02-02 13:31:24', '2018-02-02 13:31:24'),
(35, 55, 115, 1, 1, '2018-02-02 15:03:11', '2018-02-02 15:03:11'),
(36, 59, 119, 1, 1, '2018-02-03 16:24:17', '2018-02-03 16:24:17'),
(37, 59, 120, 2, 1, '2018-02-03 16:24:20', '2018-02-03 16:24:20'),
(38, 59, 121, 3, 1, '2018-02-03 16:24:22', '2018-02-03 16:24:22'),
(39, 65, 123, 1, 1, '2018-02-06 12:58:30', '2018-02-06 12:58:30'),
(40, 69, 124, 1, 1, '2018-02-06 15:28:11', '2018-02-06 15:28:11'),
(41, 69, 125, 2, 1, '2018-02-06 15:28:11', '2018-02-06 15:28:11'),
(42, 69, 126, 3, 1, '2018-02-06 15:28:11', '2018-02-06 15:28:11'),
(43, 73, 127, 1, 1, '2018-02-06 15:42:03', '2018-02-06 15:42:03'),
(44, 73, 128, 2, 1, '2018-02-06 15:42:03', '2018-02-06 15:42:03'),
(45, 73, 129, 3, 1, '2018-02-06 15:42:03', '2018-02-06 15:42:03'),
(46, 74, 130, 1, 1, '2018-02-06 15:45:29', '2018-02-06 15:45:29'),
(47, 74, 131, 2, 1, '2018-02-06 15:45:31', '2018-02-06 15:45:31'),
(48, 74, 132, 3, 1, '2018-02-06 15:45:33', '2018-02-06 15:45:33'),
(49, 75, 133, 1, 1, '2018-02-12 12:39:30', '2018-02-12 12:39:30'),
(50, 75, 134, 2, 1, '2018-02-12 12:39:32', '2018-02-12 12:39:32'),
(51, 75, 135, 3, 1, '2018-02-12 12:39:34', '2018-02-12 12:39:34'),
(52, 76, 136, 1, 1, '2018-03-13 11:30:06', '2018-03-13 11:30:06');

-- --------------------------------------------------------

--
-- Table structure for table `media__media`
--

CREATE TABLE `media__media` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) NOT NULL,
  `provider_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_status` int(11) NOT NULL,
  `provider_reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_metadata` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `length` decimal(10,0) DEFAULT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_size` int(11) DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_is_flushable` tinyint(1) DEFAULT NULL,
  `cdn_flush_at` datetime DEFAULT NULL,
  `cdn_status` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media__media`
--

INSERT INTO `media__media` (`id`, `name`, `description`, `enabled`, `provider_name`, `provider_status`, `provider_reference`, `provider_metadata`, `width`, `height`, `length`, `content_type`, `content_size`, `copyright`, `author_name`, `context`, `cdn_is_flushable`, `cdn_flush_at`, `cdn_status`, `updated_at`, `created_at`) VALUES
(2, 'icon.png', NULL, 1, 'sonata.media.provider.image', 1, '77573ce803610c7624df235f5c170765ddc0159a.png', '{"filename":"icon.png"}', 512, 512, NULL, 'image/png', 70237, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-04 16:30:00', '2017-12-04 16:30:00'),
(3, '1512385755259.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'ea92f473ecfdc073078c11de7c2588f512ccc6b6.jpeg', '{"filename":"1512385755259.jpg"}', 267, 267, NULL, 'image/jpeg', 7428, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-04 17:01:12', '2017-12-04 17:01:12'),
(4, '1512387491801.jpg', NULL, 1, 'sonata.media.provider.image', 1, '346e378928e453c9c592c4420926baef955546b8.jpeg', '{"filename":"1512387491801.jpg"}', 200, 200, NULL, 'image/jpeg', 4299, NULL, NULL, 'event', NULL, NULL, NULL, '2017-12-04 17:08:14', '2017-12-04 17:08:14'),
(5, '1512388517206.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'fcca4634e3d0272f92c83c453ae227ba5a1123cd.jpeg', '{"filename":"1512388517206.jpg"}', 200, 200, NULL, 'image/jpeg', 4299, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-04 17:25:21', '2017-12-04 17:25:21'),
(7, '1512451908699.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'b3ea7da7bcfe07d3c8b672a327a10ea09ef913f9.jpeg', '{"filename":"1512451908699.jpg"}', 200, 200, NULL, 'image/jpeg', 3638, NULL, NULL, 'event', NULL, NULL, NULL, '2017-12-05 11:01:53', '2017-12-05 11:01:53'),
(8, '1512388517206.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'ebd6f6cc70682cce43a61de228230e74aff3e639.jpeg', '{"filename":"1512388517206.jpg"}', 200, 200, NULL, 'image/jpeg', 4299, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 11:05:05', '2017-12-05 11:05:05'),
(9, '1512451727480.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'e6d816154e0f8deb87412e47f70fd600cfd88b58.jpeg', '{"filename":"1512451727480.jpg"}', 267, 267, NULL, 'image/jpeg', 7459, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 11:07:34', '2017-12-05 11:07:34'),
(10, '1512385755259.jpg', NULL, 1, 'sonata.media.provider.image', 1, '261d07a09ffabde0e8574702633f02ad57cbd7e9.jpeg', '{"filename":"1512385755259.jpg"}', 267, 267, NULL, 'image/jpeg', 7428, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 11:07:34', '2017-12-05 11:07:34'),
(11, '1512451727480.jpg', NULL, 1, 'sonata.media.provider.image', 1, '8127935f4041e613f858af298682bae53dcd41aa.jpeg', '{"filename":"1512451727480.jpg"}', 267, 267, NULL, 'image/jpeg', 7459, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 11:07:52', '2017-12-05 11:07:52'),
(12, '1512388517206.jpg', NULL, 1, 'sonata.media.provider.image', 1, '7c2fe14a9abff5b006dab784738c4ed57343b655.jpeg', '{"filename":"1512388517206.jpg"}', 200, 200, NULL, 'image/jpeg', 4299, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 11:07:52', '2017-12-05 11:07:52'),
(13, '1512387491801.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'bd1785bcf101f0b16022619551f4d064c3af560e.jpeg', '{"filename":"1512387491801.jpg"}', 200, 200, NULL, 'image/jpeg', 4299, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 11:07:52', '2017-12-05 11:07:52'),
(15, '1512451727480.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'ad362f585572306fcd01b7691efc80f9d560cd28.jpeg', '{"filename":"1512451727480.jpg"}', 267, 267, NULL, 'image/jpeg', 7459, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 12:08:30', '2017-12-05 12:08:30'),
(16, '1512388517206.jpg', NULL, 1, 'sonata.media.provider.image', 1, '3f8959184c660af3dc939d235ea0063fa5556d50.jpeg', '{"filename":"1512388517206.jpg"}', 200, 200, NULL, 'image/jpeg', 4299, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 12:08:30', '2017-12-05 12:08:30'),
(17, '1512451727480.jpg', NULL, 1, 'sonata.media.provider.image', 1, '98ff69991e9b78ff6bd6405e96720c5b791e728e.jpeg', '{"filename":"1512451727480.jpg"}', 267, 267, NULL, 'image/jpeg', 7459, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 12:08:46', '2017-12-05 12:08:46'),
(18, '1512388517206.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'c2e229d560b678f0500fe6e806a7cce19136c444.jpeg', '{"filename":"1512388517206.jpg"}', 200, 200, NULL, 'image/jpeg', 4299, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 12:08:46', '2017-12-05 12:08:46'),
(19, '1512387491801.jpg', NULL, 1, 'sonata.media.provider.image', 1, '7c9d1cd58f95f7bb69aaef1f23d0481aaee518a1.jpeg', '{"filename":"1512387491801.jpg"}', 200, 200, NULL, 'image/jpeg', 4299, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 12:08:46', '2017-12-05 12:08:46'),
(21, 'IMG_20171205_105933.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'fdff51f9f566e964e2261c8c8fc6867948ebca69.jpeg', '{"filename":"IMG_20171205_105933.jpg"}', 2592, 4608, NULL, 'image/jpeg', 2078050, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 12:11:09', '2017-12-05 12:11:09'),
(25, '1512458789149.jpg', NULL, 1, 'sonata.media.provider.image', 1, '3c7957f4e4cfc838a08fe672ac480ba7cba7ccda.jpeg', '{"filename":"1512458789149.jpg"}', 200, 200, NULL, 'image/jpeg', 3372, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 12:56:32', '2017-12-05 12:56:32'),
(26, '1512458789149.jpg', NULL, 1, 'sonata.media.provider.image', 1, '32fad534ebffa653119b05d64ef90eeebb46d3c7.jpeg', '{"filename":"1512458789149.jpg"}', 200, 200, NULL, 'image/jpeg', 3372, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 12:57:05', '2017-12-05 12:57:05'),
(27, '1512451908699.jpg', NULL, 1, 'sonata.media.provider.image', 1, '1e3606897c1d37126f1f611ec134b18c07f587e1.jpeg', '{"filename":"1512451908699.jpg"}', 200, 200, NULL, 'image/jpeg', 3638, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 13:13:43', '2017-12-05 13:13:43'),
(28, '1512451535118.jpg', NULL, 1, 'sonata.media.provider.image', 1, '132207751d8aaf13623388b090484231f7f0ef7d.jpeg', '{"filename":"1512451535118.jpg"}', 441, 268, NULL, 'image/jpeg', 6583, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 13:13:43', '2017-12-05 13:13:43'),
(29, '1512451503737.jpg', NULL, 1, 'sonata.media.provider.image', 1, '1512a449c71cfaa7e725835f2425b3db0a48c4af.jpeg', '{"filename":"1512451503737.jpg"}', 441, 268, NULL, 'image/jpeg', 6583, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 13:13:44', '2017-12-05 13:13:44'),
(30, '1512460109628.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'b59b2e778544b397ff18e752ab68dc08959ef42f.jpeg', '{"filename":"1512460109628.jpg"}', 200, 200, NULL, 'image/jpeg', 5053, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-05 13:18:34', '2017-12-05 13:18:34'),
(31, '1512460677830.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'b2009ce4abc9c34904c582d1882a6150363cb48e.jpeg', '{"filename":"1512460677830.jpg"}', 200, 200, NULL, 'image/jpeg', 3510, NULL, NULL, 'event', NULL, NULL, NULL, '2017-12-05 13:27:59', '2017-12-05 13:27:59'),
(32, 'icon.png', NULL, 1, 'sonata.media.provider.image', 1, '0330df2b4f1418159e8b0d5ee4c87c365c60a948.png', '{"filename":"icon.png"}', 512, 512, NULL, 'image/png', 70237, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-12 13:59:22', '2017-12-12 13:59:22'),
(33, 'icon.png', NULL, 1, 'sonata.media.provider.image', 1, 'e3047ada733e567711ac68be8876acf568e2203a.png', '{"filename":"icon.png"}', 512, 512, NULL, 'image/png', 70237, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-12 18:18:10', '2017-12-12 18:18:10'),
(34, 'icon.png', NULL, 1, 'sonata.media.provider.image', 1, '185928317efb81d0327c48588fb30bfd7df8eb32.png', '{"filename":"icon.png"}', 512, 512, NULL, 'image/png', 70237, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-12 18:18:24', '2017-12-12 18:18:24'),
(35, '1513926213.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'faf198a1a12fb11de7e95142c613bc600ce2692c.jpeg', '{"filename":"1513926213.jpg"}', 200, 200, NULL, 'image/jpeg', 8252, NULL, NULL, 'default', NULL, NULL, NULL, '2017-12-22 12:33:33', '2017-12-22 12:33:33'),
(39, 'index.png', NULL, 1, 'sonata.media.provider.image', 1, '05f9d1d18cf6a2d40b616db3755089233cc90d1d.png', '{"filename":"index.png"}', 224, 225, NULL, 'image/png', 14311, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-05 17:31:05', '2018-01-05 17:31:05'),
(40, 'bg-imae.jpg', NULL, 1, 'sonata.media.provider.image', 1, '977b3935eab9d87eb62cb2615eb8e27979f7b255.jpeg', '{"filename":"bg-imae.jpg"}', 490, 360, NULL, 'image/jpeg', 26307, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-10 11:50:34', '2018-01-10 11:50:34'),
(72, 'yellow-appointment-reminders-32.png', NULL, 1, 'sonata.media.provider.image', 1, '02edffb313054cf8a8810fb1c37e336a38fa2feb.png', '{"filename":"yellow-appointment-reminders-32.png"}', 32, 32, NULL, 'image/png', 486, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-10 12:47:54', '2018-01-10 12:47:54'),
(74, 'yellow-appointment-reminders-32.png', NULL, 1, 'sonata.media.provider.image', 1, '213bf075d9ccc3c3728a6f0cb5c55287c93cb211.png', '{"filename":"yellow-appointment-reminders-32.png"}', 32, 32, NULL, 'image/png', 486, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-10 12:54:42', '2018-01-10 12:54:42'),
(75, 'yellow-appointment-reminders-32.png', NULL, 1, 'sonata.media.provider.image', 1, '8544fd81de7daae5a869cc945eb9d2288a3a0ce0.png', '{"filename":"yellow-appointment-reminders-32.png"}', 32, 32, NULL, 'image/png', 486, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-10 12:56:26', '2018-01-10 12:56:26'),
(76, '1515569296417.jpg', NULL, 1, 'sonata.media.provider.image', 1, '6fbda02a048bdc8602975cb55e2c2548c87b7e08.jpeg', '{"filename":"1515569296417.jpg"}', 178, 178, NULL, 'image/jpeg', 4270, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-10 12:59:00', '2018-01-10 12:59:00'),
(77, 'IMG-20180112-WA0000.jpg', NULL, 1, 'sonata.media.provider.image', 1, '9d780f25677b623042ed2eb8835c8def46f09a6a.jpeg', '{"filename":"IMG-20180112-WA0000.jpg"}', 720, 673, NULL, 'image/jpeg', 77015, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-12 10:26:54', '2018-01-12 10:26:54'),
(78, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'd8be27c28a8be3f9ba99896253649983fe99ff45.png', '{"filename":"file.jpg"}', 750, 500, NULL, 'image/png', 835764, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-12 10:27:23', '2018-01-12 10:27:23'),
(79, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, '793d2e750ad91bbef5165cd4300302c7f76fd41d.png', '{"filename":"file.jpg"}', 750, 496, NULL, 'image/png', 968885, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-12 10:37:52', '2018-01-12 10:37:52'),
(80, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, '8de83bffec1c613e99c35ddeaeae62c9d96bc58a.png', '{"filename":"file.jpg"}', 750, 496, NULL, 'image/png', 759890, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-12 10:40:38', '2018-01-12 10:40:38'),
(81, 'Screenshot_20171229-152048.png', NULL, 1, 'sonata.media.provider.image', 1, '404b9941c3c25525d0b16b0b9ccfb0a241e33ad1.png', '{"filename":"Screenshot_20171229-152048.png"}', 1080, 1920, NULL, 'image/png', 1257297, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-15 13:02:23', '2018-01-15 13:02:23'),
(82, 'Screenshot_20171228-153223.png', NULL, 1, 'sonata.media.provider.image', 1, 'b063a36fe7ac63203ee113d732daab11d5d410be.png', '{"filename":"Screenshot_20171228-153223.png"}', 1080, 1920, NULL, 'image/png', 1247825, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-15 13:03:30', '2018-01-15 13:03:30'),
(83, '1513332963450.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'e61e35169510ffc17250de0214b1b9d12f3f4444.jpeg', '{"filename":"1513332963450.jpg"}', 441, 441, NULL, 'image/jpeg', 6497, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-15 13:03:32', '2018-01-15 13:03:32'),
(84, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'f63a9132f906174d1e2735a8ea681f7d59473f64.png', '{"filename":"file.jpg"}', 750, 750, NULL, 'image/png', 998876, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-15 13:05:16', '2018-01-15 13:05:16'),
(85, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, '597d133a7dcb38279d93b651df64285ffe7799c3.png', '{"filename":"file.jpg"}', 748, 750, NULL, 'image/png', 899090, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-15 13:10:42', '2018-01-15 13:10:42'),
(86, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'f8d2ff622c8cc82ff110bf9320903c06445f61a5.png', '{"filename":"file.jpg"}', 750, 1334, NULL, 'image/png', 260944, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-15 13:16:50', '2018-01-15 13:16:50'),
(87, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, '28d92c8466d7bd8de518d5ff9377e22aa06a85b1.png', '{"filename":"file.jpg"}', 750, 1334, NULL, 'image/png', 265940, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-15 13:18:54', '2018-01-15 13:18:54'),
(88, '1516170643.jpg', NULL, 1, 'sonata.media.provider.image', 1, '9a47a46c644b25fce7cadf2f0c5c862bbd70e520.jpeg', '{"filename":"1516170643.jpg"}', 200, 200, NULL, 'image/jpeg', 9128, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-17 12:00:43', '2018-01-17 12:00:43'),
(89, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'aef55a197dc706ab74ecab3d63bf88a7fc549328.jpeg', '{"filename":"file.jpg"}', 2448, 2448, NULL, 'image/jpeg', 392376, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-30 16:01:28', '2018-01-30 16:01:28'),
(90, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, '5c1172aaf42292d108b16df295f0987f6ad9ad3f.jpeg', '{"filename":"file.jpg"}', 1836, 3264, NULL, 'image/jpeg', 258426, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-30 16:01:30', '2018-01-30 16:01:30'),
(91, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'a9107415973c8320672ac645b501976ac4fd8ba5.jpeg', '{"filename":"file.jpg"}', 750, 1334, NULL, 'image/jpeg', 80899, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-30 16:01:31', '2018-01-30 16:01:31'),
(92, '1517402477.jpg', NULL, 1, 'sonata.media.provider.image', 1, '0c0fa341cc653ea1497fa908c1175f23136d3438.jpeg', '{"filename":"1517402477.jpg"}', 200, 200, NULL, 'image/jpeg', 9341, NULL, NULL, 'default', NULL, NULL, NULL, '2018-01-31 18:11:17', '2018-01-31 18:11:17'),
(93, 'profile_avatar.jpg', NULL, 1, 'sonata.media.provider.image', 1, '81efbe6791313b7cbf3dbe56e523972d31972035.jpeg', '{"filename":"profile_avatar.jpg"}', 399, 399, NULL, 'image/jpeg', 47423, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-01 11:16:50', '2018-02-01 11:16:50'),
(94, '1516713345925.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'fe68fc01dfd04ec22903a19d5170b8c05916096c.jpeg', '{"filename":"1516713345925.jpg"}', 499, 499, NULL, 'image/jpeg', 25457, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-01 11:16:50', '2018-02-01 11:16:50'),
(95, 'postImage-1499.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'd6d3999b2866b2627cde003fdada84180828ffd7.jpeg', '{"filename":"postImage-1499.jpg"}', 500, 500, NULL, 'image/jpeg', 81903, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-01 11:16:50', '2018-02-01 11:16:50'),
(96, '1517464517783.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'a1806a46772b14f0231f57a84cf0480946bce46b.jpeg', '{"filename":"1517464517783.jpg"}', 200, 200, NULL, 'image/jpeg', 3798, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-01 11:28:25', '2018-02-01 11:28:25'),
(97, '1517465235014.jpg', NULL, 1, 'sonata.media.provider.image', 1, '09e3b1f296a88948c37ebdfc79f8f5a1ab7911e4.jpeg', '{"filename":"1517465235014.jpg"}', 200, 200, NULL, 'image/jpeg', 3257, NULL, NULL, 'event', NULL, NULL, NULL, '2018-02-01 11:37:16', '2018-02-01 11:37:16'),
(98, '1517465456653.jpg', NULL, 1, 'sonata.media.provider.image', 1, '2bec512f8f1876be31c2b204f85235ba00cf3901.jpeg', '{"filename":"1517465456653.jpg"}', 200, 200, NULL, 'image/jpeg', 3708, NULL, NULL, 'event', NULL, NULL, NULL, '2018-02-01 11:40:58', '2018-02-01 11:40:58'),
(99, '1517465580849.jpg', NULL, 1, 'sonata.media.provider.image', 1, '21a92db7e3b068c75c8d15383733ff3dae20a0b6.jpeg', '{"filename":"1517465580849.jpg"}', 200, 200, NULL, 'image/jpeg', 5481, NULL, NULL, 'event', NULL, NULL, NULL, '2018-02-01 11:43:02', '2018-02-01 11:43:02'),
(100, '1517466354.jpg', NULL, 1, 'sonata.media.provider.image', 1, '99637ce0efc4de13050fa41fb48879551dec7828.jpeg', '{"filename":"1517466354.jpg"}', 200, 200, NULL, 'image/jpeg', 2550, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-01 11:55:54', '2018-02-01 11:55:54'),
(101, '1517467121840.jpg', NULL, 1, 'sonata.media.provider.image', 1, '813e08fe5c05a9ec464660338289e93ceaf5165b.jpeg', '{"filename":"1517467121840.jpg"}', 200, 200, NULL, 'image/jpeg', 3677, NULL, NULL, 'event', NULL, NULL, NULL, '2018-02-01 12:08:43', '2018-02-01 12:08:43'),
(102, '1517467218706.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'a1186f616f408bf76e48938ac8210302b5844aaa.jpeg', '{"filename":"1517467218706.jpg"}', 200, 200, NULL, 'image/jpeg', 7965, NULL, NULL, 'event', NULL, NULL, NULL, '2018-02-01 12:10:20', '2018-02-01 12:10:20'),
(103, '1517465456653.jpg', NULL, 1, 'sonata.media.provider.image', 1, '88c265e1787c4eff05ce98fc79308dc1e9635c20.jpeg', '{"filename":"1517465456653.jpg"}', 200, 200, NULL, 'image/jpeg', 3708, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-01 13:13:49', '2018-02-01 13:13:49'),
(104, '1516883246253.jpg', NULL, 1, 'sonata.media.provider.image', 1, '418e52867c2f6fefd776c47f831130e86b1e0f85.jpeg', '{"filename":"1516883246253.jpg"}', 792, 792, NULL, 'image/jpeg', 59609, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-01 13:18:48', '2018-02-01 13:18:48'),
(105, '1517471427973.jpg', NULL, 1, 'sonata.media.provider.image', 1, '4e5e6cab3c5d8cb6c22653fe1fb7bec15da42eb6.jpeg', '{"filename":"1517471427973.jpg"}', 200, 200, NULL, 'image/jpeg', 7643, NULL, NULL, 'event', NULL, NULL, NULL, '2018-02-01 13:20:29', '2018-02-01 13:20:29'),
(106, '1517471860155.jpg', NULL, 1, 'sonata.media.provider.image', 1, '2433f0553ecc34637a8a77794ede3c3c10483ef2.jpeg', '{"filename":"1517471860155.jpg"}', 1000, 726, NULL, 'image/jpeg', 49582, NULL, NULL, 'event', NULL, NULL, NULL, '2018-02-01 13:27:42', '2018-02-01 13:27:42'),
(107, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'e574c968be0d509da05bec66793daa91a7911408.jpeg', '{"filename":"file.jpg"}', 750, 500, NULL, 'image/jpeg', 78830, NULL, NULL, 'event', NULL, NULL, NULL, '2018-02-02 11:41:35', '2018-02-02 11:41:35'),
(108, '1517555944.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'e320903dea33e5810830fb67d5bd016c26d2863f.jpeg', '{"filename":"1517555944.jpg"}', 200, 200, NULL, 'image/jpeg', 9341, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-02 12:49:05', '2018-02-02 12:49:05'),
(109, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, '891a1cf357c480260cc1278a7cd23e7bd8d1cd10.png', '{"filename":"file.jpg"}', 750, 750, NULL, 'image/png', 1093430, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-02 12:51:20', '2018-02-02 12:51:20'),
(110, '1517556186.jpg', NULL, 1, 'sonata.media.provider.image', 1, '4a58a05e8d28489f5bbad4e2acd7f92a1a6115f8.jpeg', '{"filename":"1517556186.jpg"}', 200, 200, NULL, 'image/jpeg', 2638, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-02 12:53:06', '2018-02-02 12:53:06'),
(111, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'bf5fecf74d8fae37f6732add404073411b157194.jpeg', '{"filename":"file.jpg"}', 2448, 2448, NULL, 'image/jpeg', 423128, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-02 13:31:21', '2018-02-02 13:31:21'),
(112, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'a1be32d72886c2dc76dfe45e1424f83e753f9dd3.jpeg', '{"filename":"file.jpg"}', 2448, 2448, NULL, 'image/jpeg', 392376, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-02 13:31:22', '2018-02-02 13:31:22'),
(113, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, '74947dbf09b2cf8fe8a448252da6cf14cc126221.jpeg', '{"filename":"file.jpg"}', 2448, 2448, NULL, 'image/jpeg', 392376, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-02 13:31:23', '2018-02-02 13:31:23'),
(114, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, '0f6f646f9815399a370a2f142b9944f32e3ebf9e.jpeg', '{"filename":"file.jpg"}', 750, 750, NULL, 'image/jpeg', 61679, NULL, NULL, 'event', NULL, NULL, NULL, '2018-02-02 14:46:22', '2018-02-02 14:46:22'),
(115, 'Screenshot_20171228-153223.png', NULL, 1, 'sonata.media.provider.image', 1, 'af8f33bfd32f3c9d5a3261d4663e19dd78f7e453.png', '{"filename":"Screenshot_20171228-153223.png"}', 1080, 1920, NULL, 'image/png', 1247825, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-02 15:03:10', '2018-02-02 15:03:10'),
(116, '1517564444031.jpg', NULL, 1, 'sonata.media.provider.image', 1, '6dcca02accb00c1e48869f0b159a3634d1ed4849.jpeg', '{"filename":"1517564444031.jpg"}', 636, 598, NULL, 'image/jpeg', 50901, NULL, NULL, 'event', NULL, NULL, NULL, '2018-02-02 15:10:45', '2018-02-02 15:10:45'),
(117, '1517643323456.jpg', NULL, 1, 'sonata.media.provider.image', 1, '353cd39b0ef1e24829da6267358b3906136a8dd8.jpeg', '{"filename":"1517643323456.jpg"}', 200, 200, NULL, 'image/jpeg', 3407, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-03 13:05:30', '2018-02-03 13:05:30'),
(118, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'ecca38885226e58e3469161f1f3f42f80b168d39.png', '{"filename":"file.jpg"}', 750, 500, NULL, 'image/png', 830278, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-03 15:29:28', '2018-02-03 15:29:28'),
(119, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, '7c273d13ad86853ae2ef963485d1129962135c1b.jpeg', '{"filename":"file.jpg"}', 3000, 2002, NULL, 'image/jpeg', 1050168, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-03 16:24:15', '2018-02-03 16:24:15'),
(120, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, 'adcc5fe54ad6b2712077966108a42b3f57e8b208.jpeg', '{"filename":"file.jpg"}', 4288, 2848, NULL, 'image/jpeg', 835611, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-03 16:24:17', '2018-02-03 16:24:17'),
(121, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, '72f4772add9e7adbe00713cf615674e5a9013f33.jpeg', '{"filename":"file.jpg"}', 4288, 2848, NULL, 'image/jpeg', 692289, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-03 16:24:20', '2018-02-03 16:24:20'),
(122, 'file.jpg', NULL, 1, 'sonata.media.provider.image', 1, '8a042bb82e142fabb41e383d53b5e22119a53007.jpeg', '{"filename":"file.jpg"}', 750, 750, NULL, 'image/jpeg', 54064, NULL, NULL, 'event', NULL, NULL, NULL, '2018-02-03 17:34:53', '2018-02-03 17:34:53'),
(123, 'IMG_5159.JPG', NULL, 1, 'sonata.media.provider.image', 1, 'bea876339442e1c5d2e5a14374d3d908e9844edf.jpeg', '{"filename":"IMG_5159.JPG"}', 3264, 2448, NULL, 'image/jpeg', 1198531, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-06 12:58:28', '2018-02-06 12:58:28'),
(124, '1517643300327.jpg', NULL, 1, 'sonata.media.provider.image', 1, '7f6da3fb6b1614fe0a4c2e0e9513045d902ede4a.jpeg', '{"filename":"1517643300327.jpg"}', 200, 200, NULL, 'image/jpeg', 2919, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-06 15:28:11', '2018-02-06 15:28:11'),
(125, 'postImage-1499.jpg', NULL, 1, 'sonata.media.provider.image', 1, '0254392d404c237c443eb4cf3142999b1305b87d.jpeg', '{"filename":"postImage-1499.jpg"}', 500, 500, NULL, 'image/jpeg', 81903, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-06 15:28:11', '2018-02-06 15:28:11'),
(126, '1516361771249.jpg', NULL, 1, 'sonata.media.provider.image', 1, '5cfd6756b20345854eb0efbc2e2f470cd62d7de4.jpeg', '{"filename":"1516361771249.jpg"}', 1168, 1167, NULL, 'image/jpeg', 113825, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-06 15:28:11', '2018-02-06 15:28:11'),
(127, 'IMG_5102.JPG', NULL, 1, 'sonata.media.provider.image', 1, 'e67eac77e91c0362637251f9ede2a9a94b2951eb.jpeg', '{"filename":"IMG_5102.JPG"}', 1278, 720, NULL, 'image/jpeg', 165587, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-06 15:42:02', '2018-02-06 15:42:02'),
(128, 'IMG_5103.JPG', NULL, 1, 'sonata.media.provider.image', 1, '883a4a88536d5e67f4b03b214a37bde678bde9bc.jpeg', '{"filename":"IMG_5103.JPG"}', 1278, 720, NULL, 'image/jpeg', 150559, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-06 15:42:03', '2018-02-06 15:42:03'),
(129, 'IMG_5106.JPG', NULL, 1, 'sonata.media.provider.image', 1, 'f7075758471c30dd46d318d8706752754079a4b7.jpeg', '{"filename":"IMG_5106.JPG"}', 1278, 720, NULL, 'image/jpeg', 161430, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-06 15:42:03', '2018-02-06 15:42:03'),
(130, 'IMG_5164.JPG', NULL, 1, 'sonata.media.provider.image', 1, '5b6a19c860f178510f5b5edb1bfee72df5b1e02b.jpeg', '{"filename":"IMG_5164.JPG"}', 720, 1278, NULL, 'image/jpeg', 119761, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-06 15:45:29', '2018-02-06 15:45:29'),
(131, 'IMG_5165.JPG', NULL, 1, 'sonata.media.provider.image', 1, 'a31614caf6a1c8a513c0dc61c0cc695f3ff7f066.jpeg', '{"filename":"IMG_5165.JPG"}', 3264, 2448, NULL, 'image/jpeg', 1416161, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-06 15:45:29', '2018-02-06 15:45:29'),
(132, 'IMG_5159.JPG', NULL, 1, 'sonata.media.provider.image', 1, '919b136b2fbfc27bb605a537011b5aeac2b6eb89.jpeg', '{"filename":"IMG_5159.JPG"}', 3264, 2448, NULL, 'image/jpeg', 1198531, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-06 15:45:31', '2018-02-06 15:45:31'),
(133, 'IMG_5090-1.JPG', NULL, 1, 'sonata.media.provider.image', 1, '6bd41987f5a84c3b1c610fcbed6683b77768540c.jpeg', '{"filename":"IMG_5090-1.JPG"}', 3264, 2448, NULL, 'image/jpeg', 1094906, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-12 12:39:28', '2018-02-12 12:39:28'),
(134, 'IMG_5092-1.JPG', NULL, 1, 'sonata.media.provider.image', 1, '3a4fbe224e50955fa0a7d4f41989d1550ea51445.jpeg', '{"filename":"IMG_5092-1.JPG"}', 3264, 2448, NULL, 'image/jpeg', 1134955, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-12 12:39:30', '2018-02-12 12:39:30'),
(135, 'IMG_5093-1.JPG', NULL, 1, 'sonata.media.provider.image', 1, '0012848a96049acdee540499651a38024ecc2199.jpeg', '{"filename":"IMG_5093-1.JPG"}', 3264, 2448, NULL, 'image/jpeg', 1127204, NULL, NULL, 'default', NULL, NULL, NULL, '2018-02-12 12:39:33', '2018-02-12 12:39:33'),
(136, 'BCaW71520921784.jpg', NULL, 1, 'sonata.media.provider.image', 1, '29900ae74d0de44b8e1afa2b2dace4fcf38b2e8c.jpeg', '{"filename":"BCaW71520921784.jpg"}', 860, 593, NULL, 'image/jpeg', 268275, NULL, NULL, 'default', NULL, NULL, NULL, '2018-03-13 11:30:05', '2018-03-13 11:30:05'),
(137, '1520940034.jpg', NULL, 1, 'sonata.media.provider.image', 1, '80c7f54108cc37e1620dd4419cae879ba642a933.jpeg', '{"filename":"1520940034.jpg"}', 200, 200, NULL, 'image/jpeg', 2638, NULL, NULL, 'default', NULL, NULL, NULL, '2018-03-13 16:50:34', '2018-03-13 16:50:34');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `thread_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `thread_id`, `sender_id`, `body`, `created_at`) VALUES
(3, 3, 92, 'VGFuZ28gY2hhcmxpZSBoYXMgaW52aXRlZCB5b3UgdG8gY2hhdC4=', '2018-02-01 11:30:56'),
(5, 5, 95, 'VGVzdCBoYXMgaW52aXRlZCB5b3UgdG8gY2hhdC4=', '2018-02-01 12:22:14'),
(6, 5, 92, 'VGFuZ28gY2hhcmxpZSBoYXMgYWNjZXB0ZWQgeW91ciByZXF1ZXN0Lg==', '2018-02-01 12:23:49'),
(7, 5, 92, 'SGk=', '2018-02-01 12:24:30'),
(8, 5, 92, 'SGVsbG8=', '2018-02-01 12:24:46'),
(9, 6, 92, 'VGFuZ28gY2hhcmxpZSBoYXMgaW52aXRlZCB5b3UgdG8gY2hhdC4=', '2018-02-01 12:25:59'),
(10, 5, 95, 'SGk=', '2018-02-01 12:26:44'),
(11, 5, 95, 'SGVsbG8=', '2018-02-01 12:26:52'),
(12, 7, 98, 'UmFodWwgaGFzIGludml0ZWQgeW91IHRvIGNoYXQu', '2018-02-01 13:42:45'),
(13, 7, 91, 'QW5raXQgaGFzIGFjY2VwdGVkIHlvdXIgcmVxdWVzdC4=', '2018-02-01 13:43:06'),
(14, 7, 98, 'SGVsbG8=', '2018-02-01 13:43:28'),
(15, 7, 98, 'T2tvaw==', '2018-02-01 13:43:36'),
(16, 8, 104, 'TmlzaGFudCBoYXMgaW52aXRlZCB5b3UgdG8gY2hhdC4=', '2018-02-02 13:12:05'),
(17, 8, 92, 'VGFuZ28gY2hhcmxpZSBoYXMgYWNjZXB0ZWQgeW91ciByZXF1ZXN0Lg==', '2018-02-02 13:14:20'),
(18, 8, 92, 'SGk=', '2018-02-02 13:14:31'),
(19, 8, 104, 'SGVsbG8=', '2018-02-02 13:14:47'),
(20, 8, 92, 'SG93eiB1', '2018-02-02 13:16:13'),
(21, 8, 104, 'R29vZA==', '2018-02-02 13:18:37'),
(22, 9, 90, 'QW5raXQgaGFzIGludml0ZWQgeW91IHRvIGNoYXQu', '2018-02-02 15:06:47'),
(23, 7, 91, 'SGVsbG8=', '2018-02-02 15:08:33'),
(24, 9, 91, 'UmFodWwgaGFzIGFjY2VwdGVkIHlvdXIgcmVxdWVzdC4=', '2018-02-02 15:08:38'),
(25, 9, 91, 'SGlp', '2018-02-02 15:08:44'),
(26, 9, 91, 'R3Vk', '2018-02-02 15:08:51'),
(27, 9, 91, 'T2tvaw==', '2018-02-02 15:09:02'),
(28, 9, 90, 'R290IGl0', '2018-02-02 15:09:17'),
(29, 9, 91, 'RG9uZQ==', '2018-02-02 15:09:38'),
(30, 9, 90, 'Q2hlY2sgbm90aWZpY2F0aW9u', '2018-02-02 15:19:54'),
(31, 9, 90, 'T2tvaw==', '2018-02-02 15:41:14'),
(32, 7, 91, 'SG1tbQ==', '2018-02-02 15:48:14'),
(33, 9, 90, 'R290aXQ=', '2018-02-02 15:50:42'),
(34, 9, 90, 'T2tvaw==', '2018-02-02 15:50:50'),
(35, 10, 92, 'VGFuZ28gY2hhcmxpZSBoYXMgaW52aXRlZCB5b3UgdG8gY2hhdC4=', '2018-02-03 17:28:33'),
(36, 8, 92, 'SGVsbHBv', '2018-02-03 17:32:32'),
(37, 11, 91, 'UmFodWwgaGFzIGludml0ZWQgeW91IHRvIGNoYXQu', '2018-02-03 22:17:22'),
(38, 6, 87, 'S2FyYW4gSG9vZGEgaGFzIGFjY2VwdGVkIHlvdXIgcmVxdWVzdC4=', '2018-03-07 12:12:50'),
(39, 12, 124, 'UmVldCBoYXMgaW52aXRlZCB5b3UgdG8gY2hhdC4=', '2018-03-13 17:51:06');

-- --------------------------------------------------------

--
-- Table structure for table `MessageMeta`
--

CREATE TABLE `MessageMeta` (
  `id` int(11) NOT NULL,
  `message_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `thread_id` int(11) DEFAULT NULL,
  `isRead` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `MessageMeta`
--

INSERT INTO `MessageMeta` (`id`, `message_id`, `receiver_id`, `thread_id`, `isRead`) VALUES
(1, 7, 95, 5, 1),
(2, 8, 95, 5, 1),
(3, 10, 92, 5, 1),
(4, 11, 92, 5, 1),
(5, 14, 91, 7, 1),
(6, 15, 91, 7, 1),
(7, 18, 104, 8, 1),
(8, 19, 92, 8, 1),
(9, 20, 104, 8, 1),
(10, 21, 92, 8, 1),
(11, 23, 98, 7, 0),
(12, 25, 90, 9, 1),
(13, 26, 90, 9, 1),
(14, 27, 90, 9, 1),
(15, 28, 91, 9, 1),
(16, 29, 90, 9, 1),
(17, 30, 91, 9, 1),
(18, 31, 91, 9, 1),
(19, 32, 98, 7, 0),
(20, 33, 91, 9, 1),
(21, 34, 91, 9, 1),
(22, 36, 104, 8, 0);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `fundraiser_id` int(11) DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `metadata` longtext COLLATE utf8_unicode_ci NOT NULL,
  `is_read` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `user_id`, `receiver_id`, `post_id`, `event_id`, `fundraiser_id`, `type`, `created_at`, `updated_at`, `metadata`, `is_read`) VALUES
(1, 5, 5, NULL, 1, NULL, 'event_attend', '2017-12-04 17:23:45', '2017-12-04 17:23:45', '', 1),
(2, 6, 5, 4, NULL, NULL, 'comment_tag', '2017-12-05 10:56:24', '2017-12-05 10:56:24', '"Ankit kk "', 0),
(3, 6, 5, 4, NULL, NULL, 'comment_tag', '2017-12-05 10:56:31', '2017-12-05 10:56:31', '"@"', 0),
(4, 6, 5, 4, NULL, NULL, 'comment_tag', '2017-12-05 10:56:35', '2017-12-05 10:56:35', '"    "', 0),
(5, 6, 5, 4, NULL, NULL, 'comment_tag', '2017-12-05 10:56:47', '2017-12-05 10:56:47', '" \n\n\n\n\n\n\n\n\n   \n\n\n"', 0),
(6, 6, 5, 4, NULL, NULL, 'comment_tag', '2017-12-05 10:56:57', '2017-12-05 10:56:57', '"Test"', 0),
(7, 6, 5, 2, NULL, NULL, 'comment', '2017-12-05 10:57:55', '2017-12-05 10:57:55', '"@"', 0),
(8, 6, 5, 2, NULL, NULL, 'already_comment', '2017-12-05 10:57:55', '2017-12-05 10:57:55', '"@"', 0),
(9, 6, 5, 2, NULL, NULL, 'comment', '2017-12-05 10:58:22', '2017-12-05 10:58:22', '"@nishant\n"', 0),
(10, 6, 5, 2, NULL, NULL, 'already_comment', '2017-12-05 10:58:22', '2017-12-05 10:58:22', '"@nishant\n"', 0),
(11, 5, 6, 4, NULL, NULL, 'comment', '2017-12-05 13:16:03', '2017-12-05 13:16:03', '"Hello"', 0),
(12, 5, 6, 4, NULL, NULL, 'comment', '2017-12-05 13:16:31', '2017-12-05 13:16:31', '"Okok"', 0),
(13, 5, 6, NULL, 3, NULL, 'event_comment', '2017-12-05 13:17:04', '2017-12-05 13:17:04', '"Good"', 0),
(14, 6, 6, NULL, 4, NULL, 'event_attend', '2017-12-05 13:29:28', '2017-12-05 13:29:28', '', 0),
(15, 7, 6, 14, NULL, NULL, 'comment', '2017-12-05 13:39:01', '2017-12-05 13:39:01', '"Okk"', 0),
(16, 7, 6, 14, NULL, NULL, 'comment', '2017-12-05 13:39:11', '2017-12-05 13:39:11', '"Hhhh"', 0),
(17, 7, 6, 14, NULL, NULL, 'comment', '2017-12-05 14:44:51', '2017-12-05 14:44:51', '"hello"', 0),
(18, 7, 6, 14, NULL, NULL, 'comment', '2017-12-05 14:47:03', '2017-12-05 14:47:03', '"hello"', 0),
(19, 7, 5, 2, NULL, NULL, 'comment', '2017-12-05 16:23:53', '2017-12-05 16:23:53', '"Good"', 0),
(20, 7, 5, 2, NULL, NULL, 'comment', '2017-12-05 16:26:54', '2017-12-05 16:26:54', '"Good"', 0),
(21, 7, 5, 2, NULL, NULL, 'already_comment', '2017-12-05 16:26:54', '2017-12-05 16:26:54', '"Good"', 0),
(22, 7, 6, 2, NULL, NULL, 'already_comment', '2017-12-05 16:26:54', '2017-12-05 16:26:54', '"Good"', 0),
(23, 7, 6, 14, NULL, NULL, 'comment', '2017-12-05 16:29:16', '2017-12-05 16:29:16', '"Hello test comment"', 0),
(24, 7, 6, 14, NULL, NULL, 'already_comment', '2017-12-05 16:29:16', '2017-12-05 16:29:16', '"Hello test comment"', 0),
(25, 7, 6, NULL, 4, NULL, 'event_comment', '2017-12-05 16:31:57', '2017-12-05 16:31:57', '"Event first comment"', 0),
(26, 7, 6, NULL, 4, NULL, 'event_already_comment', '2017-12-05 16:31:57', '2017-12-05 16:31:57', '"Event first comment"', 0),
(27, 87, 5, 30, NULL, NULL, 'comment_tag', '2018-01-30 12:56:06', '2018-01-30 12:56:06', '"Hi Ankit kk"', 0),
(28, 87, 6, 29, NULL, NULL, 'comment_tag', '2018-01-30 13:01:01', '2018-01-30 13:01:01', '"Nishant"', 0),
(29, 87, 12, 29, NULL, NULL, 'comment', '2018-01-30 13:01:01', '2018-01-30 13:01:01', '"Nishant"', 0),
(30, 12, 87, 30, NULL, NULL, 'comment', '2018-01-30 14:54:34', '2018-01-30 14:54:34', '"Testing"', 0),
(31, 12, 87, 30, NULL, NULL, 'comment', '2018-01-30 15:15:06', '2018-01-30 15:15:06', '"Simran"', 0),
(32, 12, 87, 30, NULL, NULL, 'comment', '2018-01-30 15:15:28', '2018-01-30 15:15:28', '"Simran"', 0),
(33, 12, 87, 30, NULL, NULL, 'comment', '2018-01-30 15:25:12', '2018-01-30 15:25:12', '"Hello comment"', 0),
(34, 12, 87, NULL, 5, NULL, 'event_comment', '2018-01-30 15:26:17', '2018-01-30 15:26:17', '"Hello"', 0),
(35, 12, 87, NULL, 5, NULL, 'event_comment', '2018-01-30 15:26:22', '2018-01-30 15:26:22', '"Hello"', 0),
(36, 12, 87, NULL, 5, NULL, 'event_comment', '2018-01-30 15:27:23', '2018-01-30 15:27:23', '"Hello"', 0),
(37, 12, 87, 30, NULL, NULL, 'comment', '2018-01-30 15:27:33', '2018-01-30 15:27:33', '"Ok"', 0),
(38, 12, 87, 30, NULL, NULL, 'comment', '2018-01-30 15:32:37', '2018-01-30 15:32:37', '"hello okok"', 0),
(39, 12, 87, 30, NULL, NULL, 'comment', '2018-01-30 16:07:53', '2018-01-30 16:07:53', '"test"', 0),
(40, 12, 87, 30, NULL, NULL, 'comment', '2018-01-30 16:30:46', '2018-01-30 16:30:46', '"test"', 0),
(41, 12, 87, 30, NULL, NULL, 'already_comment', '2018-01-30 16:30:46', '2018-01-30 16:30:46', '"test"', 0),
(42, 12, 87, 30, NULL, NULL, 'comment', '2018-01-30 16:31:21', '2018-01-30 16:31:21', '"hello okok"', 0),
(43, 12, 87, 30, NULL, NULL, 'already_comment', '2018-01-30 16:31:22', '2018-01-30 16:31:22', '"hello okok"', 1),
(44, 12, 87, 30, NULL, NULL, 'comment', '2018-01-30 16:31:26', '2018-01-30 16:31:26', '"hello okok"', 1),
(45, 12, 87, 30, NULL, NULL, 'already_comment', '2018-01-30 16:31:27', '2018-01-30 16:31:27', '"hello okok"', 1),
(46, 12, 87, 30, NULL, NULL, 'comment', '2018-01-30 16:36:46', '2018-01-30 16:36:46', '"test"', 1),
(47, 12, 87, 30, NULL, NULL, 'already_comment', '2018-01-30 16:36:47', '2018-01-30 16:36:47', '"test"', 1),
(48, 12, 87, 30, NULL, NULL, 'comment', '2018-01-30 16:40:05', '2018-01-30 16:40:05', '"test"', 1),
(49, 12, 87, 30, NULL, NULL, 'already_comment', '2018-01-30 16:40:05', '2018-01-30 16:40:05', '"test"', 1),
(50, 12, 87, 30, NULL, NULL, 'comment', '2018-01-30 16:40:11', '2018-01-30 16:40:11', '"test"', 1),
(51, 12, 87, 30, NULL, NULL, 'already_comment', '2018-01-30 16:40:11', '2018-01-30 16:40:11', '"test"', 1),
(52, 92, 6, 32, NULL, NULL, 'comment_tag', '2018-02-01 11:11:40', '2018-02-01 11:11:40', '"Nishant test"', 0),
(53, 92, 6, 32, NULL, NULL, 'comment_tag', '2018-02-01 11:11:48', '2018-02-01 11:11:48', '"Test"', 0),
(54, 92, 6, 32, NULL, NULL, 'comment_tag', '2018-02-01 11:11:57', '2018-02-01 11:11:57', '"#"', 0),
(55, 92, 6, 32, NULL, NULL, 'comment_tag', '2018-02-01 11:12:01', '2018-02-01 11:12:01', '"@"', 0),
(56, 92, 6, 32, NULL, NULL, 'comment_tag', '2018-02-01 11:12:08', '2018-02-01 11:12:08', '"@dfgv"', 0),
(57, 92, 5, 32, NULL, NULL, 'comment_tag', '2018-02-01 11:13:24', '2018-02-01 11:13:24', '"Ankit kk "', 0),
(58, 92, 6, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:19:05', '2018-02-01 11:19:05', '"Nishant An..."', 0),
(59, 92, 5, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:19:05', '2018-02-01 11:19:05', '"Nishant An..."', 0),
(60, 92, 8, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:19:05', '2018-02-01 11:19:05', '"Nishant An..."', 1),
(61, 92, 12, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:19:06', '2018-02-01 11:19:06', '"Nishant An..."', 0),
(62, 92, 6, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:19:47', '2018-02-01 11:19:47', '"Nishant , Ankit kk "', 0),
(63, 92, 5, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:19:47', '2018-02-01 11:19:47', '"Nishant , Ankit kk "', 0),
(64, 92, 8, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:19:47', '2018-02-01 11:19:47', '"Nishant , Ankit kk "', 1),
(65, 92, 12, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:19:47', '2018-02-01 11:19:47', '"Nishant , Ankit kk "', 0),
(66, 92, 6, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:19:47', '2018-02-01 11:19:47', '"Nishant , Ankit kk "', 0),
(67, 92, 5, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:19:47', '2018-02-01 11:19:47', '"Nishant , Ankit kk "', 0),
(68, 92, 6, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:20:04', '2018-02-01 11:20:04', '"Tetstsgsbb..."', 0),
(69, 92, 5, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:20:04', '2018-02-01 11:20:04', '"Tetstsgsbb..."', 0),
(70, 92, 8, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:20:04', '2018-02-01 11:20:04', '"Tetstsgsbb..."', 1),
(71, 92, 12, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:20:04', '2018-02-01 11:20:04', '"Tetstsgsbb..."', 0),
(72, 92, 6, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:20:04', '2018-02-01 11:20:04', '"Tetstsgsbb..."', 0),
(73, 92, 5, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:20:04', '2018-02-01 11:20:04', '"Tetstsgsbb..."', 0),
(74, 92, 92, NULL, 6, NULL, 'event_comment_tag', '2018-02-01 11:40:03', '2018-02-01 11:40:03', '"@ Tango charlie "', 1),
(76, 92, 92, NULL, 9, NULL, 'event_attend', '2018-02-01 11:48:38', '2018-02-01 11:48:38', '', 1),
(77, 87, 92, 33, NULL, NULL, 'comment', '2018-02-01 11:58:35', '2018-02-01 11:58:35', '"Hi"', 1),
(78, 87, 92, 33, NULL, NULL, 'already_comment', '2018-02-01 11:58:35', '2018-02-01 11:58:35', '"Hi"', 1),
(79, 87, 6, 33, NULL, NULL, 'comment_tag', '2018-02-01 11:58:48', '2018-02-01 11:58:48', '"Nishant "', 0),
(80, 87, 92, 33, NULL, NULL, 'comment', '2018-02-01 11:58:48', '2018-02-01 11:58:48', '"Nishant "', 1),
(81, 87, 92, 33, NULL, NULL, 'already_comment', '2018-02-01 11:58:48', '2018-02-01 11:58:48', '"Nishant "', 1),
(82, 95, 92, 33, NULL, NULL, 'comment_tag', '2018-02-01 12:29:17', '2018-02-01 12:29:17', '"Tango charlie "', 1),
(83, 95, 92, 33, NULL, NULL, 'comment', '2018-02-01 12:29:17', '2018-02-01 12:29:17', '"Tango charlie "', 1),
(84, 95, 87, 33, NULL, NULL, 'already_comment', '2018-02-01 12:29:17', '2018-02-01 12:29:17', '"Tango charlie "', 1),
(85, 95, 92, 33, NULL, NULL, 'already_comment', '2018-02-01 12:29:18', '2018-02-01 12:29:18', '"Tango charlie "', 1),
(86, 95, 5, NULL, 9, NULL, 'event_invite', '2018-02-01 13:38:41', '2018-02-01 13:38:41', '', 0),
(87, 95, 91, NULL, 9, NULL, 'event_invite', '2018-02-01 13:38:41', '2018-02-01 13:38:41', '', 1),
(88, 95, 96, NULL, 9, NULL, 'event_invite', '2018-02-01 13:38:41', '2018-02-01 13:38:41', '', 0),
(89, 95, 8, NULL, 9, NULL, 'event_invite', '2018-02-01 13:38:41', '2018-02-01 13:38:41', '', 1),
(90, 95, 12, NULL, 9, NULL, 'event_invite', '2018-02-01 13:38:41', '2018-02-01 13:38:41', '', 0),
(92, 8, 92, 35, NULL, NULL, 'comment_tag', '2018-02-02 11:31:25', '2018-02-02 11:31:25', '"Tango charlie"', 1),
(93, 8, 92, 35, NULL, NULL, 'comment', '2018-02-02 11:31:25', '2018-02-02 11:31:25', '"Tango charlie"', 1),
(94, 92, 12, 28, NULL, NULL, 'comment', '2018-02-02 13:08:52', '2018-02-02 13:08:52', '"Add a comm..."', 0),
(95, 92, 12, 28, NULL, NULL, 'comment', '2018-02-02 13:09:50', '2018-02-02 13:09:50', '"Add a comm..."', 0),
(96, 92, 12, 28, NULL, NULL, 'comment', '2018-02-02 13:10:08', '2018-02-02 13:10:08', '"Add a comm..."', 0),
(97, 92, 12, 28, NULL, NULL, 'comment', '2018-02-02 13:11:42', '2018-02-02 13:11:42', '"@Nishant"', 0),
(98, 92, 92, 35, NULL, NULL, 'comment_tag', '2018-02-02 13:21:56', '2018-02-02 13:21:56', '"Tango charlie"', 1),
(99, 92, 8, 35, NULL, NULL, 'already_comment', '2018-02-02 13:21:57', '2018-02-02 13:21:57', '"Tango charlie"', 0),
(100, 92, 8, 35, NULL, NULL, 'already_comment', '2018-02-02 13:23:36', '2018-02-02 13:23:36', '"@Nishant"', 0),
(101, 104, 92, 41, NULL, NULL, 'comment', '2018-02-02 13:41:03', '2018-02-02 13:41:03', '"Hello"', 1),
(102, 104, 92, 41, NULL, NULL, 'already_comment', '2018-02-02 13:41:03', '2018-02-02 13:41:03', '"Hello"', 1),
(103, 91, 92, 41, NULL, NULL, 'comment', '2018-02-02 14:35:01', '2018-02-02 14:35:01', '"Gudd"', 1),
(104, 91, 92, 41, NULL, NULL, 'already_comment', '2018-02-02 14:35:02', '2018-02-02 14:35:02', '"Gudd"', 1),
(105, 91, 104, 41, NULL, NULL, 'already_comment', '2018-02-02 14:35:02', '2018-02-02 14:35:02', '"Gudd"', 0),
(106, 91, 92, 41, NULL, NULL, 'comment', '2018-02-02 14:40:01', '2018-02-02 14:40:01', '"Okok"', 1),
(107, 91, 92, 41, NULL, NULL, 'already_comment', '2018-02-02 14:40:01', '2018-02-02 14:40:01', '"Okok"', 1),
(108, 91, 104, 41, NULL, NULL, 'already_comment', '2018-02-02 14:40:01', '2018-02-02 14:40:01', '"Okok"', 0),
(109, 92, 92, NULL, 18, NULL, 'event_attend', '2018-02-02 14:50:20', '2018-02-02 14:50:20', '', 1),
(110, 92, 8, NULL, 18, NULL, 'event_invite', '2018-02-02 14:58:36', '2018-02-02 14:58:36', '', 0),
(111, 92, 91, NULL, 18, NULL, 'event_invite', '2018-02-02 14:58:36', '2018-02-02 14:58:36', '', 1),
(112, 92, 12, NULL, 18, NULL, 'event_invite', '2018-02-02 14:58:36', '2018-02-02 14:58:36', '', 0),
(113, 92, 96, NULL, 18, NULL, 'event_invite', '2018-02-02 14:58:36', '2018-02-02 14:58:36', '', 0),
(114, 92, 5, NULL, 18, NULL, 'event_invite', '2018-02-02 14:58:36', '2018-02-02 14:58:36', '', 0),
(115, 90, 91, 42, NULL, NULL, 'comment', '2018-02-02 15:04:04', '2018-02-02 15:04:04', '"Hi nice post...."', 1),
(116, 90, 91, 42, NULL, NULL, 'comment', '2018-02-02 15:04:31', '2018-02-02 15:04:31', '"Hello"', 1),
(117, 92, 91, 42, NULL, NULL, 'comment', '2018-02-02 15:07:07', '2018-02-02 15:07:07', '"Great post"', 1),
(118, 92, 90, 42, NULL, NULL, 'already_comment', '2018-02-02 15:07:07', '2018-02-02 15:07:07', '"Great post"', 0),
(120, 90, 91, 42, NULL, NULL, 'comment', '2018-02-02 15:13:30', '2018-02-02 15:13:30', '"Nice post.."', 1),
(121, 90, 92, 42, NULL, NULL, 'already_comment', '2018-02-02 15:13:30', '2018-02-02 15:13:30', '"Nice post.."', 1),
(124, 91, 90, 42, NULL, NULL, 'already_comment', '2018-02-02 15:14:50', '2018-02-02 15:14:50', '"Hmmmm"', 0),
(125, 91, 92, 42, NULL, NULL, 'already_comment', '2018-02-02 15:14:50', '2018-02-02 15:14:50', '"Hmmmm"', 1),
(126, 90, 91, 42, NULL, NULL, 'comment', '2018-02-02 15:17:14', '2018-02-02 15:17:14', '"Gudd"', 1),
(127, 90, 91, 42, NULL, NULL, 'already_comment', '2018-02-02 15:17:14', '2018-02-02 15:17:14', '"Gudd"', 1),
(128, 90, 92, 42, NULL, NULL, 'already_comment', '2018-02-02 15:17:14', '2018-02-02 15:17:14', '"Gudd"', 1),
(189, 91, 90, 42, NULL, NULL, 'already_comment', '2018-02-02 17:10:59', '2018-02-02 17:10:59', '"@"', 0),
(190, 91, 92, 42, NULL, NULL, 'already_comment', '2018-02-02 17:10:59', '2018-02-02 17:10:59', '"@"', 1),
(191, 91, 8, 42, NULL, NULL, 'comment_tag', '2018-02-02 17:11:07', '2018-02-02 17:11:07', '"Simran Jit"', 0),
(192, 91, 90, 42, NULL, NULL, 'already_comment', '2018-02-02 17:11:07', '2018-02-02 17:11:07', '"Simran Jit"', 0),
(193, 91, 92, 42, NULL, NULL, 'already_comment', '2018-02-02 17:11:08', '2018-02-02 17:11:08', '"Simran Jit"', 1),
(194, 91, 90, 42, NULL, NULL, 'already_comment', '2018-02-02 17:11:18', '2018-02-02 17:11:18', '"sdfasdfasdfads"', 0),
(195, 91, 92, 42, NULL, NULL, 'already_comment', '2018-02-02 17:11:19', '2018-02-02 17:11:19', '"sdfasdfasdfads"', 1),
(196, 91, 90, NULL, 21, NULL, 'event_comment', '2018-02-02 17:26:51', '2018-02-02 17:26:51', '"Am"', 0),
(197, 91, 90, NULL, 21, NULL, 'event_comment', '2018-02-02 17:28:43', '2018-02-02 17:28:43', '"hi"', 0),
(198, 91, 90, NULL, 21, NULL, 'event_comment', '2018-02-02 17:32:23', '2018-02-02 17:32:23', '"test"', 0),
(199, 91, 90, NULL, 21, NULL, 'event_comment', '2018-02-02 17:32:29', '2018-02-02 17:32:29', '"Ss"', 0),
(200, 91, 90, NULL, 21, NULL, 'event_comment', '2018-02-02 17:32:40', '2018-02-02 17:32:40', '"Add a comm..."', 0),
(201, 91, 90, NULL, 21, NULL, 'event_comment', '2018-02-02 17:36:03', '2018-02-02 17:36:03', '"hi"', 0),
(202, 91, 90, 42, NULL, NULL, 'already_comment', '2018-02-02 17:37:03', '2018-02-02 17:37:03', '"Rdd"', 0),
(203, 91, 92, 42, NULL, NULL, 'already_comment', '2018-02-02 17:37:03', '2018-02-02 17:37:03', '"Rdd"', 1),
(204, 91, 90, 42, NULL, NULL, 'already_comment', '2018-02-02 17:42:02', '2018-02-02 17:42:02', '"sdfasdf"', 0),
(205, 91, 92, 42, NULL, NULL, 'already_comment', '2018-02-02 17:42:02', '2018-02-02 17:42:02', '"sdfasdf"', 1),
(206, 91, 92, 41, NULL, NULL, 'comment', '2018-02-02 17:49:48', '2018-02-02 17:49:48', '"hi"', 1),
(207, 91, 92, 41, NULL, NULL, 'already_comment', '2018-02-02 17:49:48', '2018-02-02 17:49:48', '"hi"', 1),
(208, 91, 104, 41, NULL, NULL, 'already_comment', '2018-02-02 17:49:49', '2018-02-02 17:49:49', '"hi"', 0),
(209, 91, 92, 41, NULL, NULL, 'comment', '2018-02-02 17:53:17', '2018-02-02 17:53:17', '"Hi"', 1),
(210, 91, 92, 41, NULL, NULL, 'already_comment', '2018-02-02 17:53:18', '2018-02-02 17:53:18', '"Hi"', 1),
(211, 91, 104, 41, NULL, NULL, 'already_comment', '2018-02-02 17:53:18', '2018-02-02 17:53:18', '"Hi"', 0),
(212, 91, 96, NULL, 20, NULL, 'event_invite', '2018-02-03 16:35:20', '2018-02-03 16:35:20', '', 0),
(213, 91, 5, NULL, 20, NULL, 'event_invite', '2018-02-03 16:35:20', '2018-02-03 16:35:20', '', 0),
(214, 91, 117, NULL, 20, NULL, 'event_invite', '2018-02-03 16:35:20', '2018-02-03 16:35:20', '', 0),
(215, 92, 92, NULL, 25, NULL, 'event_attend', '2018-02-03 17:38:34', '2018-02-03 17:38:34', '', 0),
(216, 92, 91, 41, NULL, NULL, 'already_comment', '2018-02-03 17:48:30', '2018-02-03 17:48:30', '"@"', 1),
(217, 92, 104, 41, NULL, NULL, 'already_comment', '2018-02-03 17:48:30', '2018-02-03 17:48:30', '"@"', 0),
(218, 92, 91, 41, NULL, NULL, 'already_comment', '2018-02-03 17:48:35', '2018-02-03 17:48:35', '"-"', 1),
(219, 92, 104, 41, NULL, NULL, 'already_comment', '2018-02-03 17:48:35', '2018-02-03 17:48:35', '"-"', 0),
(220, 92, 91, 41, NULL, NULL, 'comment_tag', '2018-02-03 17:48:46', '2018-02-03 17:48:46', '"Rahul"', 1),
(221, 92, 91, 41, NULL, NULL, 'already_comment', '2018-02-03 17:48:46', '2018-02-03 17:48:46', '"Rahul"', 1),
(222, 92, 104, 41, NULL, NULL, 'already_comment', '2018-02-03 17:48:46', '2018-02-03 17:48:46', '"Rahul"', 0),
(223, 92, 118, 41, NULL, NULL, 'comment_tag', '2018-02-03 17:48:54', '2018-02-03 17:48:54', '"RamanUser"', 0),
(224, 92, 91, 41, NULL, NULL, 'already_comment', '2018-02-03 17:48:54', '2018-02-03 17:48:54', '"RamanUser"', 1),
(225, 92, 104, 41, NULL, NULL, 'already_comment', '2018-02-03 17:48:54', '2018-02-03 17:48:54', '"RamanUser"', 0),
(226, 87, 91, 44, NULL, NULL, 'comment', '2018-02-06 12:28:58', '2018-02-06 12:28:58', '"Hi"', 0),
(227, 87, 5, 44, NULL, NULL, 'comment_tag', '2018-02-06 12:50:11', '2018-02-06 12:50:11', '"Ankit kk "', 0),
(228, 87, 91, 44, NULL, NULL, 'comment', '2018-02-06 12:50:11', '2018-02-06 12:50:11', '"Ankit kk "', 0);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `location` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `is_published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `user_id`, `gallery_id`, `description`, `location`, `url`, `type`, `link_description`, `link_title`, `link_image`, `created_at`, `updated_at`, `is_deleted`, `is_published`) VALUES
(1, 5, 1, 'Hello my first post', NULL, '', 'image', '', '', '', '2017-12-04 17:01:12', '2017-12-04 17:01:12', 0, 1),
(2, 5, 2, 'Text post', NULL, '', '', '', '', '', '2017-12-04 17:06:26', '2017-12-04 17:06:26', 0, 1),
(3, 5, 3, 'Hello another post', NULL, '', '', '', '', '', '2017-12-05 10:53:08', '2017-12-05 10:53:08', 0, 0),
(4, 6, 4, 'Test', NULL, '', '', '', '', '', '2017-12-05 10:54:19', '2017-12-05 10:54:19', 0, 1),
(5, 5, 7, 'Text posttt', NULL, '', '', '', '', '', '2017-12-05 11:01:27', '2017-12-05 11:01:27', 0, 0),
(6, 5, 8, 'www.fb.com ', NULL, '', 'website', '', '', '', '2017-12-05 11:04:32', '2017-12-05 11:04:32', 0, 0),
(7, 5, 9, 'Hello', NULL, '', 'image', '', '', '', '2017-12-05 11:05:05', '2017-12-05 11:05:05', 0, 0),
(8, 5, 11, 'Album', NULL, '', 'gallery', '', '', '', '2017-12-05 11:07:34', '2017-12-05 11:07:34', 0, 0),
(9, 5, 12, 'Another album', NULL, '', 'gallery', '', '', '', '2017-12-05 11:07:52', '2017-12-05 11:07:52', 0, 0),
(10, 5, 15, 'Post', NULL, '', 'gallery', '', '', '', '2017-12-05 12:08:30', '2017-12-05 12:08:30', 0, 0),
(11, 5, 16, 'Albumm', NULL, '', 'gallery', '', '', '', '2017-12-05 12:08:46', '2017-12-05 12:08:46', 0, 0),
(12, 5, 18, 'Img', NULL, '', 'image', '', '', '', '2017-12-05 12:11:08', '2017-12-05 12:11:08', 0, 0),
(13, 5, 22, 'Img', NULL, '', 'image', '', '', '', '2017-12-05 12:57:05', '2017-12-05 12:57:05', 0, 0),
(14, 6, 24, 'Test', NULL, '', 'gallery', '', '', '', '2017-12-05 13:13:43', '2017-12-05 13:13:43', 0, 1),
(15, 1, 25, 'description', NULL, 'http://google', 'website', NULL, NULL, NULL, '2017-12-05 17:50:48', '2017-12-05 17:50:48', 0, 1),
(16, 8, 26, 'Hi', NULL, '', '', '', '', '', '2017-12-29 10:51:01', '2017-12-29 10:51:01', 0, 0),
(17, 12, 27, 'Hi', NULL, '', '', '', '', '', '2018-01-04 11:53:07', '2018-01-04 11:53:07', 0, 0),
(18, 12, 28, 'Hi', NULL, '', '', '', '', '', '2018-01-04 11:53:37', '2018-01-04 11:53:37', 0, 0),
(19, 12, 29, 'hi', NULL, '', '', '', '', '', '2018-01-04 11:54:12', '2018-01-04 11:54:12', 0, 0),
(20, 12, 30, 'Hello', NULL, '', '', '', '', '', '2018-01-04 11:59:49', '2018-01-04 11:59:49', 0, 0),
(21, 12, 31, 'Hey', NULL, '', '', '', '', '', '2018-01-04 12:00:24', '2018-01-04 12:00:24', 0, 0),
(22, 12, 32, 'Hiiiiiii', NULL, '', '', '', '', '', '2018-01-04 12:00:49', '2018-01-04 12:00:49', 0, 0),
(23, 12, 33, 'Hi', NULL, '', '', '', '', '', '2018-01-04 12:04:20', '2018-01-04 12:04:20', 0, 1),
(24, 12, 35, 'hi', NULL, '', '', '', '', '', '2018-01-04 18:19:20', '2018-01-04 18:19:20', 1, 1),
(25, 8, 36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-05 16:49:03', '2018-01-05 16:49:03', 0, 1),
(26, 12, 37, 'cu', NULL, '', 'image', '', '', '', '2018-01-12 10:26:54', '2018-01-12 10:26:54', 0, 1),
(27, 12, 38, 'Test post', NULL, '', 'image', '', '', '', '2018-01-15 13:02:23', '2018-01-15 13:02:23', 0, 1),
(28, 12, 39, 'Another test post', NULL, '', 'gallery', '', '', '', '2018-01-15 13:03:30', '2018-01-15 13:03:30', 0, 1),
(29, 12, 41, 'Hi', NULL, '', 'image', '', '', '', '2018-01-15 13:18:54', '2018-01-15 13:18:54', 0, 1),
(30, 87, 43, 'Hshhsjsjs hdjhshshshsjsnsk jsjsnshhsjsks hsjjshshsn hshshhshsjs hshshshhsbshs hdhshshshhs hdhsnnshs', NULL, '', '', '', '', '', '2018-01-30 12:55:04', '2018-01-30 12:55:04', 0, 1),
(31, 8, 44, 'Hello', NULL, '', 'gallery', '', '', '', '2018-01-30 16:01:28', '2018-01-30 16:01:28', 0, 0),
(32, 92, 45, 'Testing of create post', NULL, '', '', '', '', '', '2018-02-01 11:07:12', '2018-02-01 11:07:12', 0, 1),
(33, 92, 46, 'Imagepost', NULL, '', 'gallery', '', '', '', '2018-02-01 11:16:50', '2018-02-01 11:16:50', 0, 1),
(34, 91, 47, 'Hello gud', NULL, '', '', '', '', '', '2018-02-01 12:36:42', '2018-02-01 12:36:42', 1, 1),
(35, 92, 48, 'Test', NULL, '', '', '', '', '', '2018-02-01 12:41:18', '2018-02-01 12:41:18', 0, 1),
(36, 97, 49, 'Image post', NULL, '', 'image', '', '', '', '2018-02-01 13:13:49', '2018-02-01 13:13:49', 1, 1),
(37, 91, 50, 'Post', NULL, '', 'image', '', '', '', '2018-02-01 13:18:48', '2018-02-01 13:18:48', 1, 1),
(38, 91, 51, 'Hello post', NULL, '', '', '', '', '', '2018-02-01 13:46:00', '2018-02-01 13:46:00', 1, 1),
(39, 104, 52, 'Teat', NULL, 'http://google.com', 'website', 'Search the world\'s information, including webpages, images, videos and more. Google has many special features to help you find exactly what you\'re looking for.', 'Google', 'http://google.com/images/branding/googleg/1x/googleg_standard_color_128dp.png', '2018-02-02 13:26:26', '2018-02-02 13:26:26', 0, 0),
(40, 92, 53, 'Test', NULL, '', '', '', '', '', '2018-02-02 13:29:02', '2018-02-02 13:29:02', 0, 1),
(41, 92, 54, '', NULL, '', 'gallery', '', '', '', '2018-02-02 13:31:21', '2018-02-02 13:31:21', 0, 1),
(42, 91, 55, 'Img post', NULL, '', 'image', '', '', '', '2018-02-02 15:03:10', '2018-02-02 15:03:10', 0, 1),
(43, 90, 56, 'Hi text post', NULL, '', '', '', '', '', '2018-02-02 15:11:36', '2018-02-02 15:11:36', 1, 1),
(44, 91, 57, 'Yahoo.com ', NULL, 'http://Yahoo.com', 'website', 'News, email and search are just the beginning. Discover more every day. Find your yodel.', 'Yahoo', 'https://s.yimg.com/dh/ap/default/130909/y_200_a.png', '2018-02-03 15:44:48', '2018-02-03 15:44:48', 0, 1),
(45, 91, 58, 'Yahoo.com dfsdfsdfasdf', NULL, 'http://Yahoo.com', 'website', 'News, email and search are just the beginning. Discover more every day. Find your yodel.', 'Yahoo', 'https://s.yimg.com/dh/ap/default/130909/y_200_a.png', '2018-02-03 15:48:37', '2018-02-03 15:48:37', 0, 0),
(46, 91, 59, 'Sadfasdfasdf', NULL, '', 'gallery', '', '', '', '2018-02-03 16:24:15', '2018-02-03 16:24:15', 0, 0),
(47, 87, 65, 'Hehjdhdhdjd\nHdhdhdhdhdh\nHdhdhehehdbe', NULL, '', 'image', '', '', '', '2018-02-06 12:58:27', '2018-02-06 12:58:27', 0, 0),
(48, 91, 69, 'Album post', NULL, '', 'gallery', '', '', '', '2018-02-06 15:28:11', '2018-02-06 15:28:11', 1, 1),
(49, 87, 71, 'Ghhshshsbs\nShsbsbnsnhsbsbs\nBsbsbsbs', NULL, '', '', '', '', '', '2018-02-06 15:33:20', '2018-02-06 15:33:20', 0, 0),
(50, 87, 72, 'Ghhiih\nHjioi\nHiihhij', NULL, '', '', '', '', '', '2018-02-06 15:35:41', '2018-02-06 15:35:41', 0, 0),
(51, 87, 73, 'Vhjjnb\nHhjjjj\nGjjkjjn\nGhnhjjj', NULL, '', 'gallery', '', '', '', '2018-02-06 15:42:02', '2018-02-06 15:42:02', 0, 0),
(52, 87, 74, 'Hhiuu\nHiihih\nHohh\n', NULL, '', 'gallery', '', '', '', '2018-02-06 15:45:29', '2018-02-06 15:45:29', 0, 0),
(53, 87, 75, 'Hello\nHi\n', NULL, '', 'gallery', '', '', '', '2018-02-12 12:39:27', '2018-02-12 12:39:27', 1, 1),
(54, 1, 76, 'Spread the word amongst your batch and friends.', NULL, NULL, 'image', NULL, NULL, NULL, '2018-03-13 11:30:05', '2018-03-13 11:30:05', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `post_hashtag`
--

CREATE TABLE `post_hashtag` (
  `post_id` int(11) NOT NULL,
  `hashtag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_hashtag`
--

INSERT INTO `post_hashtag` (`post_id`, `hashtag_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 3),
(3, 4),
(4, 3),
(4, 5),
(5, 6),
(6, 7),
(7, 2),
(8, 2),
(9, 2),
(10, 6),
(11, 2),
(12, 2),
(13, 4),
(14, 1),
(14, 3),
(14, 5),
(15, 3),
(16, 2),
(17, 1),
(18, 3),
(19, 2),
(20, 7),
(21, 5),
(22, 3),
(23, 7),
(24, 4),
(26, 2),
(27, 1),
(28, 2),
(29, 1),
(30, 2),
(30, 7),
(30, 8),
(31, 3),
(32, 1),
(32, 2),
(32, 3),
(32, 4),
(32, 5),
(32, 6),
(32, 7),
(32, 8),
(32, 9),
(32, 10),
(33, 3),
(34, 4),
(34, 7),
(35, 3),
(35, 5),
(36, 2),
(37, 4),
(38, 2),
(38, 7),
(39, 6),
(40, 1),
(40, 3),
(40, 5),
(41, 1),
(41, 3),
(41, 5),
(41, 15),
(42, 7),
(43, 2),
(44, 2),
(45, 1),
(46, 2),
(46, 4),
(47, 3),
(47, 4),
(47, 5),
(48, 4),
(49, 1),
(49, 5),
(49, 16),
(50, 2),
(50, 3),
(50, 16),
(51, 2),
(51, 16),
(51, 17),
(52, 2),
(52, 5),
(53, 4),
(53, 5),
(53, 18),
(54, 4),
(54, 19);

-- --------------------------------------------------------

--
-- Table structure for table `Settings`
--

CREATE TABLE `Settings` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Settings`
--

INSERT INTO `Settings` (`id`, `title`, `logo`, `color`, `admin_name`, `admin_password`) VALUES
(1, 'KROS Alumni', 'uploads/logo/icon.png', '#222d32', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `thread`
--

CREATE TABLE `thread` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `last_updated_at` datetime NOT NULL,
  `group_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdBy_id` int(11) DEFAULT NULL,
  `blockedBy_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `thread`
--

INSERT INTO `thread` (`id`, `status`, `created_at`, `last_updated_at`, `group_name`, `createdBy_id`, `blockedBy_id`) VALUES
(3, 0, '2018-02-01 11:30:56', '2018-02-01 11:30:56', NULL, 92, NULL),
(5, 1, '2018-02-01 12:22:14', '2018-02-01 12:26:52', NULL, 95, NULL),
(6, 1, '2018-02-01 12:25:59', '2018-02-01 12:25:59', NULL, 92, NULL),
(7, 1, '2018-02-01 13:42:45', '2018-02-02 15:48:14', NULL, 98, NULL),
(8, 1, '2018-02-02 13:12:05', '2018-02-03 17:32:32', NULL, 104, NULL),
(9, 1, '2018-02-02 15:06:47', '2018-02-02 15:50:50', NULL, 90, NULL),
(10, 0, '2018-02-03 17:28:33', '2018-02-03 17:28:33', NULL, 92, NULL),
(11, 0, '2018-02-03 22:17:22', '2018-02-03 22:17:22', NULL, 91, NULL),
(12, 0, '2018-03-13 17:51:06', '2018-03-13 17:51:06', NULL, 124, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `thread_user`
--

CREATE TABLE `thread_user` (
  `thread_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `thread_user`
--

INSERT INTO `thread_user` (`thread_id`, `user_id`) VALUES
(3, 5),
(3, 92),
(5, 92),
(5, 95),
(6, 87),
(6, 92),
(7, 91),
(7, 98),
(8, 92),
(8, 104),
(9, 90),
(9, 91),
(10, 86),
(10, 92),
(11, 12),
(11, 91),
(12, 8),
(12, 124);

-- --------------------------------------------------------

--
-- Table structure for table `UserSettings`
--

CREATE TABLE `UserSettings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `event_push` int(11) NOT NULL,
  `fundraiser_push` int(11) NOT NULL,
  `feed_push` int(11) NOT NULL,
  `chat_push` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `UserSettings`
--

INSERT INTO `UserSettings` (`id`, `user_id`, `event_push`, `fundraiser_push`, `feed_push`, `chat_push`) VALUES
(1, 2, 1, 1, 1, 1),
(2, 3, 1, 1, 1, 1),
(3, 4, 1, 1, 1, 1),
(4, 5, 1, 1, 1, 1),
(5, 6, 1, 1, 1, 1),
(6, 7, 1, 1, 1, 1),
(7, 8, 1, 1, 1, 1),
(9, 10, 1, 1, 1, 1),
(10, 11, 1, 1, 1, 1),
(11, 12, 1, 1, 1, 1),
(12, 13, 1, 1, 1, 1),
(13, 14, 1, 1, 1, 1),
(14, 15, 1, 1, 1, 1),
(15, 16, 1, 1, 1, 1),
(16, 17, 1, 1, 1, 1),
(29, 63, 1, 1, 1, 1),
(30, 64, 1, 1, 1, 1),
(31, 65, 1, 1, 1, 1),
(32, 66, 1, 1, 1, 1),
(33, 67, 1, 1, 1, 1),
(34, 68, 1, 1, 1, 1),
(35, 69, 1, 1, 1, 1),
(36, 70, 1, 1, 1, 1),
(37, 71, 1, 1, 1, 1),
(40, 74, 1, 1, 1, 1),
(41, 75, 1, 1, 1, 1),
(42, 76, 1, 1, 1, 1),
(43, 77, 1, 1, 1, 1),
(44, 78, 1, 1, 1, 1),
(45, 79, 1, 1, 1, 1),
(46, 80, 1, 1, 1, 1),
(47, 81, 1, 1, 1, 1),
(48, 82, 1, 1, 1, 1),
(49, 83, 1, 1, 1, 1),
(50, 84, 1, 1, 1, 1),
(51, 86, 1, 1, 1, 1),
(52, 87, 1, 1, 1, 1),
(53, 88, 1, 1, 1, 1),
(55, 90, 1, 1, 1, 1),
(56, 91, 1, 1, 1, 1),
(57, 92, 1, 1, 1, 1),
(59, 94, 1, 1, 1, 1),
(60, 95, 1, 1, 1, 1),
(61, 96, 1, 1, 1, 1),
(62, 97, 1, 1, 1, 1),
(63, 98, 1, 1, 1, 1),
(64, 99, 1, 1, 1, 1),
(65, 100, 1, 1, 1, 1),
(68, 103, 1, 1, 1, 1),
(69, 104, 1, 1, 1, 1),
(70, 105, 1, 1, 1, 1),
(71, 106, 1, 1, 1, 1),
(72, 107, 1, 1, 1, 1),
(73, 108, 1, 1, 1, 1),
(74, 109, 1, 1, 1, 1),
(75, 110, 1, 1, 1, 1),
(76, 111, 1, 1, 1, 1),
(77, 112, 1, 1, 1, 1),
(78, 113, 1, 1, 1, 1),
(79, 114, 1, 1, 1, 1),
(80, 115, 1, 1, 1, 1),
(81, 116, 1, 1, 1, 1),
(82, 117, 1, 1, 1, 1),
(83, 118, 1, 1, 1, 1),
(84, 119, 1, 1, 1, 1),
(85, 120, 1, 1, 1, 1),
(86, 121, 1, 1, 1, 1),
(87, 122, 1, 1, 1, 1),
(88, 123, 1, 1, 1, 1),
(89, 124, 1, 1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Activity`
--
ALTER TABLE `Activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_55026B0CA76ED395` (`user_id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9474526CA76ED395` (`user_id`),
  ADD KEY `IDX_9474526C4B89032C` (`post_id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3BAE0AA7A76ED395` (`user_id`),
  ADD KEY `IDX_3BAE0AA7EA9FDD75` (`media_id`);

--
-- Indexes for table `EventComment`
--
ALTER TABLE `EventComment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_885220D9A76ED395` (`user_id`),
  ADD KEY `IDX_885220D971F7E88B` (`event_id`);

--
-- Indexes for table `EventLikes`
--
ALTER TABLE `EventLikes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_85B85E19A76ED395` (`user_id`),
  ADD KEY `IDX_85B85E1971F7E88B` (`event_id`);

--
-- Indexes for table `event_invited_users`
--
ALTER TABLE `event_invited_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3D8992A2A76ED395` (`user_id`),
  ADD KEY `IDX_3D8992A271F7E88B` (`event_id`);

--
-- Indexes for table `event_user`
--
ALTER TABLE `event_user`
  ADD PRIMARY KEY (`event_id`,`user_id`),
  ADD KEY `IDX_92589AE271F7E88B` (`event_id`),
  ADD KEY `IDX_92589AE2A76ED395` (`user_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D2294458A76ED395` (`user_id`);

--
-- Indexes for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD KEY `IDX_957A6479EA9FDD75` (`media_id`);

--
-- Indexes for table `Fundraiser`
--
ALTER TABLE `Fundraiser`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_24225031EA9FDD75` (`media_id`),
  ADD KEY `IDX_24225031A76ED395` (`user_id`);

--
-- Indexes for table `FundraiserComment`
--
ALTER TABLE `FundraiserComment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_CD0289ECA76ED395` (`user_id`),
  ADD KEY `IDX_CD0289EC9F4D1DF6` (`fundraiser_id`);

--
-- Indexes for table `FundraiserLike`
--
ALTER TABLE `FundraiserLike`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B3CBFC089F4D1DF6` (`fundraiser_id`),
  ADD KEY `IDX_B3CBFC08A76ED395` (`user_id`);

--
-- Indexes for table `FundraiserPayment`
--
ALTER TABLE `FundraiserPayment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_345E5F8D82E709C6` (`donar_id`),
  ADD KEY `IDX_345E5F8D9F4D1DF6` (`fundraiser_id`);

--
-- Indexes for table `hash_tag`
--
ALTER TABLE `hash_tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_BF11B0389B783` (`tag`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_49CA4E7DA76ED395` (`user_id`),
  ADD KEY `IDX_49CA4E7D4B89032C` (`post_id`);

--
-- Indexes for table `media__gallery`
--
ALTER TABLE `media__gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_80D4C5414E7AF8F` (`gallery_id`),
  ADD KEY `IDX_80D4C541EA9FDD75` (`media_id`);

--
-- Indexes for table `media__media`
--
ALTER TABLE `media__media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B6BD307FE2904019` (`thread_id`),
  ADD KEY `IDX_B6BD307FF624B39D` (`sender_id`);

--
-- Indexes for table `MessageMeta`
--
ALTER TABLE `MessageMeta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9502A7CA537A1329` (`message_id`),
  ADD KEY `IDX_9502A7CACD53EDB6` (`receiver_id`),
  ADD KEY `IDX_9502A7CAE2904019` (`thread_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_BF5476CAA76ED395` (`user_id`),
  ADD KEY `IDX_BF5476CACD53EDB6` (`receiver_id`),
  ADD KEY `IDX_BF5476CA4B89032C` (`post_id`),
  ADD KEY `IDX_BF5476CA71F7E88B` (`event_id`),
  ADD KEY `IDX_BF5476CA9F4D1DF6` (`fundraiser_id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5A8A6C8DA76ED395` (`user_id`),
  ADD KEY `IDX_5A8A6C8D4E7AF8F` (`gallery_id`);

--
-- Indexes for table `post_hashtag`
--
ALTER TABLE `post_hashtag`
  ADD PRIMARY KEY (`post_id`,`hashtag_id`),
  ADD KEY `IDX_675D9D524B89032C` (`post_id`),
  ADD KEY `IDX_675D9D52FB34EF56` (`hashtag_id`);

--
-- Indexes for table `Settings`
--
ALTER TABLE `Settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `thread`
--
ALTER TABLE `thread`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_31204C833174800F` (`createdBy_id`),
  ADD KEY `IDX_31204C83CA1EB865` (`blockedBy_id`);

--
-- Indexes for table `thread_user`
--
ALTER TABLE `thread_user`
  ADD PRIMARY KEY (`thread_id`,`user_id`),
  ADD KEY `IDX_922CAC7E2904019` (`thread_id`),
  ADD KEY `IDX_922CAC7A76ED395` (`user_id`);

--
-- Indexes for table `UserSettings`
--
ALTER TABLE `UserSettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_2847E61CA76ED395` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Activity`
--
ALTER TABLE `Activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=221;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `EventComment`
--
ALTER TABLE `EventComment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `EventLikes`
--
ALTER TABLE `EventLikes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `event_invited_users`
--
ALTER TABLE `event_invited_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;
--
-- AUTO_INCREMENT for table `Fundraiser`
--
ALTER TABLE `Fundraiser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `FundraiserComment`
--
ALTER TABLE `FundraiserComment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `FundraiserLike`
--
ALTER TABLE `FundraiserLike`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `FundraiserPayment`
--
ALTER TABLE `FundraiserPayment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hash_tag`
--
ALTER TABLE `hash_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `media__gallery`
--
ALTER TABLE `media__gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `media__media`
--
ALTER TABLE `media__media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `MessageMeta`
--
ALTER TABLE `MessageMeta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=230;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `Settings`
--
ALTER TABLE `Settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `thread`
--
ALTER TABLE `thread`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `UserSettings`
--
ALTER TABLE `UserSettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Activity`
--
ALTER TABLE `Activity`
  ADD CONSTRAINT `FK_55026B0CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526C4B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `FK_9474526CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `FK_3BAE0AA7A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_3BAE0AA7EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`);

--
-- Constraints for table `EventComment`
--
ALTER TABLE `EventComment`
  ADD CONSTRAINT `FK_885220D971F7E88B` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `FK_885220D9A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `EventLikes`
--
ALTER TABLE `EventLikes`
  ADD CONSTRAINT `FK_85B85E1971F7E88B` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `FK_85B85E19A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `event_invited_users`
--
ALTER TABLE `event_invited_users`
  ADD CONSTRAINT `FK_3D8992A271F7E88B` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `FK_3D8992A2A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `event_user`
--
ALTER TABLE `event_user`
  ADD CONSTRAINT `FK_92589AE271F7E88B` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_92589AE2A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `FK_D2294458A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD CONSTRAINT `fos_user_ibfk_1` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`);

--
-- Constraints for table `Fundraiser`
--
ALTER TABLE `Fundraiser`
  ADD CONSTRAINT `FK_24225031A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_24225031EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`);

--
-- Constraints for table `FundraiserComment`
--
ALTER TABLE `FundraiserComment`
  ADD CONSTRAINT `FK_CD0289EC9F4D1DF6` FOREIGN KEY (`fundraiser_id`) REFERENCES `Fundraiser` (`id`),
  ADD CONSTRAINT `FK_CD0289ECA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `FundraiserLike`
--
ALTER TABLE `FundraiserLike`
  ADD CONSTRAINT `FK_B3CBFC089F4D1DF6` FOREIGN KEY (`fundraiser_id`) REFERENCES `Fundraiser` (`id`),
  ADD CONSTRAINT `FK_B3CBFC08A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `FundraiserPayment`
--
ALTER TABLE `FundraiserPayment`
  ADD CONSTRAINT `FK_345E5F8D82E709C6` FOREIGN KEY (`donar_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_345E5F8D9F4D1DF6` FOREIGN KEY (`fundraiser_id`) REFERENCES `Fundraiser` (`id`);

--
-- Constraints for table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `FK_49CA4E7D4B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `FK_49CA4E7DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  ADD CONSTRAINT `FK_80D4C5414E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `media__gallery` (`id`),
  ADD CONSTRAINT `FK_80D4C541EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`);

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_B6BD307FE2904019` FOREIGN KEY (`thread_id`) REFERENCES `thread` (`id`),
  ADD CONSTRAINT `FK_B6BD307FF624B39D` FOREIGN KEY (`sender_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `MessageMeta`
--
ALTER TABLE `MessageMeta`
  ADD CONSTRAINT `FK_9502A7CA537A1329` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`),
  ADD CONSTRAINT `FK_9502A7CACD53EDB6` FOREIGN KEY (`receiver_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_9502A7CAE2904019` FOREIGN KEY (`thread_id`) REFERENCES `thread` (`id`);

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `FK_BF5476CA4B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `FK_BF5476CA71F7E88B` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `FK_BF5476CA9F4D1DF6` FOREIGN KEY (`fundraiser_id`) REFERENCES `Fundraiser` (`id`),
  ADD CONSTRAINT `FK_BF5476CAA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_BF5476CACD53EDB6` FOREIGN KEY (`receiver_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `FK_5A8A6C8D4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `media__gallery` (`id`),
  ADD CONSTRAINT `FK_5A8A6C8DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `post_hashtag`
--
ALTER TABLE `post_hashtag`
  ADD CONSTRAINT `FK_675D9D524B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_675D9D52FB34EF56` FOREIGN KEY (`hashtag_id`) REFERENCES `hash_tag` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `thread`
--
ALTER TABLE `thread`
  ADD CONSTRAINT `FK_31204C833174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_31204C83CA1EB865` FOREIGN KEY (`blockedBy_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `thread_user`
--
ALTER TABLE `thread_user`
  ADD CONSTRAINT `FK_922CAC7A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_922CAC7E2904019` FOREIGN KEY (`thread_id`) REFERENCES `thread` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `UserSettings`
--
ALTER TABLE `UserSettings`
  ADD CONSTRAINT `FK_2847E61CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
/* Job Route */

Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/careers/list', 'JobController@listjobs')->name('jobs-list');
Route::post('/career/submit', 'JobController@postJob')->name('job-submit');
Route::get('/career-provider/{company?}/{last_id?}', 'JobController@index')->name('post-job');
Route::get('/career/view/{id}', 'JobController@jobView')->name('jobs-view');
Route::get('/career/delete/{id}', 'JobController@jobDelete')->name('jobs-delete');
Route::post('/career/update', 'JobController@updateJob')->name('job-update');
Route::get('/companyExist',array('as'=>'searchajax','uses'=>'JobController@companyExist'));
Route::get('/companyData',array('as'=>'getData','uses'=>'JobController@companyData'));
Route::get('datatable/getjobs', ['as'=>'datatable.getjobs','uses'=>'JobController@getJobs']);
Route::get('/career/add/', 'JobController@add')->name('add-job');
Route::get('/careerprovider/success/', 'JobController@success')->name('jobsuccess');
/*
Route::get('/', 'HomeController@login')->name('login');
*/
/* JobSeeker Route */

Route::get('/', ['middleware' => 'guest', function()
{
     return view('auth/login');
}]);

Route::get('/career-seeker/{company?}/', 'JobSeekerController@index')->name('post-resume');
Route::post('/careerseeker/submit', 'JobSeekerController@postResume')->name('resume-submit');
Route::get('/careerseeker/list', 'JobSeekerController@listJobseeker')->name('jobseeker-list');
Route::get('datatable/getjobseeker', ['as'=>'datatable.getjobseeker','uses'=>'JobSeekerController@getJobseeker']);
Route::get('/careerseeker/view/{id}', 'JobSeekerController@jobSeekerView')->name('jobseeker-view');
Route::get('/careerseeker/delete/{id}', 'JobSeekerController@jobSeekerDelete')->name('jobseeker-delete');
Route::post('/jobseeker/update', 'JobSeekerController@updatejobSeeker')->name('jobseeker-update');
Route::get('/careerseeker/add/', 'JobSeekerController@add')->name('post-jobseeker');
Route::get('/careerseeker/success/', 'JobSeekerController@success')->name('jobseekersuccess');
Route::post('/job-seeker/qualify', 'JobSeekerController@qualifyUser')->name('user-qualify');
Route::post('/job-seeker/hired', 'JobSeekerController@hiredUser')->name('user-hired');
Route::post('/job-seeker/removehired', 'JobSeekerController@removehiredUser')->name('user-remove-hired');
Route::post('/job-seeker/removequalify', 'JobSeekerController@removequalifyUser')->name('user-remove-qualify');


/* JobProvider Route */

Route::get('/careerprovider/list', 'JobProviderController@listJobprovider')->name('jobprovider-list');
Route::get('datatable/getjobprovider', ['as'=>'datatable.getjobprovider','uses'=>'JobProviderController@getJobprovider']);
Route::get('/careerprovider/view/{id}', 'JobProviderController@jobProviderView')->name('jobprovider-view');
Route::get('/careerprovider/delete/{id}', 'JobProviderController@jobProviderDelete')->name('jobprovider-delete');
Route::post('/careerprovider/update', 'JobProviderController@updatejobProvider')->name('jobprovider-update');
Route::get('/careerprovider/add/', 'JobProviderController@add')->name('post-jobprovider');
Route::post('/careerprovider/submit', 'JobProviderController@postResume')->name('jobprovider-submit');



/* Submit Url Route */

Route::get('/urls/list', 'SubmitUrlController@listUrl')->name('url-list');
Route::get('datatable/getsubmiturl', ['as'=>'datatable.getsubmiturl','uses'=>'SubmitUrlController@getsubmiturl']);
Route::post('/submiturl/update', 'SubmitUrlController@updateSubmitUrl')->name('submiturl-update');

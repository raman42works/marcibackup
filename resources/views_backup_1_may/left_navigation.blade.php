<!-- Left Side navigation -->
<div class="col-sm-2 col-md-3 col-lg-2 leftNavigation">
    <nav class="navbar navbar-light">
      <div class="collapse navbar-collapse show" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{url('/dashboard')}}">Dashboard</a>
          </li>
          <li class="nav-item">
            <span>Job Seekers</span>
                <a class="dropdown-item" href="{{url('/jobseeker/list')}}"><i class="fa fa-users" aria-hidden="true"></i> View All</a>
                <a class="dropdown-item" href="{{url('/jobseeker/add')}}"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
          </li>
          <li class="nav-item">
            <span>Job Providers</span>
                <a class="dropdown-item" href="{{url('/jobprovider/list')}}"><i class="fa fa-users" aria-hidden="true"></i> View All</a>
                <a class="dropdown-item" href="{{url('/jobprovider/add')}}"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
          </li>
          <li class="nav-item">
            <span>Jobs</span>
                <a class="dropdown-item" href="{{url('/jobs/list')}}"><i class="fa fa-users" aria-hidden="true"></i> View All</a>
                <a class="dropdown-item" href="{{url('/job/add')}}"><i class="fa fa-plus" aria-hidden="true"></i> Post a Job</a>
          </li>
          <li class="nav-item">
            <span>Submit Urls</span>
                <a class="dropdown-item" href="{{url('/urls/list')}}"><i class="fa fa-users" aria-hidden="true"></i> View All</a>
              
          </li>
        </ul>
      </div>
    </nav>
</div>
<!-- End of Left Side navigation -->

<!DOCTYPE html>
<html>
<head>
	<title>Welcome Email</title>
</head>
<body>
	<h2>Thank you for allowing WorkSource Montgomery to be a part of your career quest.  Our matching and analytics software will now go to work for you.  Should we receive any potential matching opportunities, MARCI will forward your name and resume to the career provider who is seeking your talent and skills.   Please let us hear from you, should you gain employment. </h2>
	<br>
    <h2>We are excited about your success!</h2>
</body>
</html>

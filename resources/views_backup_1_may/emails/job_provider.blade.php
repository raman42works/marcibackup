<!DOCTYPE html>
<html>
<head>
	<title>Welcome Email</title>
</head>
<body>
	<h2>Thank you for allowing WorkSource Montgomery to be a part of your talent recruitment success; our matching and analytics software will now go to work for you.  Should we receive any potential matching opportunities, MARCI will forward the candidates' name and resume to you for your further engagement.  We will follow-up with you in about two weeks- </h2>
    <br>
    <h2>We are excited about your success!</h2>
</body>
</html>

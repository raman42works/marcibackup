@extends('layouts.admin')

@section('content')

   @include('dashboard_header')

    <!--- End of Header -->

    <!-- Main Wrapper -->
    <div class="mainWrapper">
        <div class="container-fluid">
            <div class="row">

                @include('left_navigation')

                <div class="col-sm-12 col-md-9 col-lg-10 contentWrapper">
          <h2 class="mTitile">View All Job Seekers</h2>
          <h2 class="mTitile success_message success-message"></h2>
          
           
                @if(session('success'))
					<p class="success-message">{{session('success')}}</p>
				@endif
          <div class="outterWrapper table col">
            <div class="row">
              <table id="jobseeker" class="table table-striped table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                <thead>
					<tr>
						<!--<th>Id</th> -->
						<th>Name</th>
						<th>Contact</th>
						
						<th>Resume Title</th>
						<th>Submitted From</th>
						<th>Created On</th>
						<th>Employment Status</th>
						<th class="text-center">Actions</th>
						
					</tr>
			     </thead>
              <!--  <tbody>
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>demo@demo.com</td>
                    <td>95513456464</td>
                    <td>USA</td>
                    <td class="text-center">
                      <a href="#" class="">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>demo@demo.com</td>
                    <td>56568687656</td>
                    <td>USA</td>
                    <td class="text-center">
                      <a href="#" class="">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>demo@demo.com</td>
                    <td>4568769876587</td>
                    <td>USA</td>
                    <td class="text-center">
                      <a href="#" class="">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>demo@demo.com</td>
                    <td>554687687687</td>
                    <td>USA</td>
                    <td class="text-center">
                      <a href="#" class="">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>demo@demo.com</td>
                    <td>7868767687687</td>
                    <td>USA</td>
                    <td class="text-center">
                      <a href="#" class="">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                </tbody> -->
              </table>
            </div>
          </div>
        </div>
            </div>
        </div>
        <div class="modal fade job_model" id="myModal" role="dialog">
		<div class="modal-dialog">
      <!-- Modal content-->
		  <div class="modal-content">
				<div class="modal-header">
				
				  <h4 class="modal-title">Select Job</h4>
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				  
				</div>
				<div class="modal-body">
			  <!--  <div class="form-group class_radio hide" >
				<input type="radio" name="hired_value" checked="checked" value="1"  /> Hired Inside Marci
    
				<input type="radio" name="hired_value" value="0" /> Hired Outside  
				</div> -->
				<div class="form-group">
			   <label for="company_name" class='user_job_label'>Job Name</label>
				<select class="form-control user_job_id" name="user_job_id">
				@foreach($jobs as $job)
				  <option value="{{$job->id}}" {{($job->pending_job ==0  ? 'disabled':'') }} >{{$job->job_title}} - {{$job->company_profile->company_name}} (Number : {{$job->no_of_jobs}}, Available:{{$job->pending_job}} )</option>
				  
				@endforeach
				</select>
				</div>
				
				<div class="form-group outside_job hide">
			    <label for="company_name">Job Name <span>*</span></label>
				<input type='text' name='job_name' class='form-control job_data'>
				</div>
				<input type='hidden' class='jobUserID' value=''>
				  <div class="form-group">
					<input type="submit" name="submit" class="btn btn-primary mx-auto col-sm-12 col-md-2 save_user_job" value="Submit">
			     </div>
				</div>
				<!--<div class="modal-footer">
				  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div> -->
		  </div>
      
    </div>
  </div>
    </div>

@endsection
@section("scripts")
<script type="text/javascript" src='{{ asset("public/js/jquery.js") }}'></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src='{{ asset("public/js/bootstrap.min.js") }}'></script>
<script type="text/javascript">
  var deletePath = '';
  $(document).ready(function(){
    $('#datatable-responsive').on('click',".deleteAction",function(event){
      $("#alertModal").modal({"show": true});
      deletePath = $(this).attr('data-newspaper-delete-route');
      $("#alertModal").on('click',".closeModal",function(){   
        top.location.href = deletePath;
      });
    });
    $("#datatable-responsive").DataTable({
      "responsive" : true,
      "processing": true,
          "serverSide": true,
      "columnDefs": [{
                "targets": [5, 6, 8], // column or columns numbers
                "orderable": false,  // set orderable for selected columns
              }],
          "ajax": "",
        "language": {
                    "searchPlaceholder": "Search",
                    "sEmptyTable":     "No records found"
              }
    });
  })
  
  $(document).ready(function() {
    oTable = $("#jobseeker").DataTable({
        "processing": true,
        "responsive" : true,
        "serverSide": true,
        "ajax": "{{ route('datatable.getjobseeker') }}",
        "columns": [
           // {data: 'id', name: 'id'},
            {data: 'first_name', name: 'first_name'},
          //  {data: 'email', name: 'email', orderable: false, searchable: false},
            {data: 'phone_no', name: 'phone_no'},
            {data: 'resume_title', name: 'resume_title'},
          //  {data: 'resume_url', name: 'resume_url'},
            {data: 'refernce_url', name: 'refernce_url'},
            {data: 'created_at', name: 'created_at', orderable: false, searchable: false},
            {data: 'employment_status', name: 'employment_status', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false}
            
        ],"columnDefs": [
			{ className: "text-center", "targets": [ 6 ] }
       ]
    });
   });
	$(document).on('click',".qualify_user",function(event){
		event.preventDefault();
		if($(this).data('value') == 1){
			var user_id = $(this).data('id');
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			if (confirm('Are you sure you want to remove this user from job')) {
					$.ajax({
				   type:'POST',
				   url:'{{ route('user-remove-qualify') }}',
					data: {
						user_id : user_id,_token: CSRF_TOKEN
					},
				   success:function(data){
						var response= $.parseJSON(data);
					 if(response.success==1){
						 $('.success_message').text(response.message);
						 setTimeout(function(){
							   window.location.reload(1);
							}, 2000);
					 }
					 else {
						 $('.success_message').text(response.message);
						 setTimeout(function(){
							   window.location.reload(1);
							}, 2000);
					 }
				   }
				});
           

               }
		}
		else {
        $('.job_model').modal('show'); 
        $('.outside_job').removeClass('show'); 
		$('.outside_job').addClass('hide'); 
		$('.class_radio').addClass('hide'); 
        $('.class_radio').removeClass('show'); 
        $('.user_job_label').removeClass('hide'); 
        $('.user_job_id').removeClass('hide'); 
        $('.user_job_label').addClass('show'); 
        $('.user_job_id').addClass('show'); 
        $('.user_hire_job').addClass('save_user_job'); 
        $('.user_hire_job').removeClass('user_hire_job'); 
        $('.job_model').find('.jobUserID').val($(this).data('id')); 
	}
      }); 
    $(document).on('click',".hired_user",function(event){
		event.preventDefault();
		if($(this).data('value') == 1){
			var user_id = $(this).data('id');
			var jobs_id = $(this).data('job_id');
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			if (confirm('Are you sure you want to remove this user from job')) {
					$.ajax({
				   type:'POST',
				   url:'{{ route('user-remove-hired') }}',
					data: {
						user_id : user_id,_token: CSRF_TOKEN,job_id:jobs_id
					},
				   success:function(data){
						var response= $.parseJSON(data);
					 if(response.success==1){
						 $('.success_message').text(response.message);
						 setTimeout(function(){
							   window.location.reload(1);
							}, 2000);
					 }
					 else {
						 $('.success_message').text(response.message);
						 setTimeout(function(){
							   window.location.reload(1);
							}, 2000);
					 }
				   }
				});
           

               }
		}
		else {
        $('.job_model').modal('show'); 
        $('.class_radio').addClass('show'); 
        $('.save_user_job').addClass('user_hire_job'); 
        $('.save_user_job').removeClass('save_user_job'); 
        $('.class_radio').removeClass('hide'); 
        $('.job_model').find('.jobUserID').val($(this).data('id')); 
        $("input[name$='hired_value']").click(function() {
        var hired_value = $(this).val();
        if(hired_value ==1){
			$('.user_job_id').addClass('show'); 
			$('.user_job_label').addClass('show'); 
			$('.user_job_id').removeClass('hide'); 
			$('.user_job_label').removeClass('hide'); 
			$('.outside_job').removeClass('show'); 
			$('.outside_job').addClass('hide'); 
			$('.job_data').val(''); 
		}
		else {
			$('.user_job_id').addClass('hide'); 
			$('.user_job_label').addClass('hide'); 
			$('.user_job_id').removeClass('show'); 
			$('.user_job_label').removeClass('show'); 
			$('.outside_job').removeClass('hide'); 
			$('.outside_job').addClass('show'); 
		}
		});
	}
      });  
      
    $(document).on('click',".save_user_job",function(event){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		var job_id = $('.user_job_id').val();
		var user_id = $('.jobUserID').val();
         $.ajax({
               type:'POST',
               url:'{{ route('user-qualify') }}',
				data: {
                    user_id : user_id,_token: CSRF_TOKEN,job_id:job_id
                },
               success:function(data){
				    $('.job_model').modal('toggle');
				    var response= $.parseJSON(data);
                 if(response.success==1){
					 $('.success_message').text(response.message);
					 setTimeout(function(){
						   window.location.reload(1);
						}, 2000);
				 }
				 else {
					 $('.success_message').text(response.message);
					 setTimeout(function(){
						   window.location.reload(1);
						}, 2000);
				 }
               }
            });
      }); 
      
      $(document).on('click',".user_hire_job",function(event){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var radioValue = $("input[name='hired_value']:checked").val();
        console.log(radioValue);
		var job_id = $('.user_job_id').val();
		var user_id = $('.jobUserID').val();
		var job_name = $('.job_data').val();
		if(radioValue == 0 && job_name ==''){
			job_name ='a';
		}
         $.ajax({
               type:'POST',
               url:'{{ route('user-hired') }}',
				data: {
                    user_id : user_id,_token: CSRF_TOKEN,job_id:job_id,job_name:job_name
                },
               success:function(data){
				    $('.job_model').modal('toggle');
				    var response= $.parseJSON(data);
                 if(response.success==1){
					 $('.success_message').text(response.message);
					 setTimeout(function(){
						   window.location.reload(1);
						}, 2000);
				 }
				 else {
					 $('.success_message').text(response.message);
					 setTimeout(function(){
						   window.location.reload(1);
						}, 2000);
				 }
               }
            });
      }); 
      
    $(document).on("click", ".dropdown_status_change a", function(event){
      event.preventDefault();
      console.log('here');
      
    });
</script>
@endsection

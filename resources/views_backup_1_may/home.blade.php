@extends('layouts.admin')

@section('content')
  @include('dashboard_header')
<style>
.dropdown_status > span#dropdownMenuLink {
    background: transparent;
    color: #666;
    font-size: 13px;
    border-radius: 0;
    border-color: #ccc;
    padding: 10px 15px;
}
.pad_20{
	padding-top:20px
}
</style>  
  
    <!--- End of Header -->

    <!-- Main Wrapper -->
    <div class="mainWrapper">
        <div class="container-fluid">
            <div class="row">

                @include('left_navigation')

                <div class="col-sm-12 col-md-9 col-lg-10 contentWrapper">
					 <h2 class="mTitile">MARCI ENGAGEMENT DASHBOARD - categories and numbers</h2>
					<div class="row">
					 <div class="col-sm-12 col-md-12 dashboard-boxs">
                  <div class="row">
								
								<div class="col-md-3">
									<a href="#" class="tile-box tile-box-alt btn-info">
										<div class="tile-header">
											 Job Providers
										</div>
										<div class="tile-content-wrapper">
											<div class="chart-alt-10 easyPieChart" data-percent="1" style="width: 100px; height: 100px; line-height: 100px;"><span>{{$compnayCountDashBoard}}</span><canvas width="100" height="100"></canvas><canvas width="100" height="100"></canvas></div>
										</div>
									</a>
								</div>
								<div class="col-md-3">
									<a href="#" class="tile-box tile-box-alt btn-info">
										<div class="tile-header">
											 Opportunities
										</div>
										<div class="tile-content-wrapper">
											<div class="chart-alt-10 easyPieChart" style="width: 100px; height: 100px; line-height: 100px;"><span>{{$jobCountDashBoard}}</span><canvas width="100" height="100"></canvas><canvas width="100" height="100"></canvas></div>
										</div>
									</a>
								</div>
								<div class="col-md-3">
									<a href="#" class="tile-box tile-box-alt btn-warning">
										<div class="tile-header">
											 Job Seekers
										</div>
										<div class="tile-content-wrapper">
											<div class="chart-alt-10 easyPieChart" style="width: 100px; height: 100px; line-height: 100px;"><span>{{$jobSeekerCountDashBoard}}</span><canvas width="100" height="100"></canvas><canvas width="100" height="100"></canvas></div>
										</div>
									</a>
								</div>
								
								
								<div class="col-md-3">
									<a href="#" class="tile-box tile-box-alt btn-warning">
										<div class="tile-header">
											Hired
										</div>
										<div class="tile-content-wrapper">
											<div class="chart-alt-10 easyPieChart" style="width: 100px; height: 100px; line-height: 100px;"><span>{{$hiredCountDashBoard}}</span><canvas width="100" height="100"></canvas><canvas width="100" height="100"></canvas></div>
										</div>
									</a>
								</div>
								
							</div>
				     </div>
				     </div>		 
					
                	<div class="row pad_30">
					
					 <div class="col-sm-12 col-md-6 first-div">
						
				     <div class="row">
                                         <span class="label filter-label">Filter By</span>
					 <div class="col-sm-12 col-md-3">
                     <div class="form-group">
	                <div class="dropdown show">
			         <?php
			         $t=@$_GET['param'];
			         ?>
				  
				      @if ($t == 'Company')
					   <span class="btn btn-secondary open" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" 
                                        aria-expanded="false"> Company</span>
					@elseif ($t == 'Sector')
					   <span class="btn btn-secondary resolved" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" 
                                        aria-expanded="false">  Sector </span>
					@else
                    <span class="btn btn-secondary dropdown-toggle cancelled" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" 
                                        aria-expanded="false"> Company </span>
					@endif
				  <div class="dropdown-menu dropdown_status_filter" aria-labelledby="dropdownMenuLink" data-service=>
				  
				  @if ($t == 'Company')
				  <a class="dropdown-item open" href="#" data-status='1' data-status_name='Company'>Company</a>
				    <a class="dropdown-item inservice" href="#" data-status='2' data-status_name='Sector'>Sector</a>
					@elseif ($t == 'Sector')
					      <a class="dropdown-item open" href="#" data-status='1' data-status_name='Company'>Company</a>
				        <a class="dropdown-item open" href="#" data-status='1' data-status_name='Sector'>Sector</a>
					@else
				         <a class="dropdown-item inservice" href="#" data-status='2' data-status_name='Sector'>Sector</a>
				             <a class="dropdown-item open" href="#" data-status='1' data-status_name='Company'>Company</a>
					@endif
			
				  </div> 
				
					</div>
					</div>
		        </div>
	                 <div class="col-sm-12 col-md-4 sector_data {{(isset($_GET['sector']) ? "" : "hide" )}}">
                     <div class="form-group">
                          
                            <select class="form-control" name="industry" id="companyIndustry" value="{{ old('industry') }}">
                               <!---->
                               <option value=''>--SELECT--</option>
                               <option value='Accounting and Auditing Services' {{(@$_GET['sector'] == "Accounting and Auditing Services" ? "selected" : "" )}}>Accounting and Auditing Services</option>
                               <option {{(@$_GET['sector'] == "Advertising and PR Services" ? "selected" : "" )}}>Advertising and PR Services</option>
                               <option {{(@$_GET['sector'] == "Aerospace and Defense" ? "selected" : "" )}}>Aerospace and Defense</option>
                               <option {{(@$_GET['sector'] == "Agriculture/Forestry/Fishing" ? "selected" : "" )}}>Agriculture/Forestry/Fishing</option>
                               <option {{(@$_GET['sector'] == "Architectural and Design Services" ? "selected" : "" )}}>Architectural and Design Services</option>
                               <option {{(@$_GET['sector'] == "Automotive and Parts Mfg" ? "selected" : "" )}}>Automotive and Parts Mfg</option>
                               <option {{(@$_GET['sector'] == "Automotive Sales and Repair Services" ? "selected" : "" )}}>Automotive Sales and Repair Services</option>
                               <option {{(@$_GET['sector'] == "Banking" ? "selected" : "" )}}>Banking</option>
                               <option {{(@$_GET['sector'] == "BioLife/BioScience" ? "selected" : "" )}}>BioLife/BioScience</option>
                               <option {{(@$_GET['sector'] == "Biotechnology/Pharmaceuticals" ? "selected" : "" )}}>Biotechnology/Pharmaceuticals</option>
                               <option {{(@$_GET['sector'] == "Broadcasting, Music, and Film" ? "selected" : "" )}}>Broadcasting, Music, and Film</option>
                               <option {{(@$_GET['sector'] == "Business Services - Other" ? "selected" : "" )}}>Business Services - Other</option>
                               <option {{(@$_GET['sector'] == "Chemicals/Petro-Chemicals" ? "selected" : "" )}}>Chemicals/Petro-Chemicals</option>
                               <option {{(@$_GET['sector'] == "Clothing and Textile Manufacturing" ? "selected" : "" )}}>Clothing and Textile Manufacturing</option>
                               <option {{(@$_GET['sector'] == "Consulting" ? "selected" : "" )}}>Consulting</option>
                               <option {{(@$_GET['sector']== "Computer Hardware" ? "selected" : "" )}}>Computer Hardware</option>
                               <option {{(@$_GET['sector'] == "Computer Software" ? "selected" : "" )}}>Computer Software</option>
                               <option {{(@$_GET['sector'] == "Computer/IT Services" ? "selected" : "" )}}>Computer/IT Services</option>
                               <option {{(@$_GET['sector'] == "Construction - Industrial Facilities and Infrastructure" ? "selected" : "" )}}>Construction - Industrial Facilities and Infrastructure</option>
                               <option {{(@$_GET['sector'] == "Construction - Residential &amp; Commercial/Office" ? "selected" : "" )}}>Construction - Residential &amp; Commercial/Office</option>
                               <option {{(@$_GET['sector'] == "Consumer Packaged Goods Manufacturing" ? "selected" : "" )}}>Consumer Packaged Goods Manufacturing</option>
                               <option {{(@$_GET['sector'] == "Education" ? "selected" : "" )}} >Education</option>
                               <option {{(@$_GET['sector'] == "Electronics, Components, and Semiconductor Mfg" ? "selected" : "" )}}>Electronics, Components, and Semiconductor Mfg</option>
                               <option {{(@$_GET['sector'] == "Energy/Renewable" ? "selected" : "" )}}>Energy/Renewable</option>
                               <option {{(@$_GET['sector'] == "Energy and Utilities" ? "selected" : "" )}}>Energy and Utilities</option>
                               <option {{(@$_GET['sector'] == "Engineering Services" ? "selected" : "" )}}>Engineering Services</option>
                               <option {{(@$_GET['sector'] == "Entertainment Venues and Theaters" ? "selected" : "" )}}>Entertainment Venues and Theaters</option>
                               <option {{(@$_GET['sector'] == "Financial Services" ? "selected" : "" )}}>Financial Services</option>
                               <option {{(@$_GET['sector'] == "Food and Beverage Production" ? "selected" : "" )}}>Food and Beverage Production</option>
                               <option {{(@$_GET['sector'] == "Government and Military" ? "selected" : "" )}}>Government and Military</option>
                               <option {{(@$_GET['sector'] == "Healthcare Services" ? "selected" : "" )}}>Healthcare Services</option>
                               <option {{(@$_GET['sector'] == "Hospitality" ? "selected" : "" )}}>Hospitality</option>
                               <option {{(@$_GET['sector'] == "Hotels and Lodging" ? "selected" : "" )}}>Hotels and Lodging</option>
                               <option {{(@$_GET['sector'] == "Insurance" ? "selected" : "" )}}>Insurance</option>
                               <option {{(@$_GET['sector'] == "Internet Services" ? "selected" : "" )}}>Internet Services</option>
                               <option {{(@$_GET['sector'] == "Legal Services" ? "selected" : "" )}}>Legal Services</option>
                               <option {{(@$_GET['sector'] == "Management Consulting Services" ? "selected" : "" )}}>Management Consulting Services</option>
                               <option {{(@$_GET['sector'] == "Manufacturing - Other" ? "selected" : "" )}}>Manufacturing - Other</option>
                               <option {{(@$_GET['sector'] == "Marine Mfg &amp; Services" ? "selected" : "" )}}>Marine Mfg &amp; Services</option>
                               <option {{(@$_GET['sector'] == "Medical Devices and Supplies" ? "selected" : "" )}}>Medical Devices and Supplies</option>
                               <option {{(@$_GET['sector'] == "Metals and Minerals" ? "selected" : "" )}} >Metals and Minerals</option>
                               <option {{(@$_GET['sector'] == "Nonprofit Charitable" ? "selected" : "" )}}>Nonprofit Charitable Organizations</option>
                               <option {{(@$_GET['sector'] == "Other/Not Classified" ? "selected" : "" )}}>Other/Not Classified</option>
                               <option {{(@$_GET['sector'] == "Performing and Fine Arts" ? "selected" : "" )}}>Performing and Fine Arts</option>
                               <option {{(@$_GET['sector'] == "Personal and Household Services" ? "selected" : "" )}}>Personal and Household Services</option>
                               <option {{(@$_GET['sector'] == "Personnel/Staffing" ? "selected" : "" )}}>Personnel/Staffing</option>
                               <option {{(@$_GET['sector'] == "Printing and Publishing" ? "selected" : "" )}}>Printing and Publishing </option>
                               <option {{(@$_GET['sector'] == "Real Estate/Property Management" ? "selected" : "" )}}>Real Estate/Property Management</option>
                               <option {{(@$_GET['sector'] == "Rental Services" ? "selected" : "" )}}>Rental Services</option>
                               <option {{(@$_GET['sector'] == "Restaurant/Food Services" ? "selected" : "" )}}>Restaurant/Food Services</option>
                               <option {{(@$_GET['sector'] == "Retail" ? "selected" : "" )}}>Retail</option>
                               <option {{(@$_GET['sector'] == "Security and Surveillance" ? "selected" : "" )}}>Security and Surveillance</option>
                               <option {{(@$_GET['sector'] == "Sports and Physical Recreation" ? "selected" : "" )}}>Sports and Physical Recreation</option>
                               <option {{(@$_GET['sector'] == "Staffing/Employment Agencies" ? "selected" : "" )}} >Staffing/Employment Agencies</option>
                               <option {{(@$_GET['sector'] == "Technology/Cyber/IT" ? "selected" : "" )}} >Technology/Cyber/IT</option>
                               <option {{(@$_GET['sector'] == "Telecommunications Services" ? "selected" : "" )}}>Telecommunications Services</option>
                               <option {{(@$_GET['sector'] == "Telecom/Communications" ? "selected" : "" )}}>Telecom/Communications</option>
                               <option {{(@$_GET['sector'] == "Transport and Storage - Materials" ? "selected" : "" )}}>Transport and Storage - Materials </option>
                               <option {{(@$_GET['sector'] == "Travel, Transportation and Tourism" ? "selected" : "" )}}>Travel, Transportation and Tourism</option>
                               <option {{(@$_GET['sector'] == "Waste Management" ? "selected" : "" )}}>Waste Management</option>
                               <option {{(@$_GET['sector'] == "Wholesale Trade/Import-Export" ? "selected" : "" )}}>Wholesale Trade/Import-Export</option>
                            </select>
                          </div>
		        </div>
		          <div class="col-sm-12 col-md-4 company_data {{(isset($_GET['sector']) ? "hide" : "" )}}">
                     <div class="form-group">
                           <input type="text" name="company_name" class="form-control company_name" id="company_name" placeholder='Company' value="{{@$_GET['company_name']}}">
                           
                     </div>
		        </div>
		        
			  </div>
			  <h2 class="last-three-month"> Last Three Months</h2>
                  <div id="chartContainer" style="height: 370px; margin: 0px auto;"></div>  
                  </div>
                            <div class="col-xs-12 col-md-6 second-div">
                                <h2 class="last-three-month"> Loreum Ipsum</h2>
                                <div class="white-box">
                                    
                                </div>
                            </div>
                </div>
                  <div class="row pad_30">
					
					 <div class="col-sm-12 col-md-12">
						
				     <div class="row">
						  <h2 class="last-three-month">#of Companies Engaged</h2>
						  <div id="chartContainerCompany" style="height: 370px; width: 900px;margin: 0px auto;"></div>  
					 </div>	 
					 </div>	 
					 </div>	 
                </div>
            </div>
        </div>
    </div>
@endsection

@section("css")
 <link rel="stylesheet" type="text/css" href="{{asset('public/css/jquery.ui.autocomplete.css')}}">
 <link rel="stylesheet" type="text/css" href="{{asset('public/css/dashboard.css')}}">
@endsection
@section("scripts")
 <script type="text/javascript" src="{{ asset('public/js/jquery.canvasjs.min.js') }}"></script>   
 <script type="text/javascript" src='{{ asset("public/js/jquery.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/jquery-ui.min.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/jquery-validation/dist/jquery.validate.min.js") }}'></script>
<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	dataPointWidth: 30,
	title:{
		text: "Job Analytics",
		//fontColor: "red",
		//fontFamily: "Verdana",
        fontWeight: 'bold',
        fontSize: 30
	},	
	axisY: {
		title: "",
		titleFontColor: "#4F81BC",
		lineColor: "#4F81BC",
		labelFontColor: "#4F81BC",
		tickColor: "#4F81BC"
	},
	toolTip: {
		shared: true
	},
	legend: {
		cursor:"pointer",
		itemclick: toggleDataSeries
	},
	data: [
	{
		type: "column",	
		name: "Opportunity",
		legendText: "Opportunity",
		showInLegend: true,
		dataPoints:<?php echo json_encode($dataArray); ?>
	},
	{
		type: "column",	
		name: "Job Seekers",
		legendText: "Job Seekers",
		axisYType: "secondary",
		showInLegend: true,
		dataPoints:<?php echo json_encode($dataArrayUser); ?>
	},
	{
		type: "column",
		name: "Job Providers",
		legendText: "Job Providers",
		axisYType: "secondary",
		showInLegend: true, 
		dataPoints: <?php echo json_encode($dataArrayCompany); ?>
	},
	{
		type: "column",	
		name: "Hired User",
		legendText: "Hired User",
		axisYType: "secondary",
		showInLegend: true,
		dataPoints:<?php echo json_encode($dataArrayHireUser); ?>
	}]
});
chart.render();

function toggleDataSeries(e) {
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else {
		e.dataSeries.visible = true;
	}
	chart.render();
}
var chart = new CanvasJS.Chart("chartContainerCompany", {
	animationEnabled: true,
	dataPointWidth: 30,
	title:{
		text: "#of Companies Engaged",
		//fontColor: "red",
		//fontFamily: "Verdana",
        fontWeight: 'bold',
        fontSize: 30
	},	
	axisY: {
		title: "",
		titleFontColor: "#4F81BC",
		lineColor: "#4F81BC",
		labelFontColor: "#4F81BC",
		tickColor: "#4F81BC"
	},
	axisX: {
				 interval: 1,
				labelAngle: -70 
				//labelMaxWidth: 50
			},
	toolTip: {
		shared: true
	},
	legend: {
		cursor:"pointer",
		itemclick: toggleDataSeries
	},
	data: [
	{
		type: "column",	
		name: "#of Companies Engaged",
		legendText: "#of Companies Engaged",
		showInLegend: true,
		dataPoints:<?php echo json_encode($compListByCategory); ?>
	}
	]
});
chart.render();

function toggleDataSeries(e) {
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else {
		e.dataSeries.visible = true;
	}
	chart.render();
}

}
function URL_add_parameter(url, param, value){
    var hash       = {};
    var parser     = document.createElement('a');

    parser.href    = url;

    var parameters = parser.search.split(/\?|&/);

    for(var i=0; i < parameters.length; i++) {
        if(!parameters[i])
            continue;

        var ary      = parameters[i].split('=');
        hash[ary[0]] = ary[1];
    }

    hash[param] = value;

    var list = [];  
    Object.keys(hash).forEach(function (key) {
        list.push(key + '=' + hash[key]);
    });

    parser.search = '?' + list.join('&');
    return parser.href;
}

	$('.dropdown_status_filter a').click(function(e){
	      e.preventDefault();
           var status = $(this).data('status');
           var status_name = $(this).data('status_name');
           $(this).parent().prev().text(status_name);
           $(this).parent().prev().attr('data-id', status);
           if(status_name =='filter_by'){
			   location.href = '{{ route('dashboard') }}';
		   }
		   else {
			     if(status_name == 'Company'){
					 $('.sector_data').addClass('hide');
					 $('.company_data').removeClass('hide');
				 }
				 else{
					$('.company_data').addClass('hide');
					 $('.sector_data').removeClass('hide'); 
				 }
			//   location.href = URL_add_parameter(location.href, 'param', status_name);
		   }

          }); 
     
    $(document).ready(function() {
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    src = "{{ route('searchajax') }}";
     $(".company_name").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term : request.term,_token: CSRF_TOKEN
                },
                success: function(data) {
                    response(data);
                   
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
        var com_id = ui.item.id;
        srcData = "{{ route('getData') }}";
        $.ajax({
            url: srcData,
            data: {
                id: com_id
            },
            success: function (data) {
			   	    location.href = '{{ route('dashboard') }}?param=Company&company_name='+data.name+'';   	   
            },
            error: function () {
                alert('There some errors');
            }
        });
       },

    });
 
  $('#companyIndustry').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
      location.href = '{{ route('dashboard') }}?param=Sector&sector='+valueSelected+'';  
	});  
  


});
          
          
</script>
@endsection

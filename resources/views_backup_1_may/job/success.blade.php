@extends('layouts.admin')

@section('content')
<div class="col main admin-login jobPostForm successMessage">
    <div class="container">
      <!-- Content Form Wrapper -->
      <div class="col-sm-12 formWrapper">
        <div class="fullwidth">
          <img src="{{ asset('public/images/white-logo.png') }}" class="logo" alt="logo">
          <h1>Worksource Montgomery's <br> Matching analytics resume  cloud interface</h1>
          <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</h4>
          <div class="col-sm-12">
            <div class="row">
              <div col-sm-12>
                <h4 class="success-message"> {{$message}}</h4>
              </div>
              <div class="col-sm-12">
                <div class="row">
				 <?php
				 $appendUrl="";
				 if($url !=''){
					 $appendUrl.='/'.$url;
				 }
				 if($last_company_id){
					 $appendUrl.='/'.$last_company_id;
				 }
				 
				 ?>	
                  <a href="{{route('post-job')}}{{$appendUrl}}" class='color-white'>Post another job</a>
                </div>
              </div>
            </div>          
          </div>
         </div>   
				
      </div>
      <!-- End of Content Form Wrapper -->
    </div>
@endsection
@section("css")
@endsection
@section("scripts")
@endsection

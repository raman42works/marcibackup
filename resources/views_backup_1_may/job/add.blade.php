@extends('layouts.admin')

@section('content')

   @include('dashboard_header')

    <!--- End of Header -->

    <!-- Main Wrapper -->
    <div class="mainWrapper">
        <div class="container-fluid">
            <div class="row">

                @include('left_navigation')

                <div class="col-sm-12 col-md-9 col-lg-10 contentWrapper">
          <h2 class="mTitile">Post a job</h2>
          <div class="outterWrapper col">
            <div class="row">
              <div class="col-sm-12 inner">
				    @if(session()->has('message'))
					<p class="success-message">
						{{ session()->get('message') }}
					</p>
				@endif
          <div class="jobPostForm">
            <form id="jobForm" action="{{ url('/job/submit') }}" method="post" enctype="multipart/form-data">
			<?php
			$stateData = array(""=>"Select","Alabama"=>"Alabama", "Arizona"=>"Arizona", "Arkansas"=>"Arkansas",
             "California"=>"California", "Colorado"=>"Colorado", "Connecticut"=>"Connecticut",
             "Delaware"=>"Delaware", "District of Columbia"=>"District of Columbia", "Florida"=>"Florida",
             "Georgia"=>"Georgia", "Hawaii"=>"Hawaii", "Idaho"=>"Idaho","Illinois"=>"Illinois", "Indiana"=>"Indiana",
              "Iowa"=>"Iowa","Kansas"=>"Kansas", "Kentucky"=>"Kentucky", "Louisiana"=>"Louisiana",
              "Maine"=>"Maine","Maryland"=>"Maryland", "Massachusetts"=>"Massachusetts", "Michigan"=>"Michigan",
              "Minnesota"=>"Minnesota","Mississippi"=>"Mississippi", "Missouri"=>"Missouri", "Montana"=>"Montana",
              "Nebraska"=>"Nebraska","Nevada"=>"Nevada", "New Hampshire"=>"New Hampshire", "New Jersey"=>"New Jersey",
              "New Mexico"=>"New Mexico","New York"=>"New York", "North Carolina"=>"North Carolina", "North Dakota"=>"North Dakota",
              "Ohio"=>"Ohio","Oklahoma"=>"Oklahoma", "Oregon"=>"Oregon", "Pennsylvania"=>"Pennsylvania",
              "Puerto Rico"=>"Puerto Rico","Rhode Island"=>"Rhode Island", "South Carolina"=>"South Carolina", "South Dakota"=>"South Dakota",
              "Tennessee"=>"Tennessee","Texas"=>"Texas", "Utah"=>"Utah", "Vermont"=>"Vermont",
              "Virgin Islands"=>"Virgin Islands","Virginia"=>"Virginia", "Washington"=>"Washington", "West Virginia"=>"West Virginia",
              "Wisconsin"=>"Wisconsin","Wyoming"=>"Wyoming", "Armed Forces Americas"=>"Armed Forces Americas", "Armed Force Europe, the Middle East, and Canada"=>"Armed Force Europe, the Middle East, and Canada",
              "Armed Forces Pacific"=>"Armed Forces Pacific","Federated States of Micronesia"=>"Federated States of Micronesia", "Guam"=>"Guam", "American Samoa"=>"American Samoa","Northern Mariana Islands"=>"Northern Mariana Islands",  
             );
			?>	
				
              <div class="row">
                <!-- Company Details -->
                <div class="col-sm-12 boxShadow">
                  <h4>Company Details</h4>
                  <div class="row">
                    <div class="col-sm-12 col-md-8 company_name_div {{(old('is_hide') ==1  ? "col-md-9":"col-md-12") }} ">
                      <div class="row">
                        <div class="col-sm-12 col-md-12 add_new_row fake-input">
							  <div class="form-group">
								<label for="company_name">Company Name <span>*</span></label>
								<input type="text" name="company_name" class="form-control company_name" id="company_name"  value="{{ old('company_name') }}">
								<input type='hidden' name='company_id' value="{{ old('company_id') }}" id='company_id'>
								<input type='hidden' name='user_id' value="{{ old('user_id') }}" id='user_id'>
								<input type='hidden' name='is_hide' value="{{ old('is_hide') }}" id='is_hide'>
							  </div>
                          <input type='hidden'  name='refernce_url' value='<?php echo $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>' >
                          <input type='hidden'  name='refernce_name' value='' >
                          {!! $errors->first('company_name', '<p class="error-danger">:message</p>') !!}
                        </div>

                        <div class="col-sm-12 col-md-6 Industry_row {{(old('is_hide') ==1  ? "":"hide") }}" {{(old('company_id')  ? "style=display:none":"") }}>
                          <div class="form-group">
                              <label for="companySize">Company Size <span>*</span></label>
                              <select class="form-control" id="companySize" name="company_size" >
                               <option value="">--SELECT--</option>
                               <option value="1 to 9 employees" {{(old('company_size') == "1 to 9 employees" ? "selected":"") }} >1 to 9 employees</option>
								   
                               <option value="10 to 19 employees" {{ (old('company_size') == "10 to 19 employees" ? "selected":"") }}>10 to 19 employees</option>
                               <option value="20 to 49 employees" {{ (old('company_size') == "20 to 49 employees" ? "selected":"") }}>20 to 49 employees</option>
                               <option value="50 to 99 employees" {{ (old('company_size') == "50 to 99 employees" ? "selected":"") }}>50 to 99 employees </option>
                               <option value="100 to 499 employees" {{ (old('company_size') == "100 to 499 employees" ? "selected":"") }}>100 to 499 employees </option>
                               <option value="500 to 999 employees" {{ (old('company_size') == "500 to 999 employees" ? "selected":"") }}>500 to 999 employees</option>
                               <option value="1,000 to 1,499 employees" {{ (old('company_size') == "1,000 to 1,499 employees" ? "selected":"") }}>1,000 to 1,499 employees</option>
                               <option value="1,500 to 1,999 employees" {{ (old('company_size') == "1,500 to 1,999 employees" ? "selected":"") }}>1,500 to 1,999 employees</option>
                               <option value="2,000 to 2,499 employees" {{ (old('company_size') == "2,000 to 2,499 employees" ? "selected":"") }}>2,000 to 2,499 employees</option>
                               <option value="2,500 to 4,999 employees" {{ (old('company_size') == "2,500 to 4,999 employees" ? "selected":"") }}>2,500 to 4,999 employees</option>
                               <option value="5,000 to 9,999 employees" {{ (old('company_size') == "5,000 to 9,999 employees" ? "selected":"") }}>5,000 to 9,999 employees</option>
                                <option value="10,000 employees or more" {{ (old('company_size') == "10,000 employees or more" ? "selected":"") }}>10,000 employees or more</option>
                            </select>
                          </div>
                          {!! $errors->first('company_size', '<p class="error-danger">:message</p>') !!}
                        </div>
                        <div class="col-sm-12 col-md-6 Industry_row {{(old('is_hide') ==1  ? "":"hide") }}" {{(old('company_id')  ? "style=display:none":"") }}>
                          <div class="form-group">
                            <label for="companyIndustry">Company Industry <span>*</span></label>
                            <select class="form-control" name="industry" id="companyIndustry" value="{{ old('industry') }}">
                               <!---->
                               <option value=''>--SELECT--</option>
                               <option value='Accounting and Auditing Services' {{(old('industry') == "Accounting and Auditing Services" ? "selected" : "" )}}>Accounting and Auditing Services</option>
                               <option {{(old('industry') == "Advertising and PR Services" ? "selected" : "" )}}>Advertising and PR Services</option>
                               <option {{(old('industry') == "Aerospace and Defense" ? "selected" : "" )}}>Aerospace and Defense</option>
                               <option {{(old('industry') == "Agriculture/Forestry/Fishing" ? "selected" : "" )}}>Agriculture/Forestry/Fishing</option>
                               <option {{(old('industry') == "Architectural and Design Services" ? "selected" : "" )}}>Architectural and Design Services</option>
                               <option {{(old('industry') == "Automotive and Parts Mfg" ? "selected" : "" )}}>Automotive and Parts Mfg</option>
                               <option {{(old('industry') == "Automotive Sales and Repair Services" ? "selected" : "" )}}>Automotive Sales and Repair Services</option>
                               <option {{(old('industry') == "Banking" ? "selected" : "" )}}>Banking</option>
                                <option {{(old('industry') == "BioLife/BioScience" ? "selected" : "" )}}>BioLife/BioScience</option>
                               <option {{(old('industry') == "Biotechnology/Pharmaceuticals" ? "selected" : "" )}}>Biotechnology/Pharmaceuticals</option>
                               <option {{(old('industry') == "Broadcasting, Music, and Film" ? "selected" : "" )}}>Broadcasting, Music, and Film</option>
                               <option {{(old('industry') == "Business Services - Other" ? "selected" : "" )}}>Business Services - Other</option>
                               <option {{(old('industry') == "Chemicals/Petro-Chemicals" ? "selected" : "" )}}>Chemicals/Petro-Chemicals</option>
                               <option {{(old('industry') == "Clothing and Textile Manufacturing" ? "selected" : "" )}}>Clothing and Textile Manufacturing</option>
                               <option {{(old('industry') == "Consulting" ? "selected" : "" )}}>Consulting</option>
                               <option {{(old('industry') == "Computer Hardware" ? "selected" : "" )}}>Computer Hardware</option>
                               <option {{(old('industry') == "Computer Software" ? "selected" : "" )}}>Computer Software</option>
                               <option {{(old('industry') == "Computer/IT Services" ? "selected" : "" )}}>Computer/IT Services</option>
                               <option {{(old('industry') == "Construction - Industrial Facilities and Infrastructure" ? "selected" : "" )}}>Construction - Industrial Facilities and Infrastructure</option>
                               <option {{(old('industry') == "Construction - Residential &amp; Commercial/Office" ? "selected" : "" )}}>Construction - Residential &amp; Commercial/Office</option>
                               <option {{(old('industry') == "Consumer Packaged Goods Manufacturing" ? "selected" : "" )}}>Consumer Packaged Goods Manufacturing</option>
                               <option {{(old('industry') == "Education" ? "selected" : "" )}} >Education</option>
                               <option {{(old('industry') == "Electronics, Components, and Semiconductor Mfg" ? "selected" : "" )}}>Electronics, Components, and Semiconductor Mfg</option>
                               <option {{(old('industry') == "Energy and Utilities" ? "selected" : "" )}}>Energy and Utilities</option>
                                <option {{(old('industry') == "Energy/Renewable" ? "selected" : "" )}}>Energy/Renewable</option>
                               <option {{(old('industry') == "Engineering Services" ? "selected" : "" )}}>Engineering Services</option>
                               <option {{(old('industry') == "Entertainment Venues and Theaters" ? "selected" : "" )}}>Entertainment Venues and Theaters</option>
                               <option {{(old('industry') == "Financial Services" ? "selected" : "" )}}>Financial Services</option>
                               <option {{(old('industry') == "Food and Beverage Production" ? "selected" : "" )}}>Food and Beverage Production</option>
                               <option {{(old('industry') == "Government and Military" ? "selected" : "" )}}>Government and Military</option>
                               <option {{(old('industry') == "Healthcare Services" ? "selected" : "" )}}>Healthcare Services</option>
                               <option {{(old('industry') == "Hospitality" ? "selected" : "" )}}>Hospitality</option>
                               <option {{(old('industry') == "Hotels and Lodging" ? "selected" : "" )}}>Hotels and Lodging</option>
                               <option {{(old('industry') == "Insurance" ? "selected" : "" )}}>Insurance</option>
                               <option {{(old('industry') == "Internet Services" ? "selected" : "" )}}>Internet Services</option>
                               <option {{(old('industry') == "Legal Services" ? "selected" : "" )}}>Legal Services</option>
                               <option {{(old('industry') == "Management Consulting Services" ? "selected" : "" )}}>Management Consulting Services</option>
                               <option {{(old('industry') == "Manufacturing - Other" ? "selected" : "" )}}>Manufacturing - Other</option>
                               <option {{(old('industry') == "Marine Mfg &amp; Services" ? "selected" : "" )}}>Marine Mfg &amp; Services</option>
                               <option {{(old('industry') == "Medical Devices and Supplies" ? "selected" : "" )}}>Medical Devices and Supplies</option>
                               <option {{(old('industry') == "Metals and Minerals" ? "selected" : "" )}} >Metals and Minerals</option>
                               <option {{(old('industry') == "Nonprofit Charitable" ? "selected" : "" )}}>Nonprofit Charitable Organizations</option>
                               <option {{(old('industry') == "Other/Not Classified" ? "selected" : "" )}}>Other/Not Classified</option>
                               <option {{(old('industry') == "Performing and Fine Arts" ? "selected" : "" )}}>Performing and Fine Arts</option>
                               <option {{(old('industry') == "Personal and Household Services" ? "selected" : "" )}}>Personal and Household Services</option>
                               <option {{(old('industry') == "Personnel/Staffing" ? "selected" : "" )}}>Personnel/Staffing</option>
                               <option {{(old('industry') == "Printing and Publishing" ? "selected" : "" )}}>Printing and Publishing </option>
                               <option {{(old('industry') == "Real Estate/Property Management" ? "selected" : "" )}}>Real Estate/Property Management</option>
                               <option {{(old('industry') == "Rental Services" ? "selected" : "" )}}>Rental Services</option>
                               <option {{(old('industry') == "Restaurant/Food Services" ? "selected" : "" )}}>Restaurant/Food Services</option>
                               <option {{(old('industry') == "Retail" ? "selected" : "" )}}>Retail</option>
                               <option {{(old('industry') == "Security and Surveillance" ? "selected" : "" )}}>Security and Surveillance</option>
                               <option {{(old('industry') == "Sports and Physical Recreation" ? "selected" : "" )}}>Sports and Physical Recreation</option>
                               <option {{(old('industry') == "Staffing/Employment Agencies" ? "selected" : "" )}} >Staffing/Employment Agencies</option>
                               <option {{(old('industry') == "Technology/Cyber/IT" ? "selected" : "" )}}>Technology/Cyber/IT</option>
                               <option {{(old('industry') == "Telecommunications Services" ? "selected" : "" )}}>Telecommunications Services</option>
                               <option {{(old('industry') == "Telecom/Communications" ? "selected" : "" )}}>Telecom/Communications</option>
                               <option {{(old('industry') == "Transport and Storage - Materials" ? "selected" : "" )}}>Transport and Storage - Materials </option>
                               <option {{(old('industry') == "Travel, Transportation and Tourism" ? "selected" : "" )}}>Travel, Transportation and Tourism</option>
                               <option {{(old('industry') == "Waste Management" ? "selected" : "" )}}>Waste Management</option>
                               <option {{(old('industry') == "Wholesale Trade/Import-Export" ? "selected" : "" )}}>Wholesale Trade/Import-Export</option>
                            </select>
                           
                          </div>
                           {!! $errors->first('industry', '<p class="error-danger">:message</p>') !!}    
                        </div> 
                                 
                      </div>
                       
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-3 logouploader {{(old('is_hide') ==1  ? "":"hide") }}">
                      <input type="file hidden" name="logouploader" class="form-control">
                      <div class="form-group text-right">
						  
                        <label>Company Logo</label>
                        <a href="javascript:void(0);" id="fileUpload">
                        <div class="col logoUpload">
                          <span>+</span>
                        </div>
                        </a>
                        <input style="display: none;" type="file" id="logo" name="logo" onchange="loadFile(event)"/>
                      </div>
                       {!! $errors->first('logo', '<p class="error-danger">:message</p>') !!}
                    </div>
                    <div class="col-sm-12">
                      <div class="row address_row {{(old('is_hide') == '0' ? "hide":"") }}">
                        <div class="col-sm-12">
                          <div class="form-group">
							  
							  <?php
							  $check ='';
                              if(old('confidential') == 1){
								 $check='checked'; 
							  }
							
							?>	
                            <small>
                              <input type="checkbox" name="confidential" value=1 id='confidential' {{$check}} {{(old('company_id') ? 'disabled' : "" )}}>
                              Keep company name confidential
                            </small>
                          </div>
                        </div>

                        <div class="col-sm-12">
                          <label><strong>ADDRESS</strong></label>
                        </div>
                        
                        <div class="col-sm-12 col-md-6">
                          <div class="form-group">
                            <label for="company_add1">Street Address 1 <span>*</span></label>
                            <input type="text" class="form-control" name="address" id="company_add1" value="{{ old('address') }}" {{(old('company_id') ? 'readonly' : "" )}}>
                          </div>
                          {!! $errors->first('address', '<p class="error-danger">:message</p>') !!}
                        </div>
                        <div class="col-sm-12 col-md-6">
                          <div class="form-group">
                            <label for="company_add2">Street Address 2 </label>
                      <input type="text" class="form-control" name="address_2" id="company_add2" value="{{ old('address_2') }}" {{(old('company_id') ? 'readonly' : "" )}}>
                          </div> 
                        </div>
                        <div class="col-sm-12 col-md-4">
                          <div class="form-group">
                            <label for="city">City <span>*</span></label>
                            <input type="text" class="form-control" id="city" name="city" value="{{ old('city') }}" {{(old('company_id') ? 'readonly' : "" )}}>
                          </div>
                           {!! $errors->first('city', '<p class="error-danger">:message</p>') !!}
                        </div>
                        <div class="col-sm-12 col-md-4">
                          <div class="form-group">
							  <?php
                              $val =false;
                              $state=old('state');
                              if(old('company_id')){
								 $val=true; 
							  }
							  
                              ?>
							  
                              <label for="state">State <span>*</span></label>
                               {{ Form::select('state', $stateData,old('state'), ['class' => 'form-control','id'=>'state','disabled' => $val]) }}
                              
                          </div>
                           {!! $errors->first('state', '<p class="error-danger">:message</p>') !!}
                        </div>
                        <div class="col-sm-12 col-md-4">
                          <div class="form-group">
                            <label for="zip">Zip Code <span>*</span></label>
                            <input type="text" class="form-control" name="zip" id="zip" value="{{ old('zip') }}" {{(old('company_id') ? 'readonly' : "" )}}>
                          </div>
                           {!! $errors->first('zip', '<p class="error-danger">:message</p>') !!}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End of Company Details -->
                <div class="clearfix"></div>

                <!-- Contact Detail-->
                <div class="col-sm-12 boxShadow">
					<h4>Contact Details</h4>
					  
						            <div class="row">         
					                 <div class="col-sm-12 col-md-6">
							  <div class="form-group">
								<label for="contact_name">Contact First Name <span>*</span></label>
								<input type="text" class="form-control" name="contact_name" id="contact_name" value="{{ old('contact_name') }}">
							  </div>
                          <p class="error-danger"> {{str_replace("contact name","contact first name",$errors->first('contact_name'))}}	</p>
                        </div>                        
							<div class="col-sm-12 col-md-6">
								  <div class="form-group">
									<label for="company_email">Contact Last Name <span>*</span></label>
									<input type="text" name="contact_last_name" id="contact_last_name" class="form-control" value="{{ old('contact_last_name') }}">
								  </div>
							   {!! $errors->first('contact_last_name', '<p class="error-danger">:message</p>') !!}
							</div>   
								 </div>  
                      <div class="row">                   
							<div class="col-sm-12 col-md-6">
								  <div class="form-group">
									<label for="company_email">Contact Email <span>*</span></label>
									<input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}">
								  </div>
								<p class="error-danger"> {{str_replace("email","contact email",$errors->first('email'))}}	</p>				   
							</div>
							<div class="col-sm-12 col-md-6">
								  <div class="form-group">
									<label for="contact_phone">Contact Phone <span>*</span></label>
									<input type="text" class="form-control" name="contact_phone" id="contact_phone" value="{{ old('contact_phone') }}">
								  </div>
							  {!! $errors->first('contact_phone', '<p class="error-danger">:message</p>') !!}
							</div>
                      </div>
                   
            
				</div>
                
                <!-- End of Contact Details -->

                <div class="clearfix"></div>

                <!-- Job Details-->
                <div class="col-sm-12 boxShadow">
                  <h4>Job Details</h4>
                  <div class="row">
                    <div class="col-sm-12 col-md-8">
                      <div class="form-group">
                        <label for="job_title">Job Title <span>*</span></label>
                        <input type="text" class="form-control" name="job_title" id="job_title" value="{{ old('job_title') }}">
                      </div>
                         {!! $errors->first('job_title', '<p class="error-danger">:message</p>') !!}
                    </div>
                    <div class="col-sm-12 col-md-4">
                      <div class="form-group">
                        <label for="jobStatus">Job Type  <span>*</span></label>
                        <select class="form-control" id="job_status" name="job_status" value="{{ old('job_status') }}">
                           <!---->
                            <option  {{(old('job_status') == "1" ? "selected":"")}} value="1" >Full Time</option>
                           <option  {{(old('job_status') == "2" ? "selected":"")}} value="2" >Temporary/ Contract</option>
                           <option  {{(old('job_status') == "3" ? "selected":"")}} value="3" >Intern</option>
                           <option  {{(old('job_status') == "4" ? "selected":"")}} value="4">Part Time</option>
                           <option {{(old('job_status') == "5" ? "selected":"")}} value="5" >Seasonal</option>
                           
                        </select>
                      </div>
                    </div>
                       {!! $errors->first('job_status', '<p class="error-danger">:message</p>') !!}
                  </div>

                  <div class="row">
                    <div class="col-sm-12 col-md-9 col-lg-4 rangeField">
                      <div class="row signleBox">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <label>Salary <small class="font-size-12">(in USD)</small></label>
                            <input type="number" class="form-control" id="" name="range" value="{{ old('range') }}">
                          </div>
                        </div>
                      </div>
                      <div class="row doubleBox" style="display: none;">
                        <div class="col-sm-5">
                          <div class="form-group">
                            <label>Min Salary <small class="font-size-12">(in USD)</small></label>
                            <input type="number" class="form-control min" id="min" name="min_range" value="{{ old('min_range') }}">
                          </div>
                        </div>
                        <div class="col-sm-2 to">
                          To
                        </div>
                        <div class="col-sm-5">
                          <div class="form-group">
                            <label>Max Salary <small class="font-size-12">(in USD)</small></label>
                            <input type="number" class="form-control max" id="max" name="max_range" value="{{ old('max_range') }}">
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-2 rangeBtn">
                      <div class="form-group">
                        <label></label>
                        <small>
                          <input type="checkbox" name="salary_range" id="rangeBtn" placeholder="Enter Amount">
                          Add Range
                        </small>
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-2">
                      <div class="form-group">
                        <label># of Positions</label>
						<select class="form-control" id="" name="no_of_jobs" value="{{ old('no_of_jobs') }}">
                           <!---->
                           <option  {{(old('no_of_jobs') == "1" ? "selected":"")}} value="1" >1</option>
                           <option  {{(old('no_of_jobs') == "2" ? "selected":"")}} value="2" >2</option>
                           <option  {{(old('no_of_jobs') == "3" ? "selected":"")}} value="3" >3</option>
                           <option  {{(old('no_of_jobs') == "4" ? "selected":"")}} value="4">4</option>
                           <option {{(old('no_of_jobs') == "5" ? "selected":"")}} value="5" >5</option>
                           <option {{(old('no_of_jobs') == "6" ? "selected":"")}} value="6" >6</option>
                           <option {{(old('no_of_jobs') == "7" ? "selected":"")}} value="7" >7</option>
                           <option {{(old('no_of_jobs') == "8" ? "selected":"")}} value="8" >8</option>
                           <option {{(old('no_of_jobs') == "9" ? "selected":"")}} value="9" >9</option>
                           <option {{(old('no_of_jobs') == "10" ? "selected":"")}} value="10" >10</option>
                           
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-2">
                      <div class="form-group">
                        <label>Bonus</label>
                        <input type="number" class="form-control " id="max" name="bonus" value="{{ old('bonus') }}">
                      </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-2">
                      <div class="form-group">
                        <label></label>
                        {{ Form::select('salaryType', ['Per Year', 'Per Year', 'Per Week','Per Month', 'Biweekly', 'Per Day'],old('salaryType'),['class' => 'form-control','id'=>'salaryType']) }}
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End of Job Details -->

                <!-- Job Requirement -->
                <div class="col-sm-12 boxShadow">
                  <h4>Job Requirements</h4>
                  <div class="row">
                    <div class="col-sm-12 col-md-6">
                      <div class="form-group">
                        <label>Minimum Education Level <span>*</span></label>
                        <select aria-label="educatonlevel" class="form-control" id="educatonLevel" name="educatonLevel" value="{{ old('educatonLevel') }}">
                           <option value="">Select</option>
                          <option {{(old('educatonLevel') == "12" ? "selected" : "" )}} value="12">Some High School Coursework</option>
						    <option {{(old('educatonLevel') == "1" ? "selected" : "" )}} value="1">High School or equivalent</option>
							<option {{(old('educatonLevel') == "2" ? "selected" : "" )}} value="2">Certification</option>
							<option {{(old('educatonLevel') == "3" ? "selected" : "" )}} value="3">Vocational</option>
							<option {{(old('educatonLevel') == "9" ? "selected" : "" )}} value="9">Some College Coursework Completed</option>
							<option {{(old('educatonLevel') == "4" ? "selected" : "" )}} value="4">Associate Degree</option>
							<option {{(old('educatonLevel') == "5" ? "selected" : "" )}} value="5">Bachelor's Degree</option>
							<option {{(old('educatonLevel') == "6" ? "selected" : "" )}} value="6">Master's Degree</option>
							<option {{(old('educatonLevel') == "7" ? "selected" : "" )}} value="7">Doctorate</option>
							<option {{(old('educatonLevel') == "8" ? "selected" : "" )}} value="8">Professional</option>
                        </select>
                      </div>
                       {!! $errors->first('educatonLevel', '<p class="error-danger">:message</p>') !!}
                    </div>
                   
                    <div class="col-sm-12 col-md-6">
                      <div class="form-group">
                        <label>Year of Experience <span>*</span></label>
                        <span class="outterSect">
                        <select aria-label="experience" class="form-control" id="experience" name="experience" value="{{ old('experience') }}">
                           <option value="">Select</option>
                           <option value="1" {{(old('experience') == "1" ? "selected" : "" )}}><1</option>
                           <option value="1-2" {{(old('experience') == "1-2" ? "selected" : "" )}}>1-2</option>
                           <option value="2-5" {{(old('experience') == "2-5" ? "selected" : "" )}}>2-5</option>
                           <option value="5-10" {{(old('experience') == "5-10" ? "selected" : "" )}}>5-10</option>
                           <option value="10+" {{(old('experience') == "10+" ? "selected" : "" )}}>10+</option>
                        </select>
                         <a href="#" role="button" aria-pressed="false" title="Nice to have" class='nice_have_exp niceTxt' data-id='1'>Nice to have
                          <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Year of Experience"></i>
                         </a>
                       </span>
                        <input type="hidden" name="expLevel" class="form-control exp_level" id="skill_level" value=1>
                      </div>
                       {!! $errors->first('experience', '<p class="error-danger">:message</p>') !!}
                    </div>
                   
                    <div class="col-sm-12 col-md-10 ">
                      <div class="form-group">
                       <label>Degree/Major <span>*</span></label>
                      </div>
                      
                       @if(Form::old('degree'))
						   @foreach(old('degree') as $key => $val)
                           <div class="form-group degreeFiled first">
                            <span class="outterSect">
							{!! Form::text('degree['.$key.']', old('degree.'.$key), array('class'=>'form-control','id'=>'degree_keywords')) !!}
							
							<a href="#" role="button" aria-pressed="false" title="Nice to have" class='nice_have niceTxt' data-id='1'>Nice to have
                <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Degree/Major"></i>
              </a>
							<input type="hidden" name="degreeLevel[]" class="form-control skill_level" id="degree_level" value=1>
								<div class="optionsBtn">
								  <a href="javascript:void(0)" id="addDegreeField" data-toggle="tooltip" title="Add Input Field">
									<i class="fa fa-plus-circle" aria-hidden="true"></i>
								  </a>
								   @if ($key > 0)
								  <a href="#" id="deleteDegreeField" data-toggle="tooltip" title="Delete Input Field">
									<i class="fa fa-minus-circle" aria-hidden="true"></i>
								  </a>
								  @endif
								</div>
							<p class="error-danger"> {{str_replace("degree.$key","Degree",$errors->first('degree.'.$key))}}
                             </p>
               </span>
                            </div>
                         
                             @endforeach 
                        @else
                        
                        <div class="form-group degreeFiled first">
                          <span class="outterSect">
							<input type="text" name="degree[]" class="form-control" id="degree_keywords" >
							<a href="#" role="button" aria-pressed="false" title="Nice to have" class='nice_have niceTxt' data-id='1'>Nice to have
                <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Degree/Major"></i>
              </a>
            
							<input type="hidden" name="degreeLevel[]" class="form-control skill_level" id="degree_level" value=1>
								<div class="optionsBtn">
								  <a href="javascript:void(0)" id="addDegreeField" data-toggle="tooltip" title="Add Input Field">
									<i class="fa fa-plus-circle" aria-hidden="true"></i>
								  </a>
								</div>
              </span>
                      </div>  
                                          
                       @endif
                    </div>
                    <div class="col-sm-12 col-md-10 ">
                      <div class="form-group">
                        <label>Skills/Keywords <span>*</span><br><small>Enter terms related to candidates industry or expertise(e.g. java, telesales, call center, retail, SOX)</small></label>
                      </div>
                      <?php
                      $old = session()->getOldInput();
                      ?>
                      @if(!empty($old['skills']))  
                      @foreach(old('skills') as $key => $val)
                      <span class="outterSect">
                           <div class="form-group skillFiled first">
							{!! Form::text('skills['.$key.']', old('skills.'.$key), array('class'=>'form-control','id'=>'skill_keywords')) !!}
							
							<a href="#" role="button" aria-pressed="false" title="Nice to have" class='nice_have niceTxt' data-id='1'>Nice to have
                <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Skills/Keywords"></i>
              </a>
							<input type="hidden" name="skillsLevel[]" class="form-control skill_level" id="skill_level" value=1>
								<div class="optionsBtn">
								  <a href="javascript:void(0)" id="addSkillField" data-toggle="tooltip" title="Add Input Field">
									<i class="fa fa-plus-circle" aria-hidden="true"></i>
								  </a>
								   @if ($key > 0)
								  <a href="#" id="deleteSkillField" data-toggle="tooltip" title="Delete Input Field">
									<i class="fa fa-minus-circle" aria-hidden="true"></i>
								  </a>
								  @endif
								</div>
						<p class="error-danger"> {{str_replace("skills.$key","Skills/Keywords",$errors->first('skills.'.$key))}}
                             </p>
                            </div>
                            </span>
                             @endforeach 
                       @else
                      <div class="form-group skillFiled first">
                        <span class="outterSect">
							<input type="text" name="skills[]" class="form-control" id="skill_keywords" >
							<a href="#" role="button" aria-pressed="false" title="Nice to have" class='nice_have niceTxt' data-id='1'>Nice to have
                <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Skills/keywords"></i>
              </a>
            </span>
							<input type="hidden" name="skillsLevel[]" class="form-control skill_level" id="skill_level" value=1>
							 
                        
							<div class="optionsBtn">
							  <a href="javascript:void(0)" id="addSkillField" data-toggle="tooltip" title="Add Input Field">
								<i class="fa fa-plus-circle" aria-hidden="true"></i>
							  </a>
							</div>
                      </div>
                      @endif
                    </div>
                  </div>
                  <input type="hidden" name="" class="form-control company_exist_name" id="skill_level">
                </div>
                <!-- End of Job Requirement -->

                <!-- Job Descrption -->
                <div class="col-sm-12 boxShadow">
                  <h4>Job Description</h4>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                       <textarea aria-hidden="true" name="job_description" value="{{ old('job_description') }}">{{ old('job_description') }}</textarea>
                      </div>
                      <div class="form-group text-center">
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                      <input type="hidden" name="admin_save" value=1>
                        <input type="submit" name="submit" class="btn btn-primary mx-auto col-sm-12 col-md-2" value="Submit">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End of Job Descrption -->
              </div>
            </form>
          </div>
        </div>
            </div>
          </div>
        </div>
            </div>
        </div>
    </div>

@endsection
@section("css")
 <link rel="stylesheet" type="text/css" href="{{asset('public/css/jquery.ui.autocomplete.css')}}">
@endsection
@section("scripts")

<script type="text/javascript" src='{{ asset("public/js/jquery.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/jquery-ui.min.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/jquery-validation/dist/jquery.validate.min.js") }}'></script>

<script>
  jQuery(document).ready(function(){
    $("#fileUpload").click(function(){
      $("#logo").trigger("click");
    })
$(".doubleBox").hide();
    $(document).on("click", "#addSkillField", function(event){
      event.preventDefault();
      if($('.skillFiled').length <= 9){
		  $(".skillFiled:last").after('<div class="form-group skillFiled"><span class="outterSect"><input type="text" name="skills[]" class="form-control" id="skill_keywords"><a href="#" role="button" aria-pressed="false" title="Nice to have" class="nice_have niceTxt" data-id="1">Nice to have <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Skills/Keywords"></i></a><input type="hidden" name="skillsLevel[]" class="form-control skill_level" id="skill_level" value=1><div class="optionsBtn"><a href="javascript:void(0)" id="addSkillField" data-toggle="tooltip" title="Add Input Field"><i class="fa fa-plus-circle" aria-hidden="true"></i></a><a href="javascript:void()" id="deleteSkillField" data-toggle="tooltip" title="Delete Input Field"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></div></span></div>');
	  }
	  else {
		 alert('You are reached to maximum skills'); 
	  }
        
    });
    
    $(document).on("click", "#addDegreeField", function(event){
      event.preventDefault();
      if($('.degreeFiled').length <= 9){
		  $(".degreeFiled:last").after('<div class="form-group degreeFiled"><span class="outterSect"><input type="text" name="degree[]" class="form-control" id="degree_keywords"><a href="#" role="button" aria-pressed="false" title="Nice to have" class="nice_have niceTxt" data-id="1">Nice to have <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Degree/Major"></i></a><input type="hidden" name="degreeLevel[]" class="form-control skill_level" id="skill_level" value=1><div class="optionsBtn"><a href="javascript:void(0)" id="addDegreeField" data-toggle="tooltip" title="Add Input Field"><i class="fa fa-plus-circle" aria-hidden="true"></i></a><a href="javascript:void()" id="deleteDegreeField" data-toggle="tooltip" title="Delete Input Field"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></div></span></div>');
	  }
	  else {
		 alert('You are reached to maximum degree'); 
	  }
        
    });

   
    $(document).on("click", "#deleteSkillField", function(event){
      event.preventDefault();
      $(this).closest(".skillFiled").remove();
    });
    $(document).on("click", "#deleteDegreeField", function(event){
      event.preventDefault();
      $(this).closest(".degreeFiled").remove();
    });
    
    
    $(document).on("click", ".nice_have", function(event){
      event.preventDefault();
      if($(this).data('id')=='1'){
		  $(this).text('Required');
		  $(this).data('id','2');
		  $(this).next('.skill_level' ).val(2);
      $(this).append('<i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Degree/Major"></i>');
	  }
	  else {
		   $(this).text('Nice to have');
		   $(this).data('id','1');
		   $(this).next('.skill_level').val(1);
       $(this).append('<i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Degree/Major"></i>');
	  }
    });
    
    $(document).on("click", ".nice_have_exp", function(event){
      event.preventDefault();
     // e.preventDefault();	
      if($(this).data('id')=='1'){
		  $(this).text('Required'); 
       $(this).append('<i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Year of Experience"></i>');
       
		  $(this).data('id','2');
		  $(this).next('.exp_level' ).val(2);
	  }
	  else {
		   $(this).text('Nice to have');
		   $(this).data('id','1');
		   $(this).next('.exp_level').val(1);
       $(this).append('<i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Year of Experience"></i>');
	  }
    });
   

    /*$("#jobForm").validate(
      {
        "rules" : {
          "company_name" : {
            "required" : true
          },
          "company_size" : {
            "required" : true
          },
          "industry" : {
            "required" : true
          },
          "contact_name" : {
            "required" : true
          },
          "contact_email" : {
            "required" : true,
            "email" : true
          },
          "contact_phone" : {
            "required" : true
          },
          "address" : {
            "required" : true
          },
          "city" : {
            "required" : true
          },
          "state" : {
            "required" : true
          },
          "zip" : {
            "required" : true
          },
          "job_title" : {
            "required" : true
          },
          "job_type" : {
            "required" : true
          },
          "job_status" : {
            "required" : true
          },
          "job_description" : {
            "required" : true
          },
          'logo': { 
                "required": false,
                "extension": "png|jpg|jpeg|gif"
            }


        }
      }
    );*/
  });
  

    $(document).ready(function() {
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    src = "{{ route('searchajax') }}";
     $(".company_name").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term : request.term,_token: CSRF_TOKEN
                },
                success: function(data) {
                    response(data);
                   
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
        var com_id = ui.item.id;
        if (!ui.item.id){
			return false;
        } 
        srcData = "{{ route('getData') }}";
        $.ajax({
            url: srcData,
            data: {
                id: com_id
            },
            success: function (data) {
				$('.company_name').css('background', 'url('+data.logo+') no-repeat scroll 7px 7px');
				$('.company_name').css('background-size', '20px');
				$('.company_name').css('padding-left', '30px');
				$('.Industry_row').hide();
				$('.address_row').hide();
				$('#is_hide').val(0);
				$('.logouploader').addClass('hide');
				$('.company_name_div').removeClass('col-md-9');
                $('.company_name_div').addClass('col-md-12');
             //  $('#companySize').val(data.company_size).attr('readonly', true);
             //  $('#companySize').find("option").prop("hidden", true);
             //  $('#companyIndustry').val(data.industry).attr('readonly', true);
             //  $('#companyIndustry').find("option").prop("hidden", true);
            //   $('#contact_name').val(data.contact_name);
            //   $('#contact_last_name').val(data.contact_last_name);
               $('#company_email').val(data.contact_email).attr('readonly', true);
               $('#company_add1').val(data.address).attr('readonly', true);
               $('#company_add2').val(data.address1).attr('readonly', true);
               $('#city').val(data.city).attr('readonly', true);
             //  $('#contact_phone').val(data.phone);
               $('#state').val(data.state).attr('readonly', true);
               $('#state').find("option").prop("hidden", true);
               $('#zip').val(data.zip).attr('readonly', true);
               $('#company_id').val(data.id);
               $('#user_id').val(data.user_id);
               $('#confidential').prop('disabled', true);
               $('.company_exist_name').val(data.name);
               if(data.confidential =='1'){
				   $('#confidential').prop('checked', true);
			   }  
			   if(data.imageExist =='0'){
				   
			   }
			   else {
				   var img=data.logo;
				   var html="<img src="+img+" class='img-fluid'>";
				   $('.logoUpload').html(html);
			   }  
			   			   
            },
            error: function () {
                alert('There some errors');
            }
        });
       },
     /* change: function () {
        console.log('ss');
               if($(this).val() != $('.company_exist_name').val()){
               $('.Industry_row').show();
               $('#contact_name').val('');
               $('#contact_last_name').val('');
               $('#company_email').val('').attr('readonly', false);
               $('#company_add1').val('').attr('readonly', false);
               $('#company_add2').val('').attr('readonly', false);
               $('#city').val('').attr('readonly', false);
               $('#contact_phone').val('');
               $('#state').val('').attr('readonly', false);
               $('#state').find("option").prop("hidden", false);
               $('#zip').val('').attr('readonly', false);
               $('#company_id').val('');
               $('#user_id').val('');
               $('#confidential').prop('checked', false);
               $('#confidential').prop('disabled', false);
               var html='<span>+</span>';
               $('.logoUpload').html(html);
               }
      } */
       
    });
    $(".company_name").on( "autocompletechange", function(event,ui) {
   if($(this).val() != $('.company_exist_name').val()){
	           $('#is_hide').val(1);
               /*$('#companySize').val('').attr('readonly', false);
               $('#companyIndustry').val('').attr('readonly', false);
               $('#companyIndustry').find("option").prop("hidden", false);
               $('#companySize').find("option").prop("hidden", false);*/
               $('.company_name').css('background', 'url()');
               $('.company_name').css('padding-left', '5px');
               $('.company_name_div').addClass('col-md-9');
               $('.company_name_div').removeClass('col-md-12');
               $('.logouploader').removeClass('hide');
               $('.Industry_row').show();
               $('#contact_name').val('');
               $('#contact_last_name').val('');
               $('#company_email').val('').attr('readonly', false);
               $('#company_add1').val('').attr('readonly', false);
               $('#company_add2').val('').attr('readonly', false);
               $('#city').val('').attr('readonly', false);
               $('#contact_phone').val('');
               $('#state').val('').attr('readonly', false);
               $('#state').find("option").prop("hidden", false);
               $('#zip').val('').attr('readonly', false);
               $('#company_id').val('');
               $('#user_id').val('');
               $('#confidential').prop('checked', false);
               $('#confidential').prop('disabled', false);
               $('.address_row').show();
               var html='<span>+</span>';
               $('.logoUpload').html(html);
         }
    });
    
   $(".company_name").data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    
    var $li = $('<li>'),
        $img = $('<img>');
    $img.attr({
      src:  item.logo,
      alt: '',
      height: '20px',
    });
    if(item.label ==''){
		console.log(1);
	}
	else {
		$li.attr('data-value', item.label);
    $li.append('<a href="javascript:void(0);">');
    if(item.value=='No Result Found'){
		    $li.find('a').html(item.label);
	}
	else {
		    $li.find('a').append($img).append(" "+item.label);
	}
	}
    
    //$li.find('a').append($img).append(item.label);
    return $li.appendTo(ul);
  };
 $(".company_name").keyup(function(){
        if($(".company_name").val() == ''){
			$('.company_name').css('background', 'url()');
            $('.company_name').css('padding-left', '5px');
               $('#company_add1').val('').attr('readonly', false);
               $('#company_add2').val('').attr('readonly', false);
               $('#city').val('').attr('readonly', false);
               $('#contact_phone').val('');
               $('#state').val('').attr('readonly', false);
               $('#state').find("option").prop("hidden", false);
               $('#zip').val('').attr('readonly', false);
               $('#company_id').val('');
               $('#user_id').val('');
               $('.address_row').show();
		}
    });

});
var loadFile = function(event) {
    var html="<img src="+URL.createObjectURL(event.target.files[0])+" class='img-fluid'>";
    $('.logoUpload').html(html);
  };
</script>


@endsection

@extends('layouts.admin')

@section('content')

   @include('dashboard_header')

    <!--- End of Header -->

    <!-- Main Wrapper -->
    <div class="mainWrapper">
        <div class="container-fluid">
            <div class="row">

                @include('left_navigation')

                <div class="col-sm-12 col-md-9 col-lg-10 contentWrapper">
          <h2 class="mTitile">View All Jobs</h2>
				  @if(session('success'))
					<p class="success-message">{{session('success')}}</p>
				@endif
          <div class="outterWrapper table col">
            <div class="row">
              <table id="jobs" class="table table-striped table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
				 
					    <thead>
           <!-- <tr>
                <th colspan="2"></th>
                <th colspan="2">Contact</th>
                <th colspan="3"></th>
            </tr> -->
                  <tr>
			  			<!--<th>Id</th> -->
						<th>Title</th>
						<th>Company</th>
						<!--<th>Email</th> -->
						<th>Contact</th>
						<th>Type</th>
						<th>Submitted From</th>
						<th>CREATED ON</th>
						<th class="text-center">Actions</th>
						
					</tr>
        
					
			     </thead>
             <!--   <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>City</th>
                    <th class="text-center">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>demo@demo.com</td>
                    <td>95513456464</td>
                    <td>USA</td>
                    <td class="text-center">
                      <a href="#" class="">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>demo@demo.com</td>
                    <td>56568687656</td>
                    <td>USA</td>
                    <td class="text-center">
                      <a href="#" class="">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>demo@demo.com</td>
                    <td>4568769876587</td>
                    <td>USA</td>
                    <td class="text-center">
                      <a href="#" class="">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>demo@demo.com</td>
                    <td>554687687687</td>
                    <td>USA</td>
                    <td class="text-center">
                      <a href="#" class="">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>demo@demo.com</td>
                    <td>7868767687687</td>
                    <td>USA</td>
                    <td class="text-center">
                      <a href="#" class="">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                </tbody>-->
              </table> 
            </div>
          </div>
        </div>

                <!-- <div class="col-sm-12 col-md-10 contentArea">
                    <h2>Jobs</h2>

                    <div id="page-content-wrapper">
                       <div id="page-content">
                          <div class="container">
                            <div id="page-title">
                          <div class="row">
                            <div class="col-md-8">
                              <h2>Publications</h2>
                            </div>
                            <div class="col-md-4 text-right">
                              <a href="{{ url('/') }}" class="btn btn-primary">Add New</a>
                            </div>
                          </div>  
                        </div>
                        <div class="panel">
                          <div class="panel-body">
                            <div class="table-responsive">
                              
                              <table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap dataTable no-footer dtr-inline" cellspacing="0" width="100%" role="grid" aria-describedby="datatable-responsive_info" style="width: 100%;">
                                <thead>
                                  <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Continent</th>
                                    <th>Country</th>
                                    <th>Language</th>
                                    <th>Website url</th>
                                    <th>Rssfeed url</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                
                                </tbody>
                              </table>
                            </div>
                          </div>
                             </div>
                          </div>
                       </div>
                    </div>




                </div> -->
            </div>
        </div>
    </div>

@endsection
@section("scripts")
<script type="text/javascript" src='{{ asset("public/js/jquery.js") }}'></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
  var deletePath = '';
  $(document).ready(function(){
    $('#datatable-responsive').on('click',".deleteAction",function(event){

      $("#alertModal").modal({"show": true});

      deletePath = $(this).attr('data-newspaper-delete-route');


      $("#alertModal").on('click',".closeModal",function(){
        
        top.location.href = deletePath;

      });
    });
    $("#datatable-responsive").DataTable({
      "responsive" : true,
      "processing": true,
      "serverSide": true,
       "ajax": "",
        "language": {
                    "searchPlaceholder": "Search",
                    "sEmptyTable":     "No records found"
              }
    });
  })
  
  $(document).ready(function() {
    oTable = $("#jobs").DataTable({
        "processing": true,
        "responsive" : true,
        "serverSide": true,
        "ajax": "{{ route('datatable.getjobs') }}",
        "columns": [
           // {data: 'id', name: 'id'},
            {data: 'job_title', name: 'job_title'},
            {data: 'company_profile_id', name: 'company_profile_id'},
            {data: 'email', name: 'email'},
           // {data: 'phone_no', name: 'phone_no'},
            {data: 'job_status', name: 'job_status'},
            {data: 'refernce_url', name: 'refernce_url'},
             {data: 'created_at', name: 'created_at', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false}
            
        ],"columnDefs": [
			{ className: "text-center action", "targets": [ 6 ] }
       ]
    });
});
</script>
@endsection

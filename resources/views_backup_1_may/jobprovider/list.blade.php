@extends('layouts.admin')

@section('content')

   @include('dashboard_header')

    <!--- End of Header -->

    <!-- Main Wrapper -->
    <div class="mainWrapper">
        <div class="container-fluid">
            <div class="row">

                @include('left_navigation')

                <div class="col-sm-12 col-md-9 col-lg-10 contentWrapper">
          <h2 class="mTitile">View All Job Providers</h2>
            @if(session('success'))
					 <p class="success-message">{{session('success')}}</p>
				@endif
          <div class="outterWrapper table col">
            <div class="row">
              <table id="jobprovider" class="table table-striped table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th class='hide'>Contact</th>           
                    <th>Company Size</th>
                    <th>Company Type</th>
                     <th>CREATED ON</th>
                    <th class="text-center">Actions</th>
                   
                  </tr>
                </thead>
          
              </table>
            </div>
          </div>
        </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")
<script type="text/javascript" src='{{ asset("public/js/jquery.js") }}'></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
  var deletePath = '';
  $(document).ready(function(){
    $('#datatable-responsive').on('click',".deleteAction",function(event){

      $("#alertModal").modal({"show": true});

      deletePath = $(this).attr('data-newspaper-delete-route');


      $("#alertModal").on('click',".closeModal",function(){
        
        top.location.href = deletePath;

      });
    });
    $("#datatable-responsive").DataTable({
      "responsive" : true,
      "processing": true,
          "serverSide": true,
      "columnDefs": [{
                "targets": [5, 6, 8], // column or columns numbers
                "orderable": false,  // set orderable for selected columns
              }],
          "ajax": "",
        "language": {
                    "searchPlaceholder": "Search",
                    "sEmptyTable":     "No records found"
              }
    });
    $(document).on('click',".delete_pro",function(event){
      event.preventDefault();
      var url = $(this).data('url');
      
      if (confirm('Are you sure you want to delete company and its all related jobs')) {
        location.href = url;
         }
      
      });
  })
  
  
  
    $(document).ready(function() {
    oTable = $("#jobprovider").DataTable({
        "processing": true,
        "responsive" : true,
        "serverSide": true,
        "ajax": "{{ route('datatable.getjobprovider') }}",
        "columns": [
            {data: 'company_name', name: 'company_name'},
           // {data: 'email', name: 'email', orderable: false, searchable: false},
            //{data: 'phone', name: 'phone'},
            {data: 'company_size', name: 'company_size'},
            {data: 'industry', name: 'industry'},
             {data: 'created_at', name: 'created_at', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false}
            
        ],"columnDefs": [
			{ className: "text-center", "targets": [ 4 ] }
       ]
    });
   });
</script>
@endsection

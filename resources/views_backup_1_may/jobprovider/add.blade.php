@extends('layouts.admin')

@section('content')

   @include('dashboard_header')

    <!--- End of Header -->

    <!-- Main Wrapper -->
    <div class="mainWrapper">
        <div class="container-fluid">
            <div class="row">

                @include('left_navigation')
               <?php
			$stateData = array(""=>"Select","Alabama"=>"Alabama", "Arizona"=>"Arizona", "Arkansas"=>"Arkansas",
             "California"=>"California", "Colorado"=>"Colorado", "Connecticut"=>"Connecticut",
             "Delaware"=>"Delaware", "District of Columbia"=>"District of Columbia", "Florida"=>"Florida",
             "Georgia"=>"Georgia", "Hawaii"=>"Hawaii", "Idaho"=>"Idaho","Illinois"=>"Illinois", "Indiana"=>"Indiana",
              "Iowa"=>"Iowa","Kansas"=>"Kansas", "Kentucky"=>"Kentucky", "Louisiana"=>"Louisiana",
              "Maine"=>"Maine","Maryland"=>"Maryland", "Massachusetts"=>"Massachusetts", "Michigan"=>"Michigan",
              "Minnesota"=>"Minnesota","Mississippi"=>"Mississippi", "Missouri"=>"Missouri", "Montana"=>"Montana",
              "Nebraska"=>"Nebraska","Nevada"=>"Nevada", "New Hampshire"=>"New Hampshire", "New Jersey"=>"New Jersey",
              "New Mexico"=>"New Mexico","New York"=>"New York", "North Carolina"=>"North Carolina", "North Dakota"=>"North Dakota",
              "Ohio"=>"Ohio","Oklahoma"=>"Oklahoma", "Oregon"=>"Oregon", "Pennsylvania"=>"Pennsylvania",
              "Puerto Rico"=>"Puerto Rico","Rhode Island"=>"Rhode Island", "South Carolina"=>"South Carolina", "South Dakota"=>"South Dakota",
              "Tennessee"=>"Tennessee","Texas"=>"Texas", "Utah"=>"Utah", "Vermont"=>"Vermont",
              "Virgin Islands"=>"Virgin Islands","Virginia"=>"Virginia", "Washington"=>"Washington", "West Virginia"=>"West Virginia",
              "Wisconsin"=>"Wisconsin","Wyoming"=>"Wyoming", "Armed Forces Americas"=>"Armed Forces Americas", "Armed Force Europe, the Middle East, and Canada"=>"Armed Force Europe, the Middle East, and Canada",
              "Armed Forces Pacific"=>"Armed Forces Pacific","Federated States of Micronesia"=>"Federated States of Micronesia", "Guam"=>"Guam", "American Samoa"=>"American Samoa","Northern Mariana Islands"=>"Northern Mariana Islands",  
             );
			?>	

                <div class="col-sm-12 col-md-9 col-lg-10 contentWrapper">
          <h2 class="mTitile">Add a NEW Job Provider</h2>
          <div class="outterWrapper col">
			    @if(session()->has('message'))
    <p class="success-message">
        {{ session()->get('message') }}
    </p>
@endif
            <div class="row">
              <div class="col-sm-12 inner">
                <div class="jobPostForm">
                  <form action="{{ url('/jobprovider/submit') }}" method="post" enctype="multipart/form-data">
                    <div class="row">
                      <!-- Company Details -->
                   <!-- Company Details -->
                <div class="col-sm-12 boxShadow">
                  <h4>Company Details</h4>
                  <div class="row">
                    <div class="col-sm-12 col-md-9">
                      <div class="row">
                        <div class="col-sm-12 col-md-12">
                          <div class="form-group">
                            <label for="company_name">Company Name <span>*</span></label>
                            <input type="text" name="company_name" class="form-control company_name" id="company_name"  value="{{ old('company_name') }}">
                            <input type='hidden' name='company_id' value="{{ old('company_id') }}" id='company_id'>
                            <input type='hidden' name='user_id' value="{{ old('user_id') }}" id='user_id'>
                          </div>
                          <input type='hidden'  name='refernce_url' value='<?php echo $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>' >
                          {!! $errors->first('company_name', '<p class="error-danger">:message</p>') !!}
                        </div>

                      

                        <div class="col-sm-12 col-md-6">
                          <div class="form-group">
                              <label for="companySize">Company Size <span>*</span></label>
                              <select class="form-control" id="companySize" name="company_size" >
                               <option value="">--SELECT--</option>
                               <option value="1 to 9 employees" {{(old('company_size') == "1 to 9 employees" ? "selected":"") }} >1 to 9 employees</option>
								   
                               <option value="10 to 19 employees" {{ (old('company_size') == "10 to 19 employees" ? "selected":"") }}>10 to 19 employees</option>
                               <option value="20 to 49 employees" {{ (old('company_size') == "20 to 49 employees" ? "selected":"") }}>20 to 49 employees</option>
                               <option value="50 to 99 employees" {{ (old('company_size') == "50 to 99 employees" ? "selected":"") }}>50 to 99 employees </option>
                               <option value="100 to 499 employees" {{ (old('company_size') == "100 to 499 employees" ? "selected":"") }}>100 to 499 employees </option>
                               <option value="500 to 999 employees" {{ (old('company_size') == "500 to 999 employees" ? "selected":"") }}>500 to 999 employees</option>
                               <option value="1,000 to 1,499 employees" {{ (old('company_size') == "1,000 to 1,499 employees" ? "selected":"") }}>1,000 to 1,499 employees</option>
                               <option value="1,500 to 1,999 employees" {{ (old('company_size') == "1,500 to 1,999 employees" ? "selected":"") }}>1,500 to 1,999 employees</option>
                               <option value="2,000 to 2,499 employees" {{ (old('company_size') == "2,000 to 2,499 employees" ? "selected":"") }}>2,000 to 2,499 employees</option>
                               <option value="2,500 to 4,999 employees" {{ (old('company_size') == "2,500 to 4,999 employees" ? "selected":"") }}>2,500 to 4,999 employees</option>
                               <option value="5,000 to 9,999 employees" {{ (old('company_size') == "5,000 to 9,999 employees" ? "selected":"") }}>5,000 to 9,999 employees</option>
                               <option>10,000 employees or more</option>
                            </select>
                          </div>
                          {!! $errors->first('company_size', '<p class="error-danger">:message</p>') !!}
                        </div>
                        <div class="col-sm-12 col-md-6">
                          <div class="form-group">
                            <label for="companyIndustry">Company Industry <span>*</span></label>
                            <select class="form-control" name="industry" id="companyIndustry" value="{{ old('industry') }}">
                               <!---->
                               <option value=''>--SELECT--</option>
                               <option value='Accounting and Auditing Services' {{(old('industry') == "Accounting and Auditing Services" ? "selected" : "" )}}>Accounting and Auditing Services</option>
                               <option {{(old('industry') == "Advertising and PR Services" ? "selected" : "" )}}>Advertising and PR Services</option>
                               <option {{(old('industry') == "Aerospace and Defense" ? "selected" : "" )}}>Aerospace and Defense</option>
                               <option {{(old('industry') == "Agriculture/Forestry/Fishing" ? "selected" : "" )}}>Agriculture/Forestry/Fishing</option>
                               <option {{(old('industry') == "Architectural and Design Services" ? "selected" : "" )}}>Architectural and Design Services</option>
                               <option {{(old('industry') == "Automotive and Parts Mfg" ? "selected" : "" )}}>Automotive and Parts Mfg</option>
                               <option {{(old('industry') == "Automotive Sales and Repair Services" ? "selected" : "" )}}>Automotive Sales and Repair Services</option>
                               <option {{(old('industry') == "Banking" ? "selected" : "" )}}>Banking</option>
                               <option {{(old('industry') == "BioLife/BioScience" ? "selected" : "" )}}>BioLife/BioScience</option>
                               <option {{(old('industry') == "Biotechnology/Pharmaceuticals" ? "selected" : "" )}}>Biotechnology/Pharmaceuticals</option>
                               <option {{(old('industry') == "Broadcasting, Music, and Film" ? "selected" : "" )}}>Broadcasting, Music, and Film</option>
                               <option {{(old('industry') == "Business Services - Other" ? "selected" : "" )}}>Business Services - Other</option>
                               <option {{(old('industry') == "Chemicals/Petro-Chemicals" ? "selected" : "" )}}>Chemicals/Petro-Chemicals</option>
                               <option {{(old('industry') == "Clothing and Textile Manufacturing" ? "selected" : "" )}}>Clothing and Textile Manufacturing</option>
                               <option {{(old('industry') == "Consulting" ? "selected" : "" )}}>Consulting</option>
                               <option {{(old('industry') == "Computer Hardware" ? "selected" : "" )}}>Computer Hardware</option>
                               <option {{(old('industry') == "Computer Software" ? "selected" : "" )}}>Computer Software</option>
                               <option {{(old('industry') == "Computer/IT Services" ? "selected" : "" )}}>Computer/IT Services</option>
                               <option {{(old('industry') == "Construction - Industrial Facilities and Infrastructure" ? "selected" : "" )}}>Construction - Industrial Facilities and Infrastructure</option>
                               <option {{(old('industry') == "Construction - Residential &amp; Commercial/Office" ? "selected" : "" )}}>Construction - Residential &amp; Commercial/Office</option>
                               <option {{(old('industry') == "Consumer Packaged Goods Manufacturing" ? "selected" : "" )}}>Consumer Packaged Goods Manufacturing</option>
                               <option {{(old('industry') == "Education" ? "selected" : "" )}} >Education</option>
                               <option {{(old('industry') == "Electronics, Components, and Semiconductor Mfg" ? "selected" : "" )}}>Electronics, Components, and Semiconductor Mfg</option>
                               <option {{(old('industry') == "Energy/Renewable" ? "selected" : "" )}}>Energy/Renewable</option>
                               <option {{(old('industry') == "Energy and Utilities" ? "selected" : "" )}}>Energy and Utilities</option>
                               <option {{(old('industry') == "Engineering Services" ? "selected" : "" )}}>Engineering Services</option>
                               <option {{(old('industry') == "Entertainment Venues and Theaters" ? "selected" : "" )}}>Entertainment Venues and Theaters</option>
                               <option {{(old('industry') == "Financial Services" ? "selected" : "" )}}>Financial Services</option>
                               <option {{(old('industry') == "Food and Beverage Production" ? "selected" : "" )}}>Food and Beverage Production</option>
                               <option {{(old('industry') == "Government and Military" ? "selected" : "" )}}>Government and Military</option>
                               <option {{(old('industry') == "Healthcare Services" ? "selected" : "" )}}>Healthcare Services</option>
                               <option {{(old('industry') == "Hospitality" ? "selected" : "" )}}>Hospitality</option>
                               <option {{(old('industry') == "Hotels and Lodging" ? "selected" : "" )}}>Hotels and Lodging</option>
                               <option {{(old('industry') == "Insurance" ? "selected" : "" )}}>Insurance</option>
                               <option {{(old('industry') == "Internet Services" ? "selected" : "" )}}>Internet Services</option>
                               <option {{(old('industry') == "Legal Services" ? "selected" : "" )}}>Legal Services</option>
                               <option {{(old('industry') == "Management Consulting Services" ? "selected" : "" )}}>Management Consulting Services</option>
                               <option {{(old('industry') == "Manufacturing - Other" ? "selected" : "" )}}>Manufacturing - Other</option>
                               <option {{(old('industry') == "Marine Mfg &amp; Services" ? "selected" : "" )}}>Marine Mfg &amp; Services</option>
                               <option {{(old('industry') == "Medical Devices and Supplies" ? "selected" : "" )}}>Medical Devices and Supplies</option>
                               <option {{(old('industry') == "Metals and Minerals" ? "selected" : "" )}} >Metals and Minerals</option>
                               <option {{(old('industry') == "Nonprofit Charitable" ? "selected" : "" )}}>Nonprofit Charitable Organizations</option>
                               <option {{(old('industry') == "Other/Not Classified" ? "selected" : "" )}}>Other/Not Classified</option>
                               <option {{(old('industry') == "Performing and Fine Arts" ? "selected" : "" )}}>Performing and Fine Arts</option>
                               <option {{(old('industry') == "Personal and Household Services" ? "selected" : "" )}}>Personal and Household Services</option>
                               <option {{(old('industry') == "Personnel/Staffing" ? "selected" : "" )}}>Personnel/Staffing</option>
                               <option {{(old('industry') == "Printing and Publishing" ? "selected" : "" )}}>Printing and Publishing </option>
                               <option {{(old('industry') == "Real Estate/Property Management" ? "selected" : "" )}}>Real Estate/Property Management</option>
                               <option {{(old('industry') == "Rental Services" ? "selected" : "" )}}>Rental Services</option>
                               <option {{(old('industry') == "Restaurant/Food Services" ? "selected" : "" )}}>Restaurant/Food Services</option>
                               <option {{(old('industry') == "Retail" ? "selected" : "" )}}>Retail</option>
                               <option {{(old('industry') == "Security and Surveillance" ? "selected" : "" )}}>Security and Surveillance</option>
                               <option {{(old('industry') == "Sports and Physical Recreation" ? "selected" : "" )}}>Sports and Physical Recreation</option>
                               <option {{(old('industry') == "Staffing/Employment Agencies" ? "selected" : "" )}} >Staffing/Employment Agencies</option>
                               <option {{(old('industry') == "Technology/Cyber/IT" ? "selected" : "" )}}>Technology/Cyber/IT</option>
                               <option {{(old('industry') == "Telecommunications Services" ? "selected" : "" )}}>Telecommunications Services</option>
                               <option {{(old('industry') == "Telecom/Communications" ? "selected" : "" )}}>Telecom/Communications</option>
                               <option {{(old('industry') == "Transport and Storage - Materials" ? "selected" : "" )}}>Transport and Storage - Materials </option>
                               <option {{(old('industry') == "Travel, Transportation and Tourism" ? "selected" : "" )}}>Travel, Transportation and Tourism</option>
                               <option {{(old('industry') == "Waste Management" ? "selected" : "" )}}>Waste Management</option>
                               <option {{(old('industry') == "Wholesale Trade/Import-Export" ? "selected" : "" )}}>Wholesale Trade/Import-Export</option>
                            </select>
                           
                          </div>
                           {!! $errors->first('industry', '<p class="error-danger">:message</p>') !!}    
                        </div>                   
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-3 logouploader">
                      <input type="file hidden" name="logouploader" class="form-control">
                      <div class="form-group text-right">
						  
                        <label>Company Logo</label>
                        <a href="javascript:void(0);" id="fileUpload">
                        <div class="col logoUpload">
                          <span>+</span>
                        </div>
                        </a>
                        <input style="display: none;" type="file" id="logo" name="logo" />
                      </div>
                       {!! $errors->first('logo', '<p class="error-danger">:message</p>') !!}

                    </div>
                    

                    
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <small>
                              <input type="checkbox" name="confidential" value=1 id='confidential'>
                              Keep company name confidential.
                            </small>
                          </div>
                        </div>

                        <div class="col-sm-12">
                          <label><strong>ADDRESS</strong></label>
                        </div>
                        
                        <div class="col-sm-12 col-md-6">
                          <div class="form-group">
                            <label for="company_add1">Street Address 1 <span>*</span></label>
                            <input type="text" class="form-control" name="address" id="company_add1" value="{{ old('address') }}">
                          </div>
                          {!! $errors->first('address', '<p class="error-danger">:message</p>') !!}
                        </div>
                        <div class="col-sm-12 col-md-6">
                          <div class="form-group">
                            <label for="company_add2">Street Address 2 </label>
                            <input type="text" class="form-control" name="address_2" id="company_add2" value="{{ old('address_2') }}">
                          </div> 
                        </div>
                        <div class="col-sm-12 col-md-4">
                          <div class="form-group">
                            <label for="city">City <span>*</span></label>
                            <input type="text" class="form-control" id="city" name="city" value="{{ old('city') }}">
                          </div>
                           {!! $errors->first('city', '<p class="error-danger">:message</p>') !!}
                        </div>
                        <div class="col-sm-12 col-md-4">
                          <div class="form-group">
                              <label for="state">State <span>*</span></label>
                               {{ Form::select('state', $stateData,old('state'), ['class' => 'form-control','id'=>'state']) }}
                              
                          </div>
                           {!! $errors->first('state', '<p class="error-danger">:message</p>') !!}
                        </div>
                        <div class="col-sm-12 col-md-4">
                          <div class="form-group">
                            <label for="zip">Zip Code <span>*</span></label>
                            <input type="text" class="form-control" name="zip" id="zip" value="{{ old('zip') }}">
                          </div>
                           {!! $errors->first('zip', '<p class="error-danger">:message</p>') !!}
                        </div>
                      </div>
                    </div>
                    
                  </div>
                  <div class="form-group text-center">
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <input type="submit" name="submit" class="btn btn-primary mx-auto col-sm-12 col-md-2" value="Submit">
                      </div>
                    </div>
                </div>
                <!-- End of Company Details -->

                      <div class="clearfix"></div>

                    

                    </div>
                    
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")
<script type="text/javascript" src='{{ asset("public/js/datatable/datatable.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/datatable/datatable-bootstrap.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/datatable/datatable-responsive.js") }}'></script>

<script type="text/javascript">
	$("#fileUpload").click(function(){
      $("#logo").trigger("click");
    })
  var deletePath = '';
  $(document).ready(function(){
    $('#datatable-responsive').on('click',".deleteAction",function(event){

      $("#alertModal").modal({"show": true});

      deletePath = $(this).attr('data-newspaper-delete-route');


      $("#alertModal").on('click',".closeModal",function(){
        
        top.location.href = deletePath;

      });
    });
    $("#datatable-responsive").DataTable({
      "responsive" : true,
      "processing": true,
          "serverSide": true,
      "columnDefs": [{
                "targets": [5, 6, 8], // column or columns numbers
                "orderable": false,  // set orderable for selected columns
              }],
          "ajax": "",
        "language": {
                    "searchPlaceholder": "Search",
                    "sEmptyTable":     "No records found"
              }
    });
  })
</script>
@endsection

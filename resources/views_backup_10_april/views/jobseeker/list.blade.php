@extends('layouts.admin')

@section('content')

   @include('dashboard_header')

    <!--- End of Header -->

    <!-- Main Wrapper -->
    <div class="mainWrapper">
        <div class="container-fluid">
            <div class="row">

                @include('left_navigation')

                <div class="col-sm-12 col-md-9 col-lg-10 contentWrapper">
          <h2 class="mTitile">View All Job Seekers</h2>
           
                @if(session('success'))
					<p class="success-message">{{session('success')}}</p>
				@endif
          <div class="outterWrapper table col">
            <div class="row">
              <table id="jobseeker" class="table table-striped table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                <thead>
					<tr>
						<!--<th>Id</th> -->
						<th>Name</th>
						<th>Contact</th>
						
						<th>Resume Title</th>
						<th>Submitted From</th>
						<th class="text-center">Actions</th>
					</tr>
			     </thead>
              <!--  <tbody>
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>demo@demo.com</td>
                    <td>95513456464</td>
                    <td>USA</td>
                    <td class="text-center">
                      <a href="#" class="">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>demo@demo.com</td>
                    <td>56568687656</td>
                    <td>USA</td>
                    <td class="text-center">
                      <a href="#" class="">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>demo@demo.com</td>
                    <td>4568769876587</td>
                    <td>USA</td>
                    <td class="text-center">
                      <a href="#" class="">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>demo@demo.com</td>
                    <td>554687687687</td>
                    <td>USA</td>
                    <td class="text-center">
                      <a href="#" class="">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>demo@demo.com</td>
                    <td>7868767687687</td>
                    <td>USA</td>
                    <td class="text-center">
                      <a href="#" class="">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <a href="">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                </tbody> -->
              </table>
            </div>
          </div>
        </div>
            </div>
        </div>
    </div>

@endsection
@section("scripts")
<script type="text/javascript" src='{{ asset("public/js/jquery.js") }}'></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
  var deletePath = '';
  $(document).ready(function(){
    $('#datatable-responsive').on('click',".deleteAction",function(event){

      $("#alertModal").modal({"show": true});

      deletePath = $(this).attr('data-newspaper-delete-route');


      $("#alertModal").on('click',".closeModal",function(){
        
        top.location.href = deletePath;

      });
    });
    $("#datatable-responsive").DataTable({
      "responsive" : true,
      "processing": true,
          "serverSide": true,
      "columnDefs": [{
                "targets": [5, 6, 8], // column or columns numbers
                "orderable": false,  // set orderable for selected columns
              }],
          "ajax": "",
        "language": {
                    "searchPlaceholder": "Search",
                    "sEmptyTable":     "No records found"
              }
    });
  })
  
  $(document).ready(function() {
    oTable = $("#jobseeker").DataTable({
        "processing": true,
        "responsive" : true,
        "serverSide": true,
        "ajax": "{{ route('datatable.getjobseeker') }}",
        "columns": [
           // {data: 'id', name: 'id'},
            {data: 'first_name', name: 'first_name'},
          //  {data: 'email', name: 'email', orderable: false, searchable: false},
            {data: 'phone_no', name: 'phone_no'},
            {data: 'resume_title', name: 'resume_title'},
          //  {data: 'resume_url', name: 'resume_url'},
            {data: 'refernce_url', name: 'refernce_url'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],"columnDefs": [
			{ className: "text-center", "targets": [ 4 ] }
       ]
    });
   });
</script>
@endsection

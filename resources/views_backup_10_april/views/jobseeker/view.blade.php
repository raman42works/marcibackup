@extends('layouts.admin')

@section('content')

   @include('dashboard_header')

    <!--- End of Header -->

    <!-- Main Wrapper -->
    <div class="mainWrapper">
        <div class="container-fluid">
            <div class="row">

                @include('left_navigation')

                <div class="col-sm-12 col-md-9 col-lg-10 contentWrapper">
                  <h2 class="mTitile">Edit job seeker</h2>
                  <div class="outterWrapper col">
					   @if(session()->has('message'))
						<p class="success-message">
							{{ session()->get('message') }}
						</p>
					@endif
                    <div class="row">
                      <div class="col-sm-12 inner">
                        <div class="jobPostForm">
                           <form id="jobForm" action="{{ url('/jobseeker/update') }}" method="post" enctype="multipart/form-data">
                            <div class="row">
								
                              <!-- Seeker Details -->
                              <div class="col-sm-12 boxShadow">
                                <h4>Job Seeker Details</h4>
                                  <?php
			$resumeOriginData = array(""=>"Select","CareerBuilder"=>"CareerBuilder", "Career"=>"Career", "College"=>"College",
             "Corporate Website"=>"Corporate Website", "Craigs List"=>"Craigs List", "Current Employee"=>"Current Employee",
             "Dice"=>"Dice", "Employee Referral"=>"Employee Referral", "HotJobs"=>"HotJobs",
             "Media"=>"Media", "Monster"=>"Monster", "Newspapers"=>"Newspapers","Niche Job Board"=>"Niche Job Board", "Outdoor   Advertising"=>"Outdoor Advertising",
              "Rehire"=>"Rehire","Search Engine"=>"Search Engine", "Social Network"=>"Social Network", "Staffing Firm"=>"Staffing Firm",
              "Walk In"=>"Walk In","Other"=>"Other"  
             );
             $stateData = array(""=>"Select","Alabama"=>"Alabama", "Arizona"=>"Arizona", "Arkansas"=>"Arkansas",
             "California"=>"California", "Colorado"=>"Colorado", "Connecticut"=>"Connecticut",
             "Delaware"=>"Delaware", "District of Columbia"=>"District of Columbia", "Florida"=>"Florida",
             "Georgia"=>"Georgia", "Hawaii"=>"Hawaii", "Idaho"=>"Idaho","Illinois"=>"Illinois", "Indiana"=>"Indiana",
              "Iowa"=>"Iowa","Kansas"=>"Kansas", "Kentucky"=>"Kentucky", "Louisiana"=>"Louisiana",
              "Maine"=>"Maine","Maryland"=>"Maryland", "Massachusetts"=>"Massachusetts", "Michigan"=>"Michigan",
              "Minnesota"=>"Minnesota","Mississippi"=>"Mississippi", "Missouri"=>"Missouri", "Montana"=>"Montana",
              "Nebraska"=>"Nebraska","Nevada"=>"Nevada", "New Hampshire"=>"New Hampshire", "New Jersey"=>"New Jersey",
              "New Mexico"=>"New Mexico","New York"=>"New York", "North Carolina"=>"North Carolina", "North Dakota"=>"North Dakota",
              "Ohio"=>"Ohio","Oklahoma"=>"Oklahoma", "Oregon"=>"Oregon", "Pennsylvania"=>"Pennsylvania",
              "Puerto Rico"=>"Puerto Rico","Rhode Island"=>"Rhode Island", "South Carolina"=>"South Carolina", "South Dakota"=>"South Dakota",
              "Tennessee"=>"Tennessee","Texas"=>"Texas", "Utah"=>"Utah", "Vermont"=>"Vermont",
              "Virgin Islands"=>"Virgin Islands","Virginia"=>"Virginia", "Washington"=>"Washington", "West Virginia"=>"West Virginia",
              "Wisconsin"=>"Wisconsin","Wyoming"=>"Wyoming", "Armed Forces Americas"=>"Armed Forces Americas", "Armed Force Europe, the Middle East, and Canada"=>"Armed Force Europe, the Middle East, and Canada",
              "Armed Forces Pacific"=>"Armed Forces Pacific","Federated States of Micronesia"=>"Federated States of Micronesia", "Guam"=>"Guam", "American Samoa"=>"American Samoa","Northern Mariana Islands"=>"Northern Mariana Islands",  
             );
             
			?>	
                                <div class="row">
                                  <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                      <label>First Name <span>*</span></label>
                                      <input type="input" class="form-control" name="firstName" id="firstName" value="{{$jobseeker['first_name']}}">
                                    </div>
                                  </div>
                                  <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                      <label>Last Name <span>*</span></label>
                                      <input type="input" class="form-control" name="lastName" id="lasttName" value="{{$jobseeker['last_name']}}">
                                    </div>
                                  </div>
                                  <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                      <label>Email <span>*</span></label>
                                      <input type="email" class="form-control" name="email" id="email" value="{{$email}}" readonly>
                                    </div>
                                  </div>
                                  <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                      <label>Phone <span>*</span></label>
                                      <input type="tel" class="form-control" name="phone" id="phone" value="{{$jobseeker['phone_no']}}">
                                    </div>
                                  </div>
                                  <div class="col-sm-12 col-md-4">
                                    <div class="form-group">
                                      <label>City <span>*</span></label>
                                      <input type="input" class="form-control" name="city" id="city" value="{{$jobseeker['city']}}">
                                    </div>
                                  </div>
                                  <div class="col-sm-12 col-md-4">
                                    <div class="form-group">
                                      <label>State <span>*</span></label>
                                       {{ Form::select('state', $stateData,$jobseeker['state'], ['class' => 'form-control','id'=>'state']) }}
                                    </div>
                                  </div>
                                  <div class="col-sm-12 col-md-4">
                                    <div class="form-group">
                                      <label>Zip Code <span>*</span></label>
                                      <input type="input" class="form-control" name="zipCode" id="zipCode" value="{{$jobseeker['zip']}}">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <!-- End of Seeker Details -->

                              <!-- Resume Details -->
                              <div class="col-sm-12 boxShadow">
                                <h4>Resume</h4>
                                <div class="row">
                                  <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                      <label>Resume Title <small> (Optional)</small></label> 
                                      <input type="type" name="resumeTitle" class="form-control" value="{{$jobseeker['resume_title']}}">
                                    </div>
                                  </div>
                                  <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                      <label>Resume Origin <small> (Optional)</small></label> 
                                       {{ Form::select('resumeOrigin', $resumeOriginData,$jobseeker['resume_origin'], ['class' => 'form-control','id'=>'resumeOrigin']) }}
                                    </div>
                                  </div>
                                  <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                      <label>Highest Education Degree <span>*</span></label> 
                                      <select class="form-control" name="educationDegree" id="educationDegree">
										  <option {{($jobseeker['highest_degree'] == "Professional" ? "selected" : "" )}} value="Professional">Professional</option>
										  <option {{($jobseeker['highest_degree'] == "Doctorate" ? "selected" : "" )}} value="Doctorate">Doctorate</option>
										  <option {{($jobseeker['highest_degree'] == "Master's Degree" ? "selected" : "" )}} value="Master's Degree">Master's Degree</option>
										  <option {{($jobseeker['highest_degree'] == "Bachelor's Degree" ? "selected" : "" )}} value="Bachelor's Degree">Bachelor's Degree</option>
										  <option {{($jobseeker['highest_degree'] == "Associate Degree" ? "selected" : "" )}} value="Associate Degree">Associate Degree</option>
										  <option {{($jobseeker['highest_degree'] == "Some College Coursework Completed" ? "selected" : "" )}} value="Some College Coursework Completed">Some College Coursework Completed</option>
										  <option {{($jobseeker['highest_degree'] == "Vocational" ? "selected" : "" )}} value="Vocational">Vocational</option>
										  <option {{($jobseeker['resume_origin'] == "Certification" ? "selected" : "" )}} value="Certification">Certification</option>
										  	<option {{($jobseeker['highest_degree'] == "High School or equivalent" ? "High School or equivalent" : "" )}} value="High School or equivalent">High School or equivalent</option>
										  	<option {{($jobseeker['highest_degree'] == "Some High School Coursework" ? "selected" : "" )}} value="Some High School Coursework">Some High School Coursework</option>
                                        <option {{($jobseeker['highest_degree'] == "Unspecified" ? "selected" : "" )}} value="Unspecified" >Unspecified</option>
									
										
										
										
										
										
										<!--
										
										<option {{($jobseeker['highest_degree'] == "Vocational - HS Diploma" ? "selected" : "" )}} value="Vocational - HS Diploma">Vocational - HS Diploma</option>
										<option {{($jobseeker['highest_degree'] == "Vocational - Degree" ? "selected" : "" )}} value="Vocational - Degree">Vocational - Degree</option>
										-->
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                      <label>Citizenship <span>*</span></label> 
                                      <select class="form-control" name="citizenship" id="educationDegree">
                                        <option value="">None</option>
                                      <option {{($jobseeker['citizenship'] == "US Citizen" ? "selected" : "" )}} value="US Citizen">US Citizen</option>
										<option {{($jobseeker['citizenship'] == "Permanent Resident" ? "selected" : "" )}} value="Permanent Resident">Permanent Resident</option>
										<option {{($jobseeker['citizenship'] == "Other" ? "selected" : "" )}} value="Other">Other</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                      <label>Resume <span>*</span></label> 
                                     <input type="file" name="resume">
                                    </div>
                                  </div>
                                  <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                      @if($jobseeker['resume_url'] !='')
                                       <a href="{{$url=\URL::to('').'/public/resume/'}}{{$jobseeker['resume_url']}}" target='_blank' class="downLoadResume">Download Resume</a>
                                       @endif
                                    </div>
                                  </div>
                                  <div class="col-sm-12">
                                    <div class="form-group text-center">
										<input type="hidden" name="jobseeker_id" value={{$jobseeker['id']}}>
                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                      <input type="submit" name="submit" class="btn btn-primary mx-auto col-sm-12 col-md-2" value="Submit">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <!-- End of Resume Details -->
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")
<script type="text/javascript" src='{{ asset("public/js/datatable/datatable.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/datatable/datatable-bootstrap.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/datatable/datatable-responsive.js") }}'></script>

<script type="text/javascript">
  var deletePath = '';
  $(document).ready(function(){
    $('#datatable-responsive').on('click',".deleteAction",function(event){

      $("#alertModal").modal({"show": true});

      deletePath = $(this).attr('data-newspaper-delete-route');


      $("#alertModal").on('click',".closeModal",function(){
        
        top.location.href = deletePath;

      });
    });
    $("#datatable-responsive").DataTable({
      "responsive" : true,
      "processing": true,
          "serverSide": true,
      "columnDefs": [{
                "targets": [5, 6, 8], // column or columns numbers
                "orderable": false,  // set orderable for selected columns
              }],
          "ajax": "",
        "language": {
                    "searchPlaceholder": "Search",
                    "sEmptyTable":     "No records found"
              }
    });
  })
</script>
@endsection

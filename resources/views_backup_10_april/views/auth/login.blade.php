@extends('layouts.admin')

@section('content')
<div class="col main admin-login">
    <div class="container">
        <div class="col login mx-auto">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-5 mx-auto centerBlock">
                    <div class="col-sm-12 text-center">
                            <img src="{{ asset('public/images/white-logo.png') }}" alt="Logo">
                            <h2>WORKSOURCE MONTGOMERY'S <br>MATCHING ANALYTICS RESUME CLOUD INTERFACE</h2>
                        </div>
                    
                    <div class="outerForm col">
                        <div class="row">
                            <div class="col formTop">
                                <div class="row">
                                    <div class="titleInfo col-sm-12">
                                        <strong>MARCI ADMIN LOGIN</strong>
                                        <p>Login to your account.</p>
                                    </div>
                                    <!-- <div class="text-right col-sm-3">
                                        <a href="#" class="btn btn-sm btn-primary">SIGN UP</a>
                                    </div> -->
                                </div>
                            </div>
                            <!-- Login Form -->
                            <form class="col-sm-12 form-horizontal" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    

                                    <div class="">
                                        <div class="input-group">
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">
                                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                                </span>
                                              </div>
                                        </div>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    

                                    <div class="">
                                        <div class="input-group">
                                            <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">
                                                    <i class="fa fa-eye" id="eyeIcon" aria-hidden="true" onclick="showPassword()""></i>
                                                </span>
                                              </div>
                                        </div>
                                      <!-- <input type="checkbox" onclick="showPassword()">Show Password -->
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row checkboxSection">
                                    <div class="col-sm-6">
                                        <p>
                                            <span class="iconOutter">
                                                <input id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> 
                                                <span class="icon"></span>
                                            </span> <label for="remember">Remember Me</label>
                                        </p>
                                    </div>
                                    
                                </div>
                                
                                <div class="row btnSection">
                                    <div class="col-sm-12">
                                        <!-- <a class="btn-link" href="{{ route('password.request') }}">
                                            Forgot Your Password?
                                        </a> -->
                                    </div>
                                    <div class="col-sm-12">
                                        <input type="submit" name="submit" class="btn btn-primary col" value="Sign In">
                                    </div>
                                    
                                </div>
                                
                            </form>
                            
                            <!-- End of Login Form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- End of Main -->
<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
@section("scripts")

<script>
function showPassword() {
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

</script>

@endsection

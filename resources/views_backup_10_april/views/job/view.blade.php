@extends('layouts.admin')

@section('content')

   @include('dashboard_header')

    <!--- End of Header -->

    <!-- Main Wrapper -->
    <div class="mainWrapper">
        <div class="container-fluid">
            <div class="row">

                @include('left_navigation')

                <div class="col-sm-12 col-md-9 col-lg-10 contentWrapper view-jobs">
					<?php
					$skills=explode(',',$job['skill']);
					$experience_level=explode(',',$job['skill_level']);
					$degrees=explode(',',$job['degree']);
					$degree_level=explode(',',$job['degree_level']);
					?>
          <h2 class="mTitile">Edit a job</h2>
          <div class="outterWrapper col">
							  @if(session()->has('message'))
				<p class="success-message">
					{{ session()->get('message') }}
				</p>
			@endif
            <div class="row">
              <div class="col-sm-12 inner">
          <div class="jobPostForm">
            <form id="jobForm" action="{{ url('/job/update') }}" method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="clearfix"></div>
                <!-- Job Details-->
                <div class="col-sm-12 boxShadow">
                  <h4>Job Details</h4>
                  <div class="row">
                    <div class="col-sm-12 col-md-8">
                      <div class="form-group">
                        <label for="job_title">Job Title <span>*</span></label>
                        <input type="text" class="form-control" name="job_title" id="job_title" value="{{$job['job_title']}}">
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                      <div class="form-group">
                        <label for="jobStatus">Job Type  <span>*</span></label>
                        <select class="form-control" id="job_status" name="job_status">
                           <!---->
                           <option  {{ ($job['job_status'] == "1" ? "selected":"")}} value="1">Full Time</option>
                           <option  {{ ($job['job_status'] == "2" ? "selected":"")}} value="2">Temporary/ Contract</option>
                           <option  {{ ($job['job_status'] == "3" ? "selected":"")}} value="3">Intern</option>
                           <option  {{($job['job_status'] == "4" ? "selected":"")}} value="4">Part Time</option>
                           <option  {{($job['job_status'] == "5" ? "selected":"")}} value="5">Seasonal</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                     <div class="col-sm-12 col-md-6 rangeField">
                      <div class="row signleBox" @if($job['sal_range'] == 1)
                           style="display: none;"
                           @endif>
                        <div class="col-sm-12">
                          <div class="form-group">
                            <label>Salary <small>(in USD)</small></label>
                            <input type="number" class="form-control" id="" name="range" value={{$job['amount']}}>
                          </div>
                        </div>
                      </div>
                      <div class="row doubleBox" 
                       @if($job['sal_range'] == 1)
                           @else
                           style="display: none;"
                           @endif
                      >
                        <div class="col-sm-5">
                          <div class="form-group">
                            <label>Min Salary <small>(in USD)</small></label>
                            <input type="number" class="form-control min" id="min" name="min_range" value={{$job['min_salary']}}>
                          </div>
                        </div>
                        <div class="col-sm-2 to">
                          To
                        </div>
                        <div class="col-sm-5">
                          <div class="form-group">
                            <label>Max Salary <small>(in USD)</small></label>
                            <input type="number" class="form-control max" id="max" name="max_range" value={{$job['max_salary']}}>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-12 col-md-2 rangeBtn">
                      <div class="form-group">
                        <label></label>
                        <small>
                          <input type="checkbox" name="salary_range" id="rangeBtn" placeholder="Enter Amount"
                           @if($job['sal_range'] == 1)
                           checked
                           @endif
                          >
                          Add Range
                        </small>
                      </div>
                    </div>

                    <div class="col-sm-12 col-md-3">
                      <div class="form-group">
                        <label>Bonus</label>
                        <input type="number" class="form-control " id="max" name="bonus" value={{$job['bonus']}}>
                      </div>
                    </div>

                    <div class="col-sm-12 col-md-3">
                      <div class="form-group">
                        <label></label>
                        {{ Form::select('salary_type', ['Per Year', 'Per Year', 'Per Week','Per Month', 'Biweekly', 'Per Day'],$job['salaryType'],['class' => 'form-control','id'=>'salaryType']) }}
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End of Job Details -->

                <!-- Job Requirement -->
                <div class="col-sm-12 boxShadow">
                  <h4>Job Requirements</h4>
                  <div class="row">
                    <div class="col-sm-12 col-md-6">
                      <div class="form-group">
                        <label>Minimum Education Level <span>*</span></label>
                         <select aria-label="educatonlevel" class="form-control" id="educatonLevel" name="educatonLevel" value="{{ old('educatonLevel') }}">
                            <option value="">Select</option>
                            <option {{($job['education_level'] == "12" ? "selected" : "" )}} value="12">Some High School Coursework</option>
						    <option {{($job['education_level'] == "1" ? "selected" : "" )}} value="1">High School or equivalent</option>
							<option {{($job['education_level'] == "2" ? "selected" : "" )}} value="2">Certification</option>
							<option {{($job['education_level'] == "3" ? "selected" : "" )}} value="3">Vocational</option>
							<option {{($job['education_level'] == "9" ? "selected" : "" )}} value="9">Some College Coursework Completed</option>
							<option {{($job['education_level'] == "4" ? "selected" : "" )}} value="4">Associate Degree</option>
							<option {{($job['education_level'] == "5" ? "selected" : "" )}} value="5">Bachelor's Degree</option>
							<option {{($job['education_level'] == "6" ? "selected" : "" )}} value="6">Master's Degree</option>
							<option {{($job['education_level']== "7" ? "selected" : "" )}} value="7">Doctorate</option>
							<option {{($job['education_level'] == "8" ? "selected" : "" )}} value="8">Professional</option>
                        </select>
             
                      </div>
                    </div>
                  <!--  <div class="col-sm-12 col-md-4">
                      <div class="form-group">
                        <label>Degree/Major <span>*</span></label>
                        <input type="text" class="form-control" name="degree" >
                        <small>(e.g. MBA, MS Math, MSCS, English)</small>
                      </div>
                    </div> -->
                    <div class="col-sm-12 col-md-6">
                      <div class="form-group">
                        <label>Year of Experience <span>*</span></label>
                        <span class="outterSect">
                        <select aria-label="experience" class="form-control" id="experience" name="experience">
                           <option value="1" {{($job['experience'] == "1" ? "selected" : "" )}}><1</option>
                           <option value="1-2" {{($job['experience'] == "1-2" ? "selected" : "" )}}>1-2</option>
                           <option value="2-5" {{($job['experience'] == "2-5" ? "selected" : "" )}}>2-5</option>
                           <option value="5-10" {{($job['experience'] == "5-10" ? "selected" : "" )}}>5-10</option>
                           <option value="10+" {{($job['experience'] == "10+" ? "selected" : "" )}}>10+</option>
                        </select>
                         <a href="#" role="button" aria-pressed="false" title="Nice to have" class='nice_have_exp niceTxt' data-id={{$job['experience_level']}}>
                         @if ($job['experience_level'] == '1')
						   Nice to have
						   @else
							Required
						@endif 
            <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Year of Experience"></i>
                         </a>
                        <input type="hidden" name="expLevel" class="form-control exp_level" id="skill_level" value={{$job['experience_level']}}>
                      </span>
                      </div>
                    </div>
                     <div class="col-sm-12 col-md-10 ">
                      <div class="form-group">
                         <label>Degree/Major <span>*</span></label>
                      </div>
                      @foreach ($degrees as $degree=>$value)
                      @if ($value != '')
                      <div class="form-group degreeFiled ">
                        <span class="outterSect">
                              <input type="text" name="degree[]" class="form-control" id="skill_keywords" value={{ $value }}>
        						   <a href="#" role="button" aria-pressed="false" title="Nice to have" class='nice_have niceTxt' data-id={{$degree_level[$degree]}}>
        							@if ($degree_level[$degree] == '1')
        							   Nice to have
        							   @else
        								Required
        							@endif  
                      <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Degree/Major"></i>
        						   </a>
                                <input type="hidden" name="degreeLevel[]" class="form-control skill_level" id="degree_level" value={{$degree_level[$degree]}}>                 
        							<div class="optionsBtn">
        								  <a href="javascript:void(0)" id="addDegreeField" data-toggle="tooltip" title="Add Input Field">
        									<i class="fa fa-plus-circle" aria-hidden="true"></i>
        								  </a>
        								  @if ($degree > 0)
        								  <a href="#" id="deleteDegreeField" data-toggle="tooltip" title="Delete Input Field">
        									<i class="fa fa-minus-circle" aria-hidden="true"></i>
        								  </a>
        							  @endif
        							</div>
                    </span>
                      
                      </div>
                      @endif 
                        @endforeach
                    </div>
                    <div class="col-sm-12 col-md-10 ">
                      <div class="form-group">
                        <label>Skills/Keywords <span>*</span> <br><small>Enter terms related to candidates industry or expertise(e.g. java, telesales, call center, retail, SOX)</small></label>
                      </div>
                      @foreach ($skills as $skill=>$value)
                      @if ($value != '')
                      <div class="form-group skillFiled ">
                        <span class="outterSect">
                      <input type="text" name="skills[]" class="form-control" id="skill_keywords" value={{ $value }}>
						   <a href="#" role="button" aria-pressed="false" title="Nice to have" class='nice_have niceTxt' data-id={{$experience_level[$skill]}}>
							@if ($experience_level[$skill] == '1')
							   Nice to have
							   @else
								Required
							@endif  

              <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Skills/Keywords"></i>
						   </a>
                        <input type="hidden" name="skillsLevel[]" class="form-control skill_level" id="skill_level" value={{$experience_level[$skill]}}>                 
							<div class="optionsBtn">
								  <a href="javascript:void(0)" id="addSkillField" data-toggle="tooltip" title="Add Input Field">
									<i class="fa fa-plus-circle" aria-hidden="true"></i>
								  </a>
								  @if ($skill > 0)
								  <a href="#" id="deleteSkillField" data-toggle="tooltip" title="Delete Input Field">
									<i class="fa fa-minus-circle" aria-hidden="true"></i>
								  </a>
							  @endif
							</div>
                      </span>
                      </div>
                      @endif 
                        @endforeach
                    </div>
                    
                  </div>
                </div>
                <!-- End of Job Requirement -->

                <!-- Job Descrption -->
                <div class="col-sm-12 boxShadow">
                  <h4>Job Description</h4>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                       <textarea aria-hidden="true" name="job_description">{{$job['job_description']}}</textarea>
                      </div>
                      <div class="form-group text-center">
				     <input type="hidden" name="job_id" value={{$job['id']}}>
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <input type="submit" name="submit" class="btn btn-primary mx-auto col-sm-12 col-md-2" value="Submit">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End of Job Descrption -->
              </div>
            </form>
          </div>
        </div>
            </div>
          </div>
        </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")
<script type="text/javascript" src='{{ asset("public/js/datatable/datatable.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/datatable/datatable-bootstrap.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/datatable/datatable-responsive.js") }}'></script>

<script type="text/javascript">
  var deletePath = '';
  $(document).ready(function(){
    $('#datatable-responsive').on('click',".deleteAction",function(event){

      $("#alertModal").modal({"show": true});

      deletePath = $(this).attr('data-newspaper-delete-route');


      $("#alertModal").on('click',".closeModal",function(){
        
        top.location.href = deletePath;

      });
    });

    $("#fileUpload").click(function(){
      $("#logo").trigger("click");
    })

    $(document).on("click", "#addSkillField", function(event){
      event.preventDefault();
      if($('.skillFiled').length <= 9){
       $(".skillFiled:last").after('<div class="form-group skillFiled"><span class="outterSect"><input type="text" name="skills[]" class="form-control" id="skill_keywords"><a href="#" role="button" aria-pressed="false" title="Nice to have" class="nice_have niceTxt" data-id="1">Nice to have <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Skills/Keywords"></i></a><input type="hidden" name="skillsLevel[]" class="form-control skill_level" id="skill_level" value=1><div class="optionsBtn"><a href="javascript:void(0)" id="addSkillField" data-toggle="tooltip" title="Add Input Field"><i class="fa fa-plus-circle" aria-hidden="true"></i></a><a href="javascript:void()" id="deleteSkillField" data-toggle="tooltip" title="Delete Input Field"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></div></span></div>');
      }
	  else {
		 alert('You are reached to maximum skills'); 
	  }
    });
    
    $(document).on("click", "#addDegreeField", function(event){
      event.preventDefault();
      if($('.degreeFiled').length <= 9){
		  $(".degreeFiled:last").after('<div class="form-group degreeFiled"><span class="outterSect"><input type="text" name="degree[]" class="form-control" id="degree_keywords"><a href="#" role="button" aria-pressed="false" title="Nice to have" class="nice_have niceTxt" data-id="1">Nice to have <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Degree/Major"></i></a><input type="hidden" name="degreeLevel[]" class="form-control skill_level" id="skill_level" value=1><div class="optionsBtn"><a href="javascript:void(0)" id="addDegreeField" data-toggle="tooltip" title="Add Input Field"><i class="fa fa-plus-circle" aria-hidden="true"></i></a><a href="javascript:void()" id="deleteDegreeField" data-toggle="tooltip" title="Delete Input Field"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></div></span></div>');
	  }
	  else {
		 alert('You are reached to maximum degree'); 
	  }
        
    });

    $(document).on("click", "#deleteSkillField", function(event){
      event.preventDefault();
      $(this).closest(".skillFiled").remove();
    });
    
    $(document).on("click", "#deleteDegreeField", function(event){
      event.preventDefault();
      $(this).closest(".degreeFiled").remove();
    });

$(document).on("click", ".nice_have", function(e){
      event.preventDefault();
      if($(this).data('id')=='1'){
		  $(this).text('Required');
      $(this).append('<i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Skills/Keywords"></i>');
		  $(this).data('id','2');
		  $(this).next('.skill_level' ).val(2);
	  }
	  else {
		   $(this).text('Nice to have');
		   $(this).data('id','1');
       $(this).append('<i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Skills/Keywords"></i>');
		   $(this).next('.skill_level').val(1);
	  }
    });
     $(document).on("click", ".nice_have_exp", function(e){
      event.preventDefault();
      e.preventDefault();	
      if($(this).data('id')=='1'){
		  $(this).text('Required');
		  $(this).data('id','2');
      $(this).append('<i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Year of Experience "></i>');
		  $(this).next('.exp_level' ).val(2);
	  }
	  else {
		   $(this).text('Nice to have');
		   $(this).data('id','1');
       $(this).append('<i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="Year of Experience "></i>');
		   $(this).next('.exp_level').val(1);
	  }
    });
  })
  

</script>
@endsection

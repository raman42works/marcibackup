<header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2 col-md-3 col-lg-2 logoHeader text-center">
                    <a href="#">
<img src="{{ asset('public/images/white-logo.png') }}" alt="white-logo">
</a>
                </div>
                <div class="col-sm-10 col-md-9 col-lg-10">
                    <nav class="navbar navbar-expand-lg">

                      <span class="mr-auto">
              Worksource Montgomery's Matching Analytics Resume Cloud Interface
            </span>
                      <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <span class="nav-link">Currently logged in as Admin</span class="">
                        </li>
                        
                        <li class="navbar-item">
                          <a href="<?php echo url('/logout'); ?>" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="nav-link"><i class="fa fa-power-off" aria-hidden="true"></i> Logout</a>
                                                     <form id="logout-form" action="<?php echo url('/logout'); ?>" method="POST" style="display: none;">
                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        </form>
                        </li>
                      </ul>
                    </nav>
                    <div class="toggleMenuIcon" id="toggleMenu">
				        <span class="top"></span>
				        <span class="middle"></span>
				        <span class="last"></span>
				    </div>
                </div>
            </div>
        </div>
    </header>

@extends('layouts.admin')

@section('content')

   @include('dashboard_header')

    <!--- End of Header -->

    <!-- Main Wrapper -->
    <div class="mainWrapper">
        <div class="container-fluid">
            <div class="row">

                @include('left_navigation')

                <div class="col-sm-12 col-md-9 col-lg-10 contentWrapper">
          <h2 class="mTitile">View All Submit url</h2>

          <div class="success-message">
            @if(session('success'))
					 <p class="success-message">{{session('success')}}</p>
				@endif
      </div>
		 <div class="success_message success-message"></div>		
          <div class="outterWrapper table col">
            <div class="row">
              <table id="jobprovider" class="table table-striped table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
                <thead>
                  <tr>
                    <th>Reference Name</th>        
                    <th>Reference Url</th>
                    <th>Count</th>
                     <th>CREATED ON</th>
                  <!--  <th class="text-center">Actions</th> -->
                   
                  </tr>
                </thead>
          
              </table>
            </div>
          </div>
        </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")
<script type="text/javascript" src='{{ asset("public/js/jquery.js") }}'></script>
<!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script> -->
<script type="text/javascript" src='{{ asset("public/js/jquery.dataTables.min.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/dataTables.bootstrap4.min.js") }}'></script>
<script type="text/javascript">
  var deletePath = '';
  $(document).ready(function(){
    $('#datatable-responsive').on('click',".deleteAction",function(event){

      $("#alertModal").modal({"show": true});

      deletePath = $(this).attr('data-newspaper-delete-route');


      $("#alertModal").on('click',".closeModal",function(){
        
        top.location.href = deletePath;

      });
    });
    $("#datatable-responsive").DataTable({
      "responsive" : true,
      "processing": true,
          "serverSide": true,
      "columnDefs": [{
                "targets": [5, 6, 8], // column or columns numbers
                "orderable": false,  // set orderable for selected columns
              }],
          "ajax": "",
        "language": {
                    "searchPlaceholder": "Search",
                    "sEmptyTable":     "No records found"
              }
    });
    $(document).on('click',".delete_pro",function(event){
      event.preventDefault();
      var url = $(this).data('url');
      
      if (confirm('Are you sure you want to delete company and its all related jobs')) {
        location.href = url;
         }
      
      });
      
      $(document).on('keyup',".count_update",function(event){
      event.preventDefault();
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var refernce_id = $(this).data('id');
        var count = $(this).val();
        $(this).next('.count_update_btn').attr('data-id', refernce_id)
        $(this).next('.count_update_btn').attr('data-value', count)
       // console.log($(this).next('.count_update_btn').attr('data-id', refernce_id));
        
		/*
         $.ajax({
               type:'POST',
               url:'{{ route('submiturl-update') }}',
				data: {
                    refernce_id : refernce_id,_token: CSRF_TOKEN,count:count
                },
               success:function(data){
				    var response= $.parseJSON(data);
					 if(response.success==1){
						 $('.success_message').text(response.message);
						 setTimeout(function(){
							   window.location.reload(1);
							}, 2000);
					 }
					 else {
						 $('.success_message').text(response.message);
						 setTimeout(function(){
							   window.location.reload(1);
							}, 2000);
					 }
               }
            });
        */
      });
      
      $(document).on('click',".count_update_btn",function(event){
      event.preventDefault();
        
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var refernce_id = $(this).data('id');
        var count = $(this).data('value');
        console.log(count);
        console.log(refernce_id);
		
         $.ajax({
               type:'POST',
               url:'{{ route('submiturl-update') }}',
				data: {
                    refernce_id : refernce_id,_token: CSRF_TOKEN,count:count
                },
               success:function(data){
				    var response= $.parseJSON(data);
					 if(response.success==1){
						 $('.success_message').text(response.message);
						 setTimeout(function(){
							   window.location.reload(1);
							}, 7000);
					 }
					 else {
						 $('.success_message').text(response.message);
						 setTimeout(function(){
							   window.location.reload(1);
							}, 7000);
					 }
               }
            });
      
      }); 
       
  })
  
  
  
    $(document).ready(function() {
    oTable = $("#jobprovider").DataTable({
        "processing": true,
        "responsive" : true,
        "serverSide": true,
        "ajax": "{{ route('datatable.getsubmiturl') }}",
        "columns": [
            {data: 'refernce_name', name: 'refernce_name'},
           //{data: 'email', name: 'email', orderable: false, searchable: false},
           //{data: 'phone', name: 'phone'},
            {data: 'refernce_url', name: 'refernce_url'},
            {data: 'count', name: 'count'},
             {data: 'created_at', name: 'created_at', orderable: false, searchable: false},
           // {data: 'action', name: 'action', orderable: false, searchable: false}
            
        ],"columnDefs": [
			//{ className: "text-center", "targets": [ 4 ] }
       ]
    });
   });
</script>
@endsection

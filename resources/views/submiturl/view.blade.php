@extends('layouts.admin')

@section('content')

   @include('dashboard_header')

    <!--- End of Header -->

    <!-- Main Wrapper -->
    <div class="mainWrapper">
        <div class="container-fluid">
            <div class="row">

                @include('left_navigation')
               <?php
			$stateData = array(""=>"Select","Alabama"=>"Alabama", "Arizona"=>"Arizona", "Arkansas"=>"Arkansas",
             "California"=>"California", "Colorado"=>"Colorado", "Connecticut"=>"Connecticut",
             "Delaware"=>"Delaware", "District of Columbia"=>"District of Columbia", "Florida"=>"Florida",
             "Georgia"=>"Georgia", "Hawaii"=>"Hawaii", "Idaho"=>"Idaho","Illinois"=>"Illinois", "Indiana"=>"Indiana",
              "Iowa"=>"Iowa","Kansas"=>"Kansas", "Kentucky"=>"Kentucky", "Louisiana"=>"Louisiana",
              "Maine"=>"Maine","Maryland"=>"Maryland", "Massachusetts"=>"Massachusetts", "Michigan"=>"Michigan",
              "Minnesota"=>"Minnesota","Mississippi"=>"Mississippi", "Missouri"=>"Missouri", "Montana"=>"Montana",
              "Nebraska"=>"Nebraska","Nevada"=>"Nevada", "New Hampshire"=>"New Hampshire", "New Jersey"=>"New Jersey",
              "New Mexico"=>"New Mexico","New York"=>"New York", "North Carolina"=>"North Carolina", "North Dakota"=>"North Dakota",
              "Ohio"=>"Ohio","Oklahoma"=>"Oklahoma", "Oregon"=>"Oregon", "Pennsylvania"=>"Pennsylvania",
              "Puerto Rico"=>"Puerto Rico","Rhode Island"=>"Rhode Island", "South Carolina"=>"South Carolina", "South Dakota"=>"South Dakota",
              "Tennessee"=>"Tennessee","Texas"=>"Texas", "Utah"=>"Utah", "Vermont"=>"Vermont",
              "Virgin Islands"=>"Virgin Islands","Virginia"=>"Virginia", "Washington"=>"Washington", "West Virginia"=>"West Virginia",
              "Wisconsin"=>"Wisconsin","Wyoming"=>"Wyoming", "Armed Forces Americas"=>"Armed Forces Americas", "Armed Force Europe, the Middle East, and Canada"=>"Armed Force Europe, the Middle East, and Canada",
              "Armed Forces Pacific"=>"Armed Forces Pacific","Federated States of Micronesia"=>"Federated States of Micronesia", "Guam"=>"Guam", "American Samoa"=>"American Samoa","Northern Mariana Islands"=>"Northern Mariana Islands",  
             );
			?>	

                <div class="col-sm-12 col-md-9 col-lg-10 contentWrapper">
          <h2 class="mTitile">Edit a Job Provider</h2>
          <div class="outterWrapper col">
			    @if(session()->has('message'))
    <p class="success-message">
        {{ session()->get('message') }}
    </p>
@endif
            <div class="row">
              <div class="col-sm-12 inner">
                <div class="jobPostForm">
                  <form action="{{ url('/jobprovider/update') }}" method="post" enctype="multipart/form-data">
                    <div class="row">
                      <!-- Company Details -->
                      <div class="col-sm-12 boxShadow">
                        <h4>Company Details</h4>
                        <div class="row">
                          <div class="col-sm-12 col-md-9">
                            <div class="row">
                              <div class="col-sm-12 col-md-12">
                                <div class="form-group">
                                  <label for="name">Company Name <span>*</span></label>
                                  <input type="text" class="form-control" name="company_name" id="company_name" required="" value={{$jobprovider['company_name']}}>
                                </div>
                              </div>
                              <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="name">Company Size <span>*</span></label>
                                    <select class="form-control" id="companySize" name="company_size">
                                     <!---->
                                      <option value="1 to 9 employees" {{($jobprovider['company_size'] == "1 to 9 employees" ? "selected":"") }} >1 to 9 employees</option>
								   
                               <option value="10 to 19 employees" {{ ($jobprovider['company_size'] == "10 to 19 employees" ? "selected":"") }}>10 to 19 employees</option>
                               <option value="20 to 49 employees" {{ ($jobprovider['company_size'] == "20 to 49 employees" ? "selected":"") }}>20 to 49 employees</option>
                               <option value="50 to 99 employees" {{ ($jobprovider['company_size'] == "50 to 99 employees" ? "selected":"") }}>50 to 99 employees </option>
                               <option value="100 to 499 employees" {{ ($jobprovider['company_size'] == "100 to 499 employees" ? "selected":"") }}>100 to 499 employees </option>
                               <option value="500 to 999 employees" {{ ($jobprovider['company_size'] == "500 to 999 employees" ? "selected":"") }}>500 to 999 employees</option>
                               <option value="1,000 to 1,499 employees" {{ ($jobprovider['company_size'] == "1,000 to 1,499 employees" ? "selected":"") }}>1,000 to 1,499 employees</option>
                               <option value="1,500 to 1,999 employees" {{ ($jobprovider['company_size'] == "1,500 to 1,999 employees" ? "selected":"") }}>1,500 to 1,999 employees</option>
                               <option value="2,000 to 2,499 employees" {{ ($jobprovider['company_size'] == "2,000 to 2,499 employees" ? "selected":"") }}>2,000 to 2,499 employees</option>
                               <option value="2,500 to 4,999 employees" {{ ($jobprovider['company_size'] == "2,500 to 4,999 employees" ? "selected":"") }}>2,500 to 4,999 employees</option>
                               <option value="5,000 to 9,999 employees" {{ ($jobprovider['company_size'] == "5,000 to 9,999 employees" ? "selected":"") }}>5,000 to 9,999 employees</option>
                               <option>10,000 employees or more</option>
                                  </select>
                                </div>
                              </div>
                              <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                  <label for="name">Company Industry <span>*</span></label>
                                 <select class="form-control" name="industry" id="companyIndustry" value="{{ old('industry') }}">
                               <!---->
                               <option value=''>--SELECT--</option>
                               <option value='Accounting and Auditing Services' {{($jobprovider['industry'] == "Accounting and Auditing Services" ? "selected" : "" )}}>Accounting and Auditing Services</option>
                               <option {{($jobprovider['industry'] == "Advertising and PR Services" ? "selected" : "" )}}>Advertising and PR Services</option>
                               <option {{($jobprovider['industry'] == "Aerospace and Defense" ? "selected" : "" )}}>Aerospace and Defense</option>
                               <option {{($jobprovider['industry'] == "Agriculture/Forestry/Fishing" ? "selected" : "" )}}>Agriculture/Forestry/Fishing</option>
                               <option {{($jobprovider['industry'] == "Architectural and Design Services" ? "selected" : "" )}}>Architectural and Design Services</option>
                               <option {{($jobprovider['industry'] == "Automotive and Parts Mfg" ? "selected" : "" )}}>Automotive and Parts Mfg</option>
                               <option {{($jobprovider['industry'] == "Automotive Sales and Repair Services" ? "selected" : "" )}}>Automotive Sales and Repair Services</option>
                               <option {{($jobprovider['industry'] == "Banking" ? "selected" : "" )}}>Banking</option>
                               <option {{($jobprovider['industry'] == "Biotechnology/Pharmaceuticals" ? "selected" : "" )}}>Biotechnology/Pharmaceuticals</option>
                               <option {{($jobprovider['industry'] == "Broadcasting, Music, and Film" ? "selected" : "" )}}>Broadcasting, Music, and Film</option>
                               <option {{($jobprovider['industry'] == "Business Services - Other" ? "selected" : "" )}}>Business Services - Other</option>
                               <option {{($jobprovider['industry'] == "Chemicals/Petro-Chemicals" ? "selected" : "" )}}>Chemicals/Petro-Chemicals</option>
                               <option {{($jobprovider['industry'] == "Clothing and Textile Manufacturing" ? "selected" : "" )}}>Clothing and Textile Manufacturing</option>
                               <option {{($jobprovider['industry'] == "Computer Hardware" ? "selected" : "" )}}>Computer Hardware</option>
                               <option {{($jobprovider['industry'] == "Computer Software" ? "selected" : "" )}}>Computer Software</option>
                               <option {{($jobprovider['industry'] == "Computer/IT Services" ? "selected" : "" )}}>Computer/IT Services</option>
                               <option {{($jobprovider['industry'] == "Construction - Industrial Facilities and Infrastructure" ? "selected" : "" )}}>Construction - Industrial Facilities and Infrastructure</option>
                               <option {{($jobprovider['industry'] == "Construction - Residential &amp; Commercial/Office" ? "selected" : "" )}}>Construction - Residential &amp; Commercial/Office</option>
                               <option {{($jobprovider['industry'] == "Consumer Packaged Goods Manufacturing" ? "selected" : "" )}}>Consumer Packaged Goods Manufacturing</option>
                               <option {{($jobprovider['industry'] == "Education" ? "selected" : "" )}} >Education</option>
                               <option {{($jobprovider['industry'] == "Electronics, Components, and Semiconductor Mfg" ? "selected" : "" )}}>Electronics, Components, and Semiconductor Mfg</option>
                               <option {{($jobprovider['industry'] == "Energy and Utilities" ? "selected" : "" )}}>Energy and Utilities</option>
                               <option {{($jobprovider['industry'] == "Engineering Services" ? "selected" : "" )}}>Engineering Services</option>
                               <option {{($jobprovider['industry'] == "Entertainment Venues and Theaters" ? "selected" : "" )}}>Entertainment Venues and Theaters</option>
                               <option {{($jobprovider['industry'] == "Financial Services" ? "selected" : "" )}}>Financial Services</option>
                               <option {{($jobprovider['industry'] == "Food and Beverage Production" ? "selected" : "" )}}>Food and Beverage Production</option>
                               <option {{($jobprovider['industry'] == "Government and Military" ? "selected" : "" )}}>Government and Military</option>
                               <option {{($jobprovider['industry'] == "Healthcare Services" ? "selected" : "" )}}>Healthcare Services</option>
                               <option {{($jobprovider['industry'] == "Hotels and Lodging" ? "selected" : "" )}}>Hotels and Lodging</option>
                               <option {{($jobprovider['industry'] == "Insurance" ? "selected" : "" )}}>Insurance</option>
                               <option {{($jobprovider['industry'] == "Internet Services" ? "selected" : "" )}}>Internet Services</option>
                               <option {{($jobprovider['industry'] == "Legal Services" ? "selected" : "" )}}>Legal Services</option>
                               <option {{($jobprovider['industry'] == "Management Consulting Services" ? "selected" : "" )}}>Management Consulting Services</option>
                               <option {{($jobprovider['industry'] == "Manufacturing - Other" ? "selected" : "" )}}>Manufacturing - Other</option>
                               <option {{($jobprovider['industry'] == "Marine Mfg &amp; Services" ? "selected" : "" )}}>Marine Mfg &amp; Services</option>
                               <option {{($jobprovider['industry'] == "Medical Devices and Supplies" ? "selected" : "" )}}>Medical Devices and Supplies</option>
                               <option {{($jobprovider['industry'] == "Metals and Minerals" ? "selected" : "" )}} >Metals and Minerals</option>
                               <option {{($jobprovider['industry'] == "Nonprofit Charitable" ? "selected" : "" )}}>Nonprofit Charitable Organizations</option>
                               <option {{($jobprovider['industry'] == "Other/Not Classified" ? "selected" : "" )}}>Other/Not Classified</option>
                               <option {{($jobprovider['industry'] == "Performing and Fine Arts" ? "selected" : "" )}}>Performing and Fine Arts</option>
                               <option {{($jobprovider['industry'] == "Personal and Household Services" ? "selected" : "" )}}>Personal and Household Services</option>
                               <option {{($jobprovider['industry'] == "Printing and Publishing" ? "selected" : "" )}}>Printing and Publishing </option>
                               <option {{($jobprovider['industry'] == "Real Estate/Property Management" ? "selected" : "" )}}>Real Estate/Property Management</option>
                               <option {{($jobprovider['industry'] == "Rental Services" ? "selected" : "" )}}>Rental Services</option>
                               <option {{($jobprovider['industry'] == "Restaurant/Food Services" ? "selected" : "" )}}>Restaurant/Food Services</option>
                               <option {{($jobprovider['industry'] == "Retail" ? "selected" : "" )}}>Retail</option>
                               <option {{($jobprovider['industry'] == "Security and Surveillance" ? "selected" : "" )}}>Security and Surveillance</option>
                               <option {{($jobprovider['industry'] == "Sports and Physical Recreation" ? "selected" : "" )}}>Sports and Physical Recreation</option>
                               <option {{($jobprovider['industry'] == "Staffing/Employment Agencies" ? "selected" : "" )}} >Staffing/Employment Agencies</option>
                               <option {{($jobprovider['industry'] == "Telecommunications Services" ? "selected" : "" )}}>Telecommunications Services</option>
                               <option {{($jobprovider['industry'] == "ransport and Storage - Materials" ? "selected" : "" )}}>Transport and Storage - Materials </option>
                               <option {{($jobprovider['industry'] == "Travel, Transportation and Tourism" ? "selected" : "" )}}>Travel, Transportation and Tourism</option>
                               <option {{($jobprovider['industry'] == "Waste Management" ? "selected" : "" )}}>Waste Management</option>
                               <option {{($jobprovider['industry'] == "Wholesale Trade/Import-Export" ? "selected" : "" )}}>Wholesale Trade/Import-Export</option>
                            </select>
                                </div>
                              </div>                        
                            </div>
                          </div>
                          <div class="col-sm-12 col-md-3 logouploader">
                            
                            
                      <div class="form-group text-right">
						  
                        <label>Company Logo</label><input type="file hidden" name="logouploader" class="form-control">
                        <a href="javascript:void(0);" id="fileUpload">
                        <div class="col logoUpload">
							@if ($jobprovider['logo'] != '')
							<img src="{{\URL::to('').'/public/images/logos/'}}{{$jobprovider['logo']}}" class="img-fluid">
							@else
								<span>+</span>
							@endif
                          
                        </div>
                        </a>
                        <input style="display: none;" type="file" id="logo" name="logo" />
                      </div>
                          </div>
                          
                          <div class="col-sm-12">
                            <div class="row">
                              <div class="col-sm-12">
                                <div class="form-group">
                                  <small>
                                    <input type="checkbox"  name="confidential" value={{$jobprovider['confidential']}} 
                                      @if ($jobprovider['confidential'] == 1)
			                             checked="checked"
										@endif
                                    >
                                    Keep company name confidential.
                                  </small>
                                </div>
                              </div>

                              <div class="col-sm-12">
                                <label><strong>ADDRESS</strong></label>
                              </div>
                              
                              <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                  <label for="name">Street Address 1 <span>*</span></label>
                                  <input type="text" class="form-control" id="company_add1" name="address" required="" value={{$jobprovider['address']}}>
                                </div>
                              </div>
                              <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                  <label for="name">Street Address 2 <span>*</span></label>
                                  <input type="text" class="form-control" id="company_add2" name="address_2" required="" value={{$jobprovider['address1']}}>
                                </div>
                              </div>
                              <div class="col-sm-12 col-md-4">
                                <div class="form-group">
                                  <label for="name">City <span>*</span></label>
                                  <input type="text" class="form-control" id="company_name" name="city" required="" value={{$jobprovider['city']}}>
                                </div>
                              </div>
                              <div class="col-sm-12 col-md-4">
                                <div class="form-group">
                                    <label for="name">State <span>*</span></label>
                                     <input type="hidden" class="form-control" id="company_name" name="company_state" required="" value={{$jobprovider['state']}}>
                                 {{ Form::select('state', $stateData,$jobprovider['state'], ['class' => 'form-control','id'=>'state','disabled' => true]) }}
                                </div>
                              </div>
                              <div class="col-sm-12 col-md-4">
                                <div class="form-group">
                                  <label for="name">Zip Code <span>*</span></label>
                                  <input type="text" class="form-control" id="company_name" name="zip" required="" value={{$jobprovider['zip']}}>
                                </div>
                              </div>
                            </div>
                             <div class="form-group text-center">
								 <input type="hidden" name="jobprovider_id" value={{$jobprovider['id']}}>
								  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
									<input type="submit" name="submit" class="btn btn-primary mx-auto col-sm-12 col-md-2" value="Submit">
								  </div>
                          </div>
                        </div>
                      </div>
                      <!-- End of Company Details -->

                      <div class="clearfix"></div>

                    

                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")
<script type="text/javascript" src='{{ asset("public/js/datatable/datatable.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/datatable/datatable-bootstrap.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/datatable/datatable-responsive.js") }}'></script>

<script type="text/javascript">
	$("#fileUpload").click(function(){
      $("#logo").trigger("click");
    })
  var deletePath = '';
  $(document).ready(function(){
    $('#datatable-responsive').on('click',".deleteAction",function(event){

      $("#alertModal").modal({"show": true});

      deletePath = $(this).attr('data-newspaper-delete-route');


      $("#alertModal").on('click',".closeModal",function(){
        
        top.location.href = deletePath;

      });
    });
    $("#datatable-responsive").DataTable({
      "responsive" : true,
      "processing": true,
          "serverSide": true,
      "columnDefs": [{
                "targets": [5, 6, 8], // column or columns numbers
                "orderable": false,  // set orderable for selected columns
              }],
          "ajax": "",
        "language": {
                    "searchPlaceholder": "Search",
                    "sEmptyTable":     "No records found"
              }
    });
  })
</script>
@endsection

<!DOCTYPE html>
<html>
<head>
	<title>Welcome Email</title>
</head>
<body>
	<h2> Hello  {{$company_name}} </h2>
	<h2>Thank you for allowing WorkSource Montgomery (WSM) to be a part of your talent search; our matching analytics software will now go to work for you. When we receive information regarding qualified candidates, WSM staff will be in touch with you. </h2>
    <br>
    <br>
    <p>For further information about WorkSource Montgomery, please visit our website at <a href="https://worksourcemontgomery.com/">https://worksourcemontgomery.com/ </a> . </p> 
    <p>Again, thank you and good luck!</p>
    
	<h2>WSM Business Solutions Team</h2>
</body>
</html>

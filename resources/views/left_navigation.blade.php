<!-- Left Side navigation -->
<div class="col-sm-2 col-md-3 col-lg-2 leftNavigation">
    <nav class="navbar navbar-light">
      <div class="collapse navbar-collapse show" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{url('/dashboard')}}">Dashboard</a>
          </li>
          <li class="nav-item">
            <span class="seekers">Career Seekers</span>
                <a class="dropdown-item" href="{{url('/careerseeker/list')}}"><i class="fa fa-users" aria-hidden="true"></i> View All</a>
                <a class="dropdown-item" href="{{url('/careerseeker/add')}}"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
          </li>
          <li class="nav-item">
            <span class="providers">Career Providers</span>
                <a class="dropdown-item" href="{{url('/careerprovider/list')}}"><i class="fa fa-users" aria-hidden="true"></i> View All</a>
                <a class="dropdown-item" href="{{url('/careerprovider/add')}}"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
          </li>
          <li class="nav-item">
            <span class="opportunities">Career Opportunities</span>
                <a class="dropdown-item" href="{{url('/careers/list')}}"><i class="fa fa-users" aria-hidden="true"></i> View All</a>
                <a class="dropdown-item" href="{{url('/career/add')}}"><i class="fa fa-plus" aria-hidden="true"></i> Post a Career Opportunity</a>
          </li>
          <li class="nav-item">
            <span class="submit">Submit Urls</span>
                <a class="dropdown-item" href="{{url('/urls/list')}}"><i class="fa fa-users" aria-hidden="true"></i> View All</a>
              
          </li>
        </ul>
      </div>
    </nav>
</div>
<!-- End of Left Side navigation -->

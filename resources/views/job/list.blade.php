@extends('layouts.admin')

@section('content')

   @include('dashboard_header')

    <!--- End of Header -->

    <!-- Main Wrapper -->
    <div class="mainWrapper">
        <div class="container-fluid">
            <div class="row">

                @include('left_navigation')

                <div class="col-sm-12 col-md-9 col-lg-10 contentWrapper">
          <h2 class="mTitile">View All Career Opportunities</h2>
				  @if(session('success'))
					<p class="success-message">{{session('success')}}</p>
				@endif
          <div class="outterWrapper table col">
            <div class="row">
              <table id="jobs" class="table table-striped table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
				 
					    <thead>
					<tr>
			  			<!--<th>Id</th> -->
						<th>Title</th>
						<th>Company</th>
						<!--<th>Email</th> -->
						<th>Contact</th>
						<th>Type</th>
						<th>Submitted From</th>
						<th>CREATED ON</th>
						<th class="text-center">Actions</th>
						
					</tr>
			     </thead>

              </table> 
            </div>
          </div>
        </div>
        </div>
        </div>
        
          <!-- Modal content-->
        <div class="modal fade job_model search_monster-modal" id="myModal" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Search Monster</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-6 inner">
								<div class="form-group">
									<a href="#" class="btn btn-primary class_monster" target="_blank">Monster Source</a>				
								</div>
						    </div>	
						    <div class="col-sm-6 inner">
								<div class="form-group">
									<a href="#" class="btn btn-primary class_wsm" target="_blank">Monster WSM</a>		
								</div>
						    </div>	
						</div>	    	
				    </div>
				</div>
		   </div>
	   </div>	   		
</div>

@endsection
@section("scripts")
<script type="text/javascript" src='{{ asset("public/js/jquery.js") }}'></script>
<!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script> -->
<script type="text/javascript" src='{{ asset("public/js/jquery.dataTables.min.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/dataTables.bootstrap4.min.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/bootstrap.min.js") }}'></script>
<script type="text/javascript">
  var deletePath = '';
  $(document).ready(function(){
    $('#datatable-responsive').on('click',".deleteAction",function(event){

      $("#alertModal").modal({"show": true});

      deletePath = $(this).attr('data-newspaper-delete-route');


      $("#alertModal").on('click',".closeModal",function(){
        
        top.location.href = deletePath;

      });
    });
    $("#datatable-responsive").DataTable({
      "responsive" : true,
      "processing": true,
      "serverSide": true,
       "ajax": "",
        "language": {
                    "searchPlaceholder": "Search",
                    "sEmptyTable":     "No records found"
              }
    });
  })
  
  $(document).ready(function() {
    oTable = $("#jobs").DataTable({
        "processing": true,
        "responsive" : true,
        "serverSide": true,
        "ajax": "{{ route('datatable.getjobs') }}",
        "columns": [
           // {data: 'id', name: 'id'},
            {data: 'job_title', name: 'job_title'},
            {data: 'company_profile_id', name: 'company_profile_id'},
            {data: 'email', name: 'email'},
           // {data: 'phone_no', name: 'phone_no'},
            {data: 'job_status', name: 'job_status'},
            {data: 'refernce_url', name: 'refernce_url'},
             {data: 'created_at', name: 'created_at', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false}
            
        ],"columnDefs": [
			{ className: "text-center action", "targets": [ 6 ] }
       ]
    });
});

$(document).on('click',".monsterIcon",function(event){
		event.preventDefault();
		$('.job_model').modal('show'); 
		var monster_url=$(this).data('monster_url');
		$('.job_model').find('.class_monster').prop("href", monster_url);
		var monster_wsm_url=$(this).data('monstar_wsm_url');
		$('.job_model').find('.class_wsm').prop("href", monster_wsm_url);
      }); 

$(document).on('click', '.class_monster', function(e){ 
    e.preventDefault(); 
    var url = $(this).attr('href'); 
     $('.job_model').modal('hide'); 
    window.open(url, '_blank');
});
$(document).on('click', '.class_wsm', function(e){ 
    e.preventDefault(); 
    var url = $(this).attr('href'); 
     $('.job_model').modal('hide'); 
    window.open(url, '_blank');
});

</script>
@endsection

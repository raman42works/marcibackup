@extends('layouts.admin')

@section('content')
<div class="col main admin-login jobPostForm successMessage">
    <div class="container">
      <!-- Content Form Wrapper -->
      <div class="col-sm-12 formWrapper">
        <div class="fullwidth">
          <img src="{{ asset('public/images/white-logo.png') }}" class="logo" alt="logo">
          <h1>Worksource Montgomery's <br> Matching analytics resume  cloud interface</h1>
          <h4>Thank you for allowing WorkSource Montgomery (WSM) to be a part of your talent search; our matching analytics software will now go to work for you. When we receive information regarding qualified candidates, WSM staff will be in touch with you.</h4>
          <p>For further information about WorkSource Montgomery, please visit our website at <a href="https://worksourcemontgomery.com/" style="color:#fff">https://worksourcemontgomery.com/ </a> . </p> 

			<p>Again, thank you and good luck!</p>
			
			<h2>WSM Business Solutions Team</h2>
          <div class="col-sm-12">
            <div class="row">
              <div col-sm-12>
                <h4 class="success-message"> {{$message}}</h4>
              </div>
              <div class="col-sm-12">
                <div class="row">
				 <?php
				 $appendUrl="";
				 if($url !=''){
					 $appendUrl.='/'.$url;
				 }
				 if($last_company_id){
					 $appendUrl.='/'.$last_company_id;
				 }
				 
				 ?>	
                  <a href="{{route('post-job')}}{{$appendUrl}}" class='color-white'>Post another Career Opportunity</a>
                </div>
              </div>
            </div>          
          </div>
         </div>   
				
      </div>
      <!-- End of Content Form Wrapper -->
    </div>
@endsection
@section("css")
@endsection
@section("scripts")
@endsection

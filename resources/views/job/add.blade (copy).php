@extends('layouts.admin')

@section('content')

   @include('dashboard_header')

    <!--- End of Header -->

    <!-- Main Wrapper -->
    <div class="mainWrapper">
        <div class="container-fluid">
            <div class="row">

                @include('left_navigation')

                <div class="col-sm-12 col-md-10 contentWrapper">
          <h2 class="mTitile">Post a job</h2>
          <div class="outterWrapper col">
            <div class="row">
              <div class="col-sm-12 inner">
          <div class="jobPostForm">
            <form id="jobForm" action="{{ url('/job/submit') }}" method="post" enctype="multipart/form-data">
              <div class="row">
                <!-- Company Details -->
                <div class="col-sm-12 boxShadow">
                  <h4>Company Details</h4>
                  <div class="row">
                    <div class="col-sm-12 col-md-9">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <label for="company_name">Company Name <span>*</span></label>
                            <input type="text" name="company_name" class="form-control" id="company_name">
                          </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                          <div class="form-group">
                              <label for="companySize">Company Size <span>*</span></label>
                              <select class="form-control" id="companySize" name="company_size">
                               <option value="">--SELECT--</option>
                               <option>1 to 9 employees</option>
                               <option>10 to 19 employees</option>
                               <option>20 to 49 employees</option>
                               <option>50 to 99 employees </option>
                               <option>100 to 499 employees</option>
                               <option>500 to 999 employees</option>
                               <option>1,000 to 1,499 employees</option>
                               <option>1,500 to 1,999 employees</option>
                               <option>2,000 to 2,499 employees</option>
                               <option>2,500 to 4,999 employees</option>
                               <option>5,000 to 9,999 employees</option>
                               <option>10,000 employees or more</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                          <div class="form-group">
                            <label for="companyIndustry">Company Industry <span>*</span></label>
                            <select class="form-control" name="industry" id="companyIndustry">
                               <!---->
                               <option>--SELECT--</option>
                               <option>Accounting and Auditing Services</option>
                               <option>Advertising and PR Services</option>
                               <option>Aerospace and Defense</option>
                               <option>Agriculture/Forestry/Fishing</option>
                               <option>Architectural and Design Services</option>
                               <option>Automotive and Parts Mfg</option>
                               <option>Automotive Sales and Repair Services</option>
                               <option>Banking</option>
                               <option>Biotechnology/Pharmaceuticals</option>
                               <option>Broadcasting, Music, and Film</option>
                               <option>Business Services - Other</option>
                               <option>Chemicals/Petro-Chemicals</option>
                               <option>Clothing and Textile Manufacturing</option>
                               <option>Computer Hardware</option>
                               <option>Computer Software</option>
                               <option>Computer/IT Services</option>
                               <option>Construction - Industrial Facilities and Infrastructure</option>
                               <option>Construction - Residential &amp; Commercial/Office</option>
                               <option>Consumer Packaged Goods Manufacturing</option>
                               <option>Education</option>
                               <option>Electronics, Components, and Semiconductor Mfg</option>
                               <option>Energy and Utilities</option>
                               <option>Engineering Services</option>
                               <option>Entertainment Venues and Theaters</option>
                               <option>Financial Services</option>
                               <option>Food and Beverage Production</option>
                               <option>Government and Military</option>
                               <option>Healthcare Services</option>
                               <option>Hotels and Lodging</option>
                               <option>Insurance</option>
                               <option>Internet Services</option>
                               <option>Legal Services</option>
                               <option>Management Consulting Services</option>
                               <option>Manufacturing - Other</option>
                               <option>Marine Mfg &amp; Services</option>
                               <option>Medical Devices and Supplies</option>
                               <option>Metals and Minerals</option>
                               <option>Nonprofit Charitable Organizations</option>
                               <option>Other/Not Classified</option>
                               <option>Performing and Fine Arts</option>
                               <option>Personal and Household Services</option>
                               <option>Printing and Publishing </option>
                               <option>Real Estate/Property Management</option>
                               <option>Rental Services</option>
                               <option>Restaurant/Food Services</option>
                               <option>Retail</option>
                               <option>Security and Surveillance</option>
                               <option>Sports and Physical Recreation</option>
                               <option>Staffing/Employment Agencies</option>
                               <option>Telecommunications Services</option>
                               <option>Transport and Storage - Materials </option>
                               <option>Travel, Transportation and Tourism</option>
                               <option>Waste Management</option>
                               <option>Wholesale Trade/Import-Export</option>
                            </select>
                          </div>
                        </div>                        
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-3 logouploader">
                      <input type="file hidden" name="logouploader" class="form-control">
                      <div class="form-group text-right">
                        <label>Company Logo</label>
                        <a href="javascript:void(0);" id="fileUpload">
                        <div class="col logoUpload">
                          <span>+</span>
                        </div>
                        </a>
                        <input style="display: none;" type="file" id="logo" name="logo" />
                      </div>
                      

                    </div>
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-sm-12 col-md-4">
                          <div class="form-group">
                            <label for="contact_name">Contact Name <span>*</span></label>
                            <input type="text" class="form-control" name="contact_name" id="contact_name">
                          </div>
                        </div>                        
                        <div class="col-sm-12 col-md-4">
                          <div class="form-group">
                            <label for="company_email">Contact Email <span>*</span></label>
                            <input type="text" class="form-control" name="contact_email" id="company_email">
                          </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                          <div class="form-group">
                            <label for="contact_phone">Contact Phone <span>*</span></label>
                            <input type="text" class="form-control" name="contact_phone" id="contact_phone">
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <small>
                              <input type="checkbox" name="confidential">
                              Keep company name confidential.
                            </small>
                          </div>
                        </div>

                        <div class="col-sm-12">
                          <label><strong>ADDRESS</strong></label>
                        </div>
                        
                        <div class="col-sm-12 col-md-6">
                          <div class="form-group">
                            <label for="company_add1">Street Address 1 <span>*</span></label>
                            <input type="text" class="form-control" name="address" id="company_add1">
                          </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                          <div class="form-group">
                            <label for="company_add2">Street Address 2 </label>
                            <input type="text" class="form-control" name="address_2" id="company_add2">
                          </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                          <div class="form-group">
                            <label for="city">City <span>*</span></label>
                            <input type="text" class="form-control" id="city">
                          </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                          <div class="form-group">
                              <label for="state">State <span>*</span></label>
                              <select class="form-control" id="state" name="state">
                               <!---->
                               <option value="">--SELECT--</option>
                               <option>Alabama</option>
                               <option>Alaska</option>
                               <option>Arizona</option>
                               <option>Arkansas</option>
                               <option>California</option>
                               <option>Colorado</option>
                               <option>Connecticut</option>
                               <option>Delaware</option>
                               <option>District of Columbia</option>
                               <option>Florida</option>
                               <option>Georgia</option>
                               <option>Hawaii</option>
                               <option>Idaho</option>
                               <option>Illinois</option>
                               <option>Indiana</option>
                               <option>Iowa</option>
                               <option>Kansas</option>
                               <option>Kentucky</option>
                               <option>Louisiana</option>
                               <option>Maine</option>
                               <option>Maryland</option>
                               <option>Massachusetts</option>
                               <option>Michigan</option>
                               <option>Minnesota</option>
                               <option>Mississippi</option>
                               <option>Missouri</option>
                               <option>Montana</option>
                               <option>Nebraska</option>
                               <option>Nevada</option>
                               <option>New Hampshire</option>
                               <option>New Jersey</option>
                               <option>New Mexico</option>
                               <option>New York</option>
                               <option>North Carolina</option>
                               <option>North Dakota</option>
                               <option>Ohio</option>
                               <option>Oklahoma</option>
                               <option>Oregon</option>
                               <option>Pennsylvania</option>
                               <option>Puerto Rico</option>
                               <option>Rhode Island</option>
                               <option>South Carolina</option>
                               <option>South Dakota</option>
                               <option>Tennessee</option>
                               <option>Texas</option>
                               <option>Utah</option>
                               <option>Vermont</option>
                               <option>Virgin Islands</option>
                               <option>Virginia</option>
                               <option>Washington</option>
                               <option>West Virginia</option>
                               <option>Wisconsin</option>
                               <option>Wyoming</option>
                               <option>Armed Forces Americas</option>
                               <option>Armed Force Europe, the Middle East, and Canada</option>
                               <option>Armed Forces Pacific</option>
                               <option>Federated States of Micronesia</option>
                               <option>Guam</option>
                               <option>American Samoa</option>
                               <option>Northern Mariana Islands</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                          <div class="form-group">
                            <label for="zip">Zip Code <span>*</span></label>
                            <input type="text" class="form-control" name="zip" id="zip">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End of Company Details -->

                <div class="clearfix"></div>

                <!-- Job Details-->
                <div class="col-sm-12 boxShadow">
                  <h4>Job Details</h4>
                  <div class="row">
                    <div class="col-sm-12 col-md-8">
                      <div class="form-group">
                        <label for="job_title">Job Title <span>*</span></label>
                        <input type="text" class="form-control" name="job_title" id="job_title">
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                      <div class="form-group">
                        <label for="jobStatus">Job Status  <span>*</span></label>
                        <select class="form-control" id="job_status" name="job_status">
                           <!---->
                           <option value="4">Full Time</option>
                           <option value="5">Part Time</option>
                           <option value="26">Per Diem</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12 col-md-4 rangeField">
                      <div class="row signleBox">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <label>Range</label>
                            <input type="number" class="form-control" id="" name="range">
                          </div>
                        </div>
                      </div>
                      <div class="row doubleBox" style="display: none;">
                        <div class="col-sm-5">
                          <div class="form-group">
                            <label>Min</label>
                            <input type="number" class="form-control min" id="min" name="min_range">
                          </div>
                        </div>
                        <div class="col-sm-2 to">
                          To
                        </div>
                        <div class="col-sm-5">
                          <div class="form-group">
                            <label>Max</label>
                            <input type="number" class="form-control max" id="max" name="max_range">
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-12 col-md-2 rangeBtn">
                      <div class="form-group">
                        <label></label>
                        <small>
                          <input type="checkbox" name="" id="rangeBtn" placeholder="Enter Amount">
                          Add Range
                        </small>
                      </div>
                    </div>

                    <div class="col-sm-12 col-md-3">
                      <div class="form-group">
                        <label>Bonus</label>
                        <input type="number" class="form-control " id="max" name="bonus">
                      </div>
                    </div>

                    <div class="col-sm-12 col-md-3">
                      <div class="form-group">
                        <label></label>
                        <select aria-label="Add Range" class="form-control" id="salaryType" name="salaryType">
                           <!---->
                           <option value="1">Per Year</option>
                           <option value="2">Per Hour</option>
                           <option value="3">Per Week</option>
                           <option value="4">Per Month</option>
                           <option value="5">Biweekly</option>
                           <option value="6">Per Day</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End of Job Details -->

                <!-- Job Requirement -->
                <div class="col-sm-12 boxShadow">
                  <h4>Job Requirements</h4>
                  <div class="row">
                    <div class="col-sm-12 col-md-4">
                      <div class="form-group">
                        <label>Minimum Education Level <span>*</span></label>
                        <select aria-label="educatonlevel" class="form-control" id="educatonLevel" name="educatonLevel">
                           <option value="-1">Select</option>
                           <option value="1"> Bachelor’s Degree</option>
                           <option value="2">Master’s Degree</option>
                           <option value="3">Doctorate</option>
                           <option value="4">Professional</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                      <div class="form-group">
                        <label>Degree/Major <span>*</span></label>
                        <input type="text" class="form-control">
                        <small>(e.g. MBA, MS Math, MSCS, English)</small>
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                      <div class="form-group">
                        <label>Year of Experience <span>*</span></label>
                        <select aria-label="experience" class="form-control" id="experience" name="experience">
                           <option value="-1">Select</option>
                           <option value=""><1</option>
                           <option value="">1-2</option>
                           <option value="">2-5</option>
                           <option value="">5-10</option>
                           <option value="">10+</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-10 ">
                      <div class="form-group">
                        <label>Skills/Keywords <span>*</span> <br><small>Enter terms related to candidates industry or expertise(e.g. java, telesales, call center, retail, SOX)</small></label>
                      </div>
                      <div class="form-group skillFiled first">
                        <input type="text" name="skills" class="form-control" id="skill_keywords">
                        <div class="optionsBtn">
                          <a href="javascript:void(0)" id="addSkillField" data-toggle="tooltip" title="Add Input Field">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                          </a><!-- 
                          <a href="#" id="deleteSkillField" data-toggle="tooltip" title="Delete Input Field">
                            <i class="fa fa-minus-circle" aria-hidden="true"></i>
                          </a> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End of Job Requirement -->

                <!-- Job Descrption -->
                <div class="col-sm-12 boxShadow">
                  <h4>Job Description</h4>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                       <textarea aria-hidden="true" name="job_description"></textarea>
                      </div>
                      <div class="form-group text-center">
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <input type="submit" name="submit" class="btn btn-primary mx-auto col-sm-12 col-md-2" value="Submit">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End of Job Descrption -->
              </div>
            </form>
          </div>
        </div>
            </div>
          </div>
        </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")
<script type="text/javascript" src='{{ asset("public/js/datatable/datatable.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/datatable/datatable-bootstrap.js") }}'></script>
<script type="text/javascript" src='{{ asset("public/js/datatable/datatable-responsive.js") }}'></script>

<script type="text/javascript">
  var deletePath = '';
  $(document).ready(function(){
    $('#datatable-responsive').on('click',".deleteAction",function(event){

      $("#alertModal").modal({"show": true});

      deletePath = $(this).attr('data-newspaper-delete-route');


      $("#alertModal").on('click',".closeModal",function(){
        
        top.location.href = deletePath;

      });
    });

    $("#fileUpload").click(function(){
      $("#logo").trigger("click");
    })

    $(document).on("click", "#addSkillField", function(){
      event.preventDefault();
        $(".skillFiled:last").after('<div class="form-group skillFiled"><input type="text" name="skills" class="form-control" id="skill_keywords"><div class="optionsBtn"><a href="javascript:void(0)" id="addSkillField" data-toggle="tooltip" title="Add Input Field"><i class="fa fa-plus-circle" aria-hidden="true"></i></a><a href="javascript:void()" id="deleteSkillField" data-toggle="tooltip" title="Delete Input Field"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></div></div>');
    });

    $(document).on("click", "#deleteSkillField", function(){
      event.preventDefault();
      $(this).closest(".skillFiled").remove();
    });



    $("#datatable-responsive").DataTable({
      "responsive" : true,
      "processing": true,
          "serverSide": true,
      "columnDefs": [{
                "targets": [5, 6, 8], // column or columns numbers
                "orderable": false,  // set orderable for selected columns
              }],
          "ajax": "",
        "language": {
                    "searchPlaceholder": "Search",
                    "sEmptyTable":     "No records found"
              }
    });
  })
</script>
@endsection

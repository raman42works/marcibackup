@extends('layouts.admin')

@section('content')
<div class="col main admin-login jobPostForm successMessage">
    <div class="container">
      <!-- Content Form Wrapper -->
      <div class="col-sm-12 formWrapper">
        <div class="fullwidth">
          <img src="{{ asset('public/images/white-logo.png') }}" class="logo" alt="logo">
          <h1>Worksource Montgomery's <br> Matching analytics resume  cloud interface</h1>
          <h4>Thank you for allowing WorkSource Montgomery (WSM) to be a part of your career search. Our matching and analytics software will now go to work for you. When MARCI receives any information that matches your search, the WSM Business Solutions Team will contact you with the information.</h4>
            <p>For further information about WorkSource Montgomery, please visit our website at <a href="https://worksourcemontgomery.com/" style="color:#fff">https://worksourcemontgomery.com/ </a> . </p> 

			<p>Again, thank you and good luck!</p>
			
			<h2>WSM Business Solutions Team</h2>
          
          <h4 class="success-message"> {{$message}}</h4>
          
         </div>   
				
      </div>
      <!-- End of Content Form Wrapper -->
    </div>
@endsection
@section("css")
@endsection
@section("scripts")
@endsection

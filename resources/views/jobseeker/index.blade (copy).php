@extends('layouts.admin')

@section('content')
<div class="col main admin-login jobPostForm">
    <div class="container">
      <!-- Content Form Wrapper -->
      <div class="col-sm-12 formWrapper">
        <div class="fullwidth">
          <img src="{{ asset('public/images/white-logo.png') }}" class="logo" alt="logo">
          <h1>Worksource Montgomery's <br> Matching analytics resume  cloud interface</h1>
          <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</h4>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting</p>
        </div>
        <div class="col-sm-12 inner">
			  @if(session()->has('message'))
    <p class="success-message">
        {{ session()->get('message') }}
    </p>
    @endif
          <div class="jobPostForm">
            <form id="jobForm" action="{{ url('/jobseeker/submit') }}" method="post" enctype="multipart/form-data">
              <div class="row">
				  <?php
			$resumeOriginData = array(""=>"Select","CareerBuilder"=>"CareerBuilder", "Career"=>"Career", "College"=>"College",
             "Corporate Website"=>"Corporate Website", "Craigs List"=>"Craigs List", "Current Employee"=>"Current Employee",
             "Dice"=>"Dice", "Employee Referral"=>"Employee Referral", "HotJobs"=>"HotJobs",
             "Media"=>"Media", "Monster"=>"Monster", "Newspapers"=>"Newspapers","Niche Job Board"=>"Niche Job Board", "Outdoor   Advertising"=>"Outdoor Advertising",
              "Rehire"=>"Rehire","Search Engine"=>"Search Engine", "Social Network"=>"Social Network", "Staffing Firm"=>"Staffing Firm",
              "Walk In"=>"Walk In","Other"=>"Other"  
             );
             $stateData = array(""=>"Select","Alabama"=>"Alabama", "Arizona"=>"Arizona", "Arkansas"=>"Arkansas",
             "California"=>"California", "Colorado"=>"Colorado", "Connecticut"=>"Connecticut",
             "Delaware"=>"Delaware", "District of Columbia"=>"District of Columbia", "Florida"=>"Florida",
             "Georgia"=>"Georgia", "Hawaii"=>"Hawaii", "Idaho"=>"Idaho","Illinois"=>"Illinois", "Indiana"=>"Indiana",
              "Iowa"=>"Iowa","Kansas"=>"Kansas", "Kentucky"=>"Kentucky", "Louisiana"=>"Louisiana",
              "Maine"=>"Maine","Maryland"=>"Maryland", "Massachusetts"=>"Massachusetts", "Michigan"=>"Michigan",
              "Minnesota"=>"Minnesota","Mississippi"=>"Mississippi", "Missouri"=>"Missouri", "Montana"=>"Montana",
              "Nebraska"=>"Nebraska","Nevada"=>"Nevada", "New Hampshire"=>"New Hampshire", "New Jersey"=>"New Jersey",
              "New Mexico"=>"New Mexico","New York"=>"New York", "North Carolina"=>"North Carolina", "North Dakota"=>"North Dakota",
              "Ohio"=>"Ohio","Oklahoma"=>"Oklahoma", "Oregon"=>"Oregon", "Pennsylvania"=>"Pennsylvania",
              "Puerto Rico"=>"Puerto Rico","Rhode Island"=>"Rhode Island", "South Carolina"=>"South Carolina", "South Dakota"=>"South Dakota",
              "Tennessee"=>"Tennessee","Texas"=>"Texas", "Utah"=>"Utah", "Vermont"=>"Vermont",
              "Virgin Islands"=>"Virgin Islands","Virginia"=>"Virginia", "Washington"=>"Washington", "West Virginia"=>"West Virginia",
              "Wisconsin"=>"Wisconsin","Wyoming"=>"Wyoming", "Armed Forces Americas"=>"Armed Forces Americas", "Armed Force Europe, the Middle East, and Canada"=>"Armed Force Europe, the Middle East, and Canada",
              "Armed Forces Pacific"=>"Armed Forces Pacific","Federated States of Micronesia"=>"Federated States of Micronesia", "Guam"=>"Guam", "American Samoa"=>"American Samoa","Northern Mariana Islands"=>"Northern Mariana Islands",  
             );
             
			?>	
				  
                <!-- Seeker Details -->
                  <div class="col-sm-12 boxShadow">
                    <h4>Job Seeker Details</h4>
                    <div class="row">
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>First Name <span>*</span></label>
                          <input type="input" class="form-control" name="firstName" id="firstName"  value="{{ old('firstName') }}">
                        </div>
                         {!! $errors->first('firstName', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Last Name <span>*</span></label>
                          <input type="input" class="form-control" name="lastName" id="firstName" value="{{ old('lastName') }}">
                        </div>
                         {!! $errors->first('lastName', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Email <span>*</span></label>
                          <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}">
                        </div>
                         {!! $errors->first('email', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Phone <span>*</span></label>
                          <input type="tel" class="form-control" name="phone" id="phone" value="{{ old('phone') }}">
                        </div>
                         {!! $errors->first('phone', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                          <label>City <span>*</span></label>
                          <input type="input" class="form-control" name="city" id="city" value="{{ old('city') }}">
                        </div>
                         {!! $errors->first('city', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                          <label>State <span>*</span></label>
                          
                           {{ Form::select('state', $stateData,old('state'), ['class' => 'form-control','id'=>'state']) }}
                        </div>
                        {!! $errors->first('state', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                          <label>Zip Code <span>*</span></label>
                          <input type="input" class="form-control" name="zipCode" id="zipCode" value="{{ old('zipCode') }}">
                        </div>
                        {!! $errors->first('zipCode', '<p class="error-danger">:message</p>') !!}
                      </div>
                    </div>
                  </div>
                  <!-- End of Seeker Details -->

                  <!-- Resume Details -->
                  <div class="col-sm-12 boxShadow">
                    <h4>Resume</h4>
                    <div class="row">
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Resume Title <small> (Optional)</small></label> 
                          <input type="type" name="resumeTitle" class="form-control" value="{{ old('resumeTitle') }}">
                        </div>
                      </div>
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Resume Origin <small> (Optional)</small></label> 
                            {{ Form::select('resumeOrigin', $resumeOriginData,old('resumeOrigin'), ['class' => 'form-control','id'=>'resumeOrigin']) }}
                        </div>
                      </div>
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Highest Education Degree <span>*</span></label> 
                          <select class="form-control" name="educationDegree" id="educationDegree">
                             <option {{(old('educationDegree') == "Professional" ? "selected" : "" )}} value="Professional">Professional</option>
                            <option {{(old('educationDegree') == "Doctorate" ? "selected" : "" )}} value="Doctorate">Doctorate</option>
                            <option {{(old('educationDegree') == "Master's Degree" ? "selected" : "" )}} value="Master's Degree">Master's Degree</option>
                            <option {{(old('educationDegree') == "Bachelor's Degree" ? "selected" : "" )}} value="Bachelor's Degree">Bachelor's Degree</option>
                             <option {{(old('educationDegree') == "Associate Degree" ? "selected" : "" )}} value="Associate Degree">Associate Degree</option>
                             <option {{(old('educationDegree') == "Some College Coursework Completed" ? "selected" : "" )}} value="Some College Coursework Completed">Some College Coursework Completed</option>
                       
                             <option {{(old('educationDegree') == "Vocational" ? "selected" : "" )}} value="Vocational">Vocational</option>
                              <option {{(old('educationDegree') == "Certification" ? "selected" : "" )}} value="Certification">Certification</option>
                              <option {{(old('educationDegree') == "High School or equivalent" ? "High School or equivalent" : "" )}} value="High School or equivalent">High School or equivalent</option>
                                    <option {{(old('educationDegree') == "Some High School Coursework" ? "selected" : "" )}} value="Some High School Coursework">Some High School Coursework</option>
                            <option {{(old('educationDegree') == "Unspecified" ? "selected" : "" )}} value="Unspecified" >Unspecified</option>
                          </select>
                        </div>
                         {!! $errors->first('educationDegree', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Citizenship <span>*</span></label> 
                          <select class="form-control" name="citizenship" id="citizenship">
                            <option value="">None</option>
                            <option {{(old('citizenship') == "US Citizen" ? "selected" : "" )}} value="US Citizen">US Citizen</option>
                            <option {{(old('citizenship') == "Permanent Resident" ? "selected" : "" )}} value="Permanent Resident">Permanent Resident</option>
                            <option {{(old('citizenship') == "Other" ? "selected" : "" )}} value="Other">Other</option>
                          </select>
                        </div>
                         {!! $errors->first('citizenship', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Resume <span>*</span></label> 
                          <input type="file" name="resume">
                        </div>
                         {!! $errors->first('resume', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <input type='text' name='filePath' value="{{ Session::get('fileLogo')}} " style='display:none'>
                        <div class="col-sm-12 col-md-12">
                      
                      @if(Session::has('fileLogo')) 
						<div class="form-group">
						  Recent File Upload: {{ Session::get('fileLogo')}} 
						</div>
						@endif
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group text-center">
							  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
							  <input type='hidden'  name='refernce_name' value='{{$company}}' >
							  <input type='hidden'  name="admin_save" value=0 >
							   <input type='hidden'  name='refernce_url' value='<?php echo $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>' >
                          <input type="submit" name="submit" class="btn btn-primary mx-auto col-sm-12 col-md-2" value="Submit">
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End of Resume Details -->

              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- End of Content Form Wrapper -->
    </div>
@endsection
@section("css")
<link rel="stylesheet" type="text/css" href="{{ asset('public/css/jquery.auto-complete.css') }}">


@endsection
@section("scripts")
<script type="text/javascript" src='{{ asset("public/js/jquery-validation/dist/jquery.validate.min.js") }}'></script>
 
<script>
  jQuery(document).ready(function(){
    $("#fileUpload").click(function(){
      $("#logo").trigger("click");
    })
    /*$("#jobForm").validate(
      {
        "rules" : {
          "company_name" : {
            "required" : true
          },
          "company_size" : {
            "required" : true
          },
          "industry" : {
            "required" : true
          },
          "contact_name" : {
            "required" : true
          },
          "contact_email" : {
            "required" : true,
            "email" : true
          },
          "contact_phone" : {
            "required" : true
          },
          "address" : {
            "required" : true
          },
          "city" : {
            "required" : true
          },
          "state" : {
            "required" : true
          },
          "zip" : {
            "required" : true
          },
          "job_title" : {
            "required" : true
          },
          "job_type" : {
            "required" : true
          },
          "job_status" : {
            "required" : true
          },
          "job_description" : {
            "required" : true
          },
          'logo': { 
                "required": false,
                "extension": "png|jpg|jpeg|gif"
            }


        }
      }
    );*/
  });
</script>


@endsection

@extends('layouts.admin')

@section('content')
<div class="col main admin-login jobPostForm">
    <div class="container">
      <!-- Content Form Wrapper -->
      <div class="col-sm-12 formWrapper">
        <div class="fullwidth">
          <img src="{{ asset('public/images/white-logo.png') }}" class="logo" alt="logo">
          <h1>WELCOME TO MARCI (MATCHING ANALYTICS RESUME CLOUD INTERFACE)</h1>
          <h4>WorkSource Montgomery is pleased to support your career search. Please submit your resume AND fill out the required information</h4>
          <p>Note: Please be advised, your information will only be shared by WSM staff with potential employers. WSM's employer partners will contact you if your resume matches their needs.</p>
        </div>
        <div class="col-sm-12 inner">
			  @if(session()->has('message'))
				<p class="success-message">
					{{ session()->get('message') }}
				</p>
			  @endif
          <div class="jobPostForm">
            <form id="jobForm" action="{{ url('/careerseeker/submit') }}" method="post" enctype="multipart/form-data">
              <div class="row">
				  <?php
			$resumeOriginData = array(""=>"Select","CareerBuilder"=>"CareerBuilder", "Career"=>"Career", "College"=>"College",
             "Corporate Website"=>"Corporate Website", "Craigs List"=>"Craigs List", "Current Employee"=>"Current Employee",
             "Dice"=>"Dice", "Employee Referral"=>"Employee Referral", "HotJobs"=>"HotJobs",
             "Media"=>"Media", "Monster"=>"Monster", "Newspapers"=>"Newspapers","Niche Job Board"=>"Niche Job Board", "Outdoor   Advertising"=>"Outdoor Advertising",
              "Rehire"=>"Rehire","Search Engine"=>"Search Engine", "Social Network"=>"Social Network", "Staffing Firm"=>"Staffing Firm",
              "Walk In"=>"Walk In","Other"=>"Other"  
             );
            $stateData = array(""=>"Select","Alabama"=>"Alabama","Alaska"=>"Alaska",  "Arizona"=>"Arizona", "Arkansas"=>"Arkansas",
             "California"=>"California", "Colorado"=>"Colorado", "Connecticut"=>"Connecticut",
             "Delaware"=>"Delaware", "District of Columbia"=>"District of Columbia", "Florida"=>"Florida",
             "Georgia"=>"Georgia", "Hawaii"=>"Hawaii", "Idaho"=>"Idaho","Illinois"=>"Illinois", "Indiana"=>"Indiana",
              "Iowa"=>"Iowa","Kansas"=>"Kansas", "Kentucky"=>"Kentucky", "Louisiana"=>"Louisiana",
              "Maine"=>"Maine","Maryland"=>"Maryland", "Massachusetts"=>"Massachusetts", "Michigan"=>"Michigan",
              "Minnesota"=>"Minnesota","Mississippi"=>"Mississippi", "Missouri"=>"Missouri", "Montana"=>"Montana",
              "Nebraska"=>"Nebraska","Nevada"=>"Nevada", "New Hampshire"=>"New Hampshire", "New Jersey"=>"New Jersey",
              "New Mexico"=>"New Mexico","New York"=>"New York", "North Carolina"=>"North Carolina", "North Dakota"=>"North Dakota",
              "Ohio"=>"Ohio","Oklahoma"=>"Oklahoma", "Oregon"=>"Oregon", "Pennsylvania"=>"Pennsylvania",
              "Puerto Rico"=>"Puerto Rico","Rhode Island"=>"Rhode Island", "South Carolina"=>"South Carolina", "South Dakota"=>"South Dakota",
              "Tennessee"=>"Tennessee","Texas"=>"Texas", "Utah"=>"Utah", "Vermont"=>"Vermont",
              "Virgin Islands"=>"Virgin Islands","Virginia"=>"Virginia", "Washington"=>"Washington", "West Virginia"=>"West Virginia",
              "Wisconsin"=>"Wisconsin","Wyoming"=>"Wyoming", "Armed Forces Americas"=>"Armed Forces Americas", "Armed Force Europe, the Middle East, and Canada"=>"Armed Force Europe, the Middle East, and Canada",
              "Armed Forces Pacific"=>"Armed Forces Pacific","Federated States of Micronesia"=>"Federated States of Micronesia", "Guam"=>"Guam", "American Samoa"=>"American Samoa","Northern Mariana Islands"=>"Northern Mariana Islands",  
             );
             
			?>	
				  
                <!-- Seeker Details -->
                  <div class="col-sm-12 boxShadow">
                    <h4>Career Seeker Details</h4>
                    <div class="row">
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>First Name <span>*</span></label>
                          <input type="input" class="form-control" name="firstName" id="firstName"  value="{{ old('firstName') }}">
                        </div>
                         {!! $errors->first('firstName', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Last Name <span>*</span></label>
                          <input type="input" class="form-control" name="lastName" id="firstName" value="{{ old('lastName') }}">
                        </div>
                         {!! $errors->first('lastName', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Email <span>*</span></label>
                          <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}">
                        </div>
                         {!! $errors->first('email', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Phone <span>*</span></label>
                          <input type="tel" class="form-control phone" name="phone" id="phone" value="{{ old('phone') }}">
                        </div>
                         {!! $errors->first('phone', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                          <label>City <span>*</span></label>
                          <input type="input" class="form-control" name="city" id="city" value="{{ old('city') }}">
                        </div>
                         {!! $errors->first('city', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                          <label>State <span>*</span></label>
                          
                           {{ Form::select('state', $stateData,old('state'), ['class' => 'form-control','id'=>'state']) }}
                        </div>
                        {!! $errors->first('state', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                          <label>Zip Code <span>*</span></label>
                          <input type="input" class="form-control" name="zipCode" id="zipCode" value="{{ old('zipCode') }}">
                        </div>
                        {!! $errors->first('zipCode', '<p class="error-danger">:message</p>') !!}
                      </div>
                    </div>
                  </div>
                  <!-- End of Seeker Details -->

                  <!-- Resume Details -->
                  <div class="col-sm-12 boxShadow">
                    <h4>Resume</h4>
                    <div class="row">
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Resume Title <small> (Optional)</small></label> 
                          <input type="type" name="resumeTitle" class="form-control" value="{{ old('resumeTitle') }}">
                        </div>
                      </div>
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Resume Origin <small> (Optional)</small></label> 
                            {{ Form::select('resumeOrigin', $resumeOriginData,old('resumeOrigin'), ['class' => 'form-control','id'=>'resumeOrigin']) }}
                        </div>
                      </div>
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Highest Education Degree <span>*</span></label> 
                          <select class="form-control" name="educationDegree" id="educationDegree">
							    <option value="">Select</option>
                             <option {{(old('educationDegree') == "Professional" ? "selected" : "" )}} value="Professional">Professional</option>
                            <option {{(old('educationDegree') == "Doctorate" ? "selected" : "" )}} value="Doctorate">Doctorate</option>
                            <option {{(old('educationDegree') == "Master's Degree" ? "selected" : "" )}} value="Master's Degree">Master's Degree</option>
                            <option {{(old('educationDegree') == "Bachelor's Degree" ? "selected" : "" )}} value="Bachelor's Degree">Bachelor's Degree</option>
                             <option {{(old('educationDegree') == "Associate Degree" ? "selected" : "" )}} value="Associate Degree">Associate Degree</option>
                             <option {{(old('educationDegree') == "Some College Coursework Completed" ? "selected" : "" )}} value="Some College Coursework Completed">Some College Coursework Completed</option>
                       
                             <option {{(old('educationDegree') == "Vocational" ? "selected" : "" )}} value="Vocational">Vocational</option>
                              <option {{(old('educationDegree') == "Certification" ? "selected" : "" )}} value="Certification">Certification</option>
                              <option {{(old('educationDegree') == "High School or equivalent" ? "High School or equivalent" : "" )}} value="High School or equivalent">High School or equivalent</option>
                                    <option {{(old('educationDegree') == "Some High School Coursework" ? "selected" : "" )}} value="Some High School Coursework">Some High School Coursework</option>
                            <option {{(old('educationDegree') == "Unspecified" ? "selected" : "" )}} value="Unspecified" >Unspecified</option>
                          </select>
                        </div>
                         {!! $errors->first('educationDegree', '<p class="error-danger">:message</p>') !!}
                      </div>
                      <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Citizenship <span>*</span></label> 
                          <select class="form-control" name="citizenship" id="citizenship">
                            <option value="">Select</option>
                               <option {{(old('citizenship') == "None" ? "selected" : "" )}} value="None">None</option>
                            <option {{(old('citizenship') == "US Citizen" ? "selected" : "" )}} value="US Citizen">US Citizen</option>
                            <option {{(old('citizenship') == "Permanent Resident" ? "selected" : "" )}} value="Permanent Resident">Permanent Resident</option>
                            <option {{(old('citizenship') == "Other" ? "selected" : "" )}} value="Other">Other</option>
                          </select>
                        </div>
                         {!! $errors->first('citizenship', '<p class="error-danger">:message</p>') !!}
                      </div>
                       <div class="col-sm-12 col-md-12">
                         <div class="form-group">
                        <label>What shifts are you available to work? Please check all that apply: </label>
                        
                         <div class="form-group checkboxes">
						<label class="col-md-12 checkbox-inline" for="checkboxes-0">
					   
						  <input type="checkbox" name="shift_type[]" id="checkboxes-0" 
						  
						  {{ (is_array(old('shift_type')) && in_array(1, old('shift_type'))) ? ' checked' : '' }}
						   value="1">
						   <span> Day Shift </span>
						</label>
						<label class="col-md-12 checkbox-inline" for="checkboxes-1">
						  <input type="checkbox" name="shift_type[]" id="checkboxes-1"  {{ (is_array(old('shift_type')) && in_array(2, old('shift_type'))) ? ' checked' : '' }} value="2">
						 <span>   Evening Shift </span>
						</label>
						<label class="col-md-12 checkbox-inline" for="checkboxes-2">
						  <input type="checkbox" name="shift_type[]" id="checkboxes-2"  {{ (is_array(old('shift_type')) && in_array(3, old('shift_type'))) ? ' checked' : '' }} value="3">
						<span>  Night Shift</span>
						</label>
						</div>
						</div>
                      </div>
                      <div class="col-sm-12 col-md-12" style="margin-top: 20px;">
                        <div class="form-group">
                          <label>Resume <span>*</span></label> 
                          <input type="file" name="resume">
                           <p class="error-file">The following are acceptable document types: DOC, DOCX, TEXT, RTF, HTML, PDF, Zip file. File size can be up to 512 KB.</p>
                       <!-- <input type="file"  name="resume[]" id="resume" multiple> -->
                        </div>
                         {!! $errors->first('resume', '<p class="error-danger">:message</p>') !!}
                        
                      </div>
                      <input type='text' name='filePath' value="{{ Session::get('fileLogo')}} " style='display:none'>
                        <div class="col-sm-12 col-md-12">
                      
                      @if(Session::has('fileLogo')) 
						<div class="form-group">
						  Recent File Upload: {{ Session::get('fileLogo')}} 
						</div>
						@endif
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group text-center">
							  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
							  <input type='hidden'  name='refernce_name' value='{{$company}}' >
							  <input type='hidden'  name="admin_save" value=0 >
							   <input type='hidden'  name='refernce_url' value='<?php echo $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>' >
                          <input type="submit" name="submit" class="btn btn-primary mx-auto col-sm-12 col-md-2" value="Submit">
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End of Resume Details -->

              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- End of Content Form Wrapper -->
    </div>
     <!-- Modal content-->
        <div class="modal fade error_model" id="myModal" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Error</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-11">
							<p>Form cannot be submitted. Please correct the fields as noted in red.</p>
							</div>	    
						</div>	    	
				    </div>
				</div>
		   </div>
	   </div>	
    
@endsection
@section("css")
<link rel="stylesheet" type="text/css" href="{{ asset('public/css/jquery.auto-complete.css') }}">
<style>
	.error-file{
    padding: 0px;
    color: #000 !important;
	}
.checkboxes label {
  display: block;
  float: left;
  padding-right: 10px;
  padding-left: 22px;
  text-indent: -22px;
}
.checkboxes input {
  vertical-align: middle;
}
.checkboxes label span {
  vertical-align: middle;
  color: #4c4c4c;
}
</style>

@endsection
@section("scripts")
<script type="text/javascript" src='{{ asset("public/js/jquery-validation/dist/jquery.validate.min.js") }}'></script>
 
<script>
  jQuery(document).ready(function(){
    $("#fileUpload").click(function(){
      $("#logo").trigger("click");
    })
    var typingTimer;                //timer identifier
	var doneTypingInterval = 1500;  //time in ms, 5 second for example
	var $input = $('.phone');

	//on keyup, start the countdown
	$input.on('keyup', function () {
	  clearTimeout(typingTimer);
	  typingTimer = setTimeout(doneTyping, doneTypingInterval);
	});

	//on keydown, clear the countdown 
	$input.on('keydown', function () {
	  clearTimeout(typingTimer);
	});

	//user is "finished typing," do something
	function doneTyping () {
		text = $(".phone").val();
		text = text.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "$1-$2-$3");
		$(".phone").val(text);
	 
	}

    /*$("#jobForm").validate(
      {
        "rules" : {
          "company_name" : {
            "required" : true
          },
          "company_size" : {
            "required" : true
          },
          "industry" : {
            "required" : true
          },
          "contact_name" : {
            "required" : true
          },
          "contact_email" : {
            "required" : true,
            "email" : true
          },
          "contact_phone" : {
            "required" : true
          },
          "address" : {
            "required" : true
          },
          "city" : {
            "required" : true
          },
          "state" : {
            "required" : true
          },
          "zip" : {
            "required" : true
          },
          "job_title" : {
            "required" : true
          },
          "job_type" : {
            "required" : true
          },
          "job_status" : {
            "required" : true
          },
          "job_description" : {
            "required" : true
          },
          'logo': { 
                "required": false,
                "extension": "png|jpg|jpeg|gif"
            }


        }
      }
    );*/
  });
  /*
  $('input[type="checkbox"]').on('change', function() {
   $('input[type="checkbox"]').not(this).prop('checked', false);
});
*/
</script>

<?php

if(!$errors->isEmpty()){
	echo "<script>
	$('.error_model').modal('show'); 
	</script>";
	
}
?>

@endsection

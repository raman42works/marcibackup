<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\SubmitUrl;
use App\User;
use \Input;
use Validator;
use Datatables;
use DB;
use View;

class SubmitUrlController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['datatable','getPosts']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        return view('jobseeker/index');
    }

   
    public function listUrl(Request $request)
    {
       
       
        return view('submiturl/list');
    }
    
    
    public function updateSubmitUrl(Request $request)
    {
       try {
        $data =  $request->toArray();
        $submitUrl = SubmitUrl::find($data['refernce_id']);     
        $dataValue['success']='0';
        if (is_null($submitUrl)) {
			  $dataValue['message']='Field can not be empty. There should be some value.';
			  $dataValue['success']=0;
		}
		else {
			$submitUrl->count =$data['count']; 
			$submitUrl->save();
			$dataValue['message']='Count updated successfully.';
			$dataValue['success']=1;
		}
		echo json_encode($dataValue);
		 }
		catch (\Exception $e) {
			 $dataValue['message']='Field can not be empty. There should be some value.';
			  $dataValue['success']=0;
			echo json_encode($dataValue);
		}              
    }
    
    
    public function getsubmiturl()
    {
    	$jobproviders = SubmitUrl::query();
    	
        return Datatables::of($jobproviders)->editColumn('created_at', function($jobproviders) {
			        $link=$jobproviders->created_at->format('F d, Y');
                    return $link;
                })
                ->editColumn('count', function($jobproviders) {
			        $link='<input type="text" class="count_update input_count" value='.$jobproviders->count.' data-id='.$jobproviders->id.'>
			        <button class="count_update_btn btn" data-id="'.$jobproviders->id.'" data-value="'.$jobproviders->count.'"> Update</button>
			        ';
                   /// return $link;    
                    return $link;    
                })
                ->orderColumn('refernce_name', 'refernce_name $1')
                /*->addColumn('action', function ($jobproviders) {
					$url=\URL::to('').'/public/resume/';
					//$link='<a href='.$url.$jobseeker->resume_url.'  target="_blank">View Resume</a>';
                            return '<a href="' . route("jobprovider-view", $jobproviders->id) . '" class="" title="View"><i class="fa fa-eye"></i></a>
                            <a href="' . route("jobprovider-delete", $jobproviders->id) . '" class="delete_pro" data-url="' . route("jobprovider-delete", $jobproviders->id) . ' " title="Delete">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>';
                        })*/
            ->make(true);
    }
    
}

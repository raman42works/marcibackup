<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Jobs;
use App\User;
use App\Models\CompanyProfile;
use App\Models\SubmitUrl;
use \Input;
use Validator;
use Datatables;
use DB;
use View;
use URL;
use Mail;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;

class JobController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index','postJob','success','companyExist','companyData','datatable','getPosts']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company=null,$companyid=null,Request $request)
    {
		$comp='';
		$jobData='';
        if($companyid){
			$comp = CompanyProfile::find($companyid);		
			$jobData = Jobs::where('company_profile_id', '=', $companyid)->orderBy('id', 'desc')->first();
			
		}
		else {
			$companyid=$company;
			$comp = CompanyProfile::find($companyid);	
			$jobData = Jobs::where('company_profile_id', '=', $companyid)->orderBy('id', 'desc')->first();		
			if(is_null($comp)){
			}
			else {
				$company=null;
			}

		}
		$dataUrl = [
				'company'  => $company,
				'comp'   => $comp,
				'jobData'   => $jobData
			];
	    
	    
       // return view::make('job/index')->with('company', $company);
        return view::make('job/index')->with($dataUrl);
    }

    public function postJob(Request $request)
    {
       
        $data =  $request->toArray();
       
        if ( !isset($data['confidential'])){
        $data['confidential'] = 0;
	    }  
        else{
        $data['confidential'] = 1; 
       } 
       if ( !isset($data['salary_range'])){
        $data['salary_range'] = 0;
	    }  
        else{
        $data['salary_range'] = 1; 
       }           
        $company_id = null; 
        
        $PostCount=0;
        if($data['refernce_name'] !=''){
			if (SubmitUrl::where('refernce_name', '=', $data['refernce_name'])->exists()) {
				$PostCount = Jobs::where('refernce_name', '=', $data['refernce_name'])->count();
				$TotalCount = SubmitUrl::where('refernce_name', '=', $data['refernce_name'])->first();
				$countData=$TotalCount->count;
			//	echo $PostCount;
			//	die('11');
				if($PostCount >= $countData){
				return redirect()->back()->with('errorData', "You've exceeded the number of job postings you can submit. Contact the web administrator to increase the limit ");   
			
				}
  
			}
			else {
			/*	$dataReferce['refernce_name']=$data['refernce_name'];
				$dataReferce['refernce_url']=$data['refernce_url'];
				$dataReferce['count']=999;
				SubmitUrl::create($dataReferce);
             */
			}
			
			//$PostCount = Jobs::where('refernce_name', '=', $data['refernce_name'])->count();
		}
		
		/*if($PostCount > Config('constants.submit_url_count')){
			return redirect()->back()->with('errorData', "You've exceeded the number of job postings you can submit. Contact the web administrator to increase the limit ");   
			
		}*/
        
        if($data['company_id'] == ''){
        $this->validate($request, [
            'company_name' => 'required',
            'logo' => 'mimes:jpeg,jpg,png',
            'company_size' => 'required',
            'industry' => 'required',
            'contact_name' => 'required',
            'contact_last_name' => 'required',
            'email' => 'required|email',
            'no_of_jobs' => 'required',
           // 'email' => 'required|unique:users|email',
          //  'email' => 'required|unique:users,email,NULL,id,role,ROLE_JOB_PROVIDER|email',
         //   'contactEmail' => 'required|email',
          //  'contact_phone' => 'required|numeric',
            'contact_phone' => 'required|regex:/^[0-9]{3}\-[0-9]{3}-[0-9]{4}$/',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required', 
          //  'skills.*' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
            'skills.*' => 'required|regex:/^[a-zA-Z0-9 "+#?.-@!]+$/',
            'zip' => 'required|numeric',
            'job_title' => 'required',
            'job_status' => 'required',
            'educatonLevel' => 'required',
            'experience' => 'required',
            'degree.*' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
            
        ]);
       }
       else {
		 
			 $this->validate($request, [
				'contact_name' => 'required',
				'contact_last_name' => 'required',
				'contact_phone' => 'required|regex:/^[0-9]{3}\-[0-9]{3}-[0-9]{4}$/',
				'no_of_jobs' => 'required',
				'city' => 'required',
				//'state' => 'required',
				'email' => 'required|email',
			//	'zip' => 'required|numeric',
				'job_title' => 'required',
				'job_status' => 'required',
				'educatonLevel' => 'required',
				'experience' => 'required',
				'degree.*' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
				//'skills.*' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
				'skills.*' => 'required|regex:/^[a-zA-Z0-9 "+#?.-@!]+$/',
				//'contactEmail' => 'required|email',
			]);
		   
	   }
        if(isset($data['company_id']) && $data['company_id']!=""){
            $company_id = $data['company_id'];
            $user_id = $data['user_id'];
            
            $companyprofile = CompanyProfile::find($data['company_id']);
           // $userdata = User::find($user_id);
			//$comp->company_email = $data['contactEmail'];  
		//	$userdata->last_name = $data['contact_last_name'];  
			/*$job->job_status = $data['job_status'];  
			$job->amount = $data['range'];  
			$job->min_salary = $data['min_range'];  
			$job->max_salary = $data['max_range'];  
			$job->bonus = $data['bonus'];  
			$job->education_level = $data['educatonLevel'];  
			$job->degree = implode(",",$data['degree']);  
			$job->degree_level = implode(",",$data['degreeLevel']);  
			$job->experience = $data['experience'];  
			$job->experience_level = $data['expLevel'];  
			$job->salary_type = $data['salary_type'];  
			$job->sal_range = $data['salary_range'];  
			$job->skill_level = implode(",",$data['skillsLevel']);  
			$job->job_description = $data['job_description'];  
			$job->skill = implode(",",$data['skills']);  */
			$companyprofile->save();    
			//$userdata->save();    

        } else {
            $file = $request->file('logo');

            $logo_file = "";

            if($file){

                $logo_file = str_random(6).uniqid().'_'.$file->getClientOriginalName();

                $new_file = str_replace(' ', '', $logo_file);
                
                $file->move(public_path()."/images/logos/" ,$new_file) or die("error");
            }
            else {
				$new_file='';
			}
            
            /*
            if(User::where('email', '=', $data['email'])->where('role', '=', 'ROLE_JOB_SEEKER')->exists()){
				$user= User::where('email', '=', $data['email'])->where('role', '=', 'ROLE_JOB_SEEKER')->first();
			}
			else {
				
				 $user = User::create([
                            'name' => $data['contact_name'],
                            'email' => $data['email'],
                            'last_name' => $data['contact_last_name'],
                            'password' => bcrypt(rand(1,5)),
                            'role' => 'ROLE_JOB_PROVIDER',
                        ]);
				
			}*/
			$randomEmiail=str_random(12);
			$emails=$randomEmiail.'@yopmail.com';
			$user = User::create([
                            'name' => $data['contact_name'],
                            'email' => $emails,
                            'last_name' => $data['contact_last_name'],
                            'password' => bcrypt(rand(1,5)),
                            'role' => 'ROLE_JOB_PROVIDER',
                        ]);
           /* $user = User::create([
                            'name' => $data['contact_name'],
                            'email' => $data['email'],
                            'password' => bcrypt(rand(1,5)),
                            'role' => 'ROLE_JOB_PROVIDER',
                        ]);*/
            $user_id = $user->id;

            $companyprofile = CompanyProfile::create([
                            'user_id' => $user_id,
                            'industry' => $data['industry'],
                            'company_name' => $data['company_name'],
                            'company_size' => $data['company_size'],
                            'company_email' => $data['email'],
                            'address' => $data['address'],
                            'address1' => $data['address_2'],
                            'city' => $data['city'],
                            'state' => $data['state'],
                            'zip' => $data['zip'],
                            'phone' => $data['contact_phone'],
                            'confidential' => $data['confidential'],
                            'logo' => $new_file,
                        ]);
                   $company_id = $companyprofile->id;     
        }  
        $job = Jobs::create([
                            'user_id' => $user_id,
                            'company_profile_id' => $company_id,
                            'job_title' => $data['job_title'],
                            'job_status' => $data['job_status'],
                            'email' => $data['email'],
                            'amount' => $data['range'],
                            'min_salary' => $data['min_range'],
                            'max_salary' => $data['min_range'],
                            'bonus' => $data['bonus'],
                            'phone_no' => $data['contact_phone'],
                            'first_name' => $data['contact_name'],
                            'last_name' => $data['contact_last_name'],
                            'education_level' => $data['educatonLevel'],
                            'degree' => implode(",",$data['degree']),
                            'degree_level' => implode(",",$data['degreeLevel']),
                            'experience' => $data['experience'],
                            'salary_type' => $data['salaryType'],
                            'experience_level' => $data['expLevel'],
                            'skill_level' => implode(",",$data['skillsLevel']),
                            'sal_range' => $data['salary_range'],
                            'job_description' => $data['job_description'],
                            'refernce_url' => $data['refernce_url'],
                            'refernce_name' => $data['refernce_name'],
                            'no_of_jobs' => $data['no_of_jobs'],
                            'job_shift_type' => $data['job_shift_type'],
                            'pending_job' => $data['no_of_jobs'],
                            'skill' =>implode(",",$data['skills']),
                        ]);
        $emailcontent = array (
				'name' => $data['contact_name'],
				'company_name' => $companyprofile->company_name,
				'emailmessage' => '',
				'email' => $data['email']
				);
			Mail::send('emails.job_provider', $emailcontent, function($message) use ($emailcontent)
			{
                $message->from('do-not-reply@worksourcemontgomery.com', 'WSM Business Solutions');
				$message->to($emailcontent['email'])
				->subject('Thank You for Submitting Your Career Opportunity');
			});                
         
         if($data['refernce_name'] !=''){
			if (SubmitUrl::where('refernce_name', '=', $data['refernce_name'])->exists()) {
			}
			else {
				$dataReferce['refernce_name']=$data['refernce_name'];
				$dataReferce['refernce_url']=$data['refernce_url'];
				$dataReferce['count']=999;
				SubmitUrl::create($dataReferce);
             
			}
		}
                        
        if($data['admin_save']==1){
			return redirect()->back()->with('message', 'Career opportunity saved successfully');
		} 
		else {
			//return redirect()->route('jobsuccess');
			
			return \Redirect::route('jobsuccess', ['id'=>$company_id,'url'=>$data['refernce_name']])->with('message', 'Job saved successfully!!!');
		    //return view::make('job/success')->with($data);
			//return view::make('job/success')->with('message', 'Job saved successfully');
		}      
                        
     
                 
    }
    public function listjobs(Request $request)
    {
       
       
        return view('job/list');
    }
    public function view(Request $request)
    {
        
        return view('job/index');
    }
    public function add(Request $request)
    {
        
        return view('job/add');
    }
    public function success(Request $request)
    {
        $data =  $request->toArray();
        
        $dataUrl = [
				'message'  => 'Career opportunity saved successfully',
				'last_company_id'   => $data['id'],
				'url'   => (isset($data['url'])) ? $data['url'] : '' ,
			];
	    return view::make('job/success')->with($dataUrl);		
      //  return view::make('job/success')->with('message', 'Job saved successfully');
    }
    
    public function companyExist(Request $request)
    {
        $query = $request->get('term','');
        
        $companys=CompanyProfile::where('company_name','LIKE','%'.$query.'%')->get();
        
        $data=array();
        foreach ($companys as $company) {
			    $url=\URL::to('').'/public/images/logos/';
                $data[]=array('value'=>$company->company_name,'id'=>$company->id,'logo'=>$url.$company->logo);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }
    
    public function companyData(Request $request)
    {
        $query = $request->get('id','');
        $company=CompanyProfile::where('id',$query)->first();
        $url=\URL::to('').'/public/images/logos/';
        $imageExist=0;
        if($company->logo !=''){
			$imageExist=1;
		}
        $data=array('name'=>$company->company_name,'industry'=>$company->industry,'company_size'=>$company->company_size,
        'id'=>$company->id,'address'=>$company->address,'address1'=>$company->address1,'state'=>$company->state,'city'=>$company->city,'zip'=>$company->zip,'phone'=>$company->phone,'confidential'=>$company->confidential,'user_id'=>@$company->user->id,'logo'=>$url.$company->logo,'imageExist'=>$imageExist);
        return $data;
    }
    public function jobView($id)
    {
        $job = Jobs::find($id);
        return view::make('job/view')->with('job', $job);
    }
    
    public function jobDelete($id)
    {
        $job = Jobs::find($id);
        $job->delete();
        return redirect()->route('jobs-list')->withSuccess('Career Opportunity deleted successfully');
    }
    
    public function updateJob(Request $request)
    {
       
        $data =  $request->toArray();
        
    
        $this->validate($request, [
            'job_title' => 'required'
            
        ]);
      
        
        if ( !isset($data['salary_range'])){
        $data['salary_range'] = 0;
	    }  
        else{
        $data['salary_range'] = 1; 
       } 
        $job = Jobs::find($data['job_id']);
        $job->job_title = $data['job_title'];  
        $job->job_status = $data['job_status'];  
        $job->amount = $data['range'];  
        $job->min_salary = $data['min_range'];  
        $job->max_salary = $data['max_range'];  
        $job->bonus = $data['bonus'];  
        $job->education_level = $data['educatonLevel'];  
        $job->degree = implode(",",$data['degree']);  
        $job->degree_level = implode(",",$data['degreeLevel']);  
        $job->experience = $data['experience'];  
        $job->experience_level = $data['expLevel'];  
        $job->salary_type = $data['salary_type'];  
        $job->sal_range = $data['salary_range'];  
        $job->first_name = $data['contact_name'];  
        $job->last_name = $data['contact_last_name'];  
        $job->email = $data['email'];  
        $job->job_shift_type = $data['job_shift_type'];  
        $job->skill_level = implode(",",$data['skillsLevel']);  
        $job->skill_level = implode(",",$data['skillsLevel']);  
        $job->job_description = $data['job_description'];  
        $job->skill = implode(",",$data['skills']);  
        $job->save();       
        return redirect()->back()->with('message', 'Career Opportunity updated successfully.');                 
    }
    
    
    public function getJobs()
    {
    	$jobs = Jobs::query();
    	
        return Datatables::of($jobs)->editColumn('company_profile_id', function($job) {
			        $status='';
                    return $job->company_profile->company_name;
                })->editColumn('created_at', function($job) {
			        $link=$job->created_at->format('F d, Y');
                    return $link;
                })->editColumn('email', function($job) {
			        $link='<span class="mailSpan"><a href=mailto:'.$job->email.'> '.$job->email.'</a> </span><br> <span class="phoneSpan"><a href=tel:'.$job->phone_no.'> '.$job->phone_no.'</a></span>';
                    return $link;
               /* })->editColumn('phone_no', function($job) {
			        $link='<a href=tel:'.$job->phone_no.'> '.$job->phone_no.'</a>';
                    return $link;  */  
                  })->editColumn('job_title', function($job) {
			        $link='<span>('.$job->no_of_jobs. ')  </span>'.$job->job_title;
                    return $link; 
                  })->editColumn('job_status', function($job) {
			        $link='';
			        if($job->job_status == 1){
						$link='Full Time';
					}
					if($job->job_status==2){
						$link='Temporary/ Contract';
					}
					if($job->job_status==3){
						$link='Intern';
					}
					if($job->job_status==4){
						$link='Part Time';
					}
					if($job->job_status==5){
						$link='Seasonal';
					}
					
                    return $link; 
                  })
                ->editColumn('refernce_url', function($job) {
					$url=$job->refernce_url;
					$linkname='';
					if (strpos($url,'add') !== false) {
						$linkname='Admin';
					}
					
					if($job->refernce_name ==''){
						$link=$linkname;
					}
					else {
						$link='<a href='.$job->refernce_url.' target="_blank">'.$job->refernce_name.'</a>';
					}
			        
                    return $link;
                })->orderColumn('job_title', 'created_at desc')
                ->addColumn('action', function ($job) {
					$experience_levelData='nth';
					$expParameter='yex';
					if($job->experience_level ==2){
						$experience_levelData='req';
						$expParameter='yexf';
					}
	    $skills=explode(',',$job->skill);
	    $experience_level=explode(',',$job->skill_level);
	    $degrees=explode(',',$job->degree);
		$degree_level=explode(',',$job->degree_level);
	    $vat='';
	    $vat1='';
	    foreach ($skills as $skill=>$value) {
			if($value != ''){
				$experience_leve='nth';
			if($experience_level[$skill] == '2'){
				$experience_leve='req';
		    } 	
			$vat.=$value.' '.$experience_leve.',';	
			}
		}
		foreach ($degrees as $degree=>$value) {
			if($value != ''){
					$experience_lev='nth';
					if($degree_level[$degree] == '2'){
						$experience_lev='req';
					} 	
			         $vat1.= str_replace(" ", "+", $value).' '.$experience_lev.',';
                     //$vat1 = str_replace(" ", "+", $vat1);
                     //$vat1 = urlencode( $vat1 );

			}
		} 
		if($job->sal_range==1){
			$sal='&tsni='.$job->min_salary.'&tnsalmax='.$job->max_salary.'';
		}
		else {
			$sal='&tsni=0&tnsalmax='.$job->amount.'';
		} 
		$Defaultlocation='&loc=Maryland-100';

        //::::: Job Title ::::://

        $jobTitle = urlencode( str_replace(" ", "+", $job->job_title) );

        //::::: Skills ::::://

        $vat = str_replace(' and ', ' nth,', $vat);
        $vat = urlencode( $vat );

        //::::: Major ::::://

        $vat1 = urlencode( $vat1 );

		         		
		$monsterUrl='https://hiring.monster.com/jcm/resumesearch/EnhancedSearchResults.aspx?seng=trovix&co=US&jt='.$jobTitle.'
		&'.$expParameter.'='.$job->experience.' '.$experience_levelData.'&sk='.$vat.'&edumjr='.$vat1.'&minedulvid='.$job->education_level.'&loc=Maryland-100&tjtid='.$job->job_status.'&rb=1';


		$monsterWSMUrl='https://hiring.monster.com/jcm/resumesearch/EnhancedSearchResults.aspx?seng=trovix&co=US&jt='.$jobTitle.'
		&'.$expParameter.'='.$job->experience.' '.$experience_levelData.'&sk='.$vat.'&edumjr='.$vat1.'&minedulvid='.$job->education_level.'&loc=Maryland-100&tjtid='.$job->job_status.'&rb=12054';
		/*$monsterUrl='https://hiring.monster.com/jcm/resumesearch/EnhancedSearchResults.aspx?seng=trovix&co=US&jt='.$job->job_title.'
		&'.$expParameter.'='.$job->experience.'+'.$experience_levelData.'&sk='.$vat.'';
					        */
                            return '<a href="' . route("jobs-view", $job->id) . '" class="" title="Edit"><i class="fa fa-pencil"></i></a>
                            <a href="' . route("jobs-delete", $job->id) . '" title="Delete">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a><a href="'.$monsterUrl.'" target="_blank" class="monsterIcon" title="Search on monster" data-monster_url="'.$monsterUrl.'" data-monstar_wsm_url="'.$monsterWSMUrl.'">
                        <img src="'.URL::to('').'/public/images/monsterIcon.png" class="img-fluid" alt="iconMonster">
                      </a>';
                        })
            ->make(true);
    }
    
}

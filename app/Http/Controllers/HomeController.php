<?php

namespace App\Http\Controllers;

use App\Models\CompanyProfile;
use App\Models\UserProfile;
use App\User;
use App\Models\Jobs;
use Illuminate\Http\Request;
use DB;
use DateTime;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$datavalue =  $request->toArray();
		$job_list=[];
		$company_list=[];
		$user_list=[];
		$hire_user_list=[];
		$dt=[];
		for($i=1; $i<=4;$i++){
			 if(ceil(date('n', time())/3) >= $i){
				 
				 if($i ==1){
					 $daa['label']='First';
				 }
				 else if($i ==2){
					 $daa['label']='Second';
				 }
				 else if($i ==3){
					 $daa['label']='Third';
				 }
				  else if($i ==4){
					  $daa['label']='Fourth';
				 }
				 else if($i ==1){
				 }
				 $daa['y']=0;
				 $dt[]=$daa;
			 } 
        }
		$compnayCountDashBoard=count(CompanyProfile::all()); // count the job providers
		$jobSeekerCountDashBoard=count(UserProfile::all());   // count the job seekers
		$jobCountDashBoard=count(Jobs::all());  // count the jobs
	//	$jobPendingCountDashBoard=DB::table('jobs')->sum('pending_job');
		$hiredCountDashBoard=DB::table('user_profile')->where('is_hired', '=', 1)->where('job_id', '!=', null)->count(); // count hired user
			$da=[];
		/*	$daa['label']=date('F');
			$daa['y']=0;
			$da[]=$daa;*/
		// generate the last 5 months array	
		/*for ($i = 1; $i < 3; $i++) {
			$daa['label']=date('F', strtotime("-$i month"));
			$daa['y']=0;
			$da[]=$daa;
		}*/
		for($x=2; $x>=0;$x--){

			$daa['label']=date('F', strtotime(date('Y-m')." -" . $x . " month"));
			$daa['y']=0;
			$da[]=$daa;
       }
      

	  // start filter the data	
		if(isset($datavalue['param'])){
		if($datavalue['param'] == 'Sector')
		    {
				
				$specialities = CompanyProfile::where('industry',$datavalue['sector'])->where( DB::raw('YEAR(created_at)'), '=', '2018' )->pluck('id');
				
				$ids=$specialities->toArray();
				$jobs = DB::table('jobs')
					->select('jobs.*')
					->whereIn('company_profile_id', $specialities)->get();
			    $jobsid = DB::table('jobs')
					->select('jobs.*')
					->whereIn('company_profile_id', $specialities)->pluck('id');
			    $users = DB::table('user_profile')
					->select('user_profile.*')
					->whereIn('job_id', $jobsid)->get();
				$companies=CompanyProfile::where('industry',$datavalue['sector'])->get();
				$dataArray=[];
				$dataArrayCompany=[];
				$dataArrayUser=[];
				foreach($jobs as $job){
					$data['id']=$job->id;
					$data['count']=$job->no_of_jobs;
					$timestamp = strtotime($job->created_at);
					$day = date('D', $timestamp);
					$month = date('F', $timestamp);
					$data['month']=$month;
					$curMonth = date('m', $timestamp);
					$curQuarter = ceil($curMonth/3);
					if($curQuarter ==1){
					 $data['curQuarter']='First';
				 }
				 else if($curQuarter ==2){
					 $data['curQuarter']='Second';
				 }
				 else if($curQuarter ==3){
					 $data['curQuarter']='Third';
				 }
				  else if($curQuarter ==4){
					  $data['curQuarter']='Fourth';
				 }
				 else if($curQuarter ==1){
				 }
					$dataArray[]=$data;
				}
				foreach($companies as $company){
					$data['id']=$company->id;
					$data['month']=$company->created_at->format('F');
					$curMonth = date('m', $company->created_at->timestamp);
					$curQuarter = ceil($curMonth/3);
					if($curQuarter ==1){
					 $data['curQuarter']='First';
				 }
				 else if($curQuarter ==2){
					 $data['curQuarter']='Second';
				 }
				 else if($curQuarter ==3){
					 $data['curQuarter']='Third';
				 }
				  else if($curQuarter ==4){
					  $data['curQuarter']='Fourth';
				 }
				 else if($curQuarter ==1){
				 }
					$dataArrayCompany[]=$data;
				}
				foreach($users as $user){
					$data['id']=$user->id;
					$timestamp = strtotime($user->created_at);
					$day = date('D', $timestamp);
					$month = date('F', $timestamp);
					$data['month']=$month;
					$curMonth = date('m', $timestamp);
					$curQuarter = ceil($curMonth/3);
					if($curQuarter ==1){
					 $data['curQuarter']='First';
				 }
				 else if($curQuarter ==2){
					 $data['curQuarter']='Second';
				 }
				 else if($curQuarter ==3){
					 $data['curQuarter']='Third';
				 }
				  else if($curQuarter ==4){
					  $data['curQuarter']='Fourth';
				 }
				 else if($curQuarter ==1){
				 }
					$dataArrayUser[]=$data;
				}
					$output = array();
				foreach($dataArray as $current) {
					// create the array key if it doesn't exist already
					if(!array_key_exists($current['month'], $output)) {
						$output[$current['month']] = $current['count'];

					}
					else {

					$output[$current['month']] += $current['count'];
					}
				}
			//	print_r($output);
				
				//$jobsArray = array_count_values(array_column($dataArray,'month'));
				$jobsArray = $output;
				$companyArray = array_count_values(array_column($dataArrayCompany,'month'));
				$userArray = array_count_values(array_column($dataArrayUser,'month'));
				
				foreach($da as $dd =>$key){
					$job_list[$dd]['label']=$key['label'];
					$job_list[$dd]['y']=(isset($jobsArray[$key['label']]) ? $jobsArray[$key['label']] :0);
				}
				foreach($da as $dd =>$key){
					$company_list[$dd]['label']=$key['label'];
					$company_list[$dd]['y']=(isset($companyArray[$key['label']]) ? $companyArray[$key['label']] :0);
				}
				foreach($da as $dd =>$key){
					$hire_user_list[$dd]['label']=$key['label'];
					$hire_user_list[$dd]['y']=(isset($userArray[$key['label']]) ? $userArray[$key['label']] :0);
				}		

			}
	   else {
				
				$specialities = CompanyProfile::where('company_name',$datavalue['company_name'])->where( DB::raw('YEAR(created_at)'), '=', '2018' )->pluck('id');
				$ids=$specialities->toArray();
				$jobs = DB::table('jobs')
					->select('jobs.*')
					->whereIn('company_profile_id', $specialities)->get();
			    $jobsid = DB::table('jobs')
					->select('jobs.*')
					->whereIn('company_profile_id', $specialities)->pluck('id');
			    $users = DB::table('user_profile')
					->select('user_profile.*')
					->where('is_hired',1)
					->whereIn('job_id', $jobsid)->get();
				$companies=CompanyProfile::all();
				$dataArray=[];
				$dataArrayCompany=[];
				$dataArrayUser=[];
				foreach($jobs as $job){
					$data['id']=$job->id;
					$timestamp = strtotime($job->created_at);
					$data['count']=$job->no_of_jobs;
					$day = date('D', $timestamp);
					$month = date('F', $timestamp);
					$data['month']=$month;
					$curMonth = date('m', $timestamp);
					$curQuarter = ceil($curMonth/3);
					if($curQuarter ==1){
					 $data['curQuarter']='First';
				 }
				 else if($curQuarter ==2){
					 $data['curQuarter']='Second';
				 }
				 else if($curQuarter ==3){
					 $data['curQuarter']='Third';
				 }
				  else if($curQuarter ==4){
					  $data['curQuarter']='Fourth';
				 }
				 else if($curQuarter ==1){
				 }
					$dataArray[]=$data;
				}
				foreach($companies as $company){
					$data['id']=$company->id;
					$data['month']=$company->created_at->format('F');
					$curMonth = date('m', $company->created_at->timestamp);
					$curQuarter = ceil($curMonth/3);
					if($curQuarter ==1){
					 $data['curQuarter']='First';
				 }
				 else if($curQuarter ==2){
					 $data['curQuarter']='Second';
				 }
				 else if($curQuarter ==3){
					 $data['curQuarter']='Third';
				 }
				  else if($curQuarter ==4){
					  $data['curQuarter']='Fourth';
				 }
				 else if($curQuarter ==1){
				 }
					$dataArrayCompany[]=$data;
				}
				foreach($users as $user){
					$data['id']=$user->id;
					$timestamp = strtotime($user->created_at);
					$day = date('D', $timestamp);
					$month = date('F', $timestamp);
					$curMonth = date('m', $timestamp);
					$curQuarter = ceil($curMonth/3);
					if($curQuarter ==1){
					 $data['curQuarter']='First';
				 }
				 else if($curQuarter ==2){
					 $data['curQuarter']='Second';
				 }
				 else if($curQuarter ==3){
					 $data['curQuarter']='Third';
				 }
				  else if($curQuarter ==4){
					  $data['curQuarter']='Fourth';
				 }
				 else if($curQuarter ==1){
				 }
					$data['month']=$month;
					$dataArrayUser[]=$data;
				}
				$output = array();
				foreach($dataArray as $current) {
					// create the array key if it doesn't exist already
					if(!array_key_exists($current['month'], $output)) {
						$output[$current['month']] = $current['count'];

					}
					else {
						$output[$current['month']] += $current['count'];
					}
				}		
				//$jobsArray = array_count_values(array_column($dataArray,'month'));
				$jobsArray = $output;
				$companyArray = array_count_values(array_column($dataArrayCompany,'month'));
				$userArray = array_count_values(array_column($dataArrayUser,'month'));
				
				foreach($da as $dd =>$key){
					$job_list[$dd]['label']=$key['label'];
					$job_list[$dd]['y']=(isset($jobsArray[$key['label']]) ? $jobsArray[$key['label']] :0);
				}
				foreach($da as $dd =>$key){
					//$company_list[$dd]['label']=$key['label'];
					//$company_list[$dd]['y']=(isset($companyArray[$key['label']]) ? $companyArray[$key['label']] :0);
				}
				foreach($da as $dd =>$key){
					$hire_user_list[$dd]['label']=$key['label'];
					$hire_user_list[$dd]['y']=(isset($userArray[$key['label']]) ? $userArray[$key['label']] :0);
				}		

				
				
			}
		}
		else {
			
		$jobs=Jobs::where( DB::raw('YEAR(created_at)'), '=', '2018' )->get();
		$companies=CompanyProfile::where( DB::raw('YEAR(created_at)'), '=', '2018' )->get();
		$users=UserProfile::where( DB::raw('YEAR(created_at)'), '=', '2018' )->get();
		$dataArray=[];
		$dataArrayCompany=[];
		$dataArrayUser=[];
		foreach($jobs as $job){
			$data['id']=$job->id;
			$data['month']=$job->created_at->format('F');
			$data['count']=$job->no_of_jobs;
			$curMonth = date('m', $job->created_at->timestamp);
			$curQuarter = ceil($curMonth/3);
			if($curQuarter ==1){
					 $data['curQuarter']='First';
				 }
				 else if($curQuarter ==2){
					 $data['curQuarter']='Second';
				 }
				 else if($curQuarter ==3){
					 $data['curQuarter']='Third';
				 }
				  else if($curQuarter ==4){
					  $data['curQuarter']='Fourth';
				 }
				 else if($curQuarter ==1){
				 }
			$dataArray[]=$data;
		}

		foreach($companies as $company){
			$data['id']=$company->id;
			$data['month']=$company->created_at->format('F');
			$curMonth = date('m', $company->created_at->timestamp);
			$curQuarter = ceil($curMonth/3);
			if($curQuarter ==1){
					 $data['curQuarter']='First';
				 }
				 else if($curQuarter ==2){
					 $data['curQuarter']='Second';
				 }
				 else if($curQuarter ==3){
					 $data['curQuarter']='Third';
				 }
				  else if($curQuarter ==4){
					  $data['curQuarter']='Fourth';
				 }
				 else if($curQuarter ==1){
				 }
			$dataArrayCompany[]=$data;
		}
		foreach($users as $user){
			$data['id']=$user->id;
			$data['month']=$user->created_at->format('F');
			$curMonth = date('m', $user->created_at->timestamp);
			$curQuarter = ceil($curMonth/3);
			if($curQuarter ==1){
					 $data['curQuarter']='First';
				 }
				 else if($curQuarter ==2){
					 $data['curQuarter']='Second';
				 }
				 else if($curQuarter ==3){
					 $data['curQuarter']='Third';
				 }
				  else if($curQuarter ==4){
					  $data['curQuarter']='Fourth';
				 }
				 else if($curQuarter ==1){
				 }
			//$data['curQuarter']=$curQuarter;
			$dataArrayUser[]=$data;
		}
		$output = array();
		foreach($dataArray as $current) {
			// create the array key if it doesn't exist already
			if(!array_key_exists($current['month'], $output)) {
				$output[$current['month']] = $current['count'];

			}
			else {

			$output[$current['month']] += $current['count'];
			}
		}
		//print_r($output);
				
		//$jobsArray = array_count_values(array_column($dataArray,'month'));
		$jobsArray = $output;
		$companyArray = array_count_values(array_column($dataArrayCompany,'month'));
		$userArray = array_count_values(array_column($dataArrayUser,'month'));
		//print_r($jobsArray);
        foreach($da as $dd =>$key){
			$job_list[$dd]['label']=$key['label'];
			$job_list[$dd]['y']=(isset($jobsArray[$key['label']]) ? $jobsArray[$key['label']] :0);
		}
		foreach($da as $dd =>$key){
			$company_list[$dd]['label']=$key['label'];
			$company_list[$dd]['y']=(isset($companyArray[$key['label']]) ? $companyArray[$key['label']] :0);
		}
		foreach($da as $dd =>$key){
			$user_list[$dd]['label']=$key['label'];
			$user_list[$dd]['y']=(isset($userArray[$key['label']]) ? $userArray[$key['label']] :0);
		}		
	    }
	    $dataArrayUser=$user_list;
		$dataArray=$job_list;
		$dataArrayCompany=$company_list;
		$dataArrayHireUser=$hire_user_list;
		$dataArrayHireUser =$dataArrayHireUser;
		$dataArrayCompany =$dataArrayCompany;
		$dataArray =$dataArray;
		$dataArrayUser =$dataArrayUser;
		$results = DB::select('SELECT `industry`, COUNT(*) as count FROM `company_profile` GROUP BY `industry`');
		//print_r($results);
		$companyByCategory=[];
		foreach($results as $companyData){
			
			if($companyData->industry =='BioLife/BioScience'){
				$companyDataCategory['label']=$companyData->industry;
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if($companyData->industry =='Healthcare Services'){
				$companyDataCategory['label']='Healthcare';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if($companyData->industry =='Construction - Industrial Facilities and Infrastructure'){
				$companyDataCategory['label']='Construction';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if($companyData->industry =='Construction - Residential &amp; Commercial/Office'){
				$companyDataCategory['label']='Construction';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if($companyData->industry =='Engineering Services'){
				$companyDataCategory['label']='Engineering';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if($companyData->industry =='Telecom/Communications'){
				$companyDataCategory['label']='Telecom/Communications';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if($companyData->industry =='Technology/Cyber/IT'){
				$companyDataCategory['label']='Technology/Cyber/IT';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if($companyData->industry =='Education'){
				$companyDataCategory['label']='Education';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if($companyData->industry =='Hospitality'){
				$companyDataCategory['label']='Hospitality';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if($companyData->industry =='Energy/Renewable'){
				$companyDataCategory['label']='Energy/Renewable';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if($companyData->industry =='Consulting'){
				$companyDataCategory['label']='Consulting';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if($companyData->industry =='Financial Services'){
				$companyDataCategory['label']='Finance/Banking';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if($companyData->industry =='Banking'){
				$companyDataCategory['label']='Finance/Banking';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if($companyData->industry =='Personnel/Staffing'){
				$companyDataCategory['label']='Personnel/Staffing';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if($companyData->industry =='Retail'){
				$companyDataCategory['label']='Retail';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if(strpos($companyData->industry,'Transportation') !== false ){
				$companyDataCategory['label']='Transportation';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			else if(strpos($companyData->industry,'Food and Beverage') !== false ){
				$companyDataCategory['label']='Food and Beverage';
				$companyDataCategory['y']=$companyData->count;
				$companyByCategory[]=$companyDataCategory;
			}
			
			//$companyDataCategory['label']=$companyData->industry;
			//$companyDataCategory['y']=$companyData->count;
			//$companyByCategory[]=$companyDataCategory;
		}
		 $companyCategory = array( 
            "0" => array (
               "label" => "BioLife/BioScience",
               "y" => 0
            ),
            
            "1" => array (
               "label" => "Healthcare",
               "y" => 0
            ),
            "2" => array (
               "label" => "Construction",
               "y" => 0 
            ),
            "3" => array (
               "label" => "Engineering",
               "y" => 0 
            ),
            "4" => array (
               "label" => "Transportation",
               "y" => 0 
            ),
            "5" => array (
               "label" => "Telecom/Communications",
               "y" => 0
            ),
            "6" => array (
               "label" => "Technology/Cyber/IT",
               "y" => 0 
            ),
            "7" => array (
               "label" => "Education",
               "y" 
            ),
            "8" => array (
               "label" => "Hospitality",
               "y" => 0 
            ),
            "9" => array (
               "label" => "Energy/Renewable",
               "y" => 0
            ),
            "10" => array (
               "label" => "Consulting",
               "y" => 0 
            ),
            "11" => array (
               "label" => "Finance/Banking",
               "y" 
            ),
            "12" => array (
               "label" => "Personnel/Staffing",
               "y" => 0
            ),
            "13" => array (
               "label" => "Food and Beverage",
               "y" => 0
            ),
            "14" => array (
               "label" => "Retail",
               "y" => 0 
            )
         );
         
		$outputArray = array();
		foreach($companyByCategory as $current) {
			//print_r($current);
			//die();
			// create the array key if it doesn't exist already
			if(!array_key_exists($current['label'], $outputArray)) {
				$outputArray[$current['label']] = $current['y'];

			}
			else {

			$outputArray[$current['label']] += $current['y'];
			}
		}
		$compListByCategory=[];
		foreach($companyCategory as $dd =>$key){
			$compListByCategory[$dd]['label']=$key['label'];
			$compListByCategory[$dd]['y']=(isset($outputArray[$key['label']]) ? $outputArray[$key['label']] :0);
		}

        return view('home',compact('dataArrayUser','dataArray','dataArrayCompany','dataArrayHireUser','compnayCountDashBoard',
					'jobSeekerCountDashBoard','jobCountDashBoard','hiredCountDashBoard','compListByCategory'));
    }
    
    public function unique_by_keys($haystack=array(),$needles=array()){
    foreach($haystack as $row){
        $key=implode('',array_intersect_key($row,array_flip($needles)));  // declare unique key
        if(!isset($result[$key])){$result[$key]=$row;} // save row if non-duplicate
    }
    return array_values($result);
}
    
    
    
    
}

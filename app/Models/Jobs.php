<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Jobs extends \Eloquent {
    /**
     * The database table used by the model.
     *
     * @var string
     */
   // protected $timestamps = true;

    
    protected $fillable = array(
        'id',
        'user_id',
        'company_profile_id',
        'job_title',
        'job_status',
        'email',
        'first_name',
        'last_name',
        'phone_no',
        'amount',
        'min_salary',
        'max_salary',
        'bonus',
        'skill',
        'skill_level',
        'salary_type',
        'sal_range',
        'education_level',
        'degree',
        'degree_level',
        'experience',
        'experience_level',
        'job_description',
        'refernce_url',
        'refernce_name',
        'no_of_jobs',
        'pending_job',
        'job_shift_type'
    );
    
    protected $table = 'jobs';
    
    public function company_profile()
    {
        return $this->belongsTo('App\Models\CompanyProfile');
    }
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
  //  protected $hidden = array('password', 'temp_pass', 'updated_at');    
}

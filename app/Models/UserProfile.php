<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserProfile extends \Eloquent {
    /**
     * The database table used by the model.
     *
     * @var string
     */
   // protected $timestamps = false;

    protected $fillable = array(
        'id',
        'user_id',
        'first_name',
        'last_name',
        'user_password',
        'city',
        'state',
        'zip',
        'phone_no',
        'resume_title',
        'resume_origin',
        'resume_url',
        'highest_degree',
        'citizenship',
        'refernce_url',
        'refernce_name',
        'is_qualified',
        'is_hired',
        'hired_outside',
        'job_id',
        'job_name',
        'qualify_job_id',
        'shift_type',
    );
    
    protected $table = 'user_profile';
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
      
}

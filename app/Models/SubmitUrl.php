<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubmitUrl extends \Eloquent {
    /**
     * The database table used by the model.
     *
     * @var string
     */
   // protected $timestamps = true;

    
    protected $fillable = array(
        'id',
        'refernce_url',
        'refernce_name',
        'count'
    );
    
    protected $table = 'submit_url';
       
}

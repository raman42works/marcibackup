<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyProfile extends \Eloquent {
    /**
     * The database table used by the model.
     *
     * @var string
     */
   // protected $timestamps = false;

    protected $fillable = array(
        'id',
        'user_id',
        'company_name',
        'company_email',
        'industry',
        'company_size',
        'address1',
        'address',
        'city',
        'state',
        'zip',
        'phone',
        'confidential',
        'logo',
    );
    
    protected $table = 'company_profile';
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function jobs()
    {
        return $this->hasMany('App\Models\Jobs');
    }
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
  //  protected $hidden = array('password', 'temp_pass', 'updated_at');    
}

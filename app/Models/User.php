<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends \Eloquent {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    
    protected $fillable = array(
        'id',
        'name',
        'email',
        'password',
        'remember_token',
        'role',
        'last_name'
    );
    
    protected $table = 'users';
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'temp_pass', 'updated_at');  
    
    
     public function company()
		{
			return $this->hasMany('App\Models\CompanyProfile');
		}
    
   
  
}
